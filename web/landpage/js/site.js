function exibe(div) {
    "none" == document.getElementById(div).style.display && (document.getElementById(div).style.display = "block")
}
function valCarrinhoPp() {
    return document.getElementById("btn_paypal").disabled = !0, document.getElementById("btn_paypal").innerHTML = "<strong class='aguarde'>Aguarde...</strong>", setTimeout("document.getElementById('btn_paypal').disabled=false", 1e4), setTimeout("document.getElementById('btn_paypal').innerHTML='<i class='icon-white icon-shopping-cart'></i> <strong>PAYPAL</strong>'", 10001), !0
}
function valCarrinho() {
    return document.getElementById("btn_fp").disabled = !0, document.getElementById("btn_fp").innerHTML = "<strong class='aguarde'>Aguarde...</strong>", setTimeout("document.getElementById('btn_fp').disabled=false", 5e3), setTimeout("document.getElementById('btn_fp').innerHTML='<i class='icon-white icon-shopping-cart'></i> <strong>PAGSEGURO</strong>'", 5001), !0
}
function valCupomDesconto() {
    return "" == document.cupom.desconto.value ? (alert("Campo Cupom Obrigatório!"), document.cupom.desconto.focus(), !1) : (document.getElementById("btn_cupom").disabled = !0, document.getElementById("btn_cupom").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cupom').disabled=false", 5e3), setTimeout("document.getElementById('btn_cupom').innerHTML='Aplicar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/loja/funLoja.php?acao=desconto",
        data: jQuery("#cupom").serialize(),
        success: function (msg) {
            jQuery("#cupom").css("display", "none"), jQuery("#cupom_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function loadMensagem() {
    document.getElementById("btn_msg").disabled = !0, document.getElementById("btn_msg").innerHTML = "<i class='icon-white icon-refresh'></i> Aguarde...", setTimeout("document.getElementById('btn_msg').disabled=false", 5e3), setTimeout("document.getElementById('btn_msg').innerHTML='<i class='icon-white icon-refresh'></i> Atualizar'", 5001), location.reload()
}
function valCadastro() {
    return "" == document.cadastro.nome.value ? (alert("Campo Nome Obrigatório!"), document.cadastro.nome.focus(), !1) : "" == document.cadastro.email.value || -1 == document.cadastro.email.value.indexOf("@") || -1 == document.cadastro.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório!"), document.cadastro.email.focus(), !1) : "" == document.cadastro.senha.value ? (alert("Campo Senha Obrigatório!"), document.cadastro.senha.focus(), !1) : "" == document.cadastro.captcha.value ? (alert("Captcha Obrigatório!"), document.cadastro.captcha.focus(), !1) : document.cadastro.captcha.value != document.cadastro.rcaptcha.value ? (alert("Captcha incorreto, tente novamente!"), document.cadastro.captcha.focus(), !1) : (document.getElementById("btn_cadastro").disabled = !0, document.getElementById("btn_cadastro").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cadastro').disabled=false", 5e3), setTimeout("document.getElementById('btn_cadastro').innerHTML='Criar Conta'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/cadastro",
        data: jQuery("#cadastro").serialize(),
        success: function (msg) {
            jQuery("#cadastro_sucesso").html(msg), jQuery("#cadastro").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function getLike(cod, aluno_votado) {
    return jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=like&cod=" + cod + "&aluno_votado=" + aluno_votado,
        data: jQuery("#like").serialize(),
        success: function (msg) {
            jQuery("#count" + cod).html(msg)
        }
    }), !1
}
function getLikeReplica(cod, aluno_votado) {
    return jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=like&cod=" + cod + "&aluno_votado=" + aluno_votado,
        data: jQuery("#like_replica").serialize(),
        success: function (msg) {
            jQuery("#count" + cod).html(msg)
        }
    }), !1
}
function valLogin() {
    return "" == document.login.email.value || -1 == document.login.email.value.indexOf("@") || -1 == document.login.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório!"), document.login.email.focus(), !1) : "" == document.login.senha.value ? (alert("Campo Senha Obrigatório!"), document.login.senha.focus(), !1) : (document.getElementById("btn_login").disabled = !0, document.getElementById("btn_login").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_login').disabled=false", 5e3), setTimeout("document.getElementById('btn_login').innerHTML='Entrar'", 5001), !0)
}
function valRecupSenha() {
    return "" == document.recuperar_senha.email.value || -1 == document.recuperar_senha.email.value.indexOf("@") || -1 == document.recuperar_senha.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório"), document.recuperar_senha.email.focus(), !1) : (document.getElementById("btn_rs").disabled = !0, document.getElementById("btn_rs").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rs').disabled=false", 5e3), setTimeout("document.getElementById('btn_rs').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/login/funLogin.php?acao=recuperar-senha",
        data: jQuery("#recuperar_senha").serialize(),
        success: function (msg) {
            jQuery("#senha_sucesso").html(msg), jQuery("#recuperar_senha").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valConfirmacao() {
    return "" == document.reenviar_confirmacao.email.value || -1 == document.reenviar_confirmacao.email.value.indexOf("@") || -1 == document.reenviar_confirmacao.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório"), document.reenviar_confirmacao.email.focus(), !1) : (document.getElementById("btn_confirmacao").disabled = !0, document.getElementById("btn_confirmacao").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_confirmacao').disabled=false", 5e3), setTimeout("document.getElementById('btn_confirmacao').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/login/funLogin.php?acao=reenviar-confirmacao",
        data: jQuery("#reenviar_confirmacao").serialize(),
        success: function (msg) {
            jQuery("#confirmacao_sucesso").html(msg), jQuery("#reenviar_confirmacao").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valNovaSenha() {
    return "" == document.novasenha.senha.value ? (alert("Campo Senha Obrigatório!"), document.novasenha.senha.focus(), !1) : "" == document.novasenha.rsenha.value ? (alert("Campo Repetir Senha Obrigatório!"), document.novasenha.rsenha.focus(), !1) : document.novasenha.senha.value != document.novasenha.rsenha.value ? (alert("Senhas não coincidem!"), document.novasenha.rsenha.focus(), !1) : (document.getElementById("btn_ns").disabled = !0, document.getElementById("btn_ns").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ns').disabled=false", 5e3), setTimeout("document.getElementById('btn_ns').innerHTML='Salvar'", 5001), !0)
}
function valRecado() {
    return "" == document.recado.mensagem.value ? (alert("Campo Recado Obrigatório"), document.recado.mensagem.focus(), !1) : (document.getElementById("btn_recado").disabled = !0, document.getElementById("btn_recado").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_recado').disabled=false", 5e3), setTimeout("document.getElementById('btn_recado').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=enviar-recado",
        data: jQuery("#recado").serialize(),
        success: function (msg) {
            jQuery("#recado").css("display", "none"), jQuery("#recado_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valBusca() {
    return "" == document.busca.pesquisar.value ? (alert("Campo Busca Obrigatório"), document.busca.pesquisar.focus(), !1) : (document.getElementById("btn_busca").disabled = !0, document.getElementById("btn_busca").innerHTML = "<i class='icon-search'></i> Aguarde...", setTimeout("document.getElementById('btn_busca').disabled=false", 5e3), void setTimeout("document.getElementById('btn_busca').innerHTML='<i class='icon-search'></i> Pesquisar'", 5001))
}
function valMensagemResponder() {
    return "" == document.responder_msg.responder.value ? (alert("Campo Resposta Obrigatório"), document.responder_msg.responder.focus(), !1) : (document.getElementById("btn_rm").disabled = !0, document.getElementById("btn_rm").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rm').disabled=false", 5e3), setTimeout("document.getElementById('btn_rm').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=responder",
        data: jQuery("#responder_msg").serialize(),
        success: function (msg) {
            jQuery("#responder_sucesso").html(msg), jQuery("#responder_msg").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valUsername() {
    return "" == document.cad_username.username.value ? (alert("Campo Nome de Usuário Obrigatório"), document.cad_username.username.focus(), !1) : (document.getElementById("btn_us").disabled = !0, document.getElementById("btn_us").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_us').disabled=false", 5e3), setTimeout("document.getElementById('btn_us').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=username",
        data: jQuery("#cad_username").serialize(),
        success: function (msg) {
            jQuery("#sucesso_username").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valEditarCadastro() {
    return "" == document.editar_cadastro.nome.value ? (alert("Campo Nome Obrigatório"), document.editar_cadastro.nome.focus(), !1) : "" == document.editar_cadastro.senha.value ? (alert("Campo Senha Obrigatório"), document.editar_cadastro.senha.focus(), !1) : (document.getElementById("btn_pf").disabled = !0, document.getElementById("btn_pf").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_pf').disabled=false", 5e3), setTimeout("document.getElementById('btn_pf').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=editar_cadastro",
        data: jQuery("#editar_cadastro").serialize(),
        success: function (msg) {
            jQuery("#sucesso_editar_cadastro").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function loadFoto() {
    document.getElementById("btn_foto").disabled = !0, document.getElementById("btn_foto").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_foto').disabled=false", 5e3), setTimeout("document.getElementById('btn_foto').innerHTML='Upload'", 5001)
}
function loadCapa() {
    document.getElementById("btn_capa").disabled = !0, document.getElementById("btn_capa").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_foto').disabled=false", 5e3), setTimeout("document.getElementById('btn_foto').innerHTML='Upload'", 5001)
}
function valBiografia() {
    return "" == document.cad_biografia.biografia.value ? (alert("Campo Biografia Obrigatório"), document.cad_biografia.biografia.focus(), !1) : (document.getElementById("btn_bio").disabled = !0, document.getElementById("btn_bio").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_bio').disabled=false", 5e3), setTimeout("document.getElementById('btn_bio').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=biografia",
        data: jQuery("#cad_biografia").serialize(),
        success: function (msg) {
            jQuery("#sucesso_biografia").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valContatos() {
    return document.getElementById("btn_cts").disabled = !0, document.getElementById("btn_cts").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cts').disabled=false", 5e3), setTimeout("document.getElementById('btn_cts').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/perfil/funPerfil.php?acao=contatos",
        data: jQuery("#cad_contatos").serialize(),
        success: function (msg) {
            jQuery("#sucesso_contatos").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1
}
function valNovoTopico() {
    return "" == document.cad_novo_topico.assunto.value ? (alert("Campo Assunto Obrigatório"), document.cad_novo_topico.assunto.focus(), !1) : 0 == document.cad_novo_topico.categoria.value ? (alert("Campo Categoria Obrigatório"), document.cad_novo_topico.categoria.focus(), !1) : "" == document.cad_novo_topico.mensagem.value ? (alert("Campo Mensagem Obrigatório"), document.cad_novo_topico.mensagem.focus(), !1) : (document.getElementById("btn_nt").disabled = !0, document.getElementById("btn_nt").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nt').disabled=false", 5e3), setTimeout("document.getElementById('btn_nt').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/forum/funForum.php?acao=novo-topico",
        data: jQuery("#cad_novo_topico").serialize(),
        success: function (msg) {
            jQuery("#novo_topico_sucesso").html(msg), jQuery("#cad_novo_topico").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valEditarTopico() {
    return "" == document.cad_editar_topico.assunto.value ? (alert("Campo Assunto Obrigatório"), document.cad_editar_topico.assunto.focus(), !1) : 0 == document.cad_editar_topico.categoria.value ? (alert("Campo Categoria Obrigatório"), document.cad_editar_topico.categoria.focus(), !1) : "" == document.cad_editar_topico.mensagem.value ? (alert("Campo Mensagem Obrigatório"), document.cad_editar_topico.mensagem.focus(), !1) : (document.getElementById("btn_et").disabled = !0, document.getElementById("btn_et").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nt').disabled=false", 5e3), setTimeout("document.getElementById('btn_nt').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/forum/funForum.php?acao=editar-topico",
        data: jQuery("#cad_editar_topico").serialize(),
        success: function (msg) {
            jQuery("#editar_topico_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valTopicoResponder() {
    return "" == document.responder_topico.responder.value ? (alert("Campo Resposta Obrigatório"), document.responder_topico.responder.focus(), !1) : (document.getElementById("btn_rt").disabled = !0, document.getElementById("btn_rt").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rt').disabled=false", 5e3), setTimeout("document.getElementById('btn_rt').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/forum/funForum.php?acao=responder",
        data: jQuery("#responder_topico").serialize(),
        success: function (msg) {
            jQuery("#responder_sucesso").html(msg), jQuery("#responder_topico").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valEditarResposta() {
    return "" == document.cad_editar_resposta.resposta.value ? (alert("Campo Resposta Obrigatório"), document.cad_editar_resposta.resposta.focus(), !1) : (document.getElementById("btn_er").disabled = !0, document.getElementById("btn_er").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nt').disabled=false", 5e3), setTimeout("document.getElementById('btn_nt').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/forum/funForum.php?acao=editar-resposta",
        data: jQuery("#cad_editar_resposta").serialize(),
        success: function (msg) {
            jQuery("#editar_resposta_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valContato() {
    return "" == document.cad_contato.assunto.value ? (alert("Campo Assunto Obrigatório"), document.cad_contato.assunto.focus(), !1) : "" == document.cad_contato.mensagem.value ? (alert("Campo Mensagem Obrigatório"), document.cad_contato.mensagem.focus(), !1) : (document.getElementById("btn_contato").disabled = !0, document.getElementById("btn_contato").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_contato').disabled=false", 5e3), setTimeout("document.getElementById('btn_contato').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/contato/funContato.php",
        data: jQuery("#cad_contato").serialize(),
        success: function (msg) {
            jQuery("#contato_sucesso").html(msg), jQuery("#cad_contato").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valComentario() {
    return "" == document.comentario.comentario.value ? (alert("Campo Comentário Obrigatório"), document.comentario.comentario.focus(), !1) : (document.getElementById("btn_comentario").disabled = !0, document.getElementById("btn_comentario").innerHTML = "...", setTimeout("document.getElementById('btn_comentario').disabled=false", 5e3), setTimeout("document.getElementById('btn_comentario').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=comentario",
        data: jQuery("#comentario").serialize(),
        success: function (msg) {
            jQuery("#comentario_sucesso").html(msg).fadeOut(12e3), jQuery("#comentario")[0].reset(), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valComentarioResponder() {
    return "" == document.responder_comentario.responder.value ? (alert("Campo Resposta Obrigatório"), document.responder_comentario.responder.focus(), !1) : (document.getElementById("btn_cr").disabled = !0, document.getElementById("btn_cr").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cr').disabled=false", 5e3), setTimeout("document.getElementById('btn_cr').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=responder",
        data: jQuery("#responder_comentario").serialize(),
        success: function (msg) {
            jQuery("#resposta_sucesso").html(msg), jQuery("#responder_comentario").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valNovoCurso() {
    return "" == document.novo_curso.categoria.value ? (alert("Campo Categoria do Curso Obrigatório"), document.novo_curso.categoria.focus(), !1) : "" == document.novo_curso.nome.value ? (alert("Campo Nome do Curso Obrigatório"), document.novo_curso.nome.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Criar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=novo-curso",
        data: jQuery("#novo_curso").serialize(),
        success: function (msg) {
            jQuery("#novo_curso_sucesso").html(msg), jQuery("#novo_curso").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmNovoCurso() {
    return "" == document.novo_curso.categoria.value ? (alert("Campo Categoria do Curso Obrigatório"), document.novo_curso.categoria.focus(), !1) : "" == document.novo_curso.nome.value ? (alert("Campo Nome do Curso Obrigatório"), document.novo_curso.nome.focus(), !1) : "" == document.novo_curso.professor_id.value ? (alert("Campo Professor Obrigatório"), document.novo_curso.professor_id.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Criar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=novo-curso",
        data: jQuery("#novo_curso").serialize(),
        success: function (msg) {
            jQuery("#novo_curso_sucesso").html(msg), jQuery("#novo_curso").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valFotoCurso() {
    return "" == document.foto_curso.foto.value ? (alert("Campo Imagem Ilustrativa Obrigatório"), document.foto_curso.foto.focus(), !1) : (document.getElementById("btn_fc").disabled = !0, document.getElementById("btn_fc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_fc').disabled=false", 1e4), setTimeout("document.getElementById('btn_fc').innerHTML='Upload'", 10001), !0)
}
function valFotoCover() {
    return "" == document.foto_cover.cover.value ? (alert("Campo Imagem Ilustrativa Obrigatório"), document.foto_cover.cover.focus(), !1) : (document.getElementById("btn_cc").disabled = !0, document.getElementById("btn_cc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cc').disabled=false", 1e4), setTimeout("document.getElementById('btn_cc').innerHTML='Upload'", 10001), !0)
}
function valInfoCurso() {
    return "" == document.info_curso.lancamento.value ? (alert("Campo Data de Lançamento Obrigatório"), document.info_curso.lancamento.focus(), !1) : "" == document.info_curso.nome.value ? (alert("Campo Nome do Curso Obrigatório"), document.info_curso.nome.focus(), !1) : "" == document.info_curso.titulo_apresentacao.value ? (alert("Campo Nome de Apresentação Obrigatório"), document.info_curso.titulo_apresentacao.focus(), !1) : "" == document.info_curso.carga.value ? (alert("Campo Carga Horária Obrigatório"), document.info_curso.carga.focus(), !1) : (document.getElementById("btn_ic").disabled = !0, document.getElementById("btn_ic").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ic').disabled=false", 1e4), setTimeout("document.getElementById('btn_ic').innerHTML='Salvar'", 10001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=info-curso",
        data: jQuery("#info_curso").serialize(),
        success: function (msg) {
            jQuery("#info_curso_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmInfoCurso() {
    return "" == document.info_curso.lancamento.value ? (alert("Campo Data de Lançamento Obrigatório"), document.info_curso.lancamento.focus(), !1) : "" == document.info_curso.nome.value ? (alert("Campo Nome do Curso Obrigatório"), document.info_curso.nome.focus(), !1) : "" == document.info_curso.titulo_apresentacao.value ? (alert("Campo Nome de Apresentação Obrigatório"), document.info_curso.titulo_apresentacao.focus(), !1) : "" == document.info_curso.periodo_acesso.value ? (alert("Campo Período de Acesso Obrigatório"), document.info_curso.periodo_acesso.focus(), !1) : "" == document.info_curso.carga.value ? (alert("Campo Carga Horária Obrigatório"), document.info_curso.carga.focus(), !1) : (document.getElementById("btn_ic").disabled = !0, document.getElementById("btn_ic").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ic').disabled=false", 1e4), setTimeout("document.getElementById('btn_ic').innerHTML='Salvar'", 10001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=info-curso",
        data: jQuery("#info_curso").serialize(),
        success: function (msg) {
            jQuery("#info_curso_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valDadosCurso() {
    return "" == document.dados_curso.resumo.value ? (alert("Campo Resumo do Curso Obrigatório"), document.dados_curso.resumo.focus(), !1) : "" == document.dados_curso.sobre.value ? (alert("Campo Sobre do Curso Obrigatório"), document.dados_curso.sobre.focus(), !1) : "" == document.dados_curso.destina.value ? (alert("Campo A quem se destina? Obrigatório"), document.dados_curso.destina.focus(), !1) : (document.getElementById("btn_dc").disabled = !0, document.getElementById("btn_dc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_dc').disabled=false", 1e4), setTimeout("document.getElementById('btn_dc').innerHTML='Salvar'", 10001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=dados-curso",
        data: jQuery("#dados_curso").serialize(),
        success: function (msg) {
            jQuery("#dados_curso_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmDadosCurso() {
    return "" == document.dados_curso.resumo.value ? (alert("Campo Resumo do Curso Obrigatório"), document.dados_curso.resumo.focus(), !1) : "" == document.dados_curso.sobre.value ? (alert("Campo Sobre do Curso Obrigatório"), document.dados_curso.sobre.focus(), !1) : "" == document.dados_curso.destina.value ? (alert("Campo A quem se destina? Obrigatório"), document.dados_curso.destina.focus(), !1) : (document.getElementById("btn_dc").disabled = !0, document.getElementById("btn_dc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_dc').disabled=false", 1e4), setTimeout("document.getElementById('btn_dc').innerHTML='Salvar'", 10001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=dados-curso",
        data: jQuery("#dados_curso").serialize(),
        success: function (msg) {
            jQuery("#dados_curso_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valDepoimento() {
    return "" == document.cad_dp.depoimento.value ? (alert("Campo Depoimento Obrigatório!"), document.cad_dp.depoimento.focus(), !1) : (document.getElementById("btn_dp").disabled = !0, document.getElementById("btn_dp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_dp').disabled=false", 5e3), setTimeout("document.getElementById('btn_dp').innerHTML='Avaliar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/loja/funLoja.php?acao=depoimento",
        data: jQuery("#cad_dp").serialize(),
        success: function (msg) {
            jQuery("#cad_dp").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#depoimento_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valNovoModulo() {
    return "" == document.cad_modulo.modulo.value ? (alert("Campo Nome do Módulo Obrigatório!"), document.cad_modulo.modulo.focus(), !1) : (document.getElementById("btn_cm").disabled = !0, document.getElementById("btn_cm").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cm').disabled=false", 5e3), setTimeout("document.getElementById('btn_cm').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=novo_modulo",
        data: jQuery("#cad_modulo").serialize(),
        success: function (msg) {
            jQuery("#cad_modulo").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#modulo_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmNovoModulo() {
    return "" == document.cad_modulo.modulo.value ? (alert("Campo Nome do Módulo Obrigatório!"), document.cad_modulo.modulo.focus(), !1) : (document.getElementById("btn_cm").disabled = !0, document.getElementById("btn_cm").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cm').disabled=false", 5e3), setTimeout("document.getElementById('btn_cm').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=novo_modulo",
        data: jQuery("#cad_modulo").serialize(),
        success: function (msg) {
            jQuery("#cad_modulo").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#modulo_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valEditarModulo() {
    return "" == document.cad_modulo.ordem_modulo.value ? (alert("Campo Ordem do Módulo Obrigatório!"), document.cad_modulo.ordem_modulo.focus(), !1) : "" == document.cad_modulo.modulo.value ? (alert("Campo Nome do Módulo Obrigatório!"), document.cad_modulo.modulo.focus(), !1) : (document.getElementById("btn_em").disabled = !0, document.getElementById("btn_em").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_em').disabled=false", 5e3), setTimeout("document.getElementById('btn_em').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar_modulo",
        data: jQuery("#cad_modulo").serialize(),
        success: function (msg) {
            jQuery("#modulo_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmEditarModulo() {
    return "" == document.cad_modulo.modulo.value ? (alert("Campo Nome do Módulo Obrigatório!"), document.cad_modulo.modulo.focus(), !1) : (document.getElementById("btn_em").disabled = !0, document.getElementById("btn_em").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_em').disabled=false", 5e3), setTimeout("document.getElementById('btn_em').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar_modulo",
        data: jQuery("#cad_modulo").serialize(),
        success: function (msg) {
            jQuery("#modulo_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valCadAula() {
    if ("" == document.cad_aula.modulo.value) return alert("Campo Módulo Obrigatório!"), document.cad_aula.modulo.focus(), !1;
    if ("" == document.cad_aula.aula.value) return alert("Campo Nome da Aula Obrigatório!"), document.cad_aula.aula.focus(), !1;
    if ("" == document.cad_aula.tipo_aula.value) return alert("Campo Tipo da Aula Obrigatório!"), document.cad_aula.tipo_aula.focus(), !1;
    if ("" == document.cad_aula.nivel.value) return alert("Campo Nível Obrigatório!"), document.cad_aula.nivel.focus(), !1;
    $("#loading").html('<div class="progress progress-striped active tooltips" style="margin:10px 0 0;"><div class="progress-bar text-center"  role="progressbar" style="width:100%"><b>Aguarde...</b></div></div>'), $("#btn_ca").addClass("disabled");
    var formData = new FormData($("#cad_aula")[0]);
    return $.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=cadastrar-aula",
        data: formData,
        async: !0,
        cache: !1,
        contentType: !1,
        processData: !1,
        success: function (msg) {
            $("#cad_aula").css("display", "none"), $(".modal-footer").css("display", "none"), $("#aula_sucesso").html(msg), $("button.close").click(function () {
                location.reload()
            })
        }
    }), !1
}
function valAdmCadAula() {
    if ("" == document.cad_aula.modulo.value) return alert("Campo Módulo Obrigatório!"), document.cad_aula.modulo.focus(), !1;
    if ("" == document.cad_aula.aula.value) return alert("Campo Nome da Aula Obrigatório!"), document.cad_aula.aula.focus(), !1;
    if ("" == document.cad_aula.tipo_aula.value) return alert("Campo Tipo da Aula Obrigatório!"), document.cad_aula.tipo_aula.focus(), !1;
    if ("" == document.cad_aula.nivel.value) return alert("Campo Nível Obrigatório!"), document.cad_aula.nivel.focus(), !1;
    $("#loading").html('<div class="progress progress-striped active tooltips" style="margin:10px 0 0;"><div class="progress-bar text-center"  role="progressbar" style="width:100%"><b>Aguarde...</b></div></div>'), $("#btn_ca").addClass("disabled");
    var formData = new FormData($("#cad_aula")[0]);
    return $.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=cadastrar-aula",
        data: formData,
        async: !0,
        cache: !1,
        contentType: !1,
        processData: !1,
        success: function (msg) {
            $("#cad_aula").css("display", "none"), $(".modal-footer").css("display", "none"), $("#aula_sucesso").html(msg), $("button.close").click(function () {
                location.reload()
            })
        }
    }), !1
}
function valEditarAula() {
    if ("" == document.cad_aula.modulo.value) return alert("Campo Módulo Obrigatório!"), document.cad_aula.modulo.focus(), !1;
    if ("" == document.cad_aula.aula.value) return alert("Campo Nome da Aula Obrigatório!"), document.cad_aula.aula.focus(), !1;
    if ("" == document.cad_aula.nivel.value) return alert("Campo Nível Obrigatório!"), document.cad_aula.nivel.focus(), !1;
    $("#loading").html('<div class="progress progress-striped active tooltips" style="margin:10px 0 0;"><div class="progress-bar text-center"  role="progressbar" style="width:100%"><b>Aguarde...</b></div></div>'), $("#btn_ea").addClass("disabled");
    var formData = new FormData($("#cad_aula")[0]);
    return $.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=editar-aula",
        data: formData,
        async: !0,
        cache: !1,
        contentType: !1,
        processData: !1,
        success: function (msg) {
            $("#aula_sucesso").html(msg).fadeOut(12e3), $("#loading").html(""), setTimeout("location.reload()", 5e3), $("button.close").click(function () {
                location.reload()
            })
        }
    }), !1
}
function valAdmEditarAula() {
    if ("" == document.cad_aula.modulo.value) return alert("Campo Módulo Obrigatório!"), document.cad_aula.modulo.focus(), !1;
    if ("" == document.cad_aula.aula.value) return alert("Campo Nome da Aula Obrigatório!"), document.cad_aula.aula.focus(), !1;
    if ("" == document.cad_aula.nivel.value) return alert("Campo Nível Obrigatório!"), document.cad_aula.nivel.focus(), !1;
    $("#loading").html('<div class="progress progress-striped active tooltips" style="margin:10px 0 0;"><div class="progress-bar text-center"  role="progressbar" style="width:100%"><b>Aguarde...</b></div></div>'), $("#btn_ea").addClass("disabled");
    var formData = new FormData($("#cad_aula")[0]);
    return $.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-aula",
        data: formData,
        async: !0,
        cache: !1,
        contentType: !1,
        processData: !1,
        success: function (msg) {
            $("#aula_sucesso").html(msg).fadeOut(12e3), $("#loading").html(""), setTimeout("location.reload()", 5e3), $("button.close").click(function () {
                location.reload()
            })
        }
    }), !1
}
function valCupom() {
    return "" == document.cad_cupom.curso.value ? (alert("Campo Curso Obrigatório!"), document.cad_cupom.curso.focus(), !1) : "" == document.cad_cupom.chave.value ? (alert("Campo Código do desconto Obrigatório!"), document.cad_cupom.chave.focus(), !1) : "" == document.cad_cupom.desconto.value ? (alert("Campo Desconto Obrigatório!"), document.cad_cupom.desconto.focus(), !1) : "" == document.cad_cupom.expira.value ? (alert("Campo Vencimento do Cupom Obrigatório!"), document.cad_cupom.expira.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=cadastrar-cupom",
        data: jQuery("#cad_cupom").serialize(),
        success: function (msg) {
            jQuery("#cad_cupom").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#cupom_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valCupom() {
    return "" == document.cad_cupom.curso.value ? (alert("Campo Curso Obrigatório!"), document.cad_cupom.curso.focus(), !1) : "" == document.cad_cupom.chave.value ? (alert("Campo Código do desconto Obrigatório!"), document.cad_cupom.chave.focus(), !1) : "" == document.cad_cupom.desconto.value ? (alert("Campo Desconto Obrigatório!"), document.cad_cupom.desconto.focus(), !1) : "" == document.cad_cupom.expira.value ? (alert("Campo Vencimento do Cupom Obrigatório!"), document.cad_cupom.expira.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=cadastrar-cupom",
        data: jQuery("#cad_cupom").serialize(),
        success: function (msg) {
            jQuery("#cad_cupom").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#cupom_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmCupom() {
    return "" == document.cad_cupom.curso.value ? (alert("Campo Curso Obrigatório!"), document.cad_cupom.curso.focus(), !1) : "" == document.cad_cupom.chave.value ? (alert("Campo Código do desconto Obrigatório!"), document.cad_cupom.chave.focus(), !1) : "" == document.cad_cupom.desconto.value ? (alert("Campo Desconto Obrigatório!"), document.cad_cupom.desconto.focus(), !1) : "" == document.cad_cupom.expira.value ? (alert("Campo Vencimento do Cupom Obrigatório!"), document.cad_cupom.expira.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=cadastrar-cupom",
        data: jQuery("#cad_cupom").serialize(),
        success: function (msg) {
            jQuery("#cad_cupom").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("#cupom_sucesso").html(msg), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valEditarCupom() {
    return "" == document.cad_cupom.curso.value ? (alert("Campo Curso Obrigatório!"), document.cad_cupom.curso.focus(), !1) : "" == document.cad_cupom.chave.value ? (alert("Campo Código do desconto Obrigatório!"), document.cad_cupom.chave.focus(), !1) : "" == document.cad_cupom.desconto.value ? (alert("Campo Desconto Obrigatório!"), document.cad_cupom.desconto.focus(), !1) : "" == document.cad_cupom.expira.value ? (alert("Campo Vencimento do Cupom Obrigatório!"), document.cad_cupom.expira.focus(), !1) : (document.getElementById("btn_ec").disabled = !0, document.getElementById("btn_ec").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ec').disabled=false", 5e3), setTimeout("document.getElementById('btn_ec').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=editar-cupom",
        data: jQuery("#cad_cupom").serialize(),
        success: function (msg) {
            jQuery("#cupom_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmEditarCupom() {
    return "" == document.cad_cupom.curso.value ? (alert("Campo Curso Obrigatório!"), document.cad_cupom.curso.focus(), !1) : "" == document.cad_cupom.chave.value ? (alert("Campo Código do desconto Obrigatório!"), document.cad_cupom.chave.focus(), !1) : "" == document.cad_cupom.desconto.value ? (alert("Campo Desconto Obrigatório!"), document.cad_cupom.desconto.focus(), !1) : "" == document.cad_cupom.expira.value ? (alert("Campo Vencimento do Cupom Obrigatório!"), document.cad_cupom.expira.focus(), !1) : (document.getElementById("btn_ec").disabled = !0, document.getElementById("btn_ec").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ec').disabled=false", 5e3), setTimeout("document.getElementById('btn_ec').innerHTML='Salvar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-cupom",
        data: jQuery("#cad_cupom").serialize(),
        success: function (msg) {
            jQuery("#cupom_sucesso").html(msg).fadeOut(12e3), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valDownload() {
    return "" == document.cad_arquivo.nome.value ? (alert("Campo Nome do Arquivo Obrigatório!"), document.cad_arquivo.nome.focus(), !1) : "" == document.cad_arquivo.arquivo.value ? (alert("Campo Arquivo Obrigatório!"), document.cad_arquivo.arquivo.focus(), !1) : (document.getElementById("btn_up").disabled = !0, document.getElementById("btn_up").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_up').disabled=false", 1e5), !0)
}
function valNovoEmail(cod) {
    return "" == document.novo_email.mensagem.value ? (alert("Campo Mensagem Obrigatório!"), document.novo_email.mensagem.focus(), !1) : (document.getElementById("btn_ne").disabled = !0, document.getElementById("btn_ne").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ne').disabled=false", 5e3), setTimeout("document.getElementById('btn_ne').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=email&cod=" + cod,
        data: jQuery("#novo_email").serialize(),
        success: function (msg) {
            jQuery("#novo_email").css("display", "none"), jQuery("#email_sucesso").html(msg), jQuery("#modal_close").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valAdmNovoEmail(cod) {
    return "" == document.novo_email.mensagem.value ? (alert("Campo Mensagem Obrigatório!"), document.novo_email.mensagem.focus(), !1) : (document.getElementById("btn_ne").disabled = !0, document.getElementById("btn_ne").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ne').disabled=false", 5e3), setTimeout("document.getElementById('btn_ne').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=email&cod=" + cod,
        data: jQuery("#novo_email").serialize(),
        success: function (msg) {
            jQuery("#novo_email").css("display", "none"), jQuery("#email_sucesso").html(msg), jQuery("#modal_close").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valCadastrarPedido() {
    return "" == document.cadastrar_pedido.TransacaoID.value ? (alert("Campo Código da Transação Obrigatório!"), document.cadastrar_pedido.TransacaoID.focus(), !1) : "" == document.cadastrar_pedido.Referencia.value ? (alert("Campo Referência Obrigatório!"), document.cadastrar_pedido.Referencia.focus(), !1) : "" == document.cadastrar_pedido.TipoPagamento.value ? (alert("Campo Forma de Pagamento Obrigatório!"), document.cadastrar_pedido.TipoPagamento.focus(), !1) : "" == document.cadastrar_pedido.StatusTransacao.value ? (alert("Campo Status da Transação Obrigatório!"), document.cadastrar_pedido.StatusTransacao.focus(), !1) : "" == document.cadastrar_pedido.ProdID.value ? (alert("Campo Curso Obrigatório!"), document.cadastrar_pedido.ProdID.focus(), !1) : (document.getElementById("btn_np").disabled = !0, document.getElementById("btn_np").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_np').disabled=false", 5e3), setTimeout("document.getElementById('btn_np').innerHTML='Cadastrar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=cadastrar-pedido",
        data: jQuery("#cadastrar_pedido").serialize(),
        success: function (msg) {
            jQuery("#cadastrar_pedido").css("display", "none"), jQuery("#pedido_sucesso").html(msg), jQuery("#footer_cadastrar").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
function valCadastroRapido() {
    return "" == document.cad_cr.nome.value || -1 == document.cad_cr.nome.value.indexOf(" ") ? (alert("Campo Nome e Sobrenome Obrigatório!"), document.cad_cr.nome.focus(), !1) : "" == document.cad_cr.email.value || -1 == document.cad_cr.email.value.indexOf("@") || -1 == document.cad_cr.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório!"), document.cad_cr.email.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/login/funLogin.php?acao=cadastro_rapido",
        data: jQuery("#cad_cr").serialize(),
        success: function (msg) {
            jQuery("#cad_cr").css("display", "none"), jQuery("#cr_sucesso").html(msg), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_cr").disabled = !0, document.getElementById("btn_cr").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cr').disabled=false", 1e4), setTimeout("document.getElementById('btn_cr').innerHTML='Cadastre-se'", 10001), !1)
}
function valConvite() {
    return "" == document.cad_cv.nome.value || -1 == document.cad_cv.nome.value.indexOf(" ") ? (alert("Campo Nome e Sobrenome Obrigatório!"), document.cad_cv.nome.focus(), !1) : "" == document.cad_cv.email.value || -1 == document.cad_cv.email.value.indexOf("@") || -1 == document.cad_cv.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório!"), document.cad_cv.email.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/login/funLogin.php?acao=convite",
        data: jQuery("#cad_cv").serialize(),
        success: function (msg) {
            jQuery("#cad_cv").css("display", "none"), jQuery("#cv_sucesso").html(msg), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_cv").disabled = !0, document.getElementById("btn_cv").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cv').disabled=false", 1e4), setTimeout("document.getElementById('btn_cv').innerHTML='Convidar'", 10001), !1)
}
function valEditarVenda() {
    return "" == document.editar_venda.Referencia.value ? (alert("Campo ID do Aluno Obrigatório!"), document.editar_venda.Referencia.focus(), !1) : "" == document.editar_venda.DataTransacao.value ? (alert("Campo Data da Transação Obrigatório!"), document.editar_venda.DataTransacao.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-venda",
        data: jQuery("#editar_venda").serialize(),
        success: function (msg) {
            jQuery("#ev_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ev").disabled = !0, document.getElementById("btn_ev").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ev').disabled=false", 1e4), setTimeout("document.getElementById('btn_ev').innerHTML='Salvar'", 10001), !1)
}
function valEditarAluno() {
    return "" == document.ed_aluno.username.value ? (alert("Campo Username Obrigatório!"), document.ed_aluno.username.focus(), !1) : "" == document.ed_aluno.nome.value ? (alert("Campo Nome Obrigatório!"), document.ed_aluno.nome.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-aluno",
        data: jQuery("#ed_aluno").serialize(),
        success: function (msg) {
            jQuery("#ea_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ea").disabled = !0, document.getElementById("btn_ea").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ea').disabled=false", 1e4), setTimeout("document.getElementById('btn_ea').innerHTML='Salvar'", 10001), !1)
}
function valMarca() {
    return "" == document.cad_marca.empresa.value ? (alert("Campo Nome da Empresa Obrigatório!"), document.cad_marca.empresa.focus(), !1) : "" == document.cad_marca.slogan.value ? (alert("Campo Slogan Obrigatório!"), document.cad_marca.slogan.focus(), !1) : "" == document.cad_marca.description.value ? (alert("Campo Descrição Obrigatório!"), document.cad_marca.description.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=marca",
        data: jQuery("#cad_marca").serialize(),
        success: function (msg) {
            jQuery("#marca_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_em").disabled = !0, document.getElementById("btn_em").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_em').disabled=false", 1e4), setTimeout("document.getElementById('btn_em').innerHTML='Salvar'", 10001), !1)
}
function valAtendimento() {
    return "" == document.cad_atendimento.email_atendimento.value ? (alert("Campo E-mail de Atendimento Obrigatório!"), document.cad_atendimento.email_atendimento.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=atendimento",
        data: jQuery("#cad_atendimento").serialize(),
        success: function (msg) {
            jQuery("#atendimento_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ea").disabled = !0, document.getElementById("btn_ea").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ea').disabled=false", 1e4), setTimeout("document.getElementById('btn_ea').innerHTML='Salvar'", 10001), !1)
}
function valCores() {
    return "" == document.cad_cores.hexa.value ? (alert("Campo Cor Predominante Obrigatório!"), document.cad_cores.hexa.focus(), !1) : "" == document.cad_cores.hover.value ? (alert("Campo Cor de Sobreposição Obrigatório!"), document.cad_cores.hover.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=cores",
        data: jQuery("#cad_cores").serialize(),
        success: function (msg) {
            jQuery("#cores_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ec").disabled = !0, document.getElementById("btn_ec").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ea').disabled=false", 1e4), setTimeout("document.getElementById('btn_ea').innerHTML='Salvar'", 10001), !1)
}
function valSocial() {
    return jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=social",
        data: jQuery("#cad_social").serialize(),
        success: function (msg) {
            jQuery("#social_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_es").disabled = !0, document.getElementById("btn_es").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_es').disabled=false", 1e4), setTimeout("document.getElementById('btn_es').innerHTML='Salvar'", 10001), !1
}
function valPagamento() {
    return "" == document.cad_pagamento.email_pagseguro.value ? (alert("Campo E-mail Pagseguro Obrigatório!"), document.cad_pagamento.email_pagseguro.focus(), !1) : "" == document.cad_pagamento.token.value ? (alert("Campo Token Pagseguro Obrigatório!"), document.cad_pagamento.token.focus(), !1) : "" == document.cad_pagamento.email_paypal.value ? (alert("Campo E-mail Paypal Obrigatório!"), document.cad_pagamento.email_paypal.focus(), !1) : "" == document.cad_pagamento.comissao.value ? (alert("Campo Comissão por Venda Obrigatório!"), document.cad_pagamento.comissao.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=pagamento",
        data: jQuery("#cad_pagamento").serialize(),
        success: function (msg) {
            jQuery("#pagamento_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ep").disabled = !0, document.getElementById("btn_ep").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ep').disabled=false", 1e4), setTimeout("document.getElementById('btn_ep').innerHTML='Salvar'", 10001), !1)
}
function valAdsense() {
    return jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=adsense",
        data: jQuery("#cad_adsense").serialize(),
        success: function (msg) {
            jQuery("#adsense_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ead").disabled = !0, document.getElementById("btn_ead").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ead').disabled=false", 1e4), setTimeout("document.getElementById('btn_ead').innerHTML='Salvar'", 10001), !1
}
function valScript() {
    return jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=script",
        data: jQuery("#cad_script").serialize(),
        success: function (msg) {
            jQuery("#script_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_esc").disabled = !0, document.getElementById("btn_esc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_esc').disabled=false", 1e4), setTimeout("document.getElementById('btn_esc').innerHTML='Salvar'", 10001), !1
}
function valAssinatura() {
    return "" == document.cad_assinatura.assinatura.value ? (alert("Campo Assinatura Obrigatório!"), document.cad_assinatura.assinatura.focus(), !1) : (document.getElementById("btn_eas").disabled = !0, document.getElementById("btn_eas").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_eas').disabled=false", 1e4), void setTimeout("document.getElementById('btn_eas').innerHTML='Upload'", 10001))
}
function valDiretor() {
    return "" == document.cad_diretor.diretor.value ? (alert("Campo Nome do Diretor Obrigatório!"), document.cad_diretor.diretor.focus(), !1) : "" == document.cad_diretor.abed.value ? (alert("Campo Associado ABED Obrigatório!"), document.cad_diretor.abed.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=diretor",
        data: jQuery("#cad_diretor").serialize(),
        success: function (msg) {
            jQuery("#diretor_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_edt").disabled = !0, document.getElementById("btn_edt").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_edt').disabled=false", 1e4), setTimeout("document.getElementById('btn_edt').innerHTML='Salvar'", 10001), !1)
}
function valCategoria() {
    return "" == document.cad_categoria.categoria.value ? (alert("Campo Nome da Categoria Obrigatório!"), document.cad_categoria.categoria.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=categoria",
        data: jQuery("#cad_categoria").serialize(),
        success: function (msg) {
            jQuery("#categoria_sucesso").html(msg), jQuery("#cad_categoria").css("display", "none"), jQuery("#categoria_forum .modal-footer").css("display", "none"), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_cca").disabled = !0, document.getElementById("btn_cca").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cca').disabled=false", 1e4), setTimeout("document.getElementById('btn_cca').innerHTML='Cadastrar'", 10001), !1)
}
function valAlbum() {
    return "" == document.cad_album.album.value ? (alert("Código do Álbum Obrigatório!"), document.cad_album.album.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=album",
        data: jQuery("#cad_album").serialize(),
        success: function (msg) {
            jQuery("#album_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_cav").disabled = !0, document.getElementById("btn_cav").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cav').disabled=false", 1e4), setTimeout("document.getElementById('btn_cav').innerHTML='Cadastrar'", 10001), !1)
}
function valCategoriaCursos() {
    return "" == document.cad_categoria_cursos.categoria.value ? (alert("Campo Nome da Categoria Obrigatório!"), document.cad_categoria_cursos.categoria.focus(), !1) : "" == document.cad_categoria_cursos.slug.value ? (alert("Campo URL Personaizada Obrigatório!"), document.cad_categoria_cursos.slug.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=categoria-cursos",
        data: jQuery("#cad_categoria_cursos").serialize(),
        success: function (msg) {
            jQuery("#categoria_cursos_sucesso").html(msg), jQuery("#cad_categoria_cursos").css("display", "none"), jQuery("#categoria_cursos .modal-footer").css("display", "none"), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ccc").disabled = !0, document.getElementById("btn_ccc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ccc').disabled=false", 1e4), setTimeout("document.getElementById('btn_ccc').innerHTML='Cadastrar'", 10001), !1)
}
function valEditarCategoria() {
    return "" == document.cad_categoria.ordem.value ? (alert("Campo Ordem da Categoria Obrigatório!"), document.cad_categoria.ordem.focus(), !1) : "" == document.cad_categoria.categoria.value ? (alert("Campo Nome da Categoria Obrigatório!"), document.cad_categoria.categoria.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-categoria",
        data: jQuery("#cad_categoria").serialize(),
        success: function (msg) {
            jQuery("#categoria_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_eca").disabled = !0, document.getElementById("btn_eca").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_eca').disabled=false", 1e4), setTimeout("document.getElementById('btn_eca').innerHTML='Cadastrar'", 10001), !1)
}
function valEditarCategoriaCursos() {
    return "" == document.cad_categoria.ordem.value ? (alert("Campo Ordem da Categoria Obrigatório!"), document.cad_categoria.ordem.focus(), !1) : "" == document.cad_categoria.categoria.value ? (alert("Campo Nome da Categoria Obrigatório!"), document.cad_categoria.categoria.focus(), !1) : "" == document.cad_categoria.slug.value ? (alert("Campo URL Personaizada Obrigatório!"), document.cad_categoria.slug.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-categoria-cursos",
        data: jQuery("#cad_categoria").serialize(),
        success: function (msg) {
            jQuery("#categoria_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_ecc").disabled = !0, document.getElementById("btn_ecc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ecc').disabled=false", 1e4), setTimeout("document.getElementById('btn_ecc').innerHTML='Cadastrar'", 10001), !1)
}
function valSmtp() {
    return "" == document.cad_smtp.hoste.value ? (alert("Campo Endereço do Host SSL Obrigatório!"), document.cad_smtp.hoste.focus(), !1) : "" == document.cad_smtp.porta.value ? (alert("Campo Número da Porta Obrigatório!"), document.cad_smtp.porta.focus(), !1) : "" == document.cad_smtp.email_notificacao.value ? (alert("Campo E-mail de Notificação Obrigatório!"), document.cad_smtp.email_notificacao.focus(), !1) : "" == document.cad_smtp.senha.value ? (alert("Campo Senha do E-mail de Notificação Obrigatório!"), document.cad_smtp.senha.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=smtp",
        data: jQuery("#cad_smtp").serialize(),
        success: function (msg) {
            jQuery("#smtp_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_esm").disabled = !0, document.getElementById("btn_esm").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_esm').disabled=false", 1e4), setTimeout("document.getElementById('btn_esm').innerHTML='Salvar'", 10001), !1)
}
function valOpauth() {
    return "" == document.cad_opauth.security_salt.value ? (alert("Campo Security Salt Obrigatório!"), document.cad_opauth.security_salt.focus(), !1) : "" == document.cad_opauth.twitter_key.value ? (alert("Campo Twitter Key Obrigatório!"), document.cad_opauth.twitter_key.focus(), !1) : "" == document.cad_opauth.twitter_secret.value ? (alert("Campo Twitter Secret Obrigatório!"), document.cad_opauth.twitter_secret.focus(), !1) : "" == document.cad_opauth.facebook_key.value ? (alert("Campo Facebook Key Obrigatório!"), document.cad_opauth.facebook_key.focus(), !1) : "" == document.cad_opauth.facebook_secret.value ? (alert("Campo Facebook Secret Obrigatório!"), document.cad_opauth.facebook_secret.focus(), !1) : "" == document.cad_opauth.google_key.value ? (alert("Campo Google Key Obrigatório!"), document.cad_opauth.google_key.focus(), !1) : "" == document.cad_opauth.google_secret.value ? (alert("Campo Google Secret Obrigatório!"), document.cad_opauth.google_secret.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=opauth",
        data: jQuery("#cad_opauth").serialize(),
        success: function (msg) {
            jQuery("#opauth_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_eop").disabled = !0, document.getElementById("btn_eop").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_eop').disabled=false", 1e4), setTimeout("document.getElementById('btn_eop').innerHTML='Salvar'", 10001), !1)
}
function valQuiz() {
    return "" == document.cad_quiz.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.cad_quiz.pergunta.focus(), !1) : "" == document.cad_quiz.opcao1.value ? (alert("Campo Opção 1 Obrigatório!"), document.cad_quiz.opcao1.focus(), !1) : "" == document.cad_quiz.opcao2.value ? (alert("Campo Opção 2 Obrigatório!"), document.cad_quiz.opcao2.focus(), !1) : "" == document.cad_quiz.opcao3.value ? (alert("Campo Opção 3 Obrigatório!"), document.cad_quiz.opcao3.focus(), !1) : "" == document.cad_quiz.opcao4.value ? (alert("Campo Opção 4 Obrigatório!"), document.cad_quiz.opcao4.focus(), !1) : "" == document.cad_quiz.resposta.value ? (alert("Campo Resposta Obrigatório!"), document.cad_quiz.resposta.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=quiz",
        data: jQuery("#cad_quiz").serialize(),
        success: function (msg) {
            jQuery("#sucesso_quiz").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_nq").disabled = !0, document.getElementById("btn_nq").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nq').disabled=false", 1e4), setTimeout("document.getElementById('btn_nq').innerHTML='Salvar'", 10001), !1)
}
function valAdmQuiz() {
    return "" == document.cad_quiz.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.cad_quiz.pergunta.focus(), !1) : "" == document.cad_quiz.opcao1.value ? (alert("Campo Opção 1 Obrigatório!"), document.cad_quiz.opcao1.focus(), !1) : "" == document.cad_quiz.opcao2.value ? (alert("Campo Opção 2 Obrigatório!"), document.cad_quiz.opcao2.focus(), !1) : "" == document.cad_quiz.opcao3.value ? (alert("Campo Opção 3 Obrigatório!"), document.cad_quiz.opcao3.focus(), !1) : "" == document.cad_quiz.opcao4.value ? (alert("Campo Opção 4 Obrigatório!"), document.cad_quiz.opcao4.focus(), !1) : "" == document.cad_quiz.resposta.value ? (alert("Campo Resposta Obrigatório!"), document.cad_quiz.resposta.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=quiz",
        data: jQuery("#cad_quiz").serialize(),
        success: function (msg) {
            jQuery("#sucesso_quiz").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_nq").disabled = !0, document.getElementById("btn_nq").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nq').disabled=false", 1e4), setTimeout("document.getElementById('btn_nq').innerHTML='Salvar'", 10001), !1)
}
function valResponderQuiz() {
    return 0 == document.resp_quiz.opcao[0].checked && 0 == document.resp_quiz.opcao[1].checked && 0 == document.resp_quiz.opcao[2].checked && 0 == document.resp_quiz.opcao[3].checked ? (alert("Escolha uma das respostas!"), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=responder-quiz",
        data: jQuery("#resp_quiz").serialize(),
        success: function (msg) {
            jQuery("#quiz_sucesso").html(msg), jQuery("#resp_quiz").hide(), jQuery("#modal_quiz").hide(), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_rq").disabled = !0, document.getElementById("btn_rq").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rq').disabled=false", 1e4), setTimeout("document.getElementById('btn_rq').innerHTML='Responder'", 10001), !1)
}
function valPergunta() {
    return "" == document.nova_pergunta.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.nova_pergunta.pergunta.focus(), !1) : "" == document.nova_pergunta.pergunta_tipo.value ? (alert("Campo Tipo da Pergunta Obrigatório!"), document.nova_pergunta.pergunta_tipo.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=pergunta",
        data: jQuery("#nova_pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), jQuery("#nova_pergunta").hide(), jQuery("#modal-pergunta .modal-footer").hide(), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_np").disabled = !0, document.getElementById("btn_np").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_np').disabled=false", 1e4), setTimeout("document.getElementById('btn_np').innerHTML='Cadastrar'", 10001), !1)
}
function valAdmPergunta() {
    return "" == document.nova_pergunta.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.nova_pergunta.pergunta.focus(), !1) : "" == document.nova_pergunta.pergunta_tipo.value ? (alert("Campo Tipo da Pergunta Obrigatório!"), document.nova_pergunta.pergunta_tipo.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=pergunta",
        data: jQuery("#nova_pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), jQuery("#nova_pergunta").hide(), jQuery("#modal-pergunta .modal-footer").hide(), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_np").disabled = !0, document.getElementById("btn_np").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_np').disabled=false", 1e4), setTimeout("document.getElementById('btn_np').innerHTML='Cadastrar'", 10001), !1)
}
function valEditarPergunta() {
    return "" == document.nova_pergunta.ordem.value ? (alert("Campo Ordem da Pergunta Obrigatório!"), document.nova_pergunta.ordem.focus(), !1) : "" == document.nova_pergunta.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.nova_pergunta.pergunta.focus(), !1) : "" == document.nova_pergunta.pergunta_tipo.value ? (alert("Campo Tipo da Pergunta Obrigatório!"), document.nova_pergunta.pergunta_tipo.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=editar-pergunta",
        data: jQuery("#nova_pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_epp").disabled = !0, document.getElementById("btn_epp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_epp').disabled=false", 1e4), setTimeout("document.getElementById('btn_epp').innerHTML='Salvar'", 10001), !1)
}
function valAdmEditarPergunta() {
    return "" == document.nova_pergunta.ordem.value ? (alert("Campo Ordem da Pergunta Obrigatório!"), document.nova_pergunta.ordem.focus(), !1) : "" == document.nova_pergunta.pergunta.value ? (alert("Campo Pergunta Obrigatório!"), document.nova_pergunta.pergunta.focus(), !1) : "" == document.nova_pergunta.pergunta_tipo.value ? (alert("Campo Tipo da Pergunta Obrigatório!"), document.nova_pergunta.pergunta_tipo.focus(), !1) : (jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=editar-pergunta",
        data: jQuery("#nova_pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), document.getElementById("btn_epp").disabled = !0, document.getElementById("btn_epp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_epp').disabled=false", 1e4), setTimeout("document.getElementById('btn_epp').innerHTML='Salvar'", 10001), !1)
}
function valGabaritoDissertativa() {
    return "" == document.gabarito.resposta.value ? (alert("Campo Resposta Obrigatório!"), document.gabarito.resposta.focus(), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=pergunta-dissertativa",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valPerguntaDissertativa() {
    return "" == document.pergunta.resposta.value ? (alert("Campo Resposta Obrigatório!"), document.pergunta.resposta.focus(), !1) : (document.getElementById("btn_rp").disabled = !0, document.getElementById("btn_rp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rp').disabled=false", 1e4), setTimeout("document.getElementById('btn_rp').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=resposta-dissertativa",
        data: jQuery("#pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), jQuery("#btn_rp").hide(), jQuery("#pergunta").hide()
        }
    }))
}
function valAdmGabaritoDissertativa() {
    return "" == document.gabarito.resposta.value ? (alert("Campo Resposta Obrigatório!"), document.gabarito.resposta.focus(), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=pergunta-dissertativa",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valGabaritoMultipla() {
    return 0 == document.gabarito.opcao1.checked && 0 == document.gabarito.opcao2.checked && 0 == document.gabarito.opcao3.checked && 0 == document.gabarito.opcao4.checked ? (alert("Escolha ao menos uma das respostas!"), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=pergunta-multipla",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valAdmGabaritoMultipla() {
    return 0 == document.gabarito.opcao1.checked && 0 == document.gabarito.opcao2.checked && 0 == document.gabarito.opcao3.checked && 0 == document.gabarito.opcao4.checked ? (alert("Escolha ao menos uma das respostas!"), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=pergunta-multipla",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valPerguntaMultipla() {
    return 0 == document.pergunta.opcao1.checked && 0 == document.pergunta.opcao2.checked && 0 == document.pergunta.opcao3.checked && 0 == document.pergunta.opcao4.checked ? (alert("Escolha ao menos uma das respostas!"), !1) : (document.getElementById("btn_rp").disabled = !0, document.getElementById("btn_rp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rp').disabled=false", 1e4), setTimeout("document.getElementById('btn_rp').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=resposta-multipla",
        data: jQuery("#pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), jQuery("#btn_rp").hide(), jQuery("#pergunta").hide()
        }
    }))
}
function valGabaritoAlternativa() {
    return 0 == document.gabarito.opcao[0].checked && 0 == document.gabarito.opcao[1].checked && 0 == document.gabarito.opcao[2].checked && 0 == document.gabarito.opcao[3].checked ? (alert("Escolha uma das respostas!"), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=pergunta-alternativa",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valAdmGabaritoAlternativa() {
    return 0 == document.gabarito.opcao[0].checked && 0 == document.gabarito.opcao[1].checked && 0 == document.gabarito.opcao[2].checked && 0 == document.gabarito.opcao[3].checked ? (alert("Escolha uma das respostas!"), !1) : (document.getElementById("btn_cg").disabled = !0, document.getElementById("btn_cg").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_cg').disabled=false", 1e4), setTimeout("document.getElementById('btn_cg').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=pergunta-alternativa",
        data: jQuery("#gabarito").serialize(),
        success: function (msg) {
            jQuery("#gabarito_sucesso").html(msg), jQuery("#btn_cg").hide(), jQuery("#gabarito").hide()
        }
    }))
}
function valPerguntaAlternativa() {
    return 0 == document.pergunta.opcao[0].checked && 0 == document.pergunta.opcao[1].checked && 0 == document.pergunta.opcao[2].checked && 0 == document.pergunta.opcao[3].checked ? (alert("Escolha uma das respostas!"), !1) : (document.getElementById("btn_rp").disabled = !0, document.getElementById("btn_rp").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rp').disabled=false", 1e4), setTimeout("document.getElementById('btn_rp').innerHTML='Responder'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/curso/funCurso.php?acao=resposta-alternativa",
        data: jQuery("#pergunta").serialize(),
        success: function (msg) {
            jQuery("#pergunta_sucesso").html(msg), jQuery("#btn_rp").hide(), jQuery("#pergunta").hide()
        }
    }))
}
function GabaritoProximo(cod) {
    $("#modal-gabarito .modal-content").load(cod)
}
function GabaritoPergunta(cod) {
    $("#modal-gabarito .modal-content").load(cod)
}
function GabaritoAnterior(cod) {
    $("#modal-gabarito .modal-content").load(cod)
}
function comecarProva(cod) {
    $("#modal-prova .modal-content").load(cod)
}
function navPergunta(cod) {
    $("#modal-prova .modal-content").load(cod)
}
function provaProximo(cod) {
    $("#modal-prova .modal-content").load(cod)
}
function provaAnterior(cod) {
    $("#modal-prova .modal-content").load(cod)
}
function valAprovarAluno() {
    return "" == document.ar_certificado.nota_final.value ? (alert("Campo Nota Final Obrigatório!"), document.ar_certificado.nota_final.focus(), !1) : (document.getElementById("btn_ac").disabled = !0, document.getElementById("btn_ac").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ac').disabled=false", 1e4), setTimeout("document.getElementById('btn_ac').innerHTML='<i class='icon-thumbs-up-alt'></i> Aprovar'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=aprovar-aluno",
        data: jQuery("#ar_certificado").serialize(),
        success: function (msg) {
            jQuery("#certificado_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }))
}
function valAdmAprovarAluno() {
    return "" == document.ar_certificado.nota_final.value ? (alert("Campo Nota Final Obrigatório!"), document.ar_certificado.nota_final.focus(), !1) : (document.getElementById("btn_ac").disabled = !0, document.getElementById("btn_ac").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_ac').disabled=false", 1e4), setTimeout("document.getElementById('btn_ac').innerHTML='<i class='icon-thumbs-up-alt'></i> Aprovar'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=aprovar-aluno",
        data: jQuery("#ar_certificado").serialize(),
        success: function (msg) {
            jQuery("#certificado_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }))
}
function valReprovarAluno() {
    return "" == document.ar_certificado.nota_final.value ? (alert("Campo Nota Final Obrigatório!"), document.ar_certificado.nota_final.focus(), !1) : (document.getElementById("btn_rc").disabled = !0, document.getElementById("btn_rc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rc').disabled=false", 1e4), setTimeout("document.getElementById('btn_rc').innerHTML='<i class='icon-thumbs-down-alt'></i> Reprovar'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=reprovar-aluno",
        data: jQuery("#ar_certificado").serialize(),
        success: function (msg) {
            jQuery("#certificado_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }))
}
function valAdmReprovarAluno() {
    return "" == document.ar_certificado.nota_final.value ? (alert("Campo Nota Final Obrigatório!"), document.ar_certificado.nota_final.focus(), !1) : (document.getElementById("btn_rc").disabled = !0, document.getElementById("btn_rc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_rc').disabled=false", 1e4), setTimeout("document.getElementById('btn_rc').innerHTML='<i class='icon-thumbs-down-alt'></i> Reprovar'", 10001), void jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=reprovar-aluno",
        data: jQuery("#ar_certificado").serialize(),
        success: function (msg) {
            jQuery("#certificado_sucesso").html(msg), setTimeout("location.reload()", 5e3), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }))
}
function valCorreta(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=resposta-correta&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valCorretaAdm(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=resposta-correta&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valParte(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=resposta-parte&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valParteAdm(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=resposta-parte&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valIncorreta(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/professor/funProfessor.php?acao=resposta-incorreta&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valIncorretaAdm(cod) {
    $("#correcao" + cod + " button").attr("disabled", "disabled"), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/adm/funAdm.php?acao=resposta-incorreta&cod=" + cod + "&correcao=sim",
        data: jQuery("#correcao" + cod).serialize(),
        success: function (msg) {
            jQuery("#correcao" + cod).hide(), jQuery("#sucesso" + cod).html(msg)
        },
        error: function () {
            alert("erro")
        }
    })
}
function valContato() {
    return "" == document.cad_contato.nome.value ? (alert("Campo Nome Obrigatório!"), document.cad_contato.nome.focus(), !1) : "" == document.cad_contato.email.value || -1 == document.cad_contato.email.value.indexOf("@") || -1 == document.cad_contato.email.value.indexOf(".") ? (alert("Campo E-mail Obrigatório"), document.cad_contato.email.focus(), !1) : "" == document.cad_contato.assunto.value ? (alert("Campo Assunto Obrigatório!"), document.cad_contato.assunto.focus(), !1) : "" == document.cad_contato.mensagem.value ? (alert("Campo Mensagem Obrigatório!"), document.cad_contato.mensagem.focus(), !1) : (document.getElementById("btn_nc").disabled = !0, document.getElementById("btn_nc").innerHTML = "Aguarde...", setTimeout("document.getElementById('btn_nc').disabled=false", 5e3), setTimeout("document.getElementById('btn_nc').innerHTML='Enviar'", 5001), jQuery.ajax({
        type: "POST",
        url: "http://" + window.location.host + "/modulos/contato/funContato.php",
        data: jQuery("#cad_contato").serialize(),
        success: function (msg) {
            jQuery("#contato_sucesso").html(msg), jQuery("#cad_contato").css("display", "none"), jQuery(".modal-footer").css("display", "none"), jQuery("button.close").click(function () {
                location.reload()
            })
        }
    }), !1)
}
$("video, audio").on("contextmenu", function (e) {
    e.preventDefault()
}), $("#formato-aula").hide(), $("#tipo_aula").on("change", function () {
    "" != this.value ? $("#formato-aula").show() : $("#formato-aula").hide(), "vimeo" == this.value ? ($("#div-vimeo").html('<textarea name="embed[vimeo]" id="vimeo" rows="3" class="form-control" placeholder="http://player.vimeo.com/video/ID_VIDEO">http://player.vimeo.com/video/ID_VIDEO</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-vimeo").html(""), "storage" == this.value ? ($("#div-storage").html('<input type="file" name="storage" id="storage" class="form-control" />'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-storage").html(""), "youtube" == this.value ? ($("#div-youtube").html('<textarea name="embed[youtube]" id="youtube" rows="3" class="form-control" placeholder="http://www.youtube.com/embed/ID_VIDEO">http://www.youtube.com/embed/ID_VIDEO</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-youtube").html(""), "videolog" == this.value ? ($("#div-videolog").html('<textarea name="embed[videolog]" id="videolog" rows="3" class="form-control" placeholder="http://embed.videolog.tv/v/index.php?id_video=ID_DO_VIDEO">http://embed.videolog.tv/v/index.php?id_video=ID_VIDEO</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-videolog").html(""), "mp4" == this.value ? ($("#div-mp4").html('<input type="file" name="mp4" id="mp4" class="form-control" />'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-mp4").html(""), "ustream" == this.value ? ($("#div-ustream").html('<textarea name="embed[ustream]" id="ustream" rows="3" class="form-control" placeholder="http://www.ustream.tv/embed/ID_STREAMING">http://www.ustream.tv/embed/ID_STREAMING</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-ustream").html(""), "eventials" == this.value ? ($("#div-eventials").html('<textarea name="embed[eventials]" id="eventials" rows="3" class="form-control" placeholder="http://www.eventials.com/talk-embed/ID_STREAMING/">http://www.eventials.com/talk-embed/ID_STREAMING/</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-eventials").html(""), "slideshare" == this.value ? ($("#div-slideshare").html('<textarea name="embed[slideshare]" id="slideshare" rows="3" class="form-control" placeholder="http://www.slideshare.net/slideshow/embed_code/ID_SLIDESHARE">http://www.slideshare.net/slideshow/embed_code/ID_SLIDESHARE</textarea>'), $("#paginas-slide").html('<div class="form-group"><label for="paginas" class="col-lg-2 control-label">Páginas</label><div class="col-lg-2"><input type="number" name="paginas" id="paginas" min="1" maxlength="5" class="form-control" required="required"></div></div>'), $("#duracao-video").html("")) : $("#div-slideshare").html(""), "speakerdeck" == this.value ? ($("#div-speakerdeck").html('<textarea name="embed[speakerdeck]" id="speakerdeck" rows="3" class="form-control" placeholder="http://speakerdeck.com/player/ID_SPEAKERDECK">http://speakerdeck.com/player/ID_SPEAKERDECK</textarea>'), $("#paginas-slide").html('<div class="form-group"><label for="paginas" class="col-lg-2 control-label">Páginas</label><div class="col-lg-2"><input type="number" name="paginas" id="paginas" min="1" maxlength="5" class="form-control" required="required"></div></div>'), $("#duracao-video").html("")) : $("#div-speakerdeck").html(""), "soundcloud" == this.value ? ($("#div-soundcloud").html('<textarea name="embed[soundcloud]" id="soundcloud" rows="3" class="form-control" placeholder="http://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/ID_SOUNDCLOUD">http://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/ID_SOUNDCLOUD</textarea>'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>')) : $("#div-soundcloud").html(""), "mp3" == this.value ? ($("#div-mp3").html('<input type="file" name="mp3" id="mp3" class="form-control" accept="application/mp3" />'), $("#duracao-video").html('<div class="form-group"><label for="duracao" class="col-lg-2 control-label">Duração</label><div class="col-lg-2"><input type="text" name="duracao" id="duracao" maxlength="5" placeholder="00:00" class="form-control" required="required"></div></div>'), $("#paginas-slide").html("")) : $("#div-mp3").html(""), "embed" == this.value ? ($("#div-embed").html('<textarea name="embed[embed]" id="embed" rows="3" class="form-control" placeholder="Cole aqui apenas a URL da EMBED "></textarea>'), $("#paginas-slide").html(""), $("#duracao-video").html("")) : $("#div-embed").html(""), "pdf" == this.value ? ($("#div-pdf").html('<input type="file" name="pdf" id="pdf" class="form-control" accept="application/pdf" />'), $("#paginas-slide").html('<div class="form-group"><label for="paginas" class="col-lg-2 control-label">Páginas</label><div class="col-lg-2"><input type="number" name="paginas" id="paginas" min="1" maxlength="5" class="form-control" required="required"></div></div>'), $("#duracao-video").html("")) : $("#div-pdf").html("")
}), $("#btn_ra").click(function () {
    $("#nao_confirmado .modal-footer, #nao_confirmado .alert, #nao_confirmado br").hide(), $(".recuperar_alunos").show();
    var frameSrc = "http://" + window.location.host + "/lib/funNaoConfirmado.php";
    $(".recuperar_alunos").attr("src", frameSrc)
}), $("#nao_confirmado .close").click(function () {
    location.reload()
}), $("#pergunta_tipo").on("change", function () {
    $("#alternativa").html(1 == this.value ? '<div class="form-group"><label class="col-lg-3 control-label"></label><div class="col-lg-8"><div class="help-block">Lembre-se, apenas 1 das respostas abaixo deverá ser correta.</div></div><label for="resposta1" class="col-lg-3 control-label">Resposta 1</label><div class="col-lg-8"><input type="text" name="resposta1" id="resposta1" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta2" class="col-lg-3 control-label">Resposta 2</label><div class="col-lg-8"><input type="text" name="resposta2" id="resposta2" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta3" class="col-lg-3 control-label">Resposta 3</label><div class="col-lg-8"><input type="text" name="resposta3" id="resposta3" maxlength="200" class="form-control" required="required"></div></div></div><div class="form-group"><label for="resposta4" class="col-lg-3 control-label">Resposta 4</label><div class="col-lg-8"><input type="text" name="resposta4" id="resposta4" maxlength="200" class="form-control" required="required"></div></div>' : ""), $("#multipla").html(3 == this.value ? '<div class="form-group"><label class="col-lg-3 control-label"></label><div class="col-lg-8"><div class="help-block">Lembre-se, mais de uma resposta pode ser a correta.</div></div><label for="resposta1" class="col-lg-3 control-label">Resposta 1</label><div class="col-lg-8"><input type="text" name="resposta1" id="resposta1" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta2" class="col-lg-3 control-label">Resposta 2</label><div class="col-lg-8"><input type="text" name="resposta2" id="resposta2" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta3" class="col-lg-3 control-label">Resposta 3</label><div class="col-lg-8"><input type="text" name="resposta3" id="resposta3" maxlength="200" class="form-control" required="required"></div></div></div><div class="form-group"><label for="resposta4" class="col-lg-3 control-label">Resposta 4</label><div class="col-lg-8"><input type="text" name="resposta4" id="resposta4" maxlength="200" class="form-control" required="required"></div></div>' : "")
}), $("#pergunta_tipo1").on("change", function () {
    $("#alternativa1").html(1 == this.value ? '<div class="form-group"><label class="col-lg-2 control-label"></label><div class="col-lg-10"><div class="help-block">Lembre-se, apenas 1 das respostas abaixo deverá estar correta.</div></div><label for="resposta1" class="col-lg-2 control-label">Resposta 1</label><div class="col-lg-4"><input type="text" name="resposta1" id="resposta1" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta2" class="col-lg-2 control-label">Resposta 2</label><div class="col-lg-4"><input type="text" name="resposta2" id="resposta2" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta3" class="col-lg-2 control-label">Resposta 3</label><div class="col-lg-4"><input type="text" name="resposta3" id="resposta3" maxlength="200" class="form-control" required="required"></div></div></div><div class="form-group"><label for="resposta4" class="col-lg-2 control-label">Resposta 4</label><div class="col-lg-4"><input type="text" name="resposta4" id="resposta4" maxlength="200" class="form-control" required="required"></div></div>' : ""), $("#multipla1").html(3 == this.value ? '<div class="form-group"><label class="col-lg-2 control-label"></label><div class="col-lg-10"><div class="help-block">Lembre-se, mais de uma resposta pode estar correta.</div></div><label for="resposta1" class="col-lg-2 control-label">Resposta 1</label><div class="col-lg-4"><input type="text" name="resposta1" id="resposta1" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta2" class="col-lg-2 control-label">Resposta 2</label><div class="col-lg-4"><input type="text" name="resposta2" id="resposta2" maxlength="200" class="form-control" required="required"></div></div><div class="form-group"><label for="resposta3" class="col-lg-2 control-label">Resposta 3</label><div class="col-lg-4"><input type="text" name="resposta3" id="resposta3" maxlength="200" class="form-control" required="required"></div></div></div><div class="form-group"><label for="resposta4" class="col-lg-2 control-label">Resposta 4</label><div class="col-lg-4"><input type="text" name="resposta4" id="resposta4" maxlength="200" class="form-control" required="required"></div></div>' : "")
});