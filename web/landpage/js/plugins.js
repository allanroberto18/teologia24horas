$(".tooltips").tooltip("hide"), $(".popovers").popover("hide"), $(function () {
    $.fn.scrollToTop = function () {
        $(this).hide().removeAttr("href"), "0" != $(window).scrollTop() && $(this).fadeIn("slow");
        var scrollDiv = $(this);
        $(window).scroll(function () {
            "0" == $(window).scrollTop() ? $(scrollDiv).fadeOut("slow") : $(scrollDiv).fadeIn("slow")
        }), $(this).click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, "slow")
        })
    }
}), $(function () {
    $("#toTop").scrollToTop()
}), $(document).ready(function () {
    var $header = $(".navbar-default"),
        $hHeight = $header.height(),
        prevTop = $(window).scrollTop();
    $(window).on("scroll", function () {
        st = $(this).scrollTop(), st > prevTop && st > $hHeight ? $header.addClass("global-header-scrolling") : $header.removeClass("global-header-scrolling"), prevTop = st
    })
}), $(".maxchar").maxlength({
    alwaysShow: !0
}), $(function () {
    $(".currency").maskMoney({
        thousands: ".",
        decimal: ",",
        symbolStay: !1
    })
}), $("#cursos a").click(function (e) {
    e.preventDefault(), $(this).tab("show")
}), $("ul.nav-tabs > li > a").on("shown.bs.tab", function (e) {
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id
});
var hash = window.location.hash;
$('#cursos a[href="' + hash + '"]').tab("show"), $('#myTab a[href="' + hash + '"]').tab("show"), $(".tabdrop").tabdrop(), $(document).ready(function () {
    $(".autosize").autosize()
});
var ias = $.ias({
    container: ".paginar",
    item: ".item",
    pagination: "#paginacao",
    next: ".proximo a"
});
ias.extension(new IASTriggerExtension({
    offset: 20,
    text: "Carregar mais..."
})), ias.extension(new IASSpinnerExtension), ias.extension(new IASNoneLeftExtension({
    html: ""
})), $(function () {
    $(".pdf").media()
}), Shadowbox.init(), $("#raty").raty();
$('#score').raty({
    score: function () {
        return $(this).attr('data-score');
    }
});