<?php

    session_start();

    $msg = "Add a new number";
    $list = null;

    if (!empty($_REQUEST))
    {
        $list = add($_REQUEST['value']);
    }
    if (isset($_REQUEST['clear']))
    {
        closeSession();
    }
    if (isset($_REQUEST['sort']))
    {
        $list = sortArray();
    }

    function closeSession()
    {
        $_SESSION['value'] = null;
    }

    function add($value)
    {
        $_SESSION['msg'] = null;
        if (empty($value) || !is_numeric($value))
        {
            $_SESSION['msg'] = "Add a valida value to sort";
            return;
        }
        $list = array();
        if (isset($_SESSION['value']))
        {
            $list = $_SESSION['value'];
        }
        $list[] = $value;

        $_SESSION['value'] = $list;

        return $list;
    }

    function sortArray()
    {
        return sort($_SESSION['value']);
    }
?>
<?php print $_SESSION['msg']; ?>
<form method="post">
    <label> Field: </label>
    <input type="text" name="value" />
    <input type="submit" name="submit" value="send" />
    <input type="submit" name="clear" value="clear" />
    <input type="submit" name="sort" value="sort" />
</form>

<?php
    print "<pre>";
    print $msg . "<br />";
    print_r($_SESSION['value']);
    print "</pre>";
?>

