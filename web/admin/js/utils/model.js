function Model(url) {
    this.url = url;
}

Model.prototype.ReturnAjax = function(value) {
    var result = "";
    $.ajax({
        url: this.url,
        type: "post",
        async: false,
        dateType: "json",
        data: {
            "param": value
        },
        beforeSend: function () {
            $(".carregando").show('slow');
        },
        success: function (data) {
            result = data;
        },
        complete: function () {
            $(".carregando").hide('slow');
        },
        error: function () {
            console.log("Erro: " + value + " - URL: " + this.url);
        }
    });
    return result;
};

Model.prototype.MakeOption = function(select, consult)
{
    var options = "<option value=''>[Selecionar Registro]</option>";
    $.each(consult, function(key, value){
        options += "<option value='" + value.id + "'>" + value.titulo + "</option>";
    });
    $(select).html(options);
}

        
