function Controller() { }

Controller.prototype.getEstados = function(div, pais, estado)
{
    var localizacao = new Localizacao();
    
    var estados = localizacao.returnEstados(pais);

    var options = "";
    if(!estados)
    {
        options += "<option value=''>[Não relacionado]</option>";
        $(div).html(options);
        $('.cidade').html(options);
        return;
    }
    $.each(estados, function(key, value){
        if (estado === value.estado) {
            options += "<option value='" + value.uf + "' selected='selected'>" + value.estado + "</option>";
        } else {
            options += "<option value='" + value.uf+ "'>" + value.estado + "</option>";
        }
    });
    $(div).html(options);
};

Controller.prototype.getCidades = function(div, uf, cidade)
{
    var localizacao = new Localizacao();
    var cidades = localizacao.returnCidades(uf);
     
    var options = "";
    if(!cidades)
    {
        $(div).html(options);
        return;
    }
    $.each(cidades, function(key, value){
        if (cidade === value.cidade) {
            options += "<option value='" + value.cidade + "' selected='selected'>" + value.cidade + "</option>";
        } else {
            options += "<option value='" + value.cidade + "'>" + value.cidade + "</option>";
        }
    });
    $(div).html(options);
};

Controller.prototype.removeTag = function(id)
{
    var tag = new Tag();
    tag.removeTag(id);
    
};