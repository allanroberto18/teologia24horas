function Localizacao() { }

Localizacao.prototype.returnEstados = function(pais) {
    if (pais != 'Brasil')
        return false;
    
    var model = new Model('/admin/Pessoa/Endereco/getUF/');
    
    return model.ReturnAjax(pais);
};
        
Localizacao.prototype.returnCidades = function(uf) {
    if (uf == "")
        return false;
    
    var model = new Model('/admin/Pessoa/Endereco/getCidades/');
    
    return model.ReturnAjax(uf);
};

