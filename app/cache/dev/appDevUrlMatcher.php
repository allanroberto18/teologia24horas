<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/bootstrap')) {
            // _assetic_bootstrap_css
            if ($pathinfo === '/css/bootstrap.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_css',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_bootstrap_css',);
            }

            if (0 === strpos($pathinfo, '/css/bootstrap_')) {
                // _assetic_bootstrap_css_0
                if ($pathinfo === '/css/bootstrap_bootstrap_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_css',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_bootstrap_css_0',);
                }

                // _assetic_bootstrap_css_1
                if ($pathinfo === '/css/bootstrap_form_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_css',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_bootstrap_css_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js')) {
            if (0 === strpos($pathinfo, '/js/bootstrap')) {
                // _assetic_bootstrap_js
                if ($pathinfo === '/js/bootstrap.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js',);
                }

                if (0 === strpos($pathinfo, '/js/bootstrap_')) {
                    // _assetic_bootstrap_js_0
                    if ($pathinfo === '/js/bootstrap_transition_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_0',);
                    }

                    // _assetic_bootstrap_js_1
                    if ($pathinfo === '/js/bootstrap_alert_2.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_1',);
                    }

                    // _assetic_bootstrap_js_2
                    if ($pathinfo === '/js/bootstrap_button_3.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_2',);
                    }

                    if (0 === strpos($pathinfo, '/js/bootstrap_c')) {
                        // _assetic_bootstrap_js_3
                        if ($pathinfo === '/js/bootstrap_carousel_4.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_3',);
                        }

                        // _assetic_bootstrap_js_4
                        if ($pathinfo === '/js/bootstrap_collapse_5.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_4',);
                        }

                    }

                    // _assetic_bootstrap_js_5
                    if ($pathinfo === '/js/bootstrap_dropdown_6.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_5',);
                    }

                    // _assetic_bootstrap_js_6
                    if ($pathinfo === '/js/bootstrap_modal_7.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_6',);
                    }

                    // _assetic_bootstrap_js_7
                    if ($pathinfo === '/js/bootstrap_tooltip_8.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_7',);
                    }

                    // _assetic_bootstrap_js_8
                    if ($pathinfo === '/js/bootstrap_popover_9.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_8',);
                    }

                    // _assetic_bootstrap_js_9
                    if ($pathinfo === '/js/bootstrap_scrollspy_10.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_9',);
                    }

                    // _assetic_bootstrap_js_10
                    if ($pathinfo === '/js/bootstrap_tab_11.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_10',);
                    }

                    // _assetic_bootstrap_js_11
                    if ($pathinfo === '/js/bootstrap_affix_12.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_11',);
                    }

                    // _assetic_bootstrap_js_12
                    if ($pathinfo === '/js/bootstrap_bc-bootstrap-collection_13.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'bootstrap_js',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_bootstrap_js_12',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/jquery')) {
                // _assetic_jquery
                if ($pathinfo === '/js/jquery.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'jquery',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_jquery',);
                }

                // _assetic_jquery_0
                if ($pathinfo === '/js/jquery_jquery.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'jquery',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_jquery_0',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/AVA')) {
            if (0 === strpos($pathinfo, '/AVA/Aluno')) {
                // ava_aluno_home
                if (rtrim($pathinfo, '/') === '/AVA/Aluno') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_aluno_home;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'ava_aluno_home');
                    }

                    return array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::homeAction',  '_route' => 'ava_aluno_home',);
                }
                not_ava_aluno_home:

                // ava_aluno_list_cursos
                if ($pathinfo === '/AVA/Aluno/meus-cursos') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_aluno_list_cursos;
                    }

                    return array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::listCursosAction',  '_route' => 'ava_aluno_list_cursos',);
                }
                not_ava_aluno_list_cursos:

                if (0 === strpos($pathinfo, '/AVA/Aluno/curso')) {
                    // ava_aluno_curso
                    if (0 === strpos($pathinfo, '/AVA/Aluno/curso/visualizar') && preg_match('#^/AVA/Aluno/curso/visualizar/(?P<idCurso>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_ava_aluno_curso;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ava_aluno_curso')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::cursoAction',));
                    }
                    not_ava_aluno_curso:

                    // ava_aluno_curso_aula
                    if (0 === strpos($pathinfo, '/AVA/Aluno/curso/aula') && preg_match('#^/AVA/Aluno/curso/aula/(?P<idAula>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_ava_aluno_curso_aula;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ava_aluno_curso_aula')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::aulaAction',));
                    }
                    not_ava_aluno_curso_aula:

                    // ava_aluno_contratar_carrinho_compra
                    if (0 === strpos($pathinfo, '/AVA/Aluno/curso/selecionar') && preg_match('#^/AVA/Aluno/curso/selecionar/(?P<idCurso>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_ava_aluno_contratar_carrinho_compra;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ava_aluno_contratar_carrinho_compra')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::carrinhoCompraAction',));
                    }
                    not_ava_aluno_contratar_carrinho_compra:

                }

                // ava_aluno_faturas
                if (rtrim($pathinfo, '/') === '/AVA/Aluno/Faturas') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_aluno_faturas;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'ava_aluno_faturas');
                    }

                    return array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::faturasCursoAction',  '_route' => 'ava_aluno_faturas',);
                }
                not_ava_aluno_faturas:

                // ava_aluno_notas
                if (rtrim($pathinfo, '/') === '/AVA/Aluno/Notas') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_aluno_notas;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'ava_aluno_notas');
                    }

                    return array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::notasCursoAction',  '_route' => 'ava_aluno_notas',);
                }
                not_ava_aluno_notas:

                // ava_aluno_ver_curso
                if (0 === strpos($pathinfo, '/AVA/Aluno/curso') && preg_match('#^/AVA/Aluno/curso/(?P<idCurso>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_aluno_ver_curso;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ava_aluno_ver_curso')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::verCursoAction',));
                }
                not_ava_aluno_ver_curso:

                // ava_material_didatico
                if (0 === strpos($pathinfo, '/AVA/Aluno/Atualizar/Fatura') && preg_match('#^/AVA/Aluno/Atualizar/Fatura\\-(?P<idFatura>[^/\\-]++)\\-(?P<tipo>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ava_material_didatico;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ava_material_didatico')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\CursoController::atualizarFaturaAction',));
                }
                not_ava_material_didatico:

                // gerar_boleto
                if (0 === strpos($pathinfo, '/AVA/Aluno/Exibir/Fatura') && preg_match('#^/AVA/Aluno/Exibir/Fatura\\-(?P<idFatura>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_gerar_boleto;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerar_boleto')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\FaturaController::gerarBoletoAction',));
                }
                not_gerar_boleto:

            }

            if (0 === strpos($pathinfo, '/AVA/Usuario')) {
                // ambiente_usuario_homepage
                if (0 === strpos($pathinfo, '/AVA/Usuario/entrar') && preg_match('#^/AVA/Usuario/entrar(?:\\-(?P<ide>[^/]++))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ambiente_usuario_homepage;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ambiente_usuario_homepage')), array (  'ide' => NULL,  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\DefaultController::indexAction',));
                }
                not_ambiente_usuario_homepage:

                if (0 === strpos($pathinfo, '/AVA/Usuario/log')) {
                    // ambiente_usuario_homepage_gerenciar_login
                    if ($pathinfo === '/AVA/Usuario/login') {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_ambiente_usuario_homepage_gerenciar_login;
                        }

                        return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\DefaultController::loginAction',  '_route' => 'ambiente_usuario_homepage_gerenciar_login',);
                    }
                    not_ambiente_usuario_homepage_gerenciar_login:

                    // ambiente_usuario_homepage_gerenciar_logout
                    if ($pathinfo === '/AVA/Usuario/logout') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_ambiente_usuario_homepage_gerenciar_logout;
                        }

                        return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\DefaultController::logoutAction',  '_route' => 'ambiente_usuario_homepage_gerenciar_logout',);
                    }
                    not_ambiente_usuario_homepage_gerenciar_logout:

                }

                // ambiente_usuario_homepage_home
                if (rtrim($pathinfo, '/') === '/AVA/Usuario/Home') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_ambiente_usuario_homepage_home;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'ambiente_usuario_homepage_home');
                    }

                    return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\DefaultController::homeAction',  '_route' => 'ambiente_usuario_homepage_home',);
                }
                not_ambiente_usuario_homepage_home:

                if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil')) {
                    // gerenciar_usuario
                    if ($pathinfo === '/AVA/Usuario/Perfil/gerenciar') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_gerenciar_usuario;
                        }

                        return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarUsuarioAction',  '_route' => 'gerenciar_usuario',);
                    }
                    not_gerenciar_usuario:

                    if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/usuario')) {
                        // gerenciar_usuario_novo
                        if ($pathinfo === '/AVA/Usuario/Perfil/usuario/novo') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_gerenciar_usuario_novo;
                            }

                            return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::novoUsuarioAction',  '_route' => 'gerenciar_usuario_novo',);
                        }
                        not_gerenciar_usuario_novo:

                        // gerenciar_usuario_salvar
                        if ($pathinfo === '/AVA/Usuario/Perfil/usuario/salvar') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_gerenciar_usuario_salvar;
                            }

                            return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::salvarUsuarioAction',  '_route' => 'gerenciar_usuario_salvar',);
                        }
                        not_gerenciar_usuario_salvar:

                        // gerenciar_usuario_editar
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/usuario/editar') && preg_match('#^/AVA/Usuario/Perfil/usuario/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_gerenciar_usuario_editar;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_usuario_editar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::editarUsuarioAction',));
                        }
                        not_gerenciar_usuario_editar:

                        // gerenciar_usuario_atualizar
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/usuario/atualizar') && preg_match('#^/AVA/Usuario/Perfil/usuario/atualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_gerenciar_usuario_atualizar;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_usuario_atualizar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::usuarioAtualizarAction',));
                        }
                        not_gerenciar_usuario_atualizar:

                    }

                    if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/senha')) {
                        // gerenciar_senha
                        if ($pathinfo === '/AVA/Usuario/Perfil/senha') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_gerenciar_senha;
                            }

                            return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarSenhaAction',  '_route' => 'gerenciar_senha',);
                        }
                        not_gerenciar_senha:

                        // gerenciar_senha_salvar
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/senha/salvar') && preg_match('#^/AVA/Usuario/Perfil/senha/salvar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_gerenciar_senha_salvar;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_senha_salvar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarSenhaSalvarAction',));
                        }
                        not_gerenciar_senha_salvar:

                        // gerenciar_senha_redefinir
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/senha/redefinir') && preg_match('#^/AVA/Usuario/Perfil/senha/redefinir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                                goto not_gerenciar_senha_redefinir;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_senha_redefinir')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::redefinirSenhaAction',));
                        }
                        not_gerenciar_senha_redefinir:

                    }

                    if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/foto')) {
                        // gerenciar_foto
                        if ($pathinfo === '/AVA/Usuario/Perfil/foto') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_gerenciar_foto;
                            }

                            return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarFotoAction',  '_route' => 'gerenciar_foto',);
                        }
                        not_gerenciar_foto:

                        // gerenciar_foto_salvar
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/foto/salvar') && preg_match('#^/AVA/Usuario/Perfil/foto/salvar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_gerenciar_foto_salvar;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_foto_salvar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarFotoSalvarAction',));
                        }
                        not_gerenciar_foto_salvar:

                    }

                    if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/e')) {
                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/endereco')) {
                            // gerenciar_endereco
                            if ($pathinfo === '/AVA/Usuario/Perfil/endereco') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_gerenciar_endereco;
                                }

                                return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::gerenciarEnderecoAction',  '_route' => 'gerenciar_endereco',);
                            }
                            not_gerenciar_endereco:

                            // gerenciar_endereco_novo
                            if ($pathinfo === '/AVA/Usuario/Perfil/endereco/novo') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_gerenciar_endereco_novo;
                                }

                                return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::novoEnderecoAction',  '_route' => 'gerenciar_endereco_novo',);
                            }
                            not_gerenciar_endereco_novo:

                            // gerenciar_endereco_salvar
                            if ($pathinfo === '/AVA/Usuario/Perfil/endereco/salvar') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_gerenciar_endereco_salvar;
                                }

                                return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::salvarEnderecoAction',  '_route' => 'gerenciar_endereco_salvar',);
                            }
                            not_gerenciar_endereco_salvar:

                            // gerenciar_endereco_editar
                            if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/endereco/editar') && preg_match('#^/AVA/Usuario/Perfil/endereco/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_gerenciar_endereco_editar;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_endereco_editar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::editarEnderecoAction',));
                            }
                            not_gerenciar_endereco_editar:

                            // gerenciar_endereco_atualizar
                            if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/endereco/atualizar') && preg_match('#^/AVA/Usuario/Perfil/endereco/atualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                    $allow = array_merge($allow, array('POST', 'PUT'));
                                    goto not_gerenciar_endereco_atualizar;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'gerenciar_endereco_atualizar')), array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::atualizarEnderecoAction',));
                            }
                            not_gerenciar_endereco_atualizar:

                        }

                        if (0 === strpos($pathinfo, '/AVA/Usuario/Perfil/email')) {
                            // perfil_edit_email_marketing
                            if ($pathinfo === '/AVA/Usuario/Perfil/email/editar') {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_perfil_edit_email_marketing;
                                }

                                return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::emailEditarAction',  '_route' => 'perfil_edit_email_marketing',);
                            }
                            not_perfil_edit_email_marketing:

                            // perfil_update_email_marketing
                            if ($pathinfo === '/AVA/Usuario/Perfil/email/atualizar') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_perfil_update_email_marketing;
                                }

                                return array (  '_controller' => 'Usuario\\AmbienteUsuarioBundle\\Controller\\UsuarioController::emailAtualizarAction',  '_route' => 'perfil_update_email_marketing',);
                            }
                            not_perfil_update_email_marketing:

                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/LandPage')) {
            // land_page_primeira_etapa_salvar
            if ($pathinfo === '/LandPage/salvar') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_land_page_primeira_etapa_salvar;
                }

                return array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::createAction',  '_route' => 'land_page_primeira_etapa_salvar',);
            }
            not_land_page_primeira_etapa_salvar:

            // land_page_primeira_etapa
            if (rtrim($pathinfo, '/') === '/LandPage') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_land_page_primeira_etapa;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'land_page_primeira_etapa');
                }

                return array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::primeiraEtapaAction',  '_route' => 'land_page_primeira_etapa',);
            }
            not_land_page_primeira_etapa:

            // land_page_segunda_etapa
            if ($pathinfo === '/LandPage/confirmar-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_land_page_segunda_etapa;
                }

                return array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::segundaEtapaAction',  '_route' => 'land_page_segunda_etapa',);
            }
            not_land_page_segunda_etapa:

            // land_page_terceira_etapa
            if (0 === strpos($pathinfo, '/LandPage/email-confirmado') && preg_match('#^/LandPage/email\\-confirmado/(?P<ide>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_land_page_terceira_etapa;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'land_page_terceira_etapa')), array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::terceiraEtapaAction',));
            }
            not_land_page_terceira_etapa:

            // land_page_solicitar_excluir_email
            if (0 === strpos($pathinfo, '/LandPage/remover-email') && preg_match('#^/LandPage/remover\\-email/(?P<identificador>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_land_page_solicitar_excluir_email;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'land_page_solicitar_excluir_email')), array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::solicitarExclusaoAction',));
            }
            not_land_page_solicitar_excluir_email:

            // land_page_excluir_email
            if (0 === strpos($pathinfo, '/LandPage/excluir-email') && preg_match('#^/LandPage/excluir\\-email/(?P<identificador>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_land_page_excluir_email;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'land_page_excluir_email')), array (  '_controller' => 'App\\LandPageBundle\\Controller\\LandPageController::excluirDaListaAction',));
            }
            not_land_page_excluir_email:

        }

        // front_pagina
        if (0 === strpos($pathinfo, '/Home') && preg_match('#^/Home/(?P<id>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_front_pagina;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_pagina')), array (  '_controller' => 'App\\HomeBundle\\Controller\\HomeController::paginaAction',));
        }
        not_front_pagina:

        // home_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_home_homepage;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home_homepage');
            }

            return array (  '_controller' => 'App\\HomeBundle\\Controller\\HomeController::homeAction',  '_route' => 'home_homepage',);
        }
        not_home_homepage:

        if (0 === strpos($pathinfo, '/c')) {
            // front_cursos
            if ($pathinfo === '/cursos') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_front_cursos;
                }

                return array (  '_controller' => 'App\\HomeBundle\\Controller\\HomeController::cursosAction',  '_route' => 'front_cursos',);
            }
            not_front_cursos:

            // front_categorias
            if (0 === strpos($pathinfo, '/categoria') && preg_match('#^/categoria/(?P<idCategoria>[^/]++)/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_front_categorias;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'front_categorias')), array (  '_controller' => 'App\\HomeBundle\\Controller\\HomeController::cursosByCategoriaAction',));
            }
            not_front_categorias:

        }

        // front_teste
        if ($pathinfo === '/teste') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_front_teste;
            }

            return array (  '_controller' => 'App\\HomeBundle\\Controller\\HomeController::testeRequestAction',  '_route' => 'front_teste',);
        }
        not_front_teste:

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/Mensagem')) {
                if (0 === strpos($pathinfo, '/admin/Mensagem/Tag')) {
                    // Mensagem_Tag
                    if (rtrim($pathinfo, '/') === '/admin/Mensagem/Tag') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Mensagem_Tag');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::indexAction',  '_route' => 'Mensagem_Tag',);
                    }

                    // Mensagem_Tag_show
                    if (preg_match('#^/admin/Mensagem/Tag/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Tag_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::showAction',));
                    }

                    // Mensagem_Tag_new
                    if ($pathinfo === '/admin/Mensagem/Tag/new') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::newAction',  '_route' => 'Mensagem_Tag_new',);
                    }

                    // Mensagem_Tag_create
                    if ($pathinfo === '/admin/Mensagem/Tag/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Mensagem_Tag_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::createAction',  '_route' => 'Mensagem_Tag_create',);
                    }
                    not_Mensagem_Tag_create:

                    // Mensagem_Tag_edit
                    if (preg_match('#^/admin/Mensagem/Tag/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Tag_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::editAction',));
                    }

                    // Mensagem_Tag_update
                    if (preg_match('#^/admin/Mensagem/Tag/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Mensagem_Tag_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Tag_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::updateAction',));
                    }
                    not_Mensagem_Tag_update:

                    // Mensagem_Tag_delete
                    if (preg_match('#^/admin/Mensagem/Tag/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Mensagem_Tag_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Tag_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::deleteAction',));
                    }
                    not_Mensagem_Tag_delete:

                    // Mensagem_Tag_Array
                    if ($pathinfo === '/admin/Mensagem/Tag/Salvar/Mensagem') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Mensagem_Tag_Array;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemTagController::salvarMensagemByTagAction',  '_route' => 'Mensagem_Tag_Array',);
                    }
                    not_Mensagem_Tag_Array:

                }

                if (0 === strpos($pathinfo, '/admin/Mensagem/Pessoa')) {
                    // Mensagem_Pessoa
                    if (rtrim($pathinfo, '/') === '/admin/Mensagem/Pessoa') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Mensagem_Pessoa');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::indexAction',  '_route' => 'Mensagem_Pessoa',);
                    }

                    // Mensagem_Pessoa_show
                    if (preg_match('#^/admin/Mensagem/Pessoa/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Pessoa_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::showAction',));
                    }

                    // Mensagem_Pessoa_new
                    if ($pathinfo === '/admin/Mensagem/Pessoa/new') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::newAction',  '_route' => 'Mensagem_Pessoa_new',);
                    }

                    // Mensagem_Pessoa_create
                    if ($pathinfo === '/admin/Mensagem/Pessoa/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Mensagem_Pessoa_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::createAction',  '_route' => 'Mensagem_Pessoa_create',);
                    }
                    not_Mensagem_Pessoa_create:

                    // Mensagem_Pessoa_edit
                    if (preg_match('#^/admin/Mensagem/Pessoa/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Pessoa_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::editAction',));
                    }

                    // Mensagem_Pessoa_update
                    if (preg_match('#^/admin/Mensagem/Pessoa/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Mensagem_Pessoa_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Pessoa_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::updateAction',));
                    }
                    not_Mensagem_Pessoa_update:

                    // Mensagem_Pessoa_delete
                    if (preg_match('#^/admin/Mensagem/Pessoa/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Mensagem_Pessoa_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_Pessoa_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemPessoaController::deleteAction',));
                    }
                    not_Mensagem_Pessoa_delete:

                }

                if (0 === strpos($pathinfo, '/admin/Mensagem/EmailMarketing')) {
                    // Mensagem_EmailMarketing
                    if (rtrim($pathinfo, '/') === '/admin/Mensagem/EmailMarketing') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Mensagem_EmailMarketing');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::indexAction',  '_route' => 'Mensagem_EmailMarketing',);
                    }

                    // Mensagem_EmailMarketing_show
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_EmailMarketing_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::showAction',));
                    }

                    // Mensagem_EmailMarketing_new
                    if ($pathinfo === '/admin/Mensagem/EmailMarketing/new') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::newAction',  '_route' => 'Mensagem_EmailMarketing_new',);
                    }

                    // Mensagem_EmailMarketing_create
                    if ($pathinfo === '/admin/Mensagem/EmailMarketing/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Mensagem_EmailMarketing_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::createAction',  '_route' => 'Mensagem_EmailMarketing_create',);
                    }
                    not_Mensagem_EmailMarketing_create:

                    // Mensagem_EmailMarketing_edit
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_EmailMarketing_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::editAction',));
                    }

                    // Mensagem_EmailMarketing_update
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Mensagem_EmailMarketing_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_EmailMarketing_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::updateAction',));
                    }
                    not_Mensagem_EmailMarketing_update:

                    // Mensagem_EmailMarketing_delete
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Mensagem_EmailMarketing_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_EmailMarketing_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemEmailMarketingController::deleteAction',));
                    }
                    not_Mensagem_EmailMarketing_delete:

                }

                // Mensagem
                if (rtrim($pathinfo, '/') === '/admin/Mensagem') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Mensagem');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::indexAction',  '_route' => 'Mensagem',);
                }

                // Mensagem_show
                if (0 === strpos($pathinfo, '/admin/Mensagem/visualizar') && preg_match('#^/admin/Mensagem/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::showAction',));
                }

                // Mensagem_new
                if (preg_match('#^/admin/Mensagem/(?P<idPessoa>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::newAction',));
                }

                // Mensagem_create
                if (preg_match('#^/admin/Mensagem/(?P<idPessoa>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Mensagem_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::createAction',));
                }
                not_Mensagem_create:

                if (0 === strpos($pathinfo, '/admin/Mensagem/EmailMarketing')) {
                    // Mensagem_email_new
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<idEmailMarketing>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_email_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::newEmailMarketingAction',));
                    }

                    // Mensagem_email_create
                    if (preg_match('#^/admin/Mensagem/EmailMarketing/(?P<idEmailMarketing>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Mensagem_email_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_email_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::createEmailMarketingAction',));
                    }
                    not_Mensagem_email_create:

                }

                // Mensagem_tag_new
                if ($pathinfo === '/admin/Mensagem/novo') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::newTagAction',  '_route' => 'Mensagem_tag_new',);
                }

                // Mensagem_tag_create
                if ($pathinfo === '/admin/Mensagem/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Mensagem_tag_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::createTagAction',  '_route' => 'Mensagem_tag_create',);
                }
                not_Mensagem_tag_create:

                // Mensagem_edit
                if (0 === strpos($pathinfo, '/admin/Mensagem/editar') && preg_match('#^/admin/Mensagem/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::editAction',));
                }

                // Mensagem_update
                if (0 === strpos($pathinfo, '/admin/Mensagem/alterar') && preg_match('#^/admin/Mensagem/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Mensagem_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Mensagem_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\MensagemController::updateAction',));
                }
                not_Mensagem_update:

            }

            if (0 === strpos($pathinfo, '/admin/Pessoa/Tag')) {
                // Pessoa_Tag
                if (rtrim($pathinfo, '/') === '/admin/Pessoa/Tag') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Pessoa_Tag');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::indexAction',  '_route' => 'Pessoa_Tag',);
                }

                // Pessoa_Tag_show
                if (preg_match('#^/admin/Pessoa/Tag/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Tag_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::showAction',));
                }

                // Pessoa_Tag_new
                if ($pathinfo === '/admin/Pessoa/Tag/new') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::newAction',  '_route' => 'Pessoa_Tag_new',);
                }

                // Pessoa_Tag_create
                if ($pathinfo === '/admin/Pessoa/Tag/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Pessoa_Tag_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::createAction',  '_route' => 'Pessoa_Tag_create',);
                }
                not_Pessoa_Tag_create:

                // Pessoa_Tag_Save_By_Pessoa
                if ($pathinfo === '/admin/Pessoa/Tag/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Pessoa_Tag_Save_By_Pessoa;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::createByPessoaAction',  '_route' => 'Pessoa_Tag_Save_By_Pessoa',);
                }
                not_Pessoa_Tag_Save_By_Pessoa:

                // Pessoa_Tag_edit
                if (preg_match('#^/admin/Pessoa/Tag/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Tag_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::editAction',));
                }

                // Pessoa_Tag_update
                if (preg_match('#^/admin/Pessoa/Tag/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Pessoa_Tag_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Tag_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::updateAction',));
                }
                not_Pessoa_Tag_update:

                // Pessoa_Tag_delete
                if (preg_match('#^/admin/Pessoa/Tag/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Pessoa_Tag_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Tag_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::deleteAction',));
                }
                not_Pessoa_Tag_delete:

                // Pessoa_Tag_Delete_ajax
                if ($pathinfo === '/admin/Pessoa/Tag/Excluir/Tag') {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Pessoa_Tag_Delete_ajax;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaTagController::excluirTagAction',  '_route' => 'Pessoa_Tag_Delete_ajax',);
                }
                not_Pessoa_Tag_Delete_ajax:

            }

            if (0 === strpos($pathinfo, '/admin/Ta')) {
                if (0 === strpos($pathinfo, '/admin/TabelaPreco')) {
                    // Tabela_Preco
                    if (rtrim($pathinfo, '/') === '/admin/TabelaPreco') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Tabela_Preco');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::indexAction',  '_route' => 'Tabela_Preco',);
                    }

                    // Tabela_Preco_show
                    if (0 === strpos($pathinfo, '/admin/TabelaPreco/visualizar') && preg_match('#^/admin/TabelaPreco/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tabela_Preco_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::showAction',));
                    }

                    // Tabela_Preco_new
                    if ($pathinfo === '/admin/TabelaPreco/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::newAction',  '_route' => 'Tabela_Preco_new',);
                    }

                    // Tabela_Preco_create
                    if ($pathinfo === '/admin/TabelaPreco/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Tabela_Preco_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::createAction',  '_route' => 'Tabela_Preco_create',);
                    }
                    not_Tabela_Preco_create:

                    // Tabela_Preco_edit
                    if (0 === strpos($pathinfo, '/admin/TabelaPreco/editar') && preg_match('#^/admin/TabelaPreco/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tabela_Preco_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::editAction',));
                    }

                    // Tabela_Preco_update
                    if (0 === strpos($pathinfo, '/admin/TabelaPreco/alterar') && preg_match('#^/admin/TabelaPreco/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Tabela_Preco_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tabela_Preco_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TabelaPrecoController::updateAction',));
                    }
                    not_Tabela_Preco_update:

                }

                if (0 === strpos($pathinfo, '/admin/Tag')) {
                    // Tag
                    if (rtrim($pathinfo, '/') === '/admin/Tag') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Tag');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::indexAction',  '_route' => 'Tag',);
                    }

                    // Tag_show
                    if (0 === strpos($pathinfo, '/admin/Tag/visualizar') && preg_match('#^/admin/Tag/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tag_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::showAction',));
                    }

                    // Tag_new
                    if ($pathinfo === '/admin/Tag/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::newAction',  '_route' => 'Tag_new',);
                    }

                    // Tag_create
                    if ($pathinfo === '/admin/Tag/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Tag_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::createAction',  '_route' => 'Tag_create',);
                    }
                    not_Tag_create:

                    // Tag_edit
                    if (0 === strpos($pathinfo, '/admin/Tag/editar') && preg_match('#^/admin/Tag/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tag_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::editAction',));
                    }

                    // Tag_update
                    if (0 === strpos($pathinfo, '/admin/Tag/alterar') && preg_match('#^/admin/Tag/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Tag_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Tag_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\TagController::updateAction',));
                    }
                    not_Tag_update:

                }

            }

            if (0 === strpos($pathinfo, '/admin/Remessa')) {
                // Remessa_Bancaria
                if (rtrim($pathinfo, '/') === '/admin/Remessa') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Remessa_Bancaria');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::indexAction',  '_route' => 'Remessa_Bancaria',);
                }

                // Remessa_Bancaria_show
                if (0 === strpos($pathinfo, '/admin/Remessa/visualizar') && preg_match('#^/admin/Remessa/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Remessa_Bancaria_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::showAction',));
                }

                // Remessa_Bancaria_new
                if ($pathinfo === '/admin/Remessa/novo') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::newAction',  '_route' => 'Remessa_Bancaria_new',);
                }

                // Remessa_Bancaria_create
                if ($pathinfo === '/admin/Remessa/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Remessa_Bancaria_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::createAction',  '_route' => 'Remessa_Bancaria_create',);
                }
                not_Remessa_Bancaria_create:

                // Remessa_Bancaria_edit
                if (0 === strpos($pathinfo, '/admin/Remessa/editar') && preg_match('#^/admin/Remessa/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Remessa_Bancaria_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::editAction',));
                }

                // Remessa_Bancaria_update
                if (0 === strpos($pathinfo, '/admin/Remessa/alterar') && preg_match('#^/admin/Remessa/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Remessa_Bancaria_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Remessa_Bancaria_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::updateAction',));
                }
                not_Remessa_Bancaria_update:

                // Remessa_Bancaria_delete
                if (0 === strpos($pathinfo, '/admin/Remessa/excluir') && preg_match('#^/admin/Remessa/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Remessa_Bancaria_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Remessa_Bancaria_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\RemessaController::deleteAction',));
                }
                not_Remessa_Bancaria_delete:

            }

            if (0 === strpos($pathinfo, '/admin/P')) {
                if (0 === strpos($pathinfo, '/admin/Pagina')) {
                    // Pagina
                    if (rtrim($pathinfo, '/') === '/admin/Pagina') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Pagina');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::indexAction',  '_route' => 'Pagina',);
                    }

                    // Pagina_show
                    if (0 === strpos($pathinfo, '/admin/Pagina/visualizar') && preg_match('#^/admin/Pagina/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::showAction',));
                    }

                    // Pagina_new
                    if ($pathinfo === '/admin/Pagina/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::newAction',  '_route' => 'Pagina_new',);
                    }

                    // Pagina_create
                    if ($pathinfo === '/admin/Pagina/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Pagina_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::createAction',  '_route' => 'Pagina_create',);
                    }
                    not_Pagina_create:

                    // Pagina_edit
                    if (0 === strpos($pathinfo, '/admin/Pagina/editar') && preg_match('#^/admin/Pagina/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::editAction',));
                    }

                    // Pagina_update
                    if (0 === strpos($pathinfo, '/admin/Pagina/alterar') && preg_match('#^/admin/Pagina/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Pagina_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::updateAction',));
                    }
                    not_Pagina_update:

                    if (0 === strpos($pathinfo, '/admin/Pagina/Pagina')) {
                        // Pagina_filho_new
                        if (preg_match('#^/admin/Pagina/Pagina\\-(?P<idPaginaPai>[^/]++)/novo$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_filho_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::newFilhoAction',));
                        }

                        // Pagina_filho_create
                        if (preg_match('#^/admin/Pagina/Pagina\\-(?P<idPaginaPai>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_Pagina_filho_create;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_filho_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::createFilhoAction',));
                        }
                        not_Pagina_filho_create:

                        // Pagina_filho_edit
                        if (preg_match('#^/admin/Pagina/Pagina\\-(?P<idPaginaPai>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_filho_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::editFilhoAction',));
                        }

                        // Pagina_filho_update
                        if (preg_match('#^/admin/Pagina/Pagina\\-(?P<idPaginaPai>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_Pagina_filho_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_filho_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::updateFilhoAction',));
                        }
                        not_Pagina_filho_update:

                    }

                    // Pagina_delete
                    if (0 === strpos($pathinfo, '/admin/Pagina/excluir') && preg_match('#^/admin/Pagina/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Pagina_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pagina_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PaginaController::deleteAction',));
                    }
                    not_Pagina_delete:

                }

                if (0 === strpos($pathinfo, '/admin/Pessoa')) {
                    if (0 === strpos($pathinfo, '/admin/Pessoa/Juridica')) {
                        // Pessoa_Juridica
                        if (rtrim($pathinfo, '/') === '/admin/Pessoa/Juridica') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Pessoa_Juridica');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::indexAction',  '_route' => 'Pessoa_Juridica',);
                        }

                        // Pessoa_Juridica_show
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Juridica/visualizar') && preg_match('#^/admin/Pessoa/Juridica/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Juridica_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::showAction',));
                        }

                        // Pessoa_Juridica_new
                        if ($pathinfo === '/admin/Pessoa/Juridica/novo') {
                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::newAction',  '_route' => 'Pessoa_Juridica_new',);
                        }

                        // Pessoa_Juridica_create
                        if ($pathinfo === '/admin/Pessoa/Juridica/salvar') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_Pessoa_Juridica_create;
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::createAction',  '_route' => 'Pessoa_Juridica_create',);
                        }
                        not_Pessoa_Juridica_create:

                        // Pessoa_Juridica_edit
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Juridica/editar') && preg_match('#^/admin/Pessoa/Juridica/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Juridica_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::editAction',));
                        }

                        // Pessoa_Juridica_update
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Juridica/alterar') && preg_match('#^/admin/Pessoa/Juridica/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_Pessoa_Juridica_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Juridica_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::updateAction',));
                        }
                        not_Pessoa_Juridica_update:

                        // Pessoa_Juridica_delete
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Juridica/excluir') && preg_match('#^/admin/Pessoa/Juridica/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                                $allow = array_merge($allow, array('POST', 'DELETE'));
                                goto not_Pessoa_Juridica_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Juridica_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaJuridicaController::deleteAction',));
                        }
                        not_Pessoa_Juridica_delete:

                    }

                    if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica')) {
                        // Pessoa_Fisica
                        if (rtrim($pathinfo, '/') === '/admin/Pessoa/Fisica') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Pessoa_Fisica');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::indexAction',  '_route' => 'Pessoa_Fisica',);
                        }

                        // Pessoa_Fisica_show
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/visualizar') && preg_match('#^/admin/Pessoa/Fisica/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::showAction',));
                        }

                        if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/e')) {
                            if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/email')) {
                                // Pessoa_Fisica_new
                                if (preg_match('#^/admin/Pessoa/Fisica/email/(?P<idEmailMarketing>[^/]++)/novo$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::newAction',));
                                }

                                // Pessoa_Fisica_create
                                if (preg_match('#^/admin/Pessoa/Fisica/email/(?P<idEmailMarketing>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                                    if ($this->context->getMethod() != 'POST') {
                                        $allow[] = 'POST';
                                        goto not_Pessoa_Fisica_create;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::createAction',));
                                }
                                not_Pessoa_Fisica_create:

                            }

                            // Pessoa_Fisica_edit
                            if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/editar') && preg_match('#^/admin/Pessoa/Fisica/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::editAction',));
                            }

                        }

                        // Pessoa_Fisica_update
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/alterar') && preg_match('#^/admin/Pessoa/Fisica/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_Pessoa_Fisica_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::updateAction',));
                        }
                        not_Pessoa_Fisica_update:

                        // Pessoa_Fisica_delete
                        if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/excluir') && preg_match('#^/admin/Pessoa/Fisica/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Fisica_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::deleteAction',));
                        }

                        if (0 === strpos($pathinfo, '/admin/Pessoa/Fisica/AtualizaPessoa')) {
                            // Pessoa_Fisica_Atualiza_EmailMarketing
                            if (rtrim($pathinfo, '/') === '/admin/Pessoa/Fisica/AtualizaPessoa') {
                                if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                                    goto not_Pessoa_Fisica_Atualiza_EmailMarketing;
                                }

                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Pessoa_Fisica_Atualiza_EmailMarketing');
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::AtualizaPessoaByCPFByIdEmailMarketingAction',  '_route' => 'Pessoa_Fisica_Atualiza_EmailMarketing',);
                            }
                            not_Pessoa_Fisica_Atualiza_EmailMarketing:

                            // Pessoa_Fisica_Atualiza_EmailMarketing_Admin
                            if (rtrim($pathinfo, '/') === '/admin/Pessoa/Fisica/AtualizaPessoaForAdmin') {
                                if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                                    goto not_Pessoa_Fisica_Atualiza_EmailMarketing_Admin;
                                }

                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Pessoa_Fisica_Atualiza_EmailMarketing_Admin');
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::AtualizaPessoaByCPFByIdEmailMarketingForAdminAction',  '_route' => 'Pessoa_Fisica_Atualiza_EmailMarketing_Admin',);
                            }
                            not_Pessoa_Fisica_Atualiza_EmailMarketing_Admin:

                        }

                        // Pessoa_Fisica_List_Faturas
                        if ($pathinfo === '/admin/Pessoa/Fisica/Faturas/list') {
                            if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                                goto not_Pessoa_Fisica_List_Faturas;
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::returnFaturaByEmailAction',  '_route' => 'Pessoa_Fisica_List_Faturas',);
                        }
                        not_Pessoa_Fisica_List_Faturas:

                        // Pessoa_Fisica_Dica_Senha
                        if (rtrim($pathinfo, '/') === '/admin/Pessoa/Fisica/DicaSenha') {
                            if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                                goto not_Pessoa_Fisica_Dica_Senha;
                            }

                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Pessoa_Fisica_Dica_Senha');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaFisicaController::dicaSenhaAction',  '_route' => 'Pessoa_Fisica_Dica_Senha',);
                        }
                        not_Pessoa_Fisica_Dica_Senha:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/Usuario')) {
                // Usuario
                if (rtrim($pathinfo, '/') === '/admin/Usuario') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Usuario');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::indexAction',  '_route' => 'Usuario',);
                }

                // Usuario_show
                if (0 === strpos($pathinfo, '/admin/Usuario/visualizar') && preg_match('#^/admin/Usuario/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Usuario_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::showAction',));
                }

                // Usuario_new
                if ($pathinfo === '/admin/Usuario/novo') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::newAction',  '_route' => 'Usuario_new',);
                }

                // Usuario_create
                if ($pathinfo === '/admin/Usuario/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Usuario_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::createAction',  '_route' => 'Usuario_create',);
                }
                not_Usuario_create:

                // Usuario_edit
                if (0 === strpos($pathinfo, '/admin/Usuario/editar') && preg_match('#^/admin/Usuario/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Usuario_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::editAction',));
                }

                // Usuario_update
                if (0 === strpos($pathinfo, '/admin/Usuario/alterar') && preg_match('#^/admin/Usuario/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Usuario_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Usuario_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::updateAction',));
                }
                not_Usuario_update:

                // Usuario_delete
                if (0 === strpos($pathinfo, '/admin/Usuario/excluir') && preg_match('#^/admin/Usuario/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Usuario_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Usuario_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\UsuarioController::deleteAction',));
                }
                not_Usuario_delete:

            }

            if (0 === strpos($pathinfo, '/admin/Pessoa/Endereco')) {
                // Pessoa_Endereco
                if (rtrim($pathinfo, '/') === '/admin/Pessoa/Endereco') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Pessoa_Endereco');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::indexAction',  '_route' => 'Pessoa_Endereco',);
                }

                if (0 === strpos($pathinfo, '/admin/Pessoa/Endereco/pessoa')) {
                    // Pessoa_Endereco_show
                    if (preg_match('#^/admin/Pessoa/Endereco/pessoa/(?P<idPessoa>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::showAction',));
                    }

                    // Pessoa_Endereco_new
                    if (preg_match('#^/admin/Pessoa/Endereco/pessoa/(?P<idPessoa>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::newAction',));
                    }

                    // Pessoa_Endereco_create
                    if (preg_match('#^/admin/Pessoa/Endereco/pessoa/(?P<idPessoa>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Pessoa_Endereco_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::createAction',));
                    }
                    not_Pessoa_Endereco_create:

                    // Pessoa_Endereco_edit
                    if (preg_match('#^/admin/Pessoa/Endereco/pessoa/(?P<idPessoa>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::editAction',));
                    }

                    // Pessoa_Endereco_update
                    if (preg_match('#^/admin/Pessoa/Endereco/pessoa/(?P<idPessoa>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Pessoa_Endereco_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::updateAction',));
                    }
                    not_Pessoa_Endereco_update:

                }

                // Pessoa_Endereco_delete
                if (0 === strpos($pathinfo, '/admin/Pessoa/Endereco/excluir') && preg_match('#^/admin/Pessoa/Endereco/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Pessoa_Endereco_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Pessoa_Endereco_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::deleteAction',));
                }
                not_Pessoa_Endereco_delete:

                if (0 === strpos($pathinfo, '/admin/Pessoa/Endereco/get')) {
                    // Pessoa_Endereco_ajax
                    if (rtrim($pathinfo, '/') === '/admin/Pessoa/Endereco/getCEP') {
                        if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                            goto not_Pessoa_Endereco_ajax;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Pessoa_Endereco_ajax');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::getCEPAction',  '_route' => 'Pessoa_Endereco_ajax',);
                    }
                    not_Pessoa_Endereco_ajax:

                    // Pessoa_Endereco_uf_ajax
                    if (rtrim($pathinfo, '/') === '/admin/Pessoa/Endereco/getUF') {
                        if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                            goto not_Pessoa_Endereco_uf_ajax;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Pessoa_Endereco_uf_ajax');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::getUFAction',  '_route' => 'Pessoa_Endereco_uf_ajax',);
                    }
                    not_Pessoa_Endereco_uf_ajax:

                    // Pessoa_Endereco_cidades_ajax
                    if (rtrim($pathinfo, '/') === '/admin/Pessoa/Endereco/getCidades') {
                        if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                            goto not_Pessoa_Endereco_cidades_ajax;
                        }

                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Pessoa_Endereco_cidades_ajax');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\PessoaEnderecoController::getCidadesAction',  '_route' => 'Pessoa_Endereco_cidades_ajax',);
                    }
                    not_Pessoa_Endereco_cidades_ajax:

                }

            }

            if (0 === strpos($pathinfo, '/admin/Contrato/Fatura')) {
                // Contrato_Fatura
                if (rtrim($pathinfo, '/') === '/admin/Contrato/Fatura') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Contrato_Fatura');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::indexAction',  '_route' => 'Contrato_Fatura',);
                }

                // Contrato_Fatura_show
                if (0 === strpos($pathinfo, '/admin/Contrato/Fatura/visualizar') && preg_match('#^/admin/Contrato/Fatura/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::showAction',));
                }

                if (0 === strpos($pathinfo, '/admin/Contrato/Fatura/Contrato')) {
                    // Contrato_Fatura_new
                    if (preg_match('#^/admin/Contrato/Fatura/Contrato/(?P<idContrato>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::newAction',));
                    }

                    // Contrato_Fatura_create
                    if (preg_match('#^/admin/Contrato/Fatura/Contrato/(?P<idContrato>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Contrato_Fatura_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::createAction',));
                    }
                    not_Contrato_Fatura_create:

                }

                // Contrato_Fatura_edit
                if (0 === strpos($pathinfo, '/admin/Contrato/Fatura/editar') && preg_match('#^/admin/Contrato/Fatura/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::editAction',));
                }

                // Contrato_Fatura_update
                if (0 === strpos($pathinfo, '/admin/Contrato/Fatura/alterar') && preg_match('#^/admin/Contrato/Fatura/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Contrato_Fatura_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::updateAction',));
                }
                not_Contrato_Fatura_update:

                // Contrato_Fatura_delete
                if (0 === strpos($pathinfo, '/admin/Contrato/Fatura/excluir') && preg_match('#^/admin/Contrato/Fatura/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Contrato_Fatura_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_Fatura_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\FaturaController::deleteAction',));
                }
                not_Contrato_Fatura_delete:

            }

            if (0 === strpos($pathinfo, '/admin/Nota/Contrato')) {
                // ContratoNota
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::indexAction',));
                }

                // ContratoNota_show
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::showAction',));
                }

                // ContratoNota_new
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::newAction',));
                }

                // ContratoNota_create
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_ContratoNota_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::createAction',));
                }
                not_ContratoNota_create:

                // ContratoNota_edit
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::editAction',));
                }

                // ContratoNota_update
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_ContratoNota_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::updateAction',));
                }
                not_ContratoNota_update:

                // ContratoNota_delete
                if (preg_match('#^/admin/Nota/Contrato/(?P<idContrato>[^/]++)/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'DELETE', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'DELETE', 'HEAD'));
                        goto not_ContratoNota_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ContratoNota_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoNotaController::deleteAction',));
                }
                not_ContratoNota_delete:

            }

            if (0 === strpos($pathinfo, '/admin/EmailMarketing')) {
                // EmailMarketing
                if (rtrim($pathinfo, '/') === '/admin/EmailMarketing') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'EmailMarketing');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::indexAction',  '_route' => 'EmailMarketing',);
                }

                // EmailMarketing_show
                if (0 === strpos($pathinfo, '/admin/EmailMarketing/visualizar') && preg_match('#^/admin/EmailMarketing/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::showAction',));
                }

                // EmailMarketing_new
                if ($pathinfo === '/admin/EmailMarketing/novo') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::newAction',  '_route' => 'EmailMarketing_new',);
                }

                // EmailMarketing_create
                if ($pathinfo === '/admin/EmailMarketing/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_EmailMarketing_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::createAction',  '_route' => 'EmailMarketing_create',);
                }
                not_EmailMarketing_create:

                // EmailMarketing_edit
                if (0 === strpos($pathinfo, '/admin/EmailMarketing/editar') && preg_match('#^/admin/EmailMarketing/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::editAction',));
                }

                // EmailMarketing_update
                if (0 === strpos($pathinfo, '/admin/EmailMarketing/alterar') && preg_match('#^/admin/EmailMarketing/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_EmailMarketing_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::updateAction',));
                }
                not_EmailMarketing_update:

                if (0 === strpos($pathinfo, '/admin/EmailMarketing/Pessoa')) {
                    // EmailMarketing_edit_email
                    if (0 === strpos($pathinfo, '/admin/EmailMarketing/Pessoa/editar') && preg_match('#^/admin/EmailMarketing/Pessoa/editar\\-(?P<idPessoa>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_edit_email')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::editEmailAction',));
                    }

                    // EmailMarketing_update_email
                    if (0 === strpos($pathinfo, '/admin/EmailMarketing/Pessoa/alterar') && preg_match('#^/admin/EmailMarketing/Pessoa/alterar\\-(?P<idPessoa>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_EmailMarketing_update_email;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_update_email')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::updateEmailAction',));
                    }
                    not_EmailMarketing_update_email:

                }

                // EmailMarketing_delete
                if (0 === strpos($pathinfo, '/admin/EmailMarketing/excluir') && preg_match('#^/admin/EmailMarketing/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_EmailMarketing_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'EmailMarketing_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\EmailMarketingController::deleteAction',));
                }
                not_EmailMarketing_delete:

            }

            if (0 === strpos($pathinfo, '/admin/Disciplina/curso')) {
                // Disciplina
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Disciplina');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::indexAction',));
                }

                // Disciplina_show
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::showAction',));
                }

                // Disciplina_new
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/novo/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Disciplina_new');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::newAction',));
                }

                // Disciplina_create
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Disciplina_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::createAction',));
                }
                not_Disciplina_create:

                // Disciplina_edit
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::editAction',));
                }

                // Disciplina_update
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Disciplina_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::updateAction',));
                }
                not_Disciplina_update:

                // Disciplina_delete
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::deleteAction',));
                }

                // Disciplina_restaurar
                if (preg_match('#^/admin/Disciplina/curso/(?P<idCurso>[^/]++)/restaurar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_restaurar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\DisciplinaController::restaurarAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/Curso/I')) {
                if (0 === strpos($pathinfo, '/admin/Curso/Imagem/curso')) {
                    // Curso_Imagem
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Curso_Imagem');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::indexAction',));
                    }

                    // Curso_Imagem_show
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::showAction',));
                    }

                    // Curso_Imagem_new
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/new$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::newAction',));
                    }

                    // Curso_Imagem_create
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/create$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Curso_Imagem_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::createAction',));
                    }
                    not_Curso_Imagem_create:

                    // Curso_Imagem_edit
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::editAction',));
                    }

                    // Curso_Imagem_update
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Curso_Imagem_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::updateAction',));
                    }
                    not_Curso_Imagem_update:

                    // Curso_Imagem_delete
                    if (preg_match('#^/admin/Curso/Imagem/curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Curso_Imagem_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Imagem_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoImagemController::deleteAction',));
                    }
                    not_Curso_Imagem_delete:

                }

                if (0 === strpos($pathinfo, '/admin/Curso/Item/curso')) {
                    // Curso_Item
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/?$#s', $pathinfo, $matches)) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Curso_Item');
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::indexAction',));
                    }

                    // Curso_Item_show
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::showAction',));
                    }

                    // Curso_Item_new
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::newAction',));
                    }

                    // Curso_Item_create
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Curso_Item_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::createAction',));
                    }
                    not_Curso_Item_create:

                    // Curso_Item_edit
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::editAction',));
                    }

                    // Curso_Item_update
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/atualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Curso_Item_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::updateAction',));
                    }
                    not_Curso_Item_update:

                    // Curso_Item_delete
                    if (preg_match('#^/admin/Curso/Item/curso/(?P<idCurso>[^/]++)/delete\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Curso_Item_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Item_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoItemController::deleteAction',));
                    }
                    not_Curso_Item_delete:

                }

            }

            if (0 === strpos($pathinfo, '/admin/P')) {
                if (0 === strpos($pathinfo, '/admin/Produto')) {
                    // Produto
                    if (rtrim($pathinfo, '/') === '/admin/Produto') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Produto');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::indexAction',  '_route' => 'Produto',);
                    }

                    // Produto_show
                    if (0 === strpos($pathinfo, '/admin/Produto/visualizar') && preg_match('#^/admin/Produto/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Produto_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::showAction',));
                    }

                    // Produto_new
                    if ($pathinfo === '/admin/Produto/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::newAction',  '_route' => 'Produto_new',);
                    }

                    // Produto_create
                    if ($pathinfo === '/admin/Produto/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Produto_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::createAction',  '_route' => 'Produto_create',);
                    }
                    not_Produto_create:

                    // Produto_edit
                    if (0 === strpos($pathinfo, '/admin/Produto/editar') && preg_match('#^/admin/Produto/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Produto_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::editAction',));
                    }

                    // Produto_update
                    if (0 === strpos($pathinfo, '/admin/Produto/alterar') && preg_match('#^/admin/Produto/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Produto_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Produto_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::updateAction',));
                    }
                    not_Produto_update:

                    // Produto_delete
                    if (0 === strpos($pathinfo, '/admin/Produto/excluir') && preg_match('#^/admin/Produto/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Produto_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Produto_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ProdutoController::deleteAction',));
                    }
                    not_Produto_delete:

                }

                if (0 === strpos($pathinfo, '/admin/Pacote')) {
                    if (0 === strpos($pathinfo, '/admin/Pacote/Curso')) {
                        if (0 === strpos($pathinfo, '/admin/Pacote/Curso/pacote')) {
                            // Curso_Ofertado_Ligacao
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/?$#s', $pathinfo, $matches)) {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Curso_Ofertado_Ligacao');
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::indexAction',));
                            }

                            // Curso_Ofertado_Ligacao_show
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::showAction',));
                            }

                            // Curso_Ofertado_Ligacao_new
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/novo$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::newAction',));
                            }

                            // Curso_Ofertado_Ligacao_create
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_Curso_Ofertado_Ligacao_create;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::createAction',));
                            }
                            not_Curso_Ofertado_Ligacao_create:

                            // Curso_Ofertado_Ligacao_edit
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::editAction',));
                            }

                            // Curso_Ofertado_Ligacao_update
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                    $allow = array_merge($allow, array('POST', 'PUT'));
                                    goto not_Curso_Ofertado_Ligacao_update;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::updateAction',));
                            }
                            not_Curso_Ofertado_Ligacao_update:

                            // Curso_Ofertado_Ligacao_delete
                            if (preg_match('#^/admin/Pacote/Curso/pacote/(?P<idCursoOfertado>[^/]++)/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                                    $allow = array_merge($allow, array('POST', 'DELETE'));
                                    goto not_Curso_Ofertado_Ligacao_delete;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Ligacao_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoLigacaoController::deleteAction',));
                            }
                            not_Curso_Ofertado_Ligacao_delete:

                        }

                        if (0 === strpos($pathinfo, '/admin/Pacote/Curso/Evento')) {
                            // Curso_Ofertado_Evento
                            if (rtrim($pathinfo, '/') === '/admin/Pacote/Curso/Evento') {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Curso_Ofertado_Evento');
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::indexAction',  '_route' => 'Curso_Ofertado_Evento',);
                            }

                            // Curso_Ofertado_Evento_show
                            if (0 === strpos($pathinfo, '/admin/Pacote/Curso/Evento/visualizar') && preg_match('#^/admin/Pacote/Curso/Evento/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Evento_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::showAction',));
                            }

                            // Curso_Ofertado_Evento_new
                            if ($pathinfo === '/admin/Pacote/Curso/Evento/novo') {
                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::newAction',  '_route' => 'Curso_Ofertado_Evento_new',);
                            }

                            // Curso_Ofertado_Evento_create
                            if ($pathinfo === '/admin/Pacote/Curso/Evento/salvar') {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_Curso_Ofertado_Evento_create;
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::createAction',  '_route' => 'Curso_Ofertado_Evento_create',);
                            }
                            not_Curso_Ofertado_Evento_create:

                            // Curso_Ofertado_Evento_edit
                            if (0 === strpos($pathinfo, '/admin/Pacote/Curso/Evento/editar') && preg_match('#^/admin/Pacote/Curso/Evento/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Evento_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::editAction',));
                            }

                            // Curso_Ofertado_Evento_update
                            if (0 === strpos($pathinfo, '/admin/Pacote/Curso/Evento/alterar') && preg_match('#^/admin/Pacote/Curso/Evento/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                    $allow = array_merge($allow, array('POST', 'PUT'));
                                    goto not_Curso_Ofertado_Evento_update;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Evento_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::updateAction',));
                            }
                            not_Curso_Ofertado_Evento_update:

                            // Curso_Ofertado_Evento_delete
                            if (0 === strpos($pathinfo, '/admin/Pacote/Curso/Evento/excluir') && preg_match('#^/admin/Pacote/Curso/Evento/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                                    $allow = array_merge($allow, array('POST', 'DELETE'));
                                    goto not_Curso_Ofertado_Evento_delete;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_Evento_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoEventoController::deleteAction',));
                            }
                            not_Curso_Ofertado_Evento_delete:

                        }

                    }

                    // Curso_Ofertado
                    if (rtrim($pathinfo, '/') === '/admin/Pacote') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Curso_Ofertado');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::indexAction',  '_route' => 'Curso_Ofertado',);
                    }

                    // Curso_Ofertado_show
                    if (0 === strpos($pathinfo, '/admin/Pacote/visualizar') && preg_match('#^/admin/Pacote/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::showAction',));
                    }

                    // Curso_Ofertado_new
                    if ($pathinfo === '/admin/Pacote/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::newAction',  '_route' => 'Curso_Ofertado_new',);
                    }

                    // Curso_Ofertado_create
                    if ($pathinfo === '/admin/Pacote/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Curso_Ofertado_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::createAction',  '_route' => 'Curso_Ofertado_create',);
                    }
                    not_Curso_Ofertado_create:

                    // Curso_Ofertado_edit
                    if (0 === strpos($pathinfo, '/admin/Pacote/editar') && preg_match('#^/admin/Pacote/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::editAction',));
                    }

                    // Curso_Ofertado_update
                    if (0 === strpos($pathinfo, '/admin/Pacote/alterar') && preg_match('#^/admin/Pacote/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Curso_Ofertado_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::updateAction',));
                    }
                    not_Curso_Ofertado_update:

                    // Curso_Ofertado_delete
                    if (0 === strpos($pathinfo, '/admin/Pacote/excluir') && preg_match('#^/admin/Pacote/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Curso_Ofertado_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Ofertado_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoOfertadoController::deleteAction',));
                    }
                    not_Curso_Ofertado_delete:

                }

            }

            if (0 === strpos($pathinfo, '/admin/C')) {
                if (0 === strpos($pathinfo, '/admin/Curso')) {
                    if (0 === strpos($pathinfo, '/admin/Curso/Categoria')) {
                        // Curso_Categoria
                        if (rtrim($pathinfo, '/') === '/admin/Curso/Categoria') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Curso_Categoria');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::indexAction',  '_route' => 'Curso_Categoria',);
                        }

                        // Curso_Categoria_show
                        if (0 === strpos($pathinfo, '/admin/Curso/Categoria/visualizar') && preg_match('#^/admin/Curso/Categoria/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Categoria_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::showAction',));
                        }

                        // Curso_Categoria_new
                        if ($pathinfo === '/admin/Curso/Categoria/novo') {
                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::newAction',  '_route' => 'Curso_Categoria_new',);
                        }

                        // Curso_Categoria_create
                        if ($pathinfo === '/admin/Curso/Categoria/salvar') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_Curso_Categoria_create;
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::createAction',  '_route' => 'Curso_Categoria_create',);
                        }
                        not_Curso_Categoria_create:

                        // Curso_Categoria_edit
                        if (0 === strpos($pathinfo, '/admin/Curso/Categoria/editar') && preg_match('#^/admin/Curso/Categoria/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Categoria_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::editAction',));
                        }

                        // Curso_Categoria_update
                        if (0 === strpos($pathinfo, '/admin/Curso/Categoria/alterar') && preg_match('#^/admin/Curso/Categoria/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_Curso_Categoria_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Categoria_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::updateAction',));
                        }
                        not_Curso_Categoria_update:

                        // Curso_Categoria_delete
                        if (0 === strpos($pathinfo, '/admin/Curso/Categoria/excluir') && preg_match('#^/admin/Curso/Categoria/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                                $allow = array_merge($allow, array('POST', 'DELETE'));
                                goto not_Curso_Categoria_delete;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_Categoria_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoCategoriaController::deleteAction',));
                        }
                        not_Curso_Categoria_delete:

                    }

                    // Curso
                    if (rtrim($pathinfo, '/') === '/admin/Curso') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'Curso');
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::indexAction',  '_route' => 'Curso',);
                    }

                    // Curso_show
                    if (0 === strpos($pathinfo, '/admin/Curso/visualizar') && preg_match('#^/admin/Curso/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::showAction',));
                    }

                    // Curso_new
                    if ($pathinfo === '/admin/Curso/novo') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::newAction',  '_route' => 'Curso_new',);
                    }

                    // Curso_create
                    if ($pathinfo === '/admin/Curso/salvar') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_Curso_create;
                        }

                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::createAction',  '_route' => 'Curso_create',);
                    }
                    not_Curso_create:

                    // Curso_edit
                    if (0 === strpos($pathinfo, '/admin/Curso/editar') && preg_match('#^/admin/Curso/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::editAction',));
                    }

                    // Curso_update
                    if (0 === strpos($pathinfo, '/admin/Curso/alterar') && preg_match('#^/admin/Curso/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_Curso_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::updateAction',));
                    }
                    not_Curso_update:

                    // Curso_delete
                    if (0 === strpos($pathinfo, '/admin/Curso/excluir') && preg_match('#^/admin/Curso/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_Curso_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Curso_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::deleteAction',));
                    }
                    not_Curso_delete:

                    // Curso_cron_reorder
                    if ($pathinfo === '/admin/Curso/reordenar-todos-os-cursos') {
                        return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CursoController::reordenarCursosAction',  '_route' => 'Curso_cron_reorder',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/Contrato')) {
                    if (0 === strpos($pathinfo, '/admin/Contrato/pessoa')) {
                        // Contrato_new
                        if (preg_match('#^/admin/Contrato/pessoa/(?P<idPessoa>[^/]++)/novo$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::newAction',));
                        }

                        // Contrato_create
                        if (preg_match('#^/admin/Contrato/pessoa/(?P<idPessoa>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_Contrato_create;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::createAction',));
                        }
                        not_Contrato_create:

                        // Contrato_edit
                        if (0 === strpos($pathinfo, '/admin/Contrato/pessoa/editar') && preg_match('#^/admin/Contrato/pessoa/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::editAction',));
                        }

                    }

                    // Contrato_delete
                    if (0 === strpos($pathinfo, '/admin/Contrato/delete') && preg_match('#^/admin/Contrato/delete\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::deleteAction',));
                    }

                    if (0 === strpos($pathinfo, '/admin/Contrato/pessoa')) {
                        // Contrato_update
                        if (0 === strpos($pathinfo, '/admin/Contrato/pessoa/alterar') && preg_match('#^/admin/Contrato/pessoa/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_Contrato_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::updateAction',));
                        }
                        not_Contrato_update:

                        // Contrato_material_didatico
                        if (preg_match('#^/admin/Contrato/pessoa/(?P<idPessoa>[^/]++)/material\\-didatico\\-(?P<idFatura>[^/\\-]++)\\-(?P<tipo>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Contrato_material_didatico')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ContratoController::atualizarFaturaAction',));
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/Cep')) {
                    if (0 === strpos($pathinfo, '/admin/Cep/E')) {
                        if (0 === strpos($pathinfo, '/admin/Cep/Endereco')) {
                            // Cep_Endereco
                            if (rtrim($pathinfo, '/') === '/admin/Cep/Endereco') {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Cep_Endereco');
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrEnderecoController::indexAction',  '_route' => 'Cep_Endereco',);
                            }

                            // Cep_Endereco_show
                            if (0 === strpos($pathinfo, '/admin/Cep/Endereco/visualizar') && preg_match('#^/admin/Cep/Endereco/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Cep_Endereco_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrEnderecoController::showAction',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/Cep/Estado')) {
                            // Cep_Estado
                            if (rtrim($pathinfo, '/') === '/admin/Cep/Estado') {
                                if (substr($pathinfo, -1) !== '/') {
                                    return $this->redirect($pathinfo.'/', 'Cep_Estado');
                                }

                                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrEstadoController::indexAction',  '_route' => 'Cep_Estado',);
                            }

                            // Cep_Estado_show
                            if (0 === strpos($pathinfo, '/admin/Cep/Estado/visualizar') && preg_match('#^/admin/Cep/Estado/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Cep_Estado_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrEstadoController::showAction',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/Cep/Cidade')) {
                        // Cep_Cidade
                        if (rtrim($pathinfo, '/') === '/admin/Cep/Cidade') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Cep_Cidade');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrCidadeController::indexAction',  '_route' => 'Cep_Cidade',);
                        }

                        // Cep_Cidade_show
                        if (0 === strpos($pathinfo, '/admin/Cep/Cidade/visualizar') && preg_match('#^/admin/Cep/Cidade/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Cep_Cidade_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrCidadeController::showAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/Cep/Bairro')) {
                        // Cep_Bairro
                        if (rtrim($pathinfo, '/') === '/admin/Cep/Bairro') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'Cep_Bairro');
                            }

                            return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrBairroController::indexAction',  '_route' => 'Cep_Bairro',);
                        }

                        // Cep_Bairro_show
                        if (0 === strpos($pathinfo, '/admin/Cep/Bairro/visualizar') && preg_match('#^/admin/Cep/Bairro/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'Cep_Bairro_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\CepbrBairroController::showAction',));
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/Disciplina/Aula/curso')) {
                // Disciplina_Aula
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/?$#s', $pathinfo, $matches)) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Disciplina_Aula');
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::indexAction',));
                }

                // Disciplina_Aula_show
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::showAction',));
                }

                // Disciplina_Aula_new
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::newAction',));
                }

                // Disciplina_Aula_create
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Disciplina_Aula_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::createAction',));
                }
                not_Disciplina_Aula_create:

                // Disciplina_Aula_edit
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::editAction',));
                }

                // Disciplina_Aula_update
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Disciplina_Aula_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::updateAction',));
                }
                not_Disciplina_Aula_update:

                // Disciplina_Aula_delete
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::deleteAction',));
                }

                // Disciplina_Aula_restaurar
                if (preg_match('#^/admin/Disciplina/Aula/curso/(?P<idCurso>[^/]++)/disciplina/(?P<idDisciplina>[^/]++)/restaurar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Disciplina_Aula_restaurar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaController::restaurarAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/Nucleo')) {
                // Nucleo
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::indexAction',));
                }

                // Nucleo_show
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::showAction',));
                }

                // Nucleo_new
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::newAction',));
                }

                // Nucleo_create
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Nucleo_create;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::createAction',));
                }
                not_Nucleo_create:

                // Nucleo_edit
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::editAction',));
                }

                // Nucleo_update
                if (preg_match('#^/admin/Nucleo/(?P<idPessoa>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Nucleo_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Nucleo_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoController::updateAction',));
                }
                not_Nucleo_update:

                if (0 === strpos($pathinfo, '/admin/Nucleo/Pessoa')) {
                    // NucleoPessoa
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::indexAction',));
                    }

                    // NucleoPessoa_show
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::showAction',));
                    }

                    // NucleoPessoa_new
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_new')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::newAction',));
                    }

                    // NucleoPessoa_create
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_NucleoPessoa_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_create')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::createAction',));
                    }
                    not_NucleoPessoa_create:

                    // NucleoPessoa_edit
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::editAction',));
                    }

                    // NucleoPessoa_update
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_NucleoPessoa_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::updateAction',));
                    }
                    not_NucleoPessoa_update:

                    // NucleoPessoa_delete
                    if (preg_match('#^/admin/Nucleo/Pessoa/(?P<idPessoa>[^/]++)/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'DELETE', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'DELETE', 'HEAD'));
                            goto not_NucleoPessoa_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'NucleoPessoa_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NucleoPessoaController::deleteAction',));
                    }
                    not_NucleoPessoa_delete:

                }

            }

            if (0 === strpos($pathinfo, '/admin/Assunto')) {
                // Assunto
                if (rtrim($pathinfo, '/') === '/admin/Assunto') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'Assunto');
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::indexAction',  '_route' => 'Assunto',);
                }

                // Assunto_show
                if (0 === strpos($pathinfo, '/admin/Assunto/visualizar') && preg_match('#^/admin/Assunto/visualizar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Assunto_show')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::showAction',));
                }

                // Assunto_new
                if ($pathinfo === '/admin/Assunto/novo') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::newAction',  '_route' => 'Assunto_new',);
                }

                // Assunto_create
                if ($pathinfo === '/admin/Assunto/salvar') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_Assunto_create;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::createAction',  '_route' => 'Assunto_create',);
                }
                not_Assunto_create:

                // Assunto_edit
                if (0 === strpos($pathinfo, '/admin/Assunto/editar') && preg_match('#^/admin/Assunto/editar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Assunto_edit')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::editAction',));
                }

                // Assunto_update
                if (0 === strpos($pathinfo, '/admin/Assunto/alterar') && preg_match('#^/admin/Assunto/alterar\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_Assunto_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Assunto_update')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::updateAction',));
                }
                not_Assunto_update:

                // Assunto_delete
                if (0 === strpos($pathinfo, '/admin/Assunto/excluir') && preg_match('#^/admin/Assunto/excluir\\-(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_Assunto_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'Assunto_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AssuntoController::deleteAction',));
                }
                not_Assunto_delete:

            }

            // admin_index
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_index');
                }

                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::homeAction',  '_route' => 'admin_index',);
            }

            // admin_teste
            if ($pathinfo === '/admin/teste') {
                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::testeAction',  '_route' => 'admin_teste',);
            }

            // admin_home
            if (rtrim($pathinfo, '/') === '/admin/Home') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_home');
                }

                return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::homeAction',  '_route' => 'admin_home',);
            }

            if (0 === strpos($pathinfo, '/admin/delete')) {
                // admin_delete_message
                if ($pathinfo === '/admin/delete') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::deleteAction',  '_route' => 'admin_delete_message',);
                }

                // admin_delete_locaweb_message
                if ($pathinfo === '/admin/delete/emails/recusados') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::deleteEmailsRecusadosAction',  '_route' => 'admin_delete_locaweb_message',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/log')) {
                // admin_login
                if ($pathinfo === '/admin/login') {
                    if (!in_array($this->context->getMethod(), array('POST', 'GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('POST', 'GET', 'HEAD'));
                        goto not_admin_login;
                    }

                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::loginAction',  '_route' => 'admin_login',);
                }
                not_admin_login:

                // admin_logout
                if ($pathinfo === '/admin/logout') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::logoutAction',  '_route' => 'admin_logout',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/EmailMarketing')) {
                // admin_list_email_delete
                if ($pathinfo === '/admin/EmailMarketing/listEmailDelete') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::listEmailDeleteAction',  '_route' => 'admin_list_email_delete',);
                }

                // admin_delete_email
                if ($pathinfo === '/admin/EmailMarketing/DeleteEmails') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AdminController::deleteEmailAction',  '_route' => 'admin_delete_email',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/Aula/Acesso/Contrato')) {
                // admin_aula_acesso_novo
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_acesso_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::novoAction',));
                }
                not_admin_aula_acesso_novo:

                // admin_aula_acesso_atualizar
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_acesso_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::updateAction',));
                }
                not_admin_aula_acesso_atualizar:

                // admin_aula_acesso_listar
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_acesso_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::listAction',));
                }
                not_admin_aula_acesso_listar:

                // admin_aula_acesso_visualizar
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_aula_acesso_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::viewAction',));
                }
                not_admin_aula_acesso_visualizar:

                // admin_aula_acesso_delete
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_aula_acesso_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::deleteAction',));
                }
                not_admin_aula_acesso_delete:

                // admin_aula_acesso_delete_selecionado
                if (preg_match('#^/admin/Aula/Acesso/Contrato/(?P<idContrato>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_aula_acesso_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_acesso_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::deleteSelecionadosAction',));
                }
                not_admin_aula_acesso_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/Questao/Aula')) {
                // admin_aula_questao_novo
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_questao_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::novoAction',));
                }
                not_admin_aula_questao_novo:

                // admin_aula_questao_atualizar
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_questao_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::updateAction',));
                }
                not_admin_aula_questao_atualizar:

                // admin_aula_questao_listar
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_aula_questao_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::listAction',));
                }
                not_admin_aula_questao_listar:

                // admin_aula_questao_visualizar
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_aula_questao_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::viewAction',));
                }
                not_admin_aula_questao_visualizar:

                // admin_aula_questao_delete
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_aula_questao_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::deleteAction',));
                }
                not_admin_aula_questao_delete:

                // admin_aula_questao_delete_selecionado
                if (preg_match('#^/admin/Questao/Aula/(?P<idAula>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_aula_questao_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoController::deleteSelecionadosAction',));
                }
                not_admin_aula_questao_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/A')) {
                if (0 === strpos($pathinfo, '/admin/Alternativa/Questao')) {
                    // admin_aula_questao_alternativa_novo
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_novo;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::novoAction',));
                    }
                    not_admin_aula_questao_alternativa_novo:

                    // admin_aula_questao_alternativa_atualizar
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_atualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::updateAction',));
                    }
                    not_admin_aula_questao_alternativa_atualizar:

                    // admin_aula_questao_alternativa_listar
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/listar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_listar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::listAction',));
                    }
                    not_admin_aula_questao_alternativa_listar:

                    // admin_aula_questao_alternativa_visualizar
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_visualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::viewAction',));
                    }
                    not_admin_aula_questao_alternativa_visualizar:

                    // admin_aula_questao_alternativa_delete
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::deleteAction',));
                    }
                    not_admin_aula_questao_alternativa_delete:

                    // admin_aula_questao_alternativa_delete_selecionado
                    if (preg_match('#^/admin/Alternativa/Questao/(?P<idQuestao>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_admin_aula_questao_alternativa_delete_selecionado;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaController::deleteSelecionadosAction',));
                    }
                    not_admin_aula_questao_alternativa_delete_selecionado:

                }

                if (0 === strpos($pathinfo, '/admin/Avaliacao/Contrato')) {
                    // admin_aula_questao_alternativa_contrato_novo
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/novo$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_contrato_novo;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::novoAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_novo:

                    // admin_aula_questao_alternativa_contrato_atualizar
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_contrato_atualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::updateAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_atualizar:

                    // admin_aula_questao_alternativa_contrato_listar
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/listar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_contrato_listar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::listAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_listar:

                    // admin_aula_questao_alternativa_contrato_visualizar
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_contrato_visualizar;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::viewAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_visualizar:

                    // admin_aula_questao_alternativa_contrato_delete
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_admin_aula_questao_alternativa_contrato_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::deleteAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_delete:

                    // admin_aula_questao_alternativa_contrato_delete_selecionado
                    if (preg_match('#^/admin/Avaliacao/Contrato/(?P<idContrato>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_admin_aula_questao_alternativa_contrato_delete_selecionado;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_aula_questao_alternativa_contrato_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaQuestaoAlternativaContratoController::deleteSelecionadosAction',));
                    }
                    not_admin_aula_questao_alternativa_contrato_delete_selecionado:

                }

            }

            if (0 === strpos($pathinfo, '/admin/Item/Forum')) {
                // admin_curso_forum_item_novo
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_item_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::novoAction',));
                }
                not_admin_curso_forum_item_novo:

                // admin_curso_forum_item_atualizar
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_item_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::updateAction',));
                }
                not_admin_curso_forum_item_atualizar:

                // admin_curso_forum_item_listar
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_item_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::listAction',));
                }
                not_admin_curso_forum_item_listar:

                // admin_curso_forum_item_visualizar
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_forum_item_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::viewAction',));
                }
                not_admin_curso_forum_item_visualizar:

                // admin_curso_forum_item_delete
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_forum_item_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::deleteAction',));
                }
                not_admin_curso_forum_item_delete:

                // admin_curso_forum_item_delete_selecionado
                if (preg_match('#^/admin/Item/Forum/(?P<idForum>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_curso_forum_item_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_item_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumItemController::deleteSelecionadosAction',));
                }
                not_admin_curso_forum_item_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/Forum/Curso')) {
                // admin_curso_forum_novo
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::novoAction',));
                }
                not_admin_curso_forum_novo:

                // admin_curso_forum_atualizar
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::updateAction',));
                }
                not_admin_curso_forum_atualizar:

                // admin_curso_forum_listar
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_forum_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::listAction',));
                }
                not_admin_curso_forum_listar:

                // admin_curso_forum_visualizar
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_forum_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::viewAction',));
                }
                not_admin_curso_forum_visualizar:

                // admin_curso_forum_delete
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_forum_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::deleteAction',));
                }
                not_admin_curso_forum_delete:

                // admin_curso_forum_delete_selecionado
                if (preg_match('#^/admin/Forum/Curso/(?P<idCurso>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_curso_forum_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_forum_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\ForumController::deleteSelecionadosAction',));
                }
                not_admin_curso_forum_delete_selecionado:

            }

            if (0 === strpos($pathinfo, '/admin/Notificacao/Curso')) {
                // admin_curso_notificacao_novo
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/novo$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_notificacao_novo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_novo')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::novoAction',));
                }
                not_admin_curso_notificacao_novo:

                // admin_curso_notificacao_atualizar
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/atualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_notificacao_atualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_atualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::updateAction',));
                }
                not_admin_curso_notificacao_atualizar:

                // admin_curso_notificacao_listar
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/listar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_admin_curso_notificacao_listar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_listar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::listAction',));
                }
                not_admin_curso_notificacao_listar:

                // admin_curso_notificacao_visualizar
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/visualizar$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_notificacao_visualizar;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_visualizar')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::viewAction',));
                }
                not_admin_curso_notificacao_visualizar:

                // admin_curso_notificacao_delete
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_admin_curso_notificacao_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_delete')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::deleteAction',));
                }
                not_admin_curso_notificacao_delete:

                // admin_curso_notificacao_delete_selecionado
                if (preg_match('#^/admin/Notificacao/Curso/(?P<idCurso>[^/]++)/delete/selecionados$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_admin_curso_notificacao_delete_selecionado;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_curso_notificacao_delete_selecionado')), array (  '_controller' => 'Admin\\AdminBundle\\Controller\\NotificacaoController::deleteSelecionadosAction',));
                }
                not_admin_curso_notificacao_delete_selecionado:

            }

        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }

        if (0 === strpos($pathinfo, '/aula')) {
            if (0 === strpos($pathinfo, '/aula/a')) {
                // ajax_aula_acesso
                if ($pathinfo === '/aula/acesso/salvar') {
                    return array (  '_controller' => 'Admin\\AdminBundle\\Controller\\AulaAcessoController::ajaxAcessoAction',  '_route' => 'ajax_aula_acesso',);
                }

                // ajax_anotacao
                if (0 === strpos($pathinfo, '/aula/anotacao') && preg_match('#^/aula/anotacao/(?P<idContrato>[^/]++)/(?P<idAula>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajax_anotacao')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\AjaxController::anotacaoAction',));
                }

            }

            if (0 === strpos($pathinfo, '/aula/forum')) {
                // ajax_forum_post
                if (preg_match('#^/aula/forum/(?P<idContrato>[^/]++)/salvar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajax_forum_post')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\AjaxController::postForumAction',));
                }

                // ajax_forum_resposta
                if (preg_match('#^/aula/forum/(?P<idContrato>[^/]++)/resposta$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajax_forum_resposta')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\AjaxController::postForumRespostaAction',));
                }

                // ajax_forum_list
                if (preg_match('#^/aula/forum/(?P<idCurso>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ajax_forum_list')), array (  '_controller' => 'Usuario\\AmbienteAlunoBundle\\Controller\\AjaxController::listForumAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
