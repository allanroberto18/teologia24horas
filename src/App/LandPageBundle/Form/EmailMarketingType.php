<?php

namespace App\LandPageBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Admin\AdminBundle\Form\EmailMarketingType as Type;

class EmailMarketingType extends Type {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email', 'text', array('label' => 'E-mail', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o E-mail')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
    }

    /**
     * @return string
     */
    public function getName() {
        return 'dados';
    }

}
