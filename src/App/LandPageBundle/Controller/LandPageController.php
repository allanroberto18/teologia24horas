<?php

namespace App\LandPageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\Pessoa;
use App\LandPageBundle\Form\EmailMarketingType;
use Symfony\Component\HttpFoundation\Request;



class LandPageController extends Controller {

    /**
     * @Route("/salvar", name="land_page_primeira_etapa_salvar")
     * @Template("LandPageBundle:LandPage:landpage1.html.twig")
     * @Method("POST")
     */
    public function createAction(Request $request) {
        $entity = new EmailMarketing();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("email" => $entity->getEmail()));

            if (!$repository instanceof EmailMarketing) {
                $em->persist($entity);

                $em->flush();

                $tag = $this->container->get('admin.tags');
                $tag->persistEmailMarketingTag($entity, 1);

                $repository = $entity;
            }
            
            $pessoa = $em->getRepository("AdminBundle:Pessoa")->findBy(array('idEmailMarketing' => $repository->getId()));
            if (count($pessoa) > 0) {
                $session = $this->get('session');
                
                $confirm = $em->getRepository("AdminBundle:EmailMarketingTag")->findOneBy(array('idEmailMarketing' => $repository->getId(), 'idTag' => 2));
                if (empty($confirm)) {
                    $session->getFlashBag()->set('info', 'Uma mensagem foi enviada para sua caixa postal, confirme seu e-mail, para ter acesso ao ambiente do aluno');
                    $this->container->get('admin.email')->alteracaoEmail($pessoa[0]);
                } else {
                    $session->getFlashBag()->set('info', 'O e-mail informado já está cadastrado, entre na Área Exclusiva e acesse o Lembrete de Senha');
                }
                return $this->redirect($this->generateUrl('home_homepage'));
            }
            
            $message = $this->container->get('admin.email');
            $message->confirmarCadastro($repository);

            return $this->redirect($this->generateUrl('land_page_segunda_etapa'));
        }

        return [
            'titulo' => 'Salvando Cadastro',
            'entity' => $entity,
            'form' => $form->createView(),
            'errors' => $errors
        ];
    }

    private function createCreateForm(EmailMarketing $entity) {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('land_page_primeira_etapa_salvar'),
            'method' => 'POST',
            'attr' => array(
                //'class' => 'form-inline',
                'novalidate' => 'novalidate'
            )
        ));

        $form->add('submit', 'submit', array('label' => "INSCREVA-SE AQUI!", 'attr' => array('class' => 'btn_cadastro form-control')));

        return $form;
    }

    /**
     * @Route("/", name="land_page_primeira_etapa")
     * @Template("LandPageBundle:LandPage:landpage2.html.twig")
     * @Method("GET")
     */
    public function primeiraEtapaAction() {
        $entity = new EmailMarketing();
        $form = $this->createCreateForm($entity);

        return [
            'titulo' => 'Bem vindo à Plataforma Teologia24Horas',
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => ''
        ];
    }

    /**
     * @Route("/confirmar-email", name="land_page_segunda_etapa")
     * @Template("LandPageBundle:LandPage:landpage2.html.twig")
     * @Method("GET")
     */
    public function segundaEtapaAction() {
        return [
            'titulo' => 'Verifique a sua caixa postal'
        ];
    }

    /**
     * @Route("/email-confirmado/{ide}", name="land_page_terceira_etapa")
     * @Template("LandPageBundle:LandPage:landpage3.html.twig")
     * @Method("GET")
     */
    public function terceiraEtapaAction($ide) {
        if (empty($ide)) {
            return $this->redirect($this->generateUrl('land_page_primeira_etapa'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:EmailMarketing')->findOneBy(array('identificador' => $ide));

        if (!$entity) {
            throw $this->createNotFoundException('Usuário não localizado');
        }

        $container = $this->container->get('admin.tags');
        $container->persistEmailMarketingTag($entity, 2);

        $mensagem = $this->container->get('admin.email');
        $mensagem->cadastroConfirmado($entity);

        return [
            'titulo' => 'Seu e-mail foi confirmado com Sucesso',
            'entity' => $entity,
        ];
    }

    /**
     * @Route("/remover-email/{identificador}", name="land_page_solicitar_excluir_email")
     * @Template("LandPageBundle:LandPage:solicitarExclusao.html.twig")
     * @Method("GET")
     */
    public function solicitarExclusaoAction($identificador) {
        if (empty($identificador)) {
            return $this->redirect($this->generateUrl("home_homepage"));
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("identificador" => $identificador));

        if (!$repository instanceof EmailMarketing) {
            return $this->redirect($this->generateUrl("home_homepage"));
        }

        return [
            'titulo' => 'Remover e-mail da lista de contatos',
            'entity' => $repository
        ];
    }

    /**
     * @Route("/excluir-email/{identificador}", name="land_page_excluir_email")
     * @Method("GET")
     */
    public function excluirDaListaAction($identificador) {
        if (empty($identificador)) {
            return $this->redirect($this->generateUrl("home_homepage"));
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("identificador" => $identificador));
        if (!$repository instanceof EmailMarketing) {
            return $this->redirect($this->generateUrl("home_homepage"));
        }
        $repository->setStatus(2);
        $em->flush();

        return $this->redirect($this->generateUrl("home_homepage"));
    }

}
