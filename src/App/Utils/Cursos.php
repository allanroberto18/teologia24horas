<?php

namespace App\Utils;

use Doctrine\ORM\EntityManager;

class Cursos
{

    private $entityManager;

    private $titulo;
    private $categoria;

    function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function returnCursos(array $params)
    {
        $params = $this->prepareParams($params);
        $repository = $this->entityManager->getRepository('AdminBundle:Curso');
        $consult = $repository->createQueryBuilder('item');
        $consult->where('item.status = :status');

        $i = 0;
        while ($i < count($params))
        {
            if ($params[$i]['value'])
            {
                $params[$i]["key"] == "idCursoCategoria" ?  $consult->andWhere("item.{$params[$i]['key']} = :{$params[$i]['key']}") : $consult->andWhere($consult->expr()->like("item.{$params[$i]['key']}", ":{$params[$i]['key']}"));
            }
            $i++;
        }
        $j = 0;

        $consult->setParameter('status', 1);
        while($j < count($params))
        {
            if ($params[$j]['value'])
            {
                $params[$j]["key"] == "idCursoCategoria" ? $consult->setParameter("{$params[$j]['key']}", $params[$j]['value']) : $consult->setParameter("{$params[$j]['key']}", "%" . $params[$j]['value'] . "%");
            }
            $j++;
        }

        return $consult->getQuery()->getResult();
    }

    public function prepareParams(array $params)
    {
        if (array_key_exists('titulo', $params))
        {
            $this->setTitulo($params['titulo']);
        }
        if (array_key_exists('categoria', $params))
        {
            $this->setCategoria($params['categoria']);
        }

        return [
            0 => [
                'key' => 'titulo',
                'value' => $this->getTitulo(),
            ],
            1 => [
                'key' => 'idCursoCategoria',
                'value' => $this->getCategoria(),
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = empty($titulo) ? false : $titulo;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = empty($categoria) ? false : $categoria;
    }
}