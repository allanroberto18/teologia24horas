<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 03/11/2015
 * Time: 10:18
 */

namespace App\Utils;

use Doctrine\ORM\EntityManager;

class Paginas
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function returnPaginas()
    {
        return [
            'quem_somos' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(1),
            'como_funciona' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(2),
            'perguntas_frequentes' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(4),
            'certificacao' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(5),
            'metodologia' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(6),
            'programa_afiliados' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(7),
            'rede_network' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(8),
            'grupos_estudos' => $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pagina')->find(9),
        ];
    }


}