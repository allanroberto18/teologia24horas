<?php

namespace App\HomeBundle\Controller;

use Admin\AdminBundle\Entity\Pagina;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Admin\AdminBundle\lib\CursoConsult;
use Admin\AdminBundle\Entity\EmailMarketing;
use App\LandPageBundle\Form\EmailMarketingType;

class HomeController extends Controller {

    /**
     * @Route("/Home/{id}/{slug}", name="front_pagina")
     * @Template("HomeBundle:Paginas:pagina.html.twig")
     * @Method("GET")
     */
    public function paginaAction($id, $slug) {

        $pagina = $this->getDoctrine()->getRepository('AdminBundle:Pagina')->find($id);

        if (!$pagina instanceof Pagina)
        {
            $this->addFlash('error', 'Página não localizada');
            return $this->redirectToRoute('home_homepage');
        }

        return [
            'titulo' => $pagina->getTitulo(),
            'conteudo' => $pagina,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/", name="home_homepage")
     * @Template("HomeBundle:Home:index.html.twig")
     * @Method({"GET", "POST"})
     */
    public function homeAction(Request $request)
    {
        $this->get('session')->clear();

        if (empty($this->get('session')->get('cursos'))) {
            $session = new Session();
            $em = $this->getDoctrine()->getManager();
            $cursoOfertado = new CursoConsult();
            $cursos = $cursoOfertado->getCursosBySlider($em);
            $session->set('cursos', $cursos);
        }

        $result = false;
        $entity = new EmailMarketing();
        $form = $this->createForm(new EmailMarketingType(), $entity);
        $form->add('submit', 'submit', array('label' => "INSCREVA-SE AQUI!", 'attr' => array('class' => 'btn_cadastro form-control')));

        $data = $request->request->all();
        if (!empty($data['dados']['email'])) {
            $email = $data['dados']['email'];

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("email" => $email));

            if (!$repository instanceof EmailMarketing) {
                $entity->setEmail($email);

                $em->persist($entity);
                $em->flush();

                $tag = $this->container->get('admin.tags');
                $tag->persistEmailMarketingTag($entity, 1);

                $repository = $entity;

            }

            $pessoa = $em->getRepository("AdminBundle:Pessoa")->findBy(array('idEmailMarketing' => $repository->getId()));
            if (count($pessoa) > 0) {
                $confirm = $em->getRepository("AdminBundle:EmailMarketingTag")->findOneBy(array('idEmailMarketing' => $repository->getId(), 'idTag' => 2));
                if (empty($confirm)) {
                    $error = [0 => 'Uma mensagem foi enviada para sua caixa postal, confirme seu e-mail, para ter acesso ao ambiente do aluno'];
                    $this->container->get('admin.email')->alteracaoEmail($pessoa[0]);
                } else {
                    $error = [0 => 'O e-mail informado já está cadastrado, entre na Área Exclusiva e acesse o Lembrete de Senha'];
                }
            }

            $result = true;

            $message = $this->container->get('admin.email');
            $message->confirmarCadastro($repository);
        }

        return array(
            'titulo' => 'Teologia24Horas: Página Inicial', 'attr' => 'Home',
            'categorias' => $this->getDoctrine()->getRepository('AdminBundle:CursoCategoria')->findAll(),
            'form' => $form->createView(),
            'result' => $result,
            'error' => empty($error) ? false : $error,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        );
    }


    private function createCreateForm(EmailMarketing $entity) {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('home_homepage'),
            'method' => 'POST',
//            'attr' => array(
//                //'class' => 'form-inline',
//                'novalidate' => 'novalidate'
//            )
        ));

        $form->add('submit', 'submit', array('label' => "INSCREVA-SE AQUI!", 'attr' => array('class' => 'btn_cadastro form-control')));

        return $form;
    }

    /**
     * @Route("/cursos", name="front_cursos")
     * @Template("HomeBundle:Home:list_cursos.html.twig")
     * @Method("POST")
     */
    public function cursosAction(Request $request)
    {
        $params = $request->request->all();

        $entity = new EmailMarketing();
        $form = $this->createCreateForm($entity);

        return [
            'titulo' => 'Teologia24Horas: Pesquisar Cursos',
            'cursos' => $this->container->get('front.cursos')->returnCursos($params),
            'categorias' => $this->getDoctrine()->getRepository('AdminBundle:CursoCategoria')->findAll(),
            'idCategoria' => '',
            'form' => $form->createView(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];

    }

    /**
     * @Route("/categoria/{idCategoria}/{slug}", name="front_categorias")
     * @Template("HomeBundle:Home:list_cursos.html.twig")
     * @Method({"GET", "POST"})
     */
    public function cursosByCategoriaAction($idCategoria, $slug)
    {
        if (empty($idCategoria))
        {
            return $this->redirectToRoute('home_homepage');
        }

        $params = [ 'categoria' => $idCategoria ];

        $entity = new EmailMarketing();
        $form = $this->createCreateForm($entity);

        return [
            'titulo' => 'Teologia24Horas: Cursos por Categoria',
            'cursos' => $this->container->get('front.cursos')->returnCursos($params),
            'idCategoria' => $idCategoria,
            'categorias' => $this->getDoctrine()->getRepository('AdminBundle:CursoCategoria')->findAll(),
            'form' => $form->createView(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/teste", name="front_teste")
     * @Template("HomeBundle:Home:teste.html.twig")
     * @Method({"GET", "POST"})
     */
    public function testeRequestAction(Request $request)
    {
        $dados = $request->request->all();

        return [
            'dados' => empty($dados) ? [ 0 => 'Aguardando registros'] : $dados,
        ];
    }
}
