<?php

namespace Usuario\AmbienteUsuarioBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsuarioControllerTest extends WebTestCase
{
    public function testGerenciarusuario()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/gerenciar');
    }

    public function testGerenciarendereco()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/endereco');
    }

    public function testGerenciardadosadicionais()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/adicional');
    }

    public function testGerenciareclesiastico()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/eclesiastico');
    }

    public function testGerenciarsenha()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/senha');
    }

    public function testGerenciarfoto()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/foto');
    }

}
