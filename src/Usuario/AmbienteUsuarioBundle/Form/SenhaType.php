<?php

namespace Usuario\AmbienteUsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SenhaType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('senha', 'password', array('label' => 'Senha: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Senha Para Acesso a Plataforma')))
            ->add('confirmaSenha', 'password', array('label' => 'Confirmar Senha: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir Confirmação de Senha')))
            ->add('dicaSenha', 'text', array('label' => 'Dica de Senha: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Dica de Senha')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Pessoa',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ava_usuario_perfil_senha';
    }

}
