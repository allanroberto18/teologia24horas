<?php

namespace Usuario\AmbienteUsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PerfilType2 extends AbstractType {

    private $dados;
    private $em;

    public function __construct($dados, $em) {
        $this->dados = $dados;
        $this->em = $em;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //->add('pfCpf', 'text', array('label' => 'CPF: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb cpf', 'placeholder' => 'Inserir o CPF do Aluno')))
                ->add('pfNome', 'text', array('label' => 'Nome Completo: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Nome do Aluno')))
                ->add('pfDataNascimento', 'date', array(
                    'label' => 'Data de Nascimento:', 'error_bubbling' => true, 'required' => true, 
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control mb datemask',
                        'placeholder' => 'Inserir a Data de Nascimento'
                    )
                ))
                ->add('pfSexo', 'choice', array('label' => 'Sexo:', 'error_bubbling' => true, 'required' => true, 
                    'choices' => array(
                        1 => 'Masculino ',
                        2 => 'Feminino ',
                    ),
                    'multiple' => false,
                    'expanded' => false,
                    'attr' => array(
                        'class' => 'mb'
                    ))
                )
                ->add('pais', 'choice', array('choices' => array(
                    "" => "[Selecione o Pais]",
                    "África do Sul" => "África do Sul",
                    "Albânia" => "Albânia",
                    "Alemanha" => "Alemanha",
                    "Andorra" => "Andorra",
                    "Angola" => "Angola",
                    "Anguilla" => "Anguilla",
                    "Antigua" => "Antigua",
                    "Arábia Saudita" => "Arábia Saudita",
                    "Argentina" => "Argentina",
                    "Armênia" => "Armênia",
                    "Aruba" => "Aruba",
                    "Austrália" => "Austrália",
                    "Áustria" => "Áustria",
                    "Azerbaijão" => "Azerbaijão",
                    "Bahamas" => "Bahamas",
                    "Bahrein" => "Bahrein",
                    "Bangladesh" => "Bangladesh",
                    "Barbados" => "Barbados",
                    "Bélgica" => "Bélgica",
                    "Benin" => "Benin",
                    "Bermudas" => "Bermudas",
                    "Botsuana" => "Botsuana",
                    "Brasil" => "Brasil",
                    "Brunei" => "Brunei",
                    "Bulgária" => "Bulgária",
                    "Burkina Fasso" => "Burkina Fasso",
                    "botão" => "botão",
                    "Cabo Verde" => "Cabo Verde",
                    "Camarões" => "Camarões",
                    "Camboja" => "Camboja",
                    "Canadá" => "Canadá",
                    "Cazaquistão" => "Cazaquistão",
                    "Chade" => "Chade",
                    "Chile" => "Chile",
                    "China" => "China",
                    "Cidade do Vaticano" => "Cidade do Vaticano",
                    "Colômbia" => "Colômbia",
                    "Congo" => "Congo",
                    "Coréia do Sul" => "Coréia do Sul",
                    "Costa do Marfim" => "Costa do Marfim",
                    "Costa Rica" => "Costa Rica",
                    "Croácia" => "Croácia",
                    "Dinamarca" => "Dinamarca",
                    "Djibuti" => "Djibuti",
                    "Dominica" => "Dominica",
                    "EUA" => "EUA",
                    "Egito" => "Egito",
                    "El Salvador" => "El Salvador",
                    "Emirados Árabes" => "Emirados Árabes",
                    "Equador" => "Equador",
                    "Eritréia" => "Eritréia",
                    "Escócia" => "Escócia",
                    "Eslováquia" => "Eslováquia",
                    "Eslovênia" => "Eslovênia",
                    "Espanha" => "Espanha",
                    "Estônia" => "Estônia",
                    "Etiópia" => "Etiópia",
                    "Fiji" => "Fiji",
                    "Filipinas" => "Filipinas",
                    "Finlândia" => "Finlândia",
                    "França" => "França",
                    "Gabão" => "Gabão",
                    "Gâmbia" => "Gâmbia",
                    "Gana" => "Gana",
                    "Geórgia" => "Geórgia",
                    "Gibraltar" => "Gibraltar",
                    "Granada" => "Granada",
                    "Grécia" => "Grécia",
                    "Guadalupe" => "Guadalupe",
                    "Guam" => "Guam",
                    "Guatemala" => "Guatemala",
                    "Guiana" => "Guiana",
                    "Guiana Francesa" => "Guiana Francesa",
                    "Guiné-bissau" => "Guiné-bissau",
                    "Haiti" => "Haiti",
                    "Holanda" => "Holanda",
                    "Honduras" => "Honduras",
                    "Hong Kong" => "Hong Kong",
                    "Hungria" => "Hungria",
                    "Iêmen" => "Iêmen",
                    "Ilhas Cayman" => "Ilhas Cayman",
                    "Ilhas Cook" => "Ilhas Cook",
                    "Ilhas Curaçao" => "Ilhas Curaçao",
                    "Ilhas Marshall" => "Ilhas Marshall",
                    "Ilhas Turks & Caicos" => "Ilhas Turks & Caicos",
                    "Ilhas Virgens (brit.)" => "Ilhas Virgens (brit.)",
                    "Ilhas Virgens(amer.)" => "Ilhas Virgens(amer.)",
                    "Ilhas Wallis e Futuna" => "Ilhas Wallis e Futuna",
                    "Índia" => "Índia",
                    "Indonésia" => "Indonésia",
                    "Inglaterra" => "Inglaterra",
                    "Irlanda" => "Irlanda",
                    "Islândia" => "Islândia",
                    "Israel" => "Israel",
                    "Itália" => "Itália",
                    "Jamaica" => "Jamaica",
                    "Japão" => "Japão",
                    "Jordânia" => "Jordânia",
                    "Kuwait" => "Kuwait",
                    "Latvia" => "Latvia",
                    "Líbano" => "Líbano",
                    "Liechtenstein" => "Liechtenstein",
                    "Lituânia" => "Lituânia",
                    "Luxemburgo" => "Luxemburgo",
                    "Macau" => "Macau",
                    "Macedônia" => "Macedônia",
                    "Madagascar" => "Madagascar",
                    "Malásia" => "Malásia",
                    "Malaui" => "Malaui",
                    "Mali" => "Mali",
                    "Malta" => "Malta",
                    "Marrocos" => "Marrocos",
                    "Martinica" => "Martinica",
                    "Mauritânia" => "Mauritânia",
                    "Mauritius" => "Mauritius",
                    "México" => "México",
                    "Moldova" => "Moldova",
                    "Mônaco" => "Mônaco",
                    "Montserrat" => "Montserrat",
                    "Nepal" => "Nepal",
                    "Nicarágua" => "Nicarágua",
                    "Niger" => "Niger",
                    "Nigéria" => "Nigéria",
                    "Noruega" => "Noruega",
                    "Nova Caledônia" => "Nova Caledônia",
                    "Nova Zelândia" => "Nova Zelândia",
                    "Omã" => "Omã",
                    "Palau" => "Palau",
                    "Panamá" => "Panamá",
                    "Papua-nova Guiné" => "Papua-nova Guiné",
                    "Paquistão" => "Paquistão",
                    "Peru" => "Peru",
                    "Polinésia Francesa" => "Polinésia Francesa",
                    "Polônia" => "Polônia",
                    "Porto Rico" => "Porto Rico",
                    "Portugal" => "Portugal",
                    "Qatar" => "Qatar",
                    "Quênia" => "Quênia",
                    "Rep. Dominicana" => "Rep. Dominicana",
                    "Rep. Tcheca" => "Rep. Tcheca",
                    "Reunion" => "Reunion",
                    "Romênia" => "Romênia",
                    "Ruanda" => "Ruanda",
                    "Rússia" => "Rússia",
                    "Saipan" => "Saipan",
                    "Samoa Americana" => "Samoa Americana",
                    "Senegal" => "Senegal",
                    "Serra Leone" => "Serra Leone",
                    "Seychelles" => "Seychelles",
                    "Singapura" => "Singapura",
                    "Síria" => "Síria",
                    "Sri Lanka" => "Sri Lanka",
                    "St. Kitts & Nevis" => "St. Kitts & Nevis",
                    "St. Lúcia" => "St. Lúcia",
                    "St. Vincent" => "St. Vincent",
                    "Sudão" => "Sudão",
                    "Suécia" => "Suécia",
                    "Suiça" => "Suiça",
                    "Suriname" => "Suriname",
                    "Tailândia" => "Tailândia",
                    "Taiwan" => "Taiwan",
                    "Tanzânia" => "Tanzânia",
                    "Togo" => "Togo",
                    "Trinidad & Tobago" => "Trinidad & Tobago",
                    "Tunísia" => "Tunísia",
                    "Turquia" => "Turquia",
                    "Ucrânia" => "Ucrânia",
                    "Uganda" => "Uganda",
                    "Uruguai" => "Uruguai",
                    "Venezuela" => "Venezuela",
                    "Vietnã" => "Vietnã",
                    "Zaire" => "Zaire",
                    "Zâmbia" => "Zâmbia",
                    "Zimbábue" => "Zimbábue",
                ), 'expanded' => false, 'multiple' => false, 'label' => 'País: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control pais mb', 'placeholder' => 'Inserir o Número')))
                ->add('estado', 'text', array('label' => 'Estado: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('id' => 'uf', 'class' => 'form-control mb uf')))
                ->add('cidade', 'text', array('label' => 'Cidade: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('id' => 'cidade', 'class' => 'form-control mb cidade')))
                ->add('celular', 'text', array('label' => 'Contato/Whatsapp: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control celular mb')))
                ->add('email', 'text', array('label' => 'E-mail: ', 'read_only' => true, 'data' => $this->dados['email'], 'error_bubbling' => true, 'required' => true,  'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o E-mail Para Acesso a Plataforma')))
                ->add('biografia', 'textarea', array('label' => 'Biografia: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Biografia', 'rows' => 5)))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Pessoa'
        ));
    }

    public function returnEstados()
    {
        $repository = $this->em->getRepository('\Admin\AdminBundle\Entity\CepbrEstado');
        $estados = $repository->createQueryBuilder('item')->orderBy('item.uf', 'ASC')->getQuery()->getResult();

        foreach ($estados as $estado) {
            if (!$estado instanceof \Admin\AdminBundle\Entity\CepbrEstado) {
                continue;
            }
            $resultSet[$estado->getUf()] = $estado->getEstado();
        }

        return $resultSet;
    }

    public function returnCidades()
    {
        $repository = $this->em->getRepository('\Admin\AdminBundle\Entity\CepbrCidade');
        $cidades = $repository->createQueryBuilder('item')->orderBy('item.cidade', 'ASC')->getQuery()->getResult();

        foreach ($cidades as $cidade) {
            if (!$cidade instanceof \Admin\AdminBundle\Entity\CepbrCidade) {
                continue;
            }
            $resultSet[$cidade->getCidade()] = $cidade->getCidade();
        }

        return $resultSet;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ava_perfil';
    }

}
