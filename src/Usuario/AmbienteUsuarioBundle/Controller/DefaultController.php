<?php

namespace Usuario\AmbienteUsuarioBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\lib\PessoaConsult;
use Admin\AdminBundle\Entity\EmailMarketing;

class DefaultController extends MainController {

    /**
     * @Route("/entrar-{ide}", name="ambiente_usuario_homepage")
     * @Method("GET")
     */
    public function indexAction($ide = null) {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:EmailMarketing')->findOneBy(array('identificador' => $ide));
        $session = $this->get('session');
        
        if (empty($ide)) {
            $session->getFlashBag()->set('info', "E-mail não localizado");
            return $this->redirect($this->generateUrl("home_homepage"));
        }
        if (!$entity instanceof EmailMarketing) {
            $session->getFlashBag()->set('info', "E-mail não localizado");
            return $this->redirect($this->generateUrl("home_homepage"));
        }

        $container = $this->container->get('admin.tags');
        $container->persistEmailMarketingTag($entity, 2);

        $login = $this->container->get('ava.login');
        $login->generateSessionByEmailMarketing($entity);
    }

    /**
     * @Route("/login", name="ambiente_usuario_homepage_gerenciar_login")
     * @Template("AmbienteUsuarioBundle:Default:login.html.twig")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request) {

        $login = $this->login();
        if (!empty($login)) {
            return $this->redirectHome();
        }

        $form = $request->request->all();

        $entity = new \Admin\AdminBundle\Entity\EmailMarketing();
        $formEmail = $this->createCreateForm($entity);

        if (!empty($form["submit"])) {
            $usuario = $this->container->get('ava.login');
            $usuario->setParams($form["email"], $form["senha"]);
            $usuario->efetuarLogin();
        }

        return [
            'titulo' => "Validar Usuário",
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'form' => $formEmail->createView(),
        ];
    }

    private function createCreateForm(\Admin\AdminBundle\Entity\EmailMarketing $entity) {
        $form = $this->createForm(new \App\LandPageBundle\Form\EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('land_page_primeira_etapa_salvar'),
            'method' => 'POST',
//            'attr' => array(
//                //'class' => 'form-inline',
//                'novalidate' => 'novalidate'
//            )
        ));

        $form->add('submit', 'submit', array('label' => "Inscreva-se aqui!", 'attr' => array('class' => 'btn btn-success btn-lg btn-block')));

        return $form;
    }

    /**
     * @Route("/logout", name="ambiente_usuario_homepage_gerenciar_logout")
     * @Method("GET")
     */
    public function logoutAction() {
        return $this->logout();
    }

    /**
     * @Route("/Home/", name="ambiente_usuario_homepage_home")
     * @Template("AmbienteUsuarioBundle:Default:index.html.twig")
     * @Method("GET")
     */
    public function homeAction() {
        $carrinho = $this->get('session')->get('carrinho');
        if (is_array($carrinho)) {
            return $this->redirect($this->generateUrl("ava_aluno_contratar_carrinho_compra", array('idCurso' => $carrinho['curso'], 'slug' => $carrinho['slug'])));
        }

        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home");

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Curso")->find(1);

        return [
            'titulo' => "Ambiente do Usuário::Principal",
            'attr' => 'home',
            'Curso' => $repository,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }
	
    
}
