<?php

namespace Usuario\AmbienteUsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller {

    public function login() {
        return $this->get('session')->get('ava');
    }

    public function redirectLogin() {
        header("location:/AVA/Usuario/login");
        exit();
    }

    public function redirectHome() {
        header("location:/AVA/Usuario/Home/");
        exit();
    }

    public function logout() {
        $this->get('session')->remove('ava');
        $this->get('session')->remove('cursos');
        $this->get('session')->remove('duracao');
        $this->get('session')->remove('hora_aula');
        $this->get('session')->remove('credito');
        $this->get('session')->remove('links');
        $this->get('session')->remove('aluno');
        $this->get('session')->remove('carrinho');
        $this->get('session')->remove('pedido');
        return $this->redirect($this->generateUrl('home_homepage'));
    }

    public function checkLogin($idCurso=null, $slug=null) {
        if (!empty($idCurso) && !empty($slug)) {
            $this->get('session')->set('carrinho', array('curso' => $idCurso, 'slug' => $slug));
        }
        $login = $this->login();

        if (empty($login)) {
            return $this->redirectLogin();
        }
    }
    
    public function checkLoginCarrinho($idCurso, $slug) {
        $login = $this->login();

        if (empty($login)) {
            return $this->redirectLogin();
        }
    }

}
