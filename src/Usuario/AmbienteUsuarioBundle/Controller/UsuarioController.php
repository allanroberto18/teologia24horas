<?php

namespace Usuario\AmbienteUsuarioBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Usuario\AmbienteUsuarioBundle\Form\PerfilType;
use Usuario\AmbienteUsuarioBundle\Form\PerfilType2;
use Usuario\AmbienteUsuarioBundle\Form\PerfilTypeNew;
use Usuario\AmbienteUsuarioBundle\Form\PerfilTypeNew2;
use Usuario\AmbienteUsuarioBundle\Form\SenhaType;
use Usuario\AmbienteUsuarioBundle\Form\FotoType;
use Usuario\AmbienteUsuarioBundle\Form\EnderecoType;

use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\PessoaEndereco;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Form\EmailMarketingType;

class UsuarioController extends MainController {

    public function checkPessoa() {
        $usuario = $this->login();

        if (empty($usuario['id'])) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Pessoa')->findOneBy(array('idEmailMarketing' => $usuario['id_email_marketing']));
            return $entity;
        }
        return $usuario;
    }

    /**
     * @Route("/Perfil/gerenciar", name="gerenciar_usuario")
     * @Method("GET")
     */
    public function gerenciarUsuarioAction() {
        $this->checkLogin();

        $entity = $this->checkPessoa();
        if ($entity instanceof Pessoa) {
            $id = $entity->getId();
            $em = $this->getDoctrine()->getManager();
            $pessoaConsult = new \Admin\AdminBundle\lib\PessoaConsult();
            $pessoaConsult->mountSessao($em, $entity);
        } else {
            $id = $entity['id'];
        }

        if (empty($entity)) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }
        return $this->redirect($this->generateUrl('gerenciar_usuario_editar', array('id' => $id)));
    }

    /**
     * @Route("/Perfil/usuario/novo", name="gerenciar_usuario_novo")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarUsuario.html.twig")
     * @Method("GET")
     */
    public function novoUsuarioAction()
    {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Cadastrar Usuário");

        $entity = new Pessoa();
        $form = $this->formUsuario($entity, $this->getDoctrine()->getManager());

        $login = $this->login();

        $pessoa = $this->checkPessoa();

        $session = new \Symfony\Component\HttpFoundation\Session\Session();

        if ($pessoa instanceof Pessoa) {
            $value = array();
            $value['id'] = $pessoa->getId();
            $value['nome'] = ($pessoa->getTipoPessoa() == 1) ? $pessoa->getPfNome() : $pessoa->getPjNomeFantasia();
            $value['id_email_marketing'] = $login['id_email_marketing'];
            $value['email'] = $pessoa->getEmail();
            $value['foto'] = ($pessoa->getPfSexo() == 1) ? 'user.png' : 'user_woman.png';

            $consult = new \Admin\AdminBundle\lib\PessoaConsult();
            $value['check'] = $consult->checkContratosForAmbiente($entity);

            $session->set('ava', $value);

            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        } else {
            $em = $this->getDoctrine()->getManager();
            $emailMarketing = $em->getRepository("AdminBundle:EmailMarketing")->find($login['id_email_marketing']);

            $message = $this->container->get('admin.email');
            $message->cadastroConfirmado($emailMarketing);
        }

        $cursoConsult = new \Admin\AdminBundle\lib\CursoConsult();
        $cursos = $cursoConsult->getCursosBySlider($em);

        $session->set("cursos", $cursos);

        return [
            'titulo' => "Cadastrar Usuário",
            'attr' => 'usuario',
            'form' => $form->createView(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'errors' => '',
        ];
    }
    /**
     * @Route("/Perfil/usuario/salvar", name="gerenciar_usuario_salvar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarUsuario.html.twig")
     * @Method("POST")
     */
    public function salvarUsuarioAction(Request $request) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Cadastrar Usuário");

        $em = $this->getDoctrine()->getManager();

        $entity = new Pessoa();

        $checkForm = 1;
        $dados = $request->request->all();
        if (!empty($dados))
        {
            switch ($dados["ava_perfil"]["pais"])
            {
                case "Brasil" :
                    $checkForm = 1;
                    break;
                default :
                    $checkForm = 2;
                    break;
            }
        }

        $form = $this->formUsuario($entity, $em, $checkForm);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $login = $this->login();

            $emailMarketing = $em->getRepository("AdminBundle:EmailMarketing")->find($login['id_email_marketing']);
            $entity->setIdEmailMarketing($emailMarketing);

            $em->persist($entity);

            $em->flush();

            $value['id'] = $entity->getId();
            $value['nome'] = ($entity->getTipoPessoa() == 1) ? $entity->getPfNome() : $entity->getPjNomeFantasia();
            $value['id_email_marketing'] = $login['id_email_marketing'];
            $value['email'] = $entity->getEmail();
            $value['foto'] = ($entity->getPfSexo() == 1) ? 'user.png' : 'user_woman.png';

            $consult = new \Admin\AdminBundle\lib\PessoaConsult();
            $value['check'] = $consult->checkContratosForAmbiente($entity);

            $session = new \Symfony\Component\HttpFoundation\Session\Session();
            $session->set('ava', $value);

            $tag = $this->container->get('admin.tags');
            $tag->persistPessoaTag($entity->getId(), 4);

            //$pessoa = $em->getRepository("AdminBundle:Pessoa")->find($entity->getId());

            $this->container->get('admin.contratacao')->persistContrato($entity->getId(), 1);

            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'Usuário',
            'errors' => $errors,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario'
        ];
    }

    private function formUsuario(Pessoa $entity, $em, $value=1) {
        $usuario = $this->login();

        switch ($value)
        {
            case 1:
                $class = new PerfilTypeNew($em, $usuario);
                break;
            case 2:
                $class = new PerfilTypeNew2($em, $usuario);
                break;
        }
        $form = $this->createForm($class, $entity, array(
            'action' => $this->generateUrl('gerenciar_usuario_salvar'),
            'method' => 'POST',
            'validation_groups' => array('perfil'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/usuario/editar-{id}", name="gerenciar_usuario_editar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarUsuarioEdit.html.twig")
     * @Method("GET")
     */
    public function editarUsuarioAction($id) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Usuário");

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pessoa entity.');
        }

        $editForm = $this->usuarioFormEdit($entity, $em);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'Usuário',
            'errors' => '',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario',
            'id' => $id,
        ];
    }

    /**
     * @Route("/Perfil/usuario/atualizar-{id}", name="gerenciar_usuario_atualizar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarUsuarioEdit.html.twig")
     * @Method({"POST", "PUT"})
     */
    public function usuarioAtualizarAction(Request $request, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pessoa entity.');
        }
        $checkForm = 1;
        $dados = $request->request->all();
        if (!empty($dados))
        {
            switch ($dados["ava_perfil"]["pais"])
            {
                case "Brasil" :
                    $checkForm = 1;
                    break;
                default :
                    $checkForm = 2;
                    break;
            }
        }
        $editForm = $this->usuarioFormEdit($entity, $em, $checkForm);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'Usuário',
            'errors' => $errors,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario',
            'id' => $id,
        ];
    }

    private function usuarioFormEdit(Pessoa $entity, $em, $value=1) {
        $usuario = $this->login();
        switch ($value)
        {
            case 1:
                $class = new PerfilType($em, $usuario);
                break;
            case 2:
                $class = new PerfilType2($em, $usuario);
                break;
        }
        $form = $this->createForm($class, $entity, array(
            'action' => $this->generateUrl('gerenciar_usuario_atualizar', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('perfil_edit'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/senha", name="gerenciar_senha")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarSenha.html.twig")
     * @Method("GET")
     */
    public function gerenciarSenhaAction() {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Senha");

        $pessoa = $this->checkPessoa();
        if (empty($pessoa)) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Pessoa")->find($pessoa["id"]);
        $editForm = $this->senhaFormEdit($entity);

        return [
            'entity' => $pessoa,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Senha',
            'modulo' => 'Usuário',
            'errors' => '',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario'
        ];
    }

    /**
     * @Route("/Perfil/senha/salvar-{id}", name="gerenciar_senha_salvar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarSenha.html.twig")
     * @Method({"POST", "PUT"})
     */
    public function gerenciarSenhaSalvarAction(Request $request, $id) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Senha");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pessoa entity.');
        }

        $editForm = $this->senhaFormEdit($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            $container = $this->container->get('admin.tags');
            $container->persistPessoaTag($entity->getId(), 5);

            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        return $this->render('AmbienteUsuarioBundle:Usuario:gerenciarSenha.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Senha',
                    'modulo' => 'Usuário',
                    'errors' => $errors,
                    'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
                    'attr' => 'usuario'
        ));
    }

    /**
     * @Route("/Perfil/senha/redefinir-{id}", name="gerenciar_senha_redefinir")
     * @Template("AmbienteUsuarioBundle:Usuario:redefinirSenha.html.twig")
     * @Method({"GET", "POST"})
     */
    public function redefinirSenhaAction(Request $request, $id) {

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Redefinir Senha");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Pessoa")->find($id);

        $error = array();
        $sessionController = $this->get('session');
        if (!$entity instanceof Pessoa) {
            $this->addFlash("error", "Usuário não localizado");
            return $this->redirect($this->generateUrl('home_homepage'));
        }

        $form = $request->request->all();

        if (!empty($form["submit"])) {
            if (empty($form["senha"])) {
                $error[] = "Campo senha é obrigatório";
            }
            if (empty($form["confirma_senha"])) {
                $error[] = "Campo confirmação de senha é obrigatório";
            }
            if (!empty($form["senha"]) && !empty($form["confirma_senha"]) && $form["senha"] != $form["confirma_senha"]) {
                $error[] = "Campo senha e confirmação de senha devem ser iguais";
            }
            if (empty($form["dica_senha"])) {
                $error[] = "Campo dica de senha é obrigatório";
            }
            if (empty($error)) {
                $entity->setSenha($form["senha"]);
                $entity->setDicaSenha($form["dica_senha"]);

                $em->flush();

                $this->addFlash("info", "Senha atualizada com sucesso");

                return $this->redirect($this->generateUrl('home_homepage'));
            }
        }
        return [
            'entity' => $entity,
            'id' => $id,
            'titulo' => 'Redefinir Senha',
            'modulo' => 'Usuário',
            'errors' => $error,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario'
        ];
    }

    private function senhaFormEdit(Pessoa $entity) {
        $form = $this->createForm(new SenhaType(), $entity, array(
            'action' => $this->generateUrl('gerenciar_senha_salvar', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('auth'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/foto", name="gerenciar_foto")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarFoto.html.twig")
     * @Method("GET")
     */
    public function gerenciarFotoAction() {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Foto");

        $pessoa = $this->checkPessoa();
        if (empty($pessoa)) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Pessoa")->find($pessoa["id"]);
        $editForm = $this->fotoFormEdit($entity);

        return [
            'entity' => $pessoa,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Foto',
            'modulo' => 'Usuário',
            'errors' => '',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario'
        ];
    }

    /**
     * @Route("/Perfil/foto/salvar-{id}", name="gerenciar_foto_salvar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarFoto.html.twig")
     * @Method({"POST", "PUT"})
     */
    public function gerenciarFotoSalvarAction(Request $request, $id) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Foto");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity && !$entity instanceof Pessoa) {
            throw $this->createNotFoundException('Unable to find Pessoa entity.');
        }

        $editForm = $this->fotoFormEdit($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            $container = $this->container->get('admin.tags');
            $container->persistPessoaTag($entity->getId(), 6);

            $session = new \Symfony\Component\HttpFoundation\Session\Session();

            $value['id'] = $entity->getId();
            $value['nome'] = $entity->getPfNome();
            $value['email'] = $entity->getEmail();
            $value['foto'] = $entity->getFoto();

            $consult = new \Admin\AdminBundle\lib\PessoaConsult();
            $value['check'] = $consult->checkContratosForAmbiente($entity);

            $session->set('ava', $value);

            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Foto',
            'modulo' => 'Usuário',
            'errors' => $errors,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'attr' => 'usuario'
        ];
    }

    private function fotoFormEdit(Pessoa $entity) {
        $form = $this->createForm(new FotoType(), $entity, array(
            'action' => $this->generateUrl('gerenciar_foto_salvar', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('file'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/endereco", name="gerenciar_endereco")
     * @Method("GET")
     */
    public function gerenciarEnderecoAction() {
        $this->checkLogin();

        $pessoa = $this->checkPessoa();

        if (empty($pessoa)) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:PessoaEndereco")->findOneBy(array("idPessoa" => $pessoa['id']));

        if (empty($repository)) {
            return $this->redirect($this->generateUrl('gerenciar_endereco_novo'));
        }
        return $this->redirect($this->generateUrl('gerenciar_endereco_editar', array('id' => $repository->getId())));
    }

    private function enderecoCreateForm(PessoaEndereco $entity, $em) {
        $form = $this->createForm(new EnderecoType($em), $entity, array(
            'action' => $this->generateUrl('gerenciar_endereco_salvar'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/endereco/novo", name="gerenciar_endereco_novo")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEndereco.html.twig")
     * @Method("GET")
     */
    public function novoEnderecoAction() {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Adicionar Endereço");

        $entity = new PessoaEndereco();
        $form = $this->enderecoCreateForm($entity, $em);

        return [
            'titulo' => "Cadastrar Endereço",
            'attr' => 'usuario',
            'errors' => '',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/Perfil/endereco/salvar", name="gerenciar_endereco_salvar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEndereco.html.twig")
     * @Method("POST")
     */
    public function salvarEnderecoAction(Request $request) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Adicionar Endereço");


        $em = $this->getDoctrine()->getManager();
        $entity = new PessoaEndereco();
        $form = $this->enderecoCreateForm($entity, $em);
        $dados = $request->request->all();
        $error = array();

        if (!empty($dados)) {
            $pessoa = $this->checkPessoa();

            $service = $this->container->get('admin.localizacao');
            $result = $service->persistLocalizacao($pessoa["id"], $dados["ava_endereco"]);

            if ($result instanceof PessoaEndereco) {
                $entity = $result;
                return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
            }

            $error = $result;
        }

        return [
            'entity' => $entity,
            'titulo' => 'Cadastrar Endereço',
            'modulo' => 'Endereço',
            'errors' => $error,
            'attr' => 'usuario',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'form' => $form->createView(),
        ];
    }

    private function enderecoEditForm(PessoaEndereco $entity, $em) {
        $form = $this->createForm(new EnderecoType($em), $entity, array(
            'action' => $this->generateUrl('gerenciar_endereco_atualizar', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/endereco/editar-{id}", name="gerenciar_endereco_editar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEnderecoEdit.html.twig")
     * @Method("GET")
     */
    public function editarEnderecoAction($id) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Endereço");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pessoa entity.');
        }

        $form = $this->enderecoEditForm($entity, $em);

        return [
            'entity' => $entity,
            'titulo' => 'Alterar Endereço',
            'modulo' => 'Endereço',
            'errors' => '',
            'attr' => 'usuario',
            'form' => $form->createView(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'id' => $id,
        ];
    }

    /**
     * @Route("/Perfil/endereco/atualizar-{id}", name="gerenciar_endereco_atualizar")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEndereco.html.twig")
     * @Method({"POST", "PUT"})
     */
    public function atualizarEnderecoAction(Request $request, $id) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Alterar Endereço");

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);
        $form = $this->enderecoEditForm($entity, $em);
        $dados = $request->request->all();
        $error = array();

        if (!empty($dados)) {
            $pessoa = $this->checkPessoa();

            $service = $this->container->get('admin.localizacao');
            $result = $service->persistLocalizacao($pessoa["id"], $dados["ava_endereco"], $id);

            if ($result instanceof PessoaEndereco) {
                $entity = $result;
                return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
            }

            $error = $result;
        }

        return [
            'entity' => $entity,
            'titulo' => 'Alterar Registro',
            'modulo' => 'Usuário',
            'errors' => $error,
            'attr' => 'endereco',
            'form' => $form->createView(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'id' => $id,
        ];
    }

    /**
     * @Route("/Perfil/email/editar", name="perfil_edit_email_marketing")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEmail.html.twig")
     * @Method("GET")
     */
    public function emailEditarAction() {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Editar E-mail");

        $em = $this->getDoctrine()->getManager();
        $entity = new EmailMarketing();

        $form = $this->emailEditForm($entity);

        return [
            'entity' => $entity,
            'titulo' => 'Editar E-mail',
            'modulo' => 'E-mail',
            'errors' => '',
            'attr' => 'usuario',
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'form' => $form->createView(),
        ];
    }

    private function emailEditForm(EmailMarketing $entity) {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('perfil_update_email_marketing'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * @Route("/Perfil/email/atualizar", name="perfil_update_email_marketing")
     * @Template("AmbienteUsuarioBundle:Usuario:gerenciarEmail.html.twig")
     * @Method("POST")
     */
    public function emailAtualizarAction(Request $request) {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Atualizar E-mail");

        $entity = new EmailMarketing();
        $form = $this->emailEditForm($entity);
        $dados = $request->request->all();
        $error = array();

        if (!empty($dados)) {
            $pessoa = $this->checkPessoa();

            $service = $this->container->get('admin.emailMarketing');

            $result = $service->changeEmailMarketing($pessoa["id"], $dados["admin_adminbundle_emailmarketing"]);

            if (empty($result)) {
                $this->get('session')->getFlashBag()->set('info', 'E-mail alterado com sucesso, entre na sua caixa postal e valide o seu e-mail');
                
                return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
            }
            
            $error[] = $result;
        }

        return $this->render('AmbienteUsuarioBundle:Usuario:gerenciarEmail.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Atualizar E-mail',
                    'modulo' => 'E-mail',
                    'errors' => $error,
                    'attr' => 'usuario',
                    'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
                    'form' => $form->createView(),
        ));
    }

}
