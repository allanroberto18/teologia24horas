<?php

namespace Usuario\AmbienteAlunoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CursoControllerTest extends WebTestCase
{
    public function testListcursos()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ver-cursos');
    }

    public function testCurso()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/curso');
    }

    public function testContratarcurso()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contratar-{idCursoOfertado}');
    }

}
