<?php

namespace Usuario\AmbienteAlunoBundle\Controller;

use Admin\AdminBundle\Entity\AulaAnotacao;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints\DateTime;
use Usuario\AmbienteUsuarioBundle\Controller\UsuarioController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\PessoaEndereco;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\CursoItem;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\ContratoItem;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\lib\ContratoConsult;
use Admin\AdminBundle\lib\CursoConsult;
use Admin\AdminBundle\lib\PessoaConsult;
use Admin\AdminBundle\Entity\Aula;
use Admin\AdminBundle\lib\CalculaFrete;

class CursoController extends UsuarioController {

    public function getPessoa($id) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository("AdminBundle:Pessoa")->find($id);
    }

    /**
     * @Route("/", name="ava_aluno_home")
     * @Template("AmbienteAlunoBundle:Curso:home.html.twig")
     * @Method("GET")
     */
    public function homeAction() {
        $this->checkLogin();

        $session = $this->checkPessoa();
        if (empty($session["id"])) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }
        $pessoa = $this->getPessoa($session["id"]);
        if (!$pessoa instanceof Pessoa) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Ambiente do Aluno");

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository("AdminBundle:Contrato");
        $contrato = $repository->createQueryBuilder('item')
                ->andWhere("item.idPessoa = :idPessoa")
                ->setParameter("idPessoa", $pessoa->getId())
                ->orderBy("item.dataContratacao", 'DESC')
                ->getQuery()
                ->getResult();

        if (empty($contrato) && count($contrato) > 0) {
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        return $this->render('AmbienteAlunoBundle:Curso:home.html.twig', array(
                    'titulo' => "Ambiente do Aluno",
                    'attr' => 'home',
                    'pessoa' => $pessoa,
                    'Curso' => $contrato[0]->getIdCurso(),
                    'Contrato' => $contrato,
                    'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ));
    }

    /**
     * @Route("/meus-cursos", name="ava_aluno_list_cursos")
     * @Template("AmbienteAlunoBundle:Curso:listCursos.html.twig")
     * @Method("GET")
     */
    public function listCursosAction() {
        $this->checkLogin();

        $session = $this->checkPessoa();
        if (empty($session["id"])) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $pessoa = $this->getPessoa($session["id"]);
        if (!$pessoa instanceof Pessoa) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Meus Cursos");

        $em = $this->getDoctrine()->getManager();
        $objConsultContratos = new ContratoConsult();
        $contratos = $objConsultContratos->returnCursosContratados($session["id"], $em);

        return [
            'titulo' => "Selecionar Curso",
            'attr' => 'meus_cursos',
            'cursosContratados' => $contratos,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/curso/visualizar/{idCurso}/{slug}", name="ava_aluno_curso")
     * @Template("AmbienteAlunoBundle:Curso:curso.html.twig")
     * @Method("GET")
     */
    public function cursoAction($idCurso, $slug) {
        $this->checkLogin();

        $session = $this->checkPessoa();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Contrato");

        $sessionController = $this->get('session');
        if (empty($session["id"]) && $session["cep"]) {
            $this->addFlash("info", "Para acessar o curso é necessário a complementação dos seus dados no seu perfil");
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        $contrato = $repository->findOneBy(array("idCurso" => $idCurso, "idPessoa" => $session["id"]));

        if (empty($contrato)) {
            $this->addFlash("info", "Curso não adquirido pelo usuário, efetue a compra e tente novamente");
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        if (!$contrato instanceof Contrato) {
            $this->addFlash("info", "O contrato não pode ser localizado");
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        $fatura = $em->getRepository('AdminBundle:Fatura')->findBy(['idContrato' => $contrato->getId()], ['id' => 'DESC'], 1);

        $data = empty($fatura[0]->getPagamentoData()) ? $contrato->getDataContratacao() : $fatura[0]->getPagamentoData();

        $tempoRestante = $contrato->getIdCurso()->getId() == 1 ? "Liberado" : $this->checkTempoRestante($data, $fatura[0]->getStatus());

        if ($tempoRestante != "Liberado" && $tempoRestante < 0)
        {
            $contrato->setStatus(2);
            $tempoRestante = 0;
        }

        $status = $contrato->getStatus();
        $curso = $contrato->getIdCurso();
        $pessoa = $contrato->getIdPessoa();

        if (!$curso instanceof Curso && !$pessoa instanceof Pessoa) {
            $this->addFlash("info", "O curso não pode ser localizado");
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        if (empty($pessoa->getCidade())) {
            $nucleo = "Palmas(TO)";
        } else {
            $nucleo = $pessoa->getCidade() . "(" . $pessoa->getEstado() . ") - ";
        }

        $calendario = new \Admin\AdminBundle\lib\MakeCalendar();

        $links = $this->mountLinks($curso, $contrato);

        $this->get('session')->set('links', $links);
        $this->get('session')->set('duracao', $curso->getDuracao());
        $this->get('session')->set('hora_aula', $curso->getHoraAula());
        $this->get('session')->set('credito', $curso->getCredito());
        $this->get('session')->set('aluno', array('nome' => $pessoa->getPfNome(), "matricula" => "", "nucleo" => $nucleo));

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Meus Cursos", $this->get("router")->generate("ava_aluno_list_cursos"));
        $breadcrumbs->addItem($curso->getTitulo());
        
        $introducao = $this->container->get('usuario.aulas')->returnIntroducao($idCurso);
        $alunos = $this->container->get('usuario.aulas')->returnAlunosByCurso($idCurso);

        return [
            'titulo' => $curso->getTitulo(),
            'attr' => 'curso',
            'Contrato' => $contrato,
            'Calendario' => $calendario->mount(),
            'grade' => $curso,
            'status' => $status,
            'introducao' => $introducao,
            'alunos' => $alunos,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
            'dias' => $tempoRestante,
        ];
    }

    /**
     * @Route("/curso/aula/{idAula}/{slug}", name="ava_aluno_curso_aula")
     * @Template("AmbienteAlunoBundle:Curso:aula.html.twig")
     * @Method("GET")
     */
    public function aulaAction($idAula) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $aula = $em->getRepository("AdminBundle:Aula")->find($idAula);

        if (empty($aula)) {
            $this->redirect($this->generateUrl("ambiente_usuario_homepage_home"));
        }
        if (!$aula instanceof Aula) {
            $this->redirect($this->generateUrl("ambiente_usuario_homepage_home"));
        }

        $aluno = $this->get('session')->get('ava');
        $contrato = $this->container->get('admin.contratacao')->getContrato2($aluno['id']);

        $curso = $contrato->getIdCurso();

        $anotacao = $this->getDoctrine()->getRepository("AdminBundle:AulaAnotacao")->findBy([ 'idContrato' => $contrato->getId() , 'idAula' => $idAula ]);

        $links = $this->get('session')->get('links');
        $linkAtual = $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $aula->getId(), "slug" => $aula->getSlug()));

        $posicao = $this->getLink($links, $linkAtual);

        $navegacao = $this->returnLinkOrdenado($links, $posicao);

        $checkAcesso = $this->container->get('usuario.acesso')->checkAcesso($aula->getId(), $contrato->getId());

        return [
            'titulo' => $aula->getIdDisciplina()->returnSemana() . " / " . $aula->getIdDisciplina()->returnAula() . " <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $aula->getIdDisciplina()->getTitulo(),
            'attr' => 'aula',
            'posicao' => $posicao,
            'navegacao' => $navegacao,
            'aula' => $aula,
            'grade' => $curso,
            'idCurso' => $curso->getId(),
            'contrato' => $contrato,
            'acesso' => $checkAcesso ? 1 : 0,
            'anotacao' => count($anotacao) > 0 ? $anotacao[0] : new AulaAnotacao(),
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/curso/selecionar/{idCurso}/{slug}", name="ava_aluno_contratar_carrinho_compra")
     * @Template("AmbienteAlunoBundle:Curso:carrinho.html.twig")
     * @Method({"GET", "POST"})
     */
    public function carrinhoCompraAction(Request $request, $idCurso, $slug) {
        $this->checkLogin($idCurso, $slug);

        // Checa o usuário cadastrado
        $session = $this->checkPessoa();
        $sessionController = $this->get('session');
        $em = $this->getDoctrine()->getManager();
        
        if (empty($session)) {
            return $this->redirect($this->generateUrl('gerenciar_usuario_novo'));
        }

        // Limpa o carrinho de compras
        $this->get('session')->remove('carrinho');
        if (!empty($this->get('session')->getFlashBag()->get("fatura"))) {
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        $curso = $em->getRepository("AdminBundle:Curso")->find($idCurso);
        if (!$curso instanceof Curso) {
            $msg[] = "Curso não localizado";
        }

        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($session["id"]);
        if (!$pessoa instanceof Pessoa) {
            $msg[] = "Usuário não localizado";
        }

        if (!empty($msg) && is_array($msg)) {
            $this->addFlash("info", implode("<br />", $msg));
            return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
        }

        $form = $request->request->all();

        $total = $curso->getIdTabelaPreco()->getValor();
        
        $status = "";
        $fatura = false;
        if (!empty($form)) {
            $status = $form["submit"];

            $contrato = $this->container->get('admin.contratacao');
            $fatura = $contrato->persistContrato($pessoa->getId(), $curso->getId());

            if ($total == 0) {
                return $this->redirect($this->generateUrl('ambiente_usuario_homepage_home'));
            }
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("Carrinho de Compras");

        return [
            'titulo' => "Calcular o Valor do Curso",
            'pessoa' => $pessoa,
            'curso' => $curso,
            'status' => $status,
            'total' => $total,
            'fatura' => $fatura,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/Faturas/", name="ava_aluno_faturas")
     * @Template("AmbienteAlunoBundle:Curso:financeiro.html.twig")
     * @Method("GET")
     */
    public function faturasCursoAction() {

        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("AVA (Ambiente Virtual de Aprendizagem)", $this->get("router")->generate("ava_aluno_list_cursos"));
        $breadcrumbs->addItem("Financeiro");

        $session = $this->checkPessoa();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Contrato")
                ->createQueryBuilder('item')
                ->where('item.idPessoa = :idPessoa')
                ->setParameter('idPessoa', $session["id"])
                ->orderBy('item.id', 'DESC')
                ->getQuery()
                ->getResult();

        return [
            'titulo' => "Financeiro",
            'attr' => 'financeiro',
            'contratos' => $repository,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/Notas/", name="ava_aluno_notas")
     * @Template("AmbienteAlunoBundle:Curso:notas.html.twig")
     * @Method("GET")
     */
    public function notasCursoAction() {
        $this->checkLogin();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Home", $this->get("router")->generate("ambiente_usuario_homepage_home"));
        $breadcrumbs->addItem("AVA (Ambiente Virtual de Aprendizagem)", $this->get("router")->generate("ava_aluno_list_cursos"));
        $breadcrumbs->addItem("AVA (Ambiente Virtual de Aprendizagem)", $this->get("router")->generate("ava_aluno_list_cursos"));
        $breadcrumbs->addItem("Financeiro");

        $session = $this->checkPessoa();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Contrato")
                ->createQueryBuilder('item')
                ->where('item.idPessoa = :idPessoa')
                ->setParameter('idPessoa', $session["id"])
                ->orderBy('item.id', 'DESC')
                ->getQuery()
                ->getResult();

        return [
            'titulo' => "Notas do Aluno",
            'attr' => 'notas',
            'contratos' => $repository,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    /**
     * @Route("/curso/{idCurso}/{slug}", name="ava_aluno_ver_curso")
     * @Template("AmbienteAlunoBundle:Curso:verCurso.html.twig")
     * @Method("GET")
     */
    public function verCursoAction($idCurso, $slug) {
        $em = $this->getDoctrine()->getManager();

        $curso = $em->getRepository('AdminBundle:Curso')->find($idCurso);

        $disciplinas = $em->getRepository("AdminBundle:Disciplina")->findBy(array('idCurso' => $idCurso));
        
		$alunos = $this->container->get('usuario.aulas')->returnAlunosByCurso($idCurso);
		
        $aulas = 0;
        
        if (count($disciplinas)) {
            for ($i = 0; $i < count($disciplinas); $i++) {
                $conteudo = $disciplinas[$i]->getConteudoAula();
                if (count($conteudo) > 0) {
                    foreach ($conteudo as $item) {
                        if ($item->getTipoConteudo() < 3) {
                            $aulas++;
                        }
                    }
                }
            }
        }

        $introducao = $this->container->get('usuario.aulas')->returnIntroducao($idCurso);

        return [
            'titulo' => $curso->getTitulo(),
            'attr' => 'contratar',
            'Curso' => $curso,
            'Imagens' => $curso->getImagem(),
            'aulas' => $aulas,
            'introducao' => $introducao,
            'alunos' => $alunos,
            'paginas'=> $this->container->get('front.paginas')->returnPaginas(),
        ];
    }

    public function mountLinks(Curso $curso, Contrato $contrato) {
        if (!$curso instanceof Curso) {
            return false;
        }
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository("AdminBundle:Disciplina");
        $disciplina = $repository->createQueryBuilder('item')
                ->where("item.idCurso = :id")
                ->orderBy("item.aula", "ASC")
                ->setParameter("id", $curso->getId())
                ->getQuery()
                ->getResult();

        $links = array();
        $links[] = array(
            'posicao' => 0,
            'link' => $this->generateUrl("ava_aluno_curso", array("idCurso" => $curso->getId(), "slug" => $curso->getSlug())),
        );
        $dia = date('d');

        for ($i = 0; $i < count($disciplina); $i++) {
            if ($disciplina[$i]->getStatus() == 1) {
                $semana = $disciplina[$i]->getSemana();
                $conteudoAula = $disciplina[$i]->getConteudoAula();
                for ($j = 0; $j < count($conteudoAula); $j++) {
                    if ($curso->getIdTabelaPreco()->getValor() == 0) {
                        $links[] = array(
                            'posicao' => count($links),
                            'link' => $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $conteudoAula[$j]->getId(), "slug" => $conteudoAula[$j]->getSlug())),
                        );
                    } else {
                        if ($conteudoAula[$j]->getStatus() == 1) {
                            if ($contrato->getStatus() == 2) {
                                $links[] = array(
                                    'posicao' => count($links),
                                    'link' => null
                                );
                            } else {
                                if ($semana == 1 && $dia > 1 || $dia < 8) {
                                    //print $semana;
                                    $links[] = array(
                                        'posicao' => count($links),
                                        'link' => $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $conteudoAula[$j]->getId(), "slug" => $conteudoAula[$j]->getSlug())),
                                    );
                                } else if ($semana == 2 && $dia > 7 || $dia <= 14) {
                                    $links[] = array(
                                        'posicao' => count($links),
                                        'link' => $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $conteudoAula[$j]->getId(), "slug" => $conteudoAula[$j]->getSlug())),
                                    );
                                } else if ($semana == 3 && $dia > 14 || $dia <= 21) {
                                    $links[] = array(
                                        'posicao' => count($links),
                                        'link' => $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $conteudoAula[$j]->getId(), "slug" => $conteudoAula[$j]->getSlug())),
                                    );
                                } else if ($semana == 4 && $dia > 21 || $dia <= 28) {
                                    $links[] = array(
                                        'posicao' => count($links),
                                        'link' => $this->generateUrl("ava_aluno_curso_aula", array("idAula" => $conteudoAula[$j]->getId(), "slug" => $conteudoAula[$j]->getSlug())),
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        return $links;
    }

    public function getLink($links, $linkAtual) {
        for ($i = 0; $i < count($links); $i++) {
            if ($linkAtual != $links[$i]['link']) {
                continue;
            }
            return $links[$i]["posicao"];
        }
    }

    public function returnLinkOrdenado($links, $posicao) {
        $proximo = empty($links[($posicao + 1)]['link']) ? "0" : $links[($posicao + 1)]['link'];
        $anterior = empty($links[($posicao - 1)]['link']) ? $links[0]['link'] : $links[($posicao - 1)]['link'];

        return array('proximo' => $proximo, 'anterior' => $anterior);
    }

    public function valorFatura(Contrato $contrato) {
        $pessoa = $contrato->getIdPessoa();
        $endereco = $pessoa->getEndereco();
        $curso = $contrato->getIdCurso();
        $frete = 0;
        $itensContrato = $contrato->getItemContrato();
        if ($itensContrato) {
            for ($i = 0; $i < count($itensContrato); $i++) {
                $frete += $this->valorFrete($endereco) + $itensContrato[$i]->getIdCursoItem()->getValor();
            }
        }
        return $curso->getIdTabelaPreco()->getValor() + $frete;
    }

    public function valorFrete(PessoaEndereco $endereco) {
        return CalculaFrete::calcular($endereco->getCep());
    }

    /**
     * @Route("/Atualizar/Fatura-{idFatura}-{tipo}", name="ava_material_didatico")
     * @Method("GET")
     */
    public function atualizarFaturaAction($idFatura, $tipo) {
        $em = $this->getDoctrine()->getManager();

        $fatura = $em->getRepository("AdminBundle:Fatura")->find($idFatura);

        if (!$fatura instanceof Fatura) {
            return false;
        }

        $contrato = $fatura->getIdContrato();
        $curso = $contrato->getIdCurso();
        $item = $em->getRepository("AdminBundle:CursoItem")->findBy(array('idCurso' => $curso->getId()));

        $tabelaPrecos = $curso->getIdTabelaPreco();
        $itemContrato = $contrato->getItemContrato();

        switch ($tipo) {
            case 1:
                if (count($itemContrato) > 0) {
                    continue;
                }

                if (count($item) > 0) {
                    $novoItem = new ContratoItem();
                    $novoItem->setIdContrato($contrato);
                    $novoItem->setIdCursoItem($item[0]);
                    $novoItem->setStatus(1);

                    $em->persist($novoItem);
                }
                $em->flush();
                $fatura->setValor($this->valorFatura($contrato));

                break;
            case 2:
                if (count($itemContrato) > 0) {
                    foreach ($itemContrato as $item) {
                        $em->remove($item);
                    }
                }
                $fatura->setValor($tabelaPrecos->getValor());
                break;
        }
        $em->flush();

        return $this->redirect($this->generateUrl("ava_aluno_faturas"));
    }

    public function checkTempoRestante(\Datetime $data, $status=1)
    {
        if ($status == 1)
        {
            return -1;
        }
        $data1 = new \DateTime($data->format('Y-m-d'));
        $data2 = new \DateTime('NOW');

        $result = 90 - $data1->diff($data2)->days;

        return  $result;
    }

}
