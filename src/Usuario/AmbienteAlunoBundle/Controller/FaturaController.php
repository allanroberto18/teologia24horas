<?php

namespace Usuario\AmbienteAlunoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\Entity\PessoaEndereco;

class FaturaController extends Controller {

    /**
     * @Route("/Exibir/Fatura-{idFatura}", name="gerar_boleto")
     * @Method("GET")
     */
    public function gerarBoletoAction($idFatura) {

        $this->get('session')->getFlashBag()->add('fatura', 'emitida');

        $em = $this->getDoctrine()->getManager();
        $fatura = $em->getRepository("AdminBundle:Fatura")->find($idFatura);
        if (!$fatura instanceof Fatura) {
            return $this->redirect($this->generateUrl("ava_aluno_list_cursos"));
        }

        $pessoa = $fatura->getIdContrato()->getIdPessoa();

        $endereco1 = !empty($pessoa->getCidade()) ? ($pessoa->getCidade() . "/" . $pessoa->getEstado()) : "Palmas/TO";
        $endereco2 = !empty($pessoa->getPais()) ? $pessoa->getPais() : "Brasil";

        $checkData = new \DateTime(date("Y-m-d"));

        if ($checkData->format("Y-m-d") > $fatura->getVencimento()->format("Y-m-d")) {
            $dataVencimento = new \DateTime(date("Y-m-d"));
            $dataVencimento->modify('+5 days');

            $fatura->setVencimento($dataVencimento);
            $em->flush();
        }

        $date = $fatura->getVencimento()->format("d/m/Y");

        $data = array(
            'payer_name' => $pessoa->getPfNome(),
            'payer_cpf_cnpj' => $pessoa->getPfCpf(),
            'payer_address_line1' => $endereco1,
            'payer_address_line2' => $endereco2,
            'payslip_value' => number_format($fatura->getValor(), 2, ',', ''),
            'payslip_discount_value' => number_format(0, 2, ',', ''),
            'payslip_due_date' => $date,
            'payslip_document_number' => $fatura->getId(),
            'payslip_description' => $fatura->getIdContrato()->getIdCurso()->getTitulo(),
        );

        return new Response($this->get('paggy_boleto.view')->render($data));
    }

}
