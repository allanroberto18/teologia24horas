<?php

namespace Usuario\AmbienteAlunoBundle\Controller;

use Admin\AdminBundle\Entity\AulaAnotacao;
use Admin\AdminBundle\Entity\Forum;
use Admin\AdminBundle\Entity\ForumItem;
use Admin\AdminBundle\Entity\ForumLigacao;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller
{
    public function anotacaoAction(Request $request, $idContrato, $idAula)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $anotacao = $em->getRepository("AdminBundle:AulaAnotacao")->findBy(['idContrato' => $idContrato, 'idAula' => $idAula]);
        if (empty($anotacao))
        {
            $entity = new AulaAnotacao();
            $entity->setTexto($data['texto']);

            $contrato = $em->getRepository("AdminBundle:Contrato")->find($idContrato);
            $aula = $em->getRepository("AdminBundle:Aula")->find($idAula);

            $entity->setIdAula($aula);
            $entity->setIdContrato($contrato);

            $em->persist($entity);
        } else {
            $anotacao[0]->setTexto($data['texto']);
        }
        $em->flush();

        return new JsonResponse(["Mensagem" => "Anotação salva com sucesso"]);
    }

    public function postForumAction(Request $request, $idContrato)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $contrato = $em->getRepository("AdminBundle:Contrato")->find($idContrato);

        $entity = new Forum();
        $entity->setTema($data['comentario']);
        $entity->setIdCurso($contrato->getIdCurso());
        $entity->setIdPessoa($contrato->getIdPessoa());

        $em->persist($entity);
        $em->flush();

        return new JsonResponse(["status" => 1, "Mensagem" => "Seu comentário foi enviado com sucesso. Aguardando liberação"]);
    }

    public function postForumRespostaAction(Request $request, $idContrato)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();

        $contrato = $em->getRepository("AdminBundle:Contrato")->find($idContrato);
        $forum = $em->getRepository("AdminBundle:Forum")->find($data['id_forum']);

        $entity = new Forum();
        $entity->setTema($data['comentario']);
        $entity->setIdCurso($contrato->getIdCurso());
        $entity->setIdPessoa($contrato->getIdPessoa());
        $entity->setTipo(2);

        $em->persist($entity);

        $resposta = new ForumLigacao();
        $resposta->setIdPost($forum);
        $resposta->setIdResposta($entity);

        $em->persist($resposta);

        $em->flush();

        return new JsonResponse(["status" => 1, "Mensagem" => "Seu comentário foi enviado com sucesso. Aguardando liberação"]);
    }

    public function listForumAction($idCurso)
    {
        $list = $this->getDoctrine()->getRepository("AdminBundle:Forum")->findBy(['idCurso' => $idCurso, 'status' => 1, 'tipo' => 1], ['datahoraCadastro' => 'DESC']);

        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($list, 'json');

        return new Response($response);
    }
}
