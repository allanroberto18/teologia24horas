<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 25/08/2015
 * Time: 10:38
 */

namespace Usuario\Utils;

use Admin\AdminBundle\Entity\AulaAcesso;
use Doctrine\ORM\EntityManager;

class Acessos
{
    private $entityManager;

    /**
     * Acesso constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function checkAcesso($idAula, $idContrato)
    {
        $consult = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\AulaAcesso')->findBy(['idAula' => $idAula, 'idContrato' => $idContrato]);

        if (count($consult) > 0)
        {
            return true;
        }
        return false;
    }
}