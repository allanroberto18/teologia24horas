<?php

namespace Usuario\Utils;

use Doctrine\ORM\EntityManager;

class Aulas
{
    private $entityManager;

    function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function returnIntroducao($idCurso)
    {
        $repository = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Disciplina')->findOneBy(['idCurso' => $idCurso, 'aula' => 0, 'semana' => 1]);
        
        if (empty($repository))
        {
            $repository = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Disciplina')->findOneBy(['idCurso' => $idCurso, 'semana' => 1]);
        }

        $aula = $repository->getConteudoAula();
        
        return $aula[0];
    }
    
    public function returnAlunosByCurso($idCurso) {
    	
    	$consult = $this->entityManager->createQueryBuilder();
        $consult->select('pessoa');
        $consult->from('\Admin\AdminBundle\Entity\Pessoa', 'pessoa');
        $consult->join(
                '\Admin\AdminBundle\Entity\Contrato', 'contrato', \Doctrine\ORM\Query\Expr\Join::WITH, 'contrato.idPessoa = pessoa.id'
        );
        $consult->where('contrato.idCurso = :curso');
        $consult->setParameter('curso', $idCurso);
        
        return $consult->getQuery()->getResult();
    }
}