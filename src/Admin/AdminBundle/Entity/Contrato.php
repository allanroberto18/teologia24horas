<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Contrato
 *
 * @ORM\Table(name="contrato")
 * @ORM\Entity
 */
class Contrato extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="contrato_id_seq")
	 */
	private $id;
	
    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="data_cadastro", type="date", nullable=true)
     */
    private $dataCadastro;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="data_contratacao", type="date", nullable=true)
     */
    private $dataContratacao;

    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa", inversedBy="contrato", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id")
     * })
     */
    private $idPessoa;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;

    /**
     * @var \Fatura
     * @ORM\OneToOne(targetEntity="Fatura", mappedBy="idContrato")
     */
    private $fatura;

    /**
     * @var \ContratoNota
     * @ORM\OneToMany(targetEntity="ContratoNota", mappedBy="idContrato", fetch="EXTRA_LAZY")
     */
    private $notas;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    function getFatura() {
        return $this->fatura;
    }

    function setFatura(\Admin\AdminBundle\Entity\Fatura $fatura) {
        $this->fatura = $fatura;
    }

    /**
     * @return \ContratoNota
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * @param \ContratoNota $notas
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;
    }

    /**
     * @var \ContratoItem
     * @ORM\OneToMany(targetEntity="ContratoItem", mappedBy="idContrato", fetch="EXTRA_LAZY")
     */
    
    private $itemContrato;
    
    function getItemContrato() {
        return $this->itemContrato;
    }

    function setItemContrato(\Admin\AdminBundle\Entity\ContratoItem $itemContrato) {
        $this->itemContrato = $itemContrato;
    }

    public function __construct() {
        $this->itemContrato = new ArrayCollection();
        $this->notas = new ArrayCollection();
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     * @return Contrato
     */
    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime 
     */
    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    /**
     * Set dataContratacao
     *
     * @param \DateTime $dataContratacao
     * @return Contrato
     */
    public function setDataContratacao($dataContratacao) {
        $this->dataContratacao = $dataContratacao;

        return $this;
    }

    /**
     * Get dataContratacao
     *
     * @return \DateTime 
     */
    public function getDataContratacao() {
        return $this->dataContratacao;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return Contrato
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null) {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa() {
        return $this->idPessoa;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     * @return Contrato
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null) {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCurso() {
        return $this->idCurso;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Contrato";
    }

}
