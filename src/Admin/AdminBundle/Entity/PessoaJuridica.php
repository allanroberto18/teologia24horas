<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;

class PessoaJuridica extends EntityMaster
{
    public function __construct() {
        $this->tipoPessoa = 2;
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pessoa_id_seq")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_pessoa", type="integer", nullable=false)
     */
    private $tipoPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_cnpj", type="string", length=25, nullable=true)
     */
    private $pjCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_insc_estadual", type="string", length=25, nullable=true)
     */
    private $pjInscEstadual;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_insc_municipal", type="string", length=25, nullable=true)
     */
    private $pjInscMunicipal;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_razao_social", type="string", length=255, nullable=true)
     */
    private $pjRazaoSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_nome_fantasia", type="string", length=255, nullable=true)
     */
    private $pjNomeFantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_contato", type="string", length=255, nullable=true)
     */
    private $pjContato;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_site", type="string", length=255, nullable=true)
     */
    private $pjSite;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="dica_senha", type="string", length=255, nullable=true)
     */
    private $dicaSenha;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=255, nullable=true)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="biografia", type="text", nullable=true)
     */
    private $biografia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $datahoraAtualizacao;

    /**
     * @var string
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var \EmailMarketing
     *
     * @ORM\ManyToOne(targetEntity="EmailMarketing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_marketing", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEmailMarketing;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set tipoPessoa
     *
     * @param integer $tipoPessoa
     * @return Pessoa
     */
    public function setTipoPessoa($tipoPessoa)
    {
        $this->tipoPessoa = $tipoPessoa;

        return $this;
    }

    /**
     * Get tipoPessoa
     *
     * @return integer 
     */
    public function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    /**
     * Set pjCnpj
     *
     * @param string $pjCnpj
     * @return Pessoa
     */
    public function setPjCnpj($pjCnpj)
    {
        $this->pjCnpj = $pjCnpj;

        return $this;
    }

    /**
     * Get pjCnpj
     *
     * @return string 
     */
    public function getPjCnpj()
    {
        return $this->pjCnpj;
    }

    /**
     * Set pjInscEstadual
     *
     * @param string $pjInscEstadual
     * @return Pessoa
     */
    public function setPjInscEstadual($pjInscEstadual)
    {
        $this->pjInscEstadual = $pjInscEstadual;

        return $this;
    }

    /**
     * Get pjInscEstadual
     *
     * @return string 
     */
    public function getPjInscEstadual()
    {
        return $this->pjInscEstadual;
    }

    /**
     * Set pjInscMunicipal
     *
     * @param string $pjInscMunicipal
     * @return Pessoa
     */
    public function setPjInscMunicipal($pjInscMunicipal)
    {
        $this->pjInscMunicipal = $pjInscMunicipal;

        return $this;
    }

    /**
     * Get pjInscMunicipal
     *
     * @return string 
     */
    public function getPjInscMunicipal()
    {
        return $this->pjInscMunicipal;
    }

    /**
     * Set pjRazaoSocial
     *
     * @param string $pjRazaoSocial
     * @return Pessoa
     */
    public function setPjRazaoSocial($pjRazaoSocial)
    {
        $this->pjRazaoSocial = $pjRazaoSocial;

        return $this;
    }

    /**
     * Get pjRazaoSocial
     *
     * @return string 
     */
    public function getPjRazaoSocial()
    {
        return $this->pjRazaoSocial;
    }

    /**
     * Set pjNomeFantasia
     *
     * @param string $pjNomeFantasia
     * @return Pessoa
     */
    public function setPjNomeFantasia($pjNomeFantasia)
    {
        $this->pjNomeFantasia = $pjNomeFantasia;

        return $this;
    }

    /**
     * Get pjNomeFantasia
     *
     * @return string 
     */
    public function getPjNomeFantasia()
    {
        return $this->pjNomeFantasia;
    }

    /**
     * Set pjContato
     *
     * @param string $pjContato
     * @return Pessoa
     */
    public function setPjContato($pjContato)
    {
        $this->pjContato = $pjContato;

        return $this;
    }

    /**
     * Get pjContato
     *
     * @return string 
     */
    public function getPjContato()
    {
        return $this->pjContato;
    }

    /**
     * Set pjSite
     *
     * @param string $pjSite
     * @return Pessoa
     */
    public function setPjSite($pjSite)
    {
        $this->pjSite = $pjSite;

        return $this;
    }

    /**
     * Get pjSite
     *
     * @return string 
     */
    public function getPjSite()
    {
        return $this->pjSite;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return Pessoa
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Pessoa
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dicaSenha
     *
     * @param string $dicaSenha
     * @return Pessoa
     */
    public function setDicaSenha($dicaSenha)
    {
        $this->dicaSenha = $dicaSenha;

        return $this;
    }

    /**
     * Get dicaSenha
     *
     * @return string 
     */
    public function getDicaSenha()
    {
        return $this->dicaSenha;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return Pessoa
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * Get senha
     *
     * @return string 
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * Set biografia
     *
     * @param string $biografia
     * @return Pessoa
     */
    public function setBiografia($biografia)
    {
        $this->biografia = $biografia;

        return $this;
    }

    /**
     * Get biografia
     *
     * @return string 
     */
    public function getBiografia()
    {
        return $this->biografia;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return EmailMarketing
     */
    function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return PessoaFisica
     */
    function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;
    }
    
    function getFoto() {
        return $this->foto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    public function getAbsolutePath() {
        return null === $this->foto ? null : $this->getUploadRootDir() . '/' . $this->foto;
    }

    public function getWebPath() {
        return null === $this->foto ? null : $this->getUploadDir() . '/' . $this->foto;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/usuarios';
    }

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    private $temp;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->foto)) {
            // store the old name to delete after the update
            $this->temp = $this->foto;
            $this->foto = null;
        } else {
            $this->foto = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->foto = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->foto);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            //$this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Set idEmailMarketing
     *
     * @param \Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing
     * @return Pessoa
     */
    public function setIdEmailMarketing(\Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing = null)
    {
        $this->idEmailMarketing = $idEmailMarketing;

        return $this;
    }

    /**
     * Get idEmailMarketing
     *
     * @return \Admin\AdminBundle\Entity\EmailMarketing 
     */
    public function getIdEmailMarketing()
    {
        return $this->idEmailMarketing;
    }
    
    /**
     * Nome da entidade
     * @return string
     */
    public function returnEntity()
    {
        return "Admin\AdminBundle\Entity\PessoaJuridica";
    }
}
