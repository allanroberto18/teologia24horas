<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MensagemPessoa
 *
 * @ORM\Table(name="mensagem_pessoa")
 * @ORM\Entity
 */
class MensagemPessoa extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="mensagem_pessoa_id_seq")
	 */
	private $id;
	
    /**
     * @var \Mensagem
     * @ORM\ManyToOne(targetEntity="Mensagem", inversedBy="mensagemPessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_mensagem", referencedColumnName="id")
     * })
     */
    private $idMensagem;

    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set idMensagem
     *
     * @param \Admin\AdminBundle\Entity\Mensagem $idMensagem
     * @return MensagemPessoa
     */
    public function setIdMensagem(\Admin\AdminBundle\Entity\Mensagem $idMensagem = null)
    {
        $this->idMensagem = $idMensagem;

        return $this;
    }

    /**
     * Get idMensagem
     *
     * @return \Admin\AdminBundle\Entity\Mensagem 
     */
    public function getIdMensagem()
    {
        return $this->idMensagem;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return MensagemPessoa
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null)
    {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa()
    {
        return $this->idPessoa;
    }
    
    public function returnEntity() {
        return '\Admin\AdminBundle\Entity\MensagemPessoa';
    }
    
}
