<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CepbrCidade
 *
 * @ORM\Table(name="cepbr_cidade")
 * @ORM\Entity
 */
class CepbrCidade
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_cidade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCidade;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="cod_ibge", type="string", length=10, nullable=false)
     */
    private $codIbge = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float", precision=10, scale=0, nullable=false)
     */
    private $area = '(0)::double precision';

    /**
     * @var \CepbrEstado
     *
     * @ORM\ManyToOne(targetEntity="CepbrEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="uf", referencedColumnName="uf")
     * })
     */
    private $uf;

    public function __toString() {
        return $this->cidade;
    }


    /**
     * Get idCidade
     *
     * @return integer 
     */
    public function getIdCidade()
    {
        return $this->idCidade;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return CepbrCidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string 
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set codIbge
     *
     * @param string $codIbge
     * @return CepbrCidade
     */
    public function setCodIbge($codIbge)
    {
        $this->codIbge = $codIbge;

        return $this;
    }

    /**
     * Get codIbge
     *
     * @return string 
     */
    public function getCodIbge()
    {
        return $this->codIbge;
    }

    /**
     * Set area
     *
     * @param float $area
     * @return CepbrCidade
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set uf
     *
     * @param \Admin\AdminBundle\Entity\CepbrEstado $uf
     * @return CepbrCidade
     */
    public function setUf(\Admin\AdminBundle\Entity\CepbrEstado $uf = null)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return \Admin\AdminBundle\Entity\CepbrEstado 
     */
    public function getUf()
    {
        return $this->uf;
    }
    
    public function returnEntity()
    {
        return "Admin\AdminBundle\Entity\CepbrCidade";
    }
}
