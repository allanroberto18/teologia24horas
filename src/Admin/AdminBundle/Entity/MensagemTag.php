<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MensagemTag
 *
 * @ORM\Table(name="mensagem_tag")
 * @ORM\Entity
 */
class MensagemTag extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="mensagem_tag_id_seq")
	 */
	private $id;
	
    /**
     * @var \Mensagem
     *
     * @ORM\ManyToOne(targetEntity="Mensagem", inversedBy="mensagemTag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_mensagem", referencedColumnName="id")
     * })
     */
    private $idMensagem;

    /**
     * @var \Tag
     *
     * @ORM\ManyToOne(targetEntity="Tag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tag", referencedColumnName="id")
     * })
     */
    private $idTag;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idMensagem
     *
     * @param \Admin\AdminBundle\Entity\Mensagem $idMensagem
     * @return MensagemTag
     */
    public function setIdMensagem(\Admin\AdminBundle\Entity\Mensagem $idMensagem = null)
    {
        $this->idMensagem = $idMensagem;

        return $this;
    }

    /**
     * Get idMensagem
     *
     * @return \Admin\AdminBundle\Entity\Mensagem 
     */
    public function getIdMensagem()
    {
        return $this->idMensagem;
    }

    /**
     * Set idTag
     *
     * @param \Admin\AdminBundle\Entity\Tag $idTag
     * @return MensagemTag
     */
    public function setIdTag(\Admin\AdminBundle\Entity\Tag $idTag = null)
    {
        $this->idTag = $idTag;

        return $this;
    }

    /**
     * Get idTag
     *
     * @return \Admin\AdminBundle\Entity\Tag 
     */
    public function getIdTag()
    {
        return $this->idTag;
    }
    
    public function returnEntity() {
        return '\Admin\AdminBundle\Entity\MensagemTag';
    }

}
