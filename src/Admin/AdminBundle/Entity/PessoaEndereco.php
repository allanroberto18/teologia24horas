<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PessoaEndereco
 *
 * @ORM\Table(name="pessoa_endereco")
 * @ORM\Entity
 */
class PessoaEndereco extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="pessoa_endereco_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * 
     * @ORM\Column(name="pais", type="string", length=155, nullable=true)
     */
    private $pais = 'Brasil';

    /**
     * @var string
     * @ORM\Column(name="cep", type="string", length=25, nullable=true)
     */
    private $cep;

    /**
     * @var string
     * @ORM\Column(name="endereco", type="text", nullable=true)
     */
    private $endereco;

    /**
     * @var string
     * @ORM\Column(name="bairro", type="string", length=255, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=255, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     * @ORM\Column(name="numero", type="string", length=50, nullable=true)
     */
    private $numero;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Cidade é obrigatório")
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Estado é obrigatório")
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var \Pessoa
     *
     * @ORM\OneToOne(targetEntity="Pessoa", inversedBy="endereco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set pais
     *
     * @param string $pais
     * @return PessoaEndereco
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set cep
     *
     * @param string $cep
     * @return PessoaEndereco
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string 
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     * @return PessoaEndereco
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string 
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return PessoaEndereco
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string 
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     * @return PessoaEndereco
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string 
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return PessoaEndereco
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return PessoaEndereco
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string 
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PessoaEndereco
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return PessoaEndereco
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null)
    {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa()
    {
        return $this->idPessoa;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\PessoaEndereco";
    }
}
