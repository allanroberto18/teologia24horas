<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Admin\AdminBundle\lib\Util;

/**
 * TabelaPreco
 *
 * @ORM\Table(name="tabela_preco")
 * @ORM\Entity
 */
class TabelaPreco extends EntityMaster {

    public function __toString() {
        return $this->titulo;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pessoa_tag_id_seq")
     */
    private $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=125, nullable=false)
     */
    private $titulo;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=2, nullable=false)
     */
    private $valor = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TabelaPreco
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return TabelaPreco
     */
    public function setValor($valor) {
        $this->valor = number_format((float)str_replace(",", ".", $valor), 2, '.', '');
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\TabelaPreco";
    }

}
