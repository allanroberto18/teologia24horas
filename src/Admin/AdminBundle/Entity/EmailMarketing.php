<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmailMarketing
 *
 * @ORM\Table(name="email_marketing", uniqueConstraints={@ORM\UniqueConstraint(name="un_email_marketing", columns={"identificador"})})
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"email", "identificador"},
 *     errorPath="email",
 *     message="Código identificador já pertence há um usuário"
 * )
 */
class EmailMarketing extends EntityMaster {

    public function __construct($identificador = null) {
        $this->setIdentificador($identificador);
        $this->tags = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="email_marketing_id_seq")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="identificador", type="string", length=32, nullable=false)
     */
    private $identificador;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo E-mail é obrigatório")
     * @Assert\Email(message="E-mail inválido")
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $datahoraAtualizacao;
    
    /**
     * @ORM\OneToOne(targetEntity="Pessoa", mappedBy="idEmailMarketing")
     */
    private $pessoa;

    /**
     * @ORM\OneToMany(targetEntity="EmailMarketingTag", mappedBy="idEmailMarketing", fetch="EXTRA_LAZY")
     */
    private $tags;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * Set identificador
     *
     * @param string $identificador
     * @return EmailMarketing
     */
    public function setIdentificador($identificador) {
        $this->identificador = empty($identificador) ? md5(date("YmdHis") . mt_rand()) : $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador() {
        return $this->identificador;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EmailMarketing
     */
    public function setEmail($email) {
        $this->email = strtolower($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return EmailMarketing
     */
    function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    function getPessoa() {
        return $this->pessoa;
    }

    function setPessoa($pessoa) {
        $this->pessoa = $pessoa;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return EmailMarketing
     */
    function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\EmailMarketing";
    }


}
