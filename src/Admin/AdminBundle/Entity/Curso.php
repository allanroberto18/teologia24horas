<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Curso
 *
 * @ORM\Table(name="curso")
 * @ORM\Entity
 */
class Curso extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="curso_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome do Curso é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     * @Gedmo\Slug(fields={"titulo"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="duracao", type="integer", nullable=false)
     */
    private $duracao = 28;

    /**
     * @var integer
     *
     * @ORM\Column(name="hora_aula", type="integer", nullable=false)
     */
    private $horaAula = 85;

    /**
     * @var integer
     *
     * @ORM\Column(name="credito", type="integer", nullable=false)
     */
    private $credito = 5;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Destaque do Curso é obrigatório")
     * @ORM\Column(name="destaque", type="string", length=80, nullable=true)
     */
    private $destaque;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Apresentação do Curso é obrigatório")
     * @ORM\Column(name="apresentacao", type="text", nullable=true)
     */
    private $apresentacao;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Chamada do Curso é obrigatório")
     * @ORM\Column(name="chamada", type="text", nullable=true)
     */
    private $chamada;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Descrição do Curso é obrigatório")
     * @ORM\Column(name="descricao", type="text", nullable=true)
     */
    private $descricao;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Objetivo do Curso é obrigatório")
     * @ORM\Column(name="objetivo", type="text", nullable=true)
     */
    private $objetivo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Área de Atuação do Curso é obrigatório")
     * @ORM\Column(name="area_atuacao", type="text", nullable=true)
     */
    private $areaAtuacao;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Público Alvo do Curso é obrigatório")
     * @ORM\Column(name="publico_alvo", type="text", nullable=true)
     */
    private $publicoAlvo;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Material Didático do Curso é obrigatório")
     * @ORM\Column(name="material_didatico", type="text", nullable=true)
     */
    private $materialDidatico;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Certificação do Curso é obrigatório")
     * @ORM\Column(name="certificacao", type="text", nullable=true)
     */
    private $certificacao;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Avaliação do Curso é obrigatório")
     * @ORM\Column(name="avaliacao", type="text", nullable=true)
     */
    private $avaliacao;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Tutoria do Curso é obrigatório")
     * @ORM\Column(name="tutoria", type="text", nullable=true)
     */
    private $tutoria;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Tecnologia do Curso é obrigatório")
     * @ORM\Column(name="tecnologia", type="text", nullable=true)
     */
    private $tecnologia;

    /**
     * @var \CursoCategoria
     *
     * @ORM\ManyToOne(targetEntity="CursoCategoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="id")
     * })
     */
    private $idCursoCategoria;

    /**
     * @var \TabelaPreco
     *
     * @ORM\ManyToOne(targetEntity="TabelaPreco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tabela_preco", referencedColumnName="id")
     * })
     */
    private $idTabelaPreco;

    /**
     * @ORM\OneToMany(targetEntity="Disciplina", mappedBy="idCurso")
     * @ORM\OrderBy({"aula" = "ASC"}) 
     */
    private $disciplina;

    /**
     * @ORM\OneToMany(targetEntity="CursoImagem", mappedBy="idCurso")
     * @ORM\OrderBy({"tipo" = "ASC"}) 
     */
    private $imagem;
    
    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id")
     * })
     */
    private $idPessoa;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ordem", type="integer", nullable=false)
     */
    private $ordem;

    /**
     * @ORM\OneToMany(targetEntity="CursoItem", mappedBy="idCurso")
     */
    private $item;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    function getDisciplina() {
        return $this->disciplina;
    }

    function setDisciplina($disciplina) {
        $this->disciplina = $disciplina;
    }

    function getImagem() {
        return $this->imagem;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    function getItem() {
        return $this->item;
    }

    function setItem($item) {
        $this->item = $item;
    }
    
    public function __get($name) {
        switch ($name) {
            case "Disciplina":
                $this->disciplina = new ArrayCollection();
                break;
            case "Imagem":
                $this->imagem = new ArrayCollection();
                break;
            case "Item":
                $this->item = new ArrayCollection();
                break;
        }
    }

    public function __toString() {
        return $this->getTitulo();
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Curso
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Curso
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set duracao
     *
     * @param integer $duracao
     * @return Curso
     */
    public function setDuracao($duracao) {
        $this->duracao = $duracao;

        return $this;
    }

    /**
     * Get duracao
     *
     * @return integer 
     */
    public function getDuracao() {
        return $this->duracao;
    }

    /**
     * Set horaAula
     *
     * @param integer $horaAula
     * @return Curso
     */
    public function setHoraAula($horaAula) {
        $this->horaAula = $horaAula;

        return $this;
    }

    /**
     * Get horaAula
     *
     * @return integer 
     */
    public function getHoraAula() {
        return $this->horaAula;
    }

    /**
     * Set credito
     *
     * @param integer $credito
     * @return Curso
     */
    public function setCredito($credito) {
        $this->credito = $credito;

        return $this;
    }

    /**
     * Get credito
     *
     * @return integer 
     */
    public function getCredito() {
        return $this->credito;
    }

    /**
     * Set destaque
     *
     * @param string $destaque
     * @return Curso
     */
    public function setDestaque($destaque) {
        $this->destaque = $destaque;

        return $this;
    }

    /**
     * Get destaque
     *
     * @return string 
     */
    public function getDestaque() {
        return $this->destaque;
    }

    /**
     * Set apresentacao
     *
     * @param string $apresentacao
     * @return Curso
     */
    public function setApresentacao($apresentacao) {
        $this->apresentacao = $apresentacao;

        return $this;
    }

    /**
     * Get apresentacao
     *
     * @return string 
     */
    public function getApresentacao() {
        return $this->apresentacao;
    }

    /**
     * Set chamada
     *
     * @param string $chamada
     * @return Curso
     */
    public function setChamada($chamada) {
        $this->chamada = $chamada;

        return $this;
    }

    /**
     * Get chamada
     *
     * @return string 
     */
    public function getChamada() {
        return $this->chamada;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Curso
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set objetivo
     *
     * @param string $objetivo
     * @return Curso
     */
    public function setObjetivo($objetivo) {
        $this->objetivo = $objetivo;

        return $this;
    }

    /**
     * Get objetivo
     *
     * @return string 
     */
    public function getObjetivo() {
        return $this->objetivo;
    }

    /**
     * Set areaAtuacao
     *
     * @param string $areaAtuacao
     * @return Curso
     */
    function setAreaAtuacao($areaAtuacao) {
        $this->areaAtuacao = $areaAtuacao;
    }

    /**
     * Get areaAtuacao
     *
     * @return string 
     */
    function getAreaAtuacao() {
        return $this->areaAtuacao;
    }

    /**
     * Set publicoAlvo
     *
     * @param string $publicoAlvo
     * @return Curso
     */
    public function setPublicoAlvo($publicoAlvo) {
        $this->publicoAlvo = $publicoAlvo;

        return $this;
    }

    /**
     * Get publicoAlvo
     *
     * @return string 
     */
    public function getPublicoAlvo() {
        return $this->publicoAlvo;
    }

    /**
     * Set materialDidatico
     *
     * @param string $materialDidatico
     * @return Curso
     */
    public function setMaterialDidatico($materialDidatico) {
        $this->materialDidatico = $materialDidatico;

        return $this;
    }

    /**
     * Get materialDidatico
     *
     * @return string 
     */
    public function getMaterialDidatico() {
        return $this->materialDidatico;
    }

    /**
     * Set certificacao
     *
     * @param string $certificacao
     * @return Curso
     */
    public function setCertificacao($certificacao) {
        $this->certificacao = $certificacao;

        return $this;
    }

    /**
     * Get certificacao
     *
     * @return string 
     */
    public function getCertificacao() {
        return $this->certificacao;
    }

    /**
     * Set avaliacao
     *
     * @param string $avaliacao
     * @return Curso
     */
    public function setAvaliacao($avaliacao) {
        $this->avaliacao = $avaliacao;

        return $this;
    }

    /**
     * Get avaliacao
     *
     * @return string 
     */
    public function getAvaliacao() {
        return $this->avaliacao;
    }

    /**
     * Set tutoria
     *
     * @param string $tutoria
     * @return Curso
     */
    public function setTutoria($tutoria) {
        $this->tutoria = $tutoria;

        return $this;
    }

    /**
     * Get tutoria
     *
     * @return string 
     */
    public function getTutoria() {
        return $this->tutoria;
    }

    /**
     * Set tecnologia
     *
     * @param string $tecnologia
     * @return Curso
     */
    public function setTecnologia($tecnologia) {
        $this->tecnologia = $tecnologia;

        return $this;
    }

    /**
     * Get tecnologia
     *
     * @return string 
     */
    public function getTecnologia() {
        return $this->tecnologia;
    }

    /**
     * Set idCursoCategoria
     *
     * @param \Admin\AdminBundle\Entity\CursoCategoria $idCursoCategoria
     * @return Curso
     */
    public function setIdCursoCategoria(\Admin\AdminBundle\Entity\CursoCategoria $idCursoCategoria = null) {
        $this->idCursoCategoria = $idCursoCategoria;

        return $this;
    }

    /**
     * Get idCursoCategoria
     *
     * @return \Admin\AdminBundle\Entity\CursoCategoria 
     */
    public function getIdCursoCategoria() {
        return $this->idCursoCategoria;
    }

    /**
     * Set idTabelaPreco
     *
     * @param \Admin\AdminBundle\Entity\TabelaPreco $idTabelaPreco
     * @return Curso
     */
    public function setIdTabelaPreco(\Admin\AdminBundle\Entity\TabelaPreco $idTabelaPreco = null) {
        $this->idTabelaPreco = $idTabelaPreco;

        return $this;
    }

    /**
     * Get idTabelaPreco
     *
     * @return \Admin\AdminBundle\Entity\TabelaPreco 
     */
    public function getIdTabelaPreco() {
        return $this->idTabelaPreco;
    }

    function getIdPessoa() {
        return $this->idPessoa;
    }

    function setIdPessoa($idPessoa) {
        $this->idPessoa = $idPessoa;
    }

    function getOrdem() {
        return $this->ordem;
    }

    function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Curso";
    }

}
