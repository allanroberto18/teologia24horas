<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AulaAcesso
 *
 * @ORM\Table(name="aula_acesso")
 * @ORM\Entity
 */
class AulaAcesso extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="aula_acesso_id_seq")
	 */
	private $id;
	
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \Aula
     *
     * @ORM\ManyToOne(targetEntity="Aula", inversedBy="acesso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aula", referencedColumnName="id")
     * })
     */
    private $idAula;

    /**
     * @var \Contrato
     *
     * @ORM\ManyToOne(targetEntity="Contrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return AulaAcesso
     */
    public function setDatahoraCadastro($datahoraCadastro)
    {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro()
    {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return AulaAcesso
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao)
    {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao()
    {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idAula
     *
     * @param \Admin\AdminBundle\Entity\Aula $idAula
     * @return AulaAcesso
     */
    public function setIdAula(\Admin\AdminBundle\Entity\Aula $idAula = null)
    {
        $this->idAula = $idAula;

        return $this;
    }

    /**
     * Get idAula
     *
     * @return \Admin\AdminBundle\Entity\Aula 
     */
    public function getIdAula()
    {
        return $this->idAula;
    }

    /**
     * Set idContrato
     *
     * @param \Admin\AdminBundle\Entity\Contrato $idContrato
     * @return AulaAcesso
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato = null)
    {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return \Admin\AdminBundle\Entity\Contrato 
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\AulaAcesso";
    }

}
