<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Mensagem
 *
 * @ORM\Table(name="mensagem")
 * @ORM\Entity
 */
class Mensagem extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="mensagem_id_seq")
	 */
	private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório", groups={"admin"})
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="O Conteúdo da Mensagem é obrigatório", groups={"admin", "ava"})
     * @ORM\Column(name="conteudo", type="text", nullable=true)
     */
    private $conteudo;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;
    
    /**
     * @var \MensagemPessoa
     * @ORM\OneToMany(targetEntity="MensagemPessoa", mappedBy="idMensagem", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"status" = "DESC"})
     */
    private $mensagemPessoa;
    
    /**
     * @var \MensagemEmailMarketing
     * @ORM\OneToMany(targetEntity="MensagemEmailMarketing", mappedBy="idMensagem", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"status" = "DESC"})
     */
    private $mensagemEmailMarketing;

    /**
     * @var \MensagemTag
     * @ORM\OneToMany(targetEntity="MensagemTag", mappedBy="idMensagem", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"status" = "DESC"})
     */
    private $mensagemTag;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    function getMensagemPessoa() {
        return $this->mensagemPessoa;
    }

    function setMensagemPessoa($mensagemPessoa) {
        $this->mensagemPessoa = $mensagemPessoa;
    }
    
    function getMensagemEmailMarketing() {
        return $this->mensagemEmailMarketing;
    }

    function setMensagemEmailMarketing($mensagemEmailMarketing) {
        $this->mensagemEmailMarketing = $mensagemEmailMarketing;
    }
    
    function getMensagemTag() {
        return $this->mensagemTag;
    }

    function setMensagemTag($mensagemTag) {
        $this->mensagemTag = $mensagemTag;
    }
            
    public function __construct() {
        $this->mensagemPessoa = new ArrayCollection();
        $this->mensagemEmailMarketing = new ArrayCollection();
        $this->mensagemTag = new ArrayCollection();
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Mensagem
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set conteudo
     *
     * @param string $conteudo
     * @return Mensagem
     */
    public function setConteudo($conteudo)
    {
        $this->conteudo = $conteudo;

        return $this;
    }

    /**
     * Get conteudo
     *
     * @return string 
     */
    public function getConteudo()
    {
        return $this->conteudo;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return Mensagem
     */
    public function setDatahoraCadastro($datahoraCadastro)
    {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro()
    {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return Mensagem
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao)
    {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao()
    {
        return $this->datahoraAtualizacao;
    }

    public function returnEntity() {
        return '\Admin\AdminBundle\Entity\Mensagem';
    }
}
