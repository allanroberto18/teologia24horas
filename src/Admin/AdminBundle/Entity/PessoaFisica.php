<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;

class PessoaFisica extends EntityMaster {

    public function __construct() {
        $this->tipoPessoa = 1;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pessoa_id_seq")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_pessoa", type="integer", nullable=false)
     */
    private $tipoPessoa = 1;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo CPF é obrigatório")
     * @ORM\Column(name="pf_cpf", type="string", length=25, nullable=true)
     */
    private $pfCpf;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome é obrigatório")
     * @Assert\Length(
     *      min = "3",
     *      minMessage = "O campo Nome deve conter mais de 3 letras"
     * )
     * @ORM\Column(name="pf_nome", type="string", length=255, nullable=true)
     */
    private $pfNome;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message="O campo Data de Nascimento é obrigatório")
     * @ORM\Column(name="pf_data_nascimento", type="date", nullable=true)
     */
    private $pfDataNascimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="pf_sexo", type="integer", nullable=true)
     */
    private $pfSexo = '1';

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Celular é obrigatório")
     * @ORM\Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo E-mail é obrigatório")
     * @Assert\Email(message="E-mail inválido")
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Dica de Senha é obrigatório", groups={"auth"})
     * @ORM\Column(name="dica_senha", type="string", length=255, nullable=true)
     */
    private $dicaSenha;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Senha é obrigatório", groups={"auth"})
     * @ORM\Column(name="senha", type="string", length=255, nullable=true)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="biografia", type="text", nullable=true)
     */
    private $biografia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $datahoraAtualizacao;

    /**
     * @var string
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var \EmailMarketing
     *
     * @ORM\ManyToOne(targetEntity="EmailMarketing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_marketing", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEmailMarketing;

    public function __toString() {
        return $this->pfNome;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set tipoPessoa
     *
     * @param integer $tipoPessoa
     * @return Pessoa
     */
    public function setTipoPessoa($tipoPessoa) {
        $this->tipoPessoa = $tipoPessoa;

        return $this;
    }
    
    /**
     * Get tipoPessoa
     *
     * @return integer 
     */
    public function getTipoPessoa() {
        return $this->tipoPessoa;
    }

    /**
     * Set pfCpf
     *
     * @param string $pfCpf
     * @return Pessoa
     */
    public function setPfCpf($pfCpf) {
        $this->pfCpf = $pfCpf;

        return $this;
    }

    /**
     * Get pfCpf
     *
     * @return string 
     */
    public function getPfCpf() {
        return $this->pfCpf;
    }

    /**
     * Set pfNome
     *
     * @param string $pfNome
     * @return Pessoa
     */
    public function setPfNome($pfNome) {
        $this->pfNome = $pfNome;

        return $this;
    }

    /**
     * Get pfNome
     *
     * @return string 
     */
    public function getPfNome() {
        return $this->pfNome;
    }

    /**
     * Set pfDataNascimento
     *
     * @param \DateTime $pfDataNascimento
     * @return Pessoa
     */
    public function setPfDataNascimento($pfDataNascimento) {
        $this->pfDataNascimento = $pfDataNascimento;

        return $this;
    }

    /**
     * Get pfDataNascimento
     *
     * @return \DateTime 
     */
    public function getPfDataNascimento() {
        return $this->pfDataNascimento;
    }

    /**
     * Set pfSexo
     *
     * @param integer $pfSexo
     * @return Pessoa
     */
    public function setPfSexo($pfSexo) {
        $this->pfSexo = $pfSexo;

        return $this;
    }

    /**
     * Get pfSexo
     *
     * @return integer 
     */
    public function getPfSexo() {
        return $this->pfSexo;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return Pessoa
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular() {
        return $this->celular;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Pessoa
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set dicaSenha
     *
     * @param string $dicaSenha
     * @return Pessoa
     */
    public function setDicaSenha($dicaSenha) {
        $this->dicaSenha = $dicaSenha;

        return $this;
    }

    /**
     * Get dicaSenha
     *
     * @return string 
     */
    public function getDicaSenha() {
        return $this->dicaSenha;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return Pessoa
     */
    public function setSenha($senha) {
        $this->senha = md5($senha);

        return $this;
    }

    /**
     * Get senha
     *
     * @return string 
     */
    public function getSenha() {
        return $this->senha;
    }

    /**
     * Set biografia
     *
     * @param string $biografia
     * @return Pessoa
     */
    public function setBiografia($biografia) {
        $this->biografia = $biografia;

        return $this;
    }

    /**
     * Get biografia
     *
     * @return string 
     */
    public function getBiografia() {
        return $this->biografia;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return EmailMarketing
     */
    function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return PessoaFisica
     */
    function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;
    }

    function getFoto() {
        return $this->foto;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    public function getAbsolutePath() {
        return null === $this->foto ? null : $this->getUploadRootDir() . '/' . $this->foto;
    }

    public function getWebPath() {
        return null === $this->foto ? null : $this->getUploadDir() . '/' . $this->foto;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'] . '/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/usuarios';
    }

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    private $temp;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->foto)) {
            // store the old name to delete after the update
            $this->temp = $this->foto;
            $this->foto = null;
        } else {
            $this->foto = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->foto = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->foto);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            //$this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    /**
     * Set idEmailMarketing
     *
     * @param \Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing
     * @return Pessoa
     */
    public function setIdEmailMarketing(\Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing = null) {
        $this->idEmailMarketing = $idEmailMarketing;

        return $this;
    }

    /**
     * Get idEmailMarketing
     *
     * @return \Admin\AdminBundle\Entity\EmailMarketing 
     */
    public function getIdEmailMarketing() {
        return $this->idEmailMarketing;
    }

    /**
     * Nome da entidade
     * @return string
     */
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\PessoaFisica";
    }

    /**
     * @Assert\True(message = "CPF inválido")
     */
    public function isCPF() {
        return \Admin\AdminBundle\lib\Validacao::validaCpf($this->pfCpf);
    }
    
    public function returnSexo() {
        switch ($this->pfSexo) {
            case 1:
                $result = "Masculino";
                break;
            case 2:
                $result = "Feminino";
                break;
        }
        return $result;
    }

}
