<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MensagemEmailMarketing
 *
 * @ORM\Table(name="mensagem_email_marketing")
 * @ORM\Entity
 */
class MensagemEmailMarketing extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="mensagem_email_marketing_id_seq")
	 */
	private $id;
	
    /**
     * @var \Mensagem
     *
     * @ORM\ManyToOne(targetEntity="Mensagem", inversedBy="mensagemEmailMarketing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_mensagem", referencedColumnName="id")
     * })
     */
    private $idMensagem;

    /**
     * @var \EmailMarketing
     *
     * @ORM\ManyToOne(targetEntity="EmailMarketing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_marketing", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEmailMarketing;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idMensagem
     *
     * @param \Admin\AdminBundle\Entity\Mensagem $idMensagem
     * @return MensagemEmailMarketing
     */
    public function setIdMensagem(\Admin\AdminBundle\Entity\Mensagem $idMensagem = null)
    {
        $this->idMensagem = $idMensagem;

        return $this;
    }

    /**
     * Get idMensagem
     *
     * @return \Admin\AdminBundle\Entity\Mensagem 
     */
    public function getIdMensagem()
    {
        return $this->idMensagem;
    }

    /**
     * Set idEmailMarketing
     *
     * @param \Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing
     * @return MensagemEmailMarketing
     */
    public function setIdEmailMarketing(\Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing = null)
    {
        $this->idEmailMarketing = $idEmailMarketing;

        return $this;
    }

    /**
     * Get idEmailMarketing
     *
     * @return \Admin\AdminBundle\Entity\EmailMarketing 
     */
    public function getIdEmailMarketing()
    {
        return $this->idEmailMarketing;
    }
    
    public function returnEntity() {
        return '\Admin\AdminBundle\Entity\MensagemEmailMarketing';
    }
}
