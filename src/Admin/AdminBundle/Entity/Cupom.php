<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cupom
 *
 * @ORM\Table(name="cupom")
 * @ORM\Entity
 */
class Cupom extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="cupom_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     *
     * @ORM\Column(name="identificador", type="string", length=32, nullable=false)
     */
    private $identificador;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=false)
     */
    private $desconto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_validade", type="date", nullable=false)
     */
    private $dataValidade;

    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set identificador
     *
     * @param string $identificador
     * @return Cupom
     */
    public function setIdentificador($identificador) {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador() {
        return $this->identificador;
    }

    /**
     * Set desconto
     *
     * @param float $desconto
     * @return Cupom
     */
    public function setDesconto($desconto) {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return float 
     */
    public function getDesconto() {
        return $this->desconto;
    }

    /**
     * Set dataValidade
     *
     * @param \DateTime $dataValidade
     * @return Cupom
     */
    public function setDataValidade($dataValidade) {
        $this->dataValidade = $dataValidade;

        return $this;
    }

    /**
     * Get dataValidade
     *
     * @return \DateTime 
     */
    public function getDataValidade() {
        return $this->dataValidade;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return Cupom
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null) {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa() {
        return $this->idPessoa;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Cupom";
    }
}
