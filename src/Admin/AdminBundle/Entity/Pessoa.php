<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Pessoa
 * @ORM\Table(
 *      name="pessoa", 
 *      uniqueConstraints={@ORM\UniqueConstraint(name="un_email", columns={"email"})}, 
 *      indexes={@ORM\Index(name="IDX_1CDFAB8272D43DCB", columns={"id_email_marketing"})}
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="E-mail informado já pertence há um usuário"
 * )
 */
class Pessoa extends EntityMaster {

    public function __toString() {
        return $this->tipoPessoa = 1 ? $this->pfNome : $this->pjRazaoSocial;
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="pessoa_id_seq")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_pessoa", type="integer", nullable=false)
     */
    private $tipoPessoa = 1;

    /**
     * @var string
     * @ORM\Column(name="pf_cpf", type="string", length=25, nullable=true)
     */
    private $pfCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_cnpj", type="string", length=25, nullable=true)
     */
    private $pjCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_insc_estadual", type="string", length=25, nullable=true)
     */
    private $pjInscEstadual;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_insc_municipal", type="string", length=25, nullable=true)
     */
    private $pjInscMunicipal;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome é obrigatório", groups={"perfil", "perfil_edit", "perfil_admin"})
     * @Assert\Length(
     *      min = "3",
     *      minMessage = "O campo Nome deve conter mais de 3 letras"
     * )
     * @ORM\Column(name="pf_nome", type="string", length=255, nullable=true)
     */
    private $pfNome;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_razao_social", type="string", length=255, nullable=true)
     */
    private $pjRazaoSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_nome_fantasia", type="string", length=255, nullable=true)
     */
    private $pjNomeFantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_contato", type="string", length=255, nullable=true)
     */
    private $pjContato;

    /**
     * @var string
     *
     * @ORM\Column(name="pj_site", type="string", length=255, nullable=true)
     */
    private $pjSite;

    /**
     * @var \DateTime
     * @ORM\Column(name="pf_data_nascimento", type="date", nullable=true)
     */
    private $pfDataNascimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="pf_sexo", type="integer", nullable=true)
     */
    private $pfSexo = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=155, nullable=true)
     */
    private $pais = 'Brasil';

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Cidade é obrigatório", groups={"perfil", "perfil_edit"})
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Estado é obrigatório", groups={"perfil", "perfil_edit"})
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string
     * @ORM\Column(name="celular", type="string", length=15, nullable=true)
     */
    private $celular;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo E-mail é obrigatório", groups={"perfil"})
     * @Assert\Email(message="E-mail inválido", groups={"perfil"})
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Dica de Senha é obrigatório", groups={"perfil", "auth"})
     * @ORM\Column(name="dica_senha", type="string", length=255, nullable=true)
     */
    private $dicaSenha;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Senha é obrigatório", groups={"perfil", "auth"})
     * @ORM\Column(name="senha", type="string", length=255, nullable=true)
     */
    private $senha;
    
    /**
     * @var type 
     * @Assert\NotBlank(message="O campo Confirmação de Senha é obrigatório", groups={"perfil", "auth"})
     */
    private $confirmaSenha;

    /**
     * @var string
     *
     * @ORM\Column(name="biografia", type="text", nullable=true)
     */
    private $biografia;

    /**
     * @var string
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \EmailMarketing
     *
     * @ORM\OneToOne(targetEntity="EmailMarketing", inversedBy="pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_marketing", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEmailMarketing;

    /**
     * @Assert\File(
     *     maxSize = "1000000",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     mimeTypesMessage = "A imagem deve ter extensão png ou jpg",
     *     groups={"file"}
     * )
     */
    private $file;
    private $temp;

    /**
     * @ORM\OneToMany(targetEntity="Contrato", mappedBy="idPessoa", fetch="EXTRA_LAZY" )
     * @ORM\OrderBy({"id" = "DESC"});
     */
    protected $contrato;

    /**
     * @ORM\OneToMany(targetEntity="Nucleo", mappedBy="idPessoa", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"titulo" = "ASC"});
     */
    protected $nucleos;

    /**
     * @ORM\OneToMany(targetEntity="PessoaTag", mappedBy="idPessoa", fetch="EXTRA_LAZY")
     */
    protected $tags;

    function getContrato() {
        return $this->contrato;
    }

    function setContrato($contrato) {
        $this->contrato = $contrato;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getNucleos()
    {
        return $this->nucleos;
    }

    /**
     * @param mixed $nucleos
     */
    public function setNucleos($nucleos)
    {
        $this->nucleos = $nucleos;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function __construct()
    {
        $this->contrato = new ArrayCollection();
        $this->nucleos = new ArrayCollection();
    }

    /**
     * Set tipoPessoa
     *
     * @param integer $tipoPessoa
     * @return Pessoa
     */
    public function setTipoPessoa($tipoPessoa) {
        $this->tipoPessoa = $tipoPessoa;

        return $this;
    }

    /**
     * Get tipoPessoa
     *
     * @return integer 
     */
    public function getTipoPessoa() {
        return $this->tipoPessoa;
    }

    /**
     * Set pfCpf
     *
     * @param string $pfCpf
     * @return Pessoa
     */
    public function setPfCpf($pfCpf) {
        $pfCpf = str_replace(".", "", $pfCpf);
        $pfCpf = str_replace("-", "", $pfCpf);
        $this->pfCpf = $pfCpf;

        return $this;
    }

    /**
     * Get pfCpf
     *
     * @return string 
     */
    public function getPfCpf() {
        return $this->pfCpf;
    }

    /**
     * Set pjCnpj
     *
     * @param string $pjCnpj
     * @return Pessoa
     */
    public function setPjCnpj($pjCnpj) {
        $pjCnpj = str_replace(".", "", $pjCnpj);
        $pjCnpj = str_replace("-", "", $pjCnpj);
        $this->pjCnpj = $pjCnpj;

        return $this;
    }

    /**
     * Get pjCnpj
     *
     * @return string 
     */
    public function getPjCnpj() {
        return $this->pjCnpj;
    }

    /**
     * Set pjInscEstadual
     *
     * @param string $pjInscEstadual
     * @return Pessoa
     */
    public function setPjInscEstadual($pjInscEstadual) {
        $this->pjInscEstadual = $pjInscEstadual;

        return $this;
    }

    /**
     * Get pjInscEstadual
     *
     * @return string 
     */
    public function getPjInscEstadual() {
        return $this->pjInscEstadual;
    }

    /**
     * Set pjInscMunicipal
     *
     * @param string $pjInscMunicipal
     * @return Pessoa
     */
    public function setPjInscMunicipal($pjInscMunicipal) {
        $this->pjInscMunicipal = $pjInscMunicipal;

        return $this;
    }

    /**
     * Get pjInscMunicipal
     *
     * @return string 
     */
    public function getPjInscMunicipal() {
        return $this->pjInscMunicipal;
    }

    /**
     * Set pfNome
     *
     * @param string $pfNome
     * @return Pessoa
     */
    public function setPfNome($pfNome) {
        $this->pfNome = ucwords(strtolower($pfNome));

        return $this;
    }

    /**
     * Get pfNome
     *
     * @return string 
     */
    public function getPfNome() {
        return $this->pfNome;
    }

    /**
     * Set pjRazaoSocial
     *
     * @param string $pjRazaoSocial
     * @return Pessoa
     */
    public function setPjRazaoSocial($pjRazaoSocial) {
        $this->pjRazaoSocial = ucwords(strtolower($pjRazaoSocial));

        return $this;
    }

    /**
     * Get pjRazaoSocial
     *
     * @return string 
     */
    public function getPjRazaoSocial() {
        return $this->pjRazaoSocial;
    }

    /**
     * Set pjNomeFantasia
     *
     * @param string $pjNomeFantasia
     * @return Pessoa
     */
    public function setPjNomeFantasia($pjNomeFantasia) {
        $this->pjNomeFantasia = ucwords(strtolower($pjNomeFantasia));

        return $this;
    }

    /**
     * Get pjNomeFantasia
     *
     * @return string 
     */
    public function getPjNomeFantasia() {
        return $this->pjNomeFantasia;
    }

    /**
     * Set pjContato
     *
     * @param string $pjContato
     * @return Pessoa
     */
    public function setPjContato($pjContato) {
        $this->pjContato = ucwords(strtolower($pjContato));

        return $this;
    }

    /**
     * Get pjContato
     *
     * @return string 
     */
    public function getPjContato() {
        return $this->pjContato;
    }

    /**
     * Set pjSite
     *
     * @param string $pjSite
     * @return Pessoa
     */
    public function setPjSite($pjSite) {
        $this->pjSite = strtolower($pjSite);

        return $this;
    }

    /**
     * Get pjSite
     *
     * @return string 
     */
    public function getPjSite() {
        return $this->pjSite;
    }

    /**
     * Set pfDataNascimento
     *
     * @param \DateTime $pfDataNascimento
     * @return Pessoa
     */
    public function setPfDataNascimento($pfDataNascimento) {
        $this->pfDataNascimento = $pfDataNascimento;

        return $this;
    }

    /**
     * Get pfDataNascimento
     *
     * @return \DateTime 
     */
    public function getPfDataNascimento() {
        return $this->pfDataNascimento;
    }

    /**
     * Set pfSexo
     *
     * @param integer $pfSexo
     * @return Pessoa
     */
    public function setPfSexo($pfSexo) {
        $this->pfSexo = $pfSexo;

        return $this;
    }

    /**
     * Get pfSexo
     *
     * @return integer 
     */
    public function getPfSexo() {
        return $this->pfSexo;
    }

    /**
     * Set pais
     *
     * @param string $pais
     * @return PessoaEndereco
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return PessoaEndereco
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PessoaEndereco
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return Pessoa
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular() {
        return $this->celular;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Pessoa
     */
    public function setEmail($email) {
        if (empty($email)) {
            $email = strtolower($this->getIdEmailMarketing()->getEmail());
        }
        $this->email = strtolower($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set dicaSenha
     *
     * @param string $dicaSenha
     * @return Pessoa
     */
    public function setDicaSenha($dicaSenha) {
        $this->dicaSenha = $dicaSenha;

        return $this;
    }

    /**
     * Get dicaSenha
     *
     * @return string 
     */
    public function getDicaSenha() {
        return $this->dicaSenha;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return Pessoa
     */
    public function setSenha($senha = null) {
        if (empty($senha)) {
            switch ($this->tipoPessoa) {
                case 1:
                    $senha = $this->pfCpf;
                    $this->dicaSenha = "CPF do usuário";
                    break;
                case 2:
                    $senha = $this->pjCnpj;
                    $this->dicaSenha = "CNPJ do usuário";
                    break;
            }
        }
        $this->senha = md5($senha);

        return $this;
    }

    /**
     * Get senha
     *
     * @return string 
     */
    public function getSenha() {
        return $this->senha;
    }
    
    function getConfirmaSenha() {
        return $this->confirmaSenha;
    }

    function setConfirmaSenha($confirmaSenha) {
        $this->confirmaSenha = md5($confirmaSenha);
    }
    
    /**
     * Set biografia
     *
     * @param string $biografia
     * @return Pessoa
     */
    public function setBiografia($biografia) {
        $this->biografia = $biografia;

        return $this;
    }

    /**
     * Get biografia
     *
     * @return string 
     */
    public function getBiografia() {
        return $this->biografia;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Pessoa
     */
    public function setFoto($foto) {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto() {
        return $this->foto;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return Pessoa
     */
    public function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return Pessoa
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    public function getAbsolutePath() {
        return null === $this->foto ? null : $this->getUploadRootDir() . '/' . $this->foto;
    }

    public function getWebPath() {
        return null === $this->foto ? null : $this->getUploadDir() . '/' . $this->foto;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return $_SERVER['DOCUMENT_ROOT'] . '/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/usuarios';
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->foto)) {
            // store the old name to delete after the update
            $this->temp = $this->foto;
            $this->foto = null;
        } else {
            $this->foto = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->foto = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->foto);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            //$this->temp = null;
        }
        $this->file = null;
    }

    

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idEmailMarketing
     *
     * @param \Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing
     * @return Pessoa
     */
    public function setIdEmailMarketing(\Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing = null) {
        $this->idEmailMarketing = $idEmailMarketing;

        return $this;
    }

    /**
     * Get idEmailMarketing
     *
     * @return \Admin\AdminBundle\Entity\EmailMarketing 
     */
    public function getIdEmailMarketing() {
        return $this->idEmailMarketing;
    }


    public function isCPF() {
        return \Admin\AdminBundle\lib\Validacao::validaCpf($this->pfCpf);
    }
    
    /**
     * @Assert\True(message = "Os campos senha e confirmação de senha deve ser iguais", groups={"perfil", "auth"})
     */
    public function isPasswordValid()
    {
        if ($this->senha == $this->confirmaSenha) {
            return true;
        }
        return false;
    }

    public function returnSexo() {
        switch ($this->pfSexo) {
            case 1:
                $result = "Masculino";
                break;
            case 2:
                $result = "Feminino";
                break;
        }
        return $result;
    }

    public function returnEntity() {
        return "\Admin\AdminBundle\Entity\Pessoa";
    }

}
