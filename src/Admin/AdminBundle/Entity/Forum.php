<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Forum
 *
 * @ORM\Table(name="forum")
 * @ORM\Entity
 */
class Forum extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="forum_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Tema é obrigatório")
     * @ORM\Column(name="tema", type="text", nullable=true)
     */
    private $tema;

    /**
     *  @ORM\Column(name="tipo", type="integer")
     */
    private $tipo = 1;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;


    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;


    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id")
     * })
     */
    private $idPessoa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }


    /**
     * Set tema
     *
     * @param string $tema
     * @return Forum
     */
    public function setTema($tema) {
        $this->tema = $tema;

        return $this;
    }

    /**
     * Get tema
     *
     * @return string 
     */
    public function getTema() {
        return $this->tema;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return Forum
     */
    public function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return Forum
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     * @return Forum
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null) {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCurso() {
        return $this->idCurso;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return Forum
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null) {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso
     */
    public function getIdPessoa() {
        return $this->idCurso;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Forum";
    }
}
