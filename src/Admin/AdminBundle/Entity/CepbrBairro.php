<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CepbrBairro
 *
 * @ORM\Table(name="cepbr_bairro")
 * @ORM\Entity
 */
class CepbrBairro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_bairro", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBairro;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=500, nullable=true)
     */
    private $bairro;

    /**
     * @var \CepbrCidade
     *
     * @ORM\ManyToOne(targetEntity="CepbrCidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cidade", referencedColumnName="id_cidade")
     * })
     */
    private $idCidade;



    /**
     * Get idBairro
     *
     * @return integer 
     */
    public function getIdBairro()
    {
        return $this->idBairro;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return CepbrBairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string 
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set idCidade
     *
     * @param \Admin\AdminBundle\Entity\CepbrCidade $idCidade
     * @return CepbrBairro
     */
    public function setIdCidade(\Admin\AdminBundle\Entity\CepbrCidade $idCidade = null)
    {
        $this->idCidade = $idCidade;

        return $this;
    }

    /**
     * Get idCidade
     *
     * @return \Admin\AdminBundle\Entity\CepbrCidade 
     */
    public function getIdCidade()
    {
        return $this->idCidade;
    }
    
    /**
     * Nome da entidade
     * @return string
     */
    public function returnEntity()
    {
        return "Admin\AdminBundle\Entity\CepbrBairro";
    }
}
