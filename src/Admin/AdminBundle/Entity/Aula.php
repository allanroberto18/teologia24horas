<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Aula
 *
 * @ORM\Table(name="aula")
 * @ORM\Entity
 */
class Aula extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="aula_id_seq")
	 */
	private $id;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_conteudo", type="integer", nullable=true)
     */
    private $tipoConteudo = 1;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     * @ORM\Column(name="resumo", type="text", nullable=true)
     */
    private $resumo;

    /**
     * @var string
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="ordem", type="integer", nullable=true)
     */
    private $ordem;

    /**
     * @var string
     *
     * @ORM\Column(name="duracao", type="string", length=255, nullable=true)
     */
    private $duracao;

    /**
     * @var \Disciplina
     *
     * @ORM\ManyToOne(targetEntity="Disciplina", inversedBy="conteudoAula")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_disciplina", referencedColumnName="id")
     * })
     */
    private $idDisciplina;
    
    /**
     * @ORM\OneToMany(targetEntity="AulaAcesso", mappedBy="idAula")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $acesso;

    public function __toString() {
        return $this->titulo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set tipoConteudo
     *
     * @param integer $tipoConteudo
     * @return Aula
     */
    public function setTipoConteudo($tipoConteudo) {
        $this->tipoConteudo = $tipoConteudo;

        return $this;
    }

    /**
     * Get tipoConteudo
     *
     * @return integer 
     */
    public function getTipoConteudo() {
        return $this->tipoConteudo;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Aula
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    public function getSlug() {
        return \Admin\AdminBundle\lib\Util::slug($this->titulo);
    }
    
    /**
     * Set titulo
     *
     * @param string $resumo
     * @return Aula
     */
    public function setResumo($resumo) {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * Get resumo
     *
     * @return string 
     */
    public function getResumo() {
        return $this->resumo;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Aula
     */
    public function setLink($link) {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink() {
        return $this->link;
    }
    
    /**
     * Set ordem
     *
     * @param integer $ordem
     * @return Aula
     */
    public function setOrdem($ordem) {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return integer
     */
    public function getOrdem() {
        return $this->ordem;
    }

    /**
     * Set duracao
     *
     * @param string $duracao
     * @return Aula
     */
    public function setDuracao($duracao) {
        $this->duracao = $duracao;

        return $this;
    }

    /**
     * Get duracao
     *
     * @return string 
     */
    public function getDuracao() {
        return $this->duracao;
    }

    /**
     * Set idDisciplina
     *
     * @param \Admin\AdminBundle\Entity\Disciplina $idDisciplina
     * @return Aula
     */
    public function setIdDisciplina(\Admin\AdminBundle\Entity\Disciplina $idDisciplina = null) {
        $this->idDisciplina = $idDisciplina;

        return $this;
    }

    /**
     * Get idDisciplina
     *
     * @return \Admin\AdminBundle\Entity\Disciplina 
     */
    public function getIdDisciplina() {
        return $this->idDisciplina;
    }

    function getAcesso() {
        return $this->acesso;
    }

    function setAcesso($acesso) {
        $this->acesso = $acesso;
    }

    /**
     * Nome da entidade
     * @return string
     */
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Aula";
    }

    public function returnTipoConteudo() {
        switch ($this->tipoConteudo) {
            case 1:
                $result = "<span class='fa fa-video-camera'></span> " . $this->duracao ;
                break;
            case 2: 
                $result = "<span class='fa fa-video-camera'></span> " . $this->duracao;
                break;
            case 3:
                $result = "<span class='fa fa-file-text'></span> " . $this->duracao . " páginas";
                break;
            case 4:
                $result = "<span class='fa fa-list-ul'></span> Avaliação";
                break;
        }
        return $result;
    }

}
