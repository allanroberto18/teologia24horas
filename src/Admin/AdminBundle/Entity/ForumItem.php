<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ForumItem
 *
 * @ORM\Table(name="forum_item")
 * @ORM\Entity
 */
class ForumItem extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="forum_item_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Comentário é obrigatório")
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \Contrato
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Contrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * @var \Forum
     *
     * @ORM\ManyToOne(targetEntity="Forum")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_forum", referencedColumnName="id")
     * })
     */
    private $idForum;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ForumItem
     */
    public function setComentario($comentario) {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario() {
        return $this->comentario;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return ForumItem
     */
    public function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return ForumItem
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idContrato
     *
     * @param \Admin\AdminBundle\Entity\Contrato $idContrato
     * @return ForumItem
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato = null) {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return \Admin\AdminBundle\Entity\Contrato 
     */
    public function getIdContrato() {
        return $this->idContrato;
    }

    /**
     * Set idForum
     *
     * @param \Admin\AdminBundle\Entity\Forum $idForum
     * @return ForumItem
     */
    public function setIdForum(\Admin\AdminBundle\Entity\Forum $idForum = null) {
        $this->idForum = $idForum;

        return $this;
    }

    /**
     * Get idForum
     *
     * @return \Admin\AdminBundle\Entity\Forum 
     */
    public function getIdForum() {
        return $this->idForum;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\ForumItem";
    }

    public function returnStatus() {
        switch ($this->status) {
            case 1:
                print "<div style='margin-top: 5px;'><span class='label label-success'><span class='fa fa-check'></span> Liberado</span></div>";
                break;
            case 2:
                print "<div style='margin-top: 5px;'><span class='label label-danger'><span class='fa fa-close'></span> Bloqueado</span></div>";
                break;
        }
    }
}
