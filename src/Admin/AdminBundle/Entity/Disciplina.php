<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Disciplina
 *
 * @ORM\Table(name="disciplina")
 * @ORM\Entity
 */
class Disciplina extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="disciplina_id_seq")
	 */
	private $id;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="semana", type="integer", nullable=true)
     */
    private $semana = 1;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Nome é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="aula", type="integer", nullable=true)
     */
    private $aula = 1;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Descrição da Disciplina é obrigatório")
     * @ORM\Column(name="descricao", type="text", nullable=true)
     */
    private $descricao;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="disciplina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;

    /**
     * @ORM\OneToMany(targetEntity="Aula", mappedBy="idDisciplina")
     * @ORM\OrderBy({"ordem" = "ASC"})
     */
    private $conteudoAula;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    function setConteudoAula($conteudoAula) {
        $this->conteudoAula = $conteudoAula;
    }

    function getConteudoAula() {
        return $this->conteudoAula;
    }

    public function __toString() {
        return $this->titulo;
    }

    public function __construct() {
        $this->conteudoAula = new ArrayCollection();
    }

    /**
     * Set semana
     *
     * @param integer $semana
     * @return Disciplina
     */
    public function setSemana($semana) {
        $this->semana = $semana;

        return $this;
    }

    /**
     * Get semana
     *
     * @return integer 
     */
    public function getSemana() {
        return $this->semana;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Disciplina
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set aula
     *
     * @param integer $aula
     * @return Disciplina
     */
    public function setAula($aula) {
        $this->aula = $aula;

        return $this;
    }

    /**
     * Get aula
     *
     * @return integer 
     */
    public function getAula() {
        return $this->aula;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Disciplina
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     * @return Disciplina
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null) {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCurso() {
        return $this->idCurso;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Disciplina";
    }

    public function returnSemana() {
        switch ($this->semana) {
            case 1:
                $result = "Primeira SEMANA / Do 1º ao 7º Dia";
                break;
            case 2:
                $result = "Segunda SEMANA / Do 8º ao 14º Dia";
                break;
            case 3:
                $result = "Terceira SEMANA / Do 15º ao 21º Dia";
                break;
            case 4:
                $result = "Quarta SEMANA / Do 22º ao 28º Dia";
                break;
        }
        return $result;
    }

    public function returnAula() {
        switch ($this->aula) {
            case 0:
                $result = "Introdução";
                break;
            case 1:
                $result = "Aula 1";
                break;
            case 2:
                $result = "Aula 2";
                break;
            case 3:
                $result = "Aulas 1 e 2";
                break;
            case 4:
                $result = "Aula 3";
                break;
            case 5:
                $result = "Aula 4";
                break;
            case 6:
                $result = "Aulas 3 e 4";
                break;
            case 7:
                $result = "Aula 5";
                break;
            case 8:
                $result = "Aula 6";
                break;
            case 9:
                $result = "Aulas 5 e 6";
                break;
            case 10:
                $result = "Aula 7";
                break;
            case 11:
                $result = "Aula 8";
                break;
            case 12:
                $result = "Aulas 7 e 8";
                break;
            case 13:
                $result = "Conclusão";
                break;
        }
        return $result;
    }

}
