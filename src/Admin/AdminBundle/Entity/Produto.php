<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Admin\AdminBundle\lib\Util;

/**
 * Produto
 *
 * @ORM\Table(name="produto")
 * @ORM\Entity
 */
class Produto extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="produto_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=80, nullable=true)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo = '1';

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=2, nullable=true)
     */
    private $valor = 0;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Peso é obrigatório")
     * @ORM\Column(name="peso", type="float", precision=10, scale=2, nullable=true)
     */
    private $peso = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Produto
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return Produto
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Produto
     */
    public function setValor($valor) {
        $this->valor = number_format((float) str_replace(",", ".", $valor), 2, '.', '');

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set peso
     *
     * @param float $peso
     * @return Produto
     */
    public function setPeso($peso) {
        $this->peso = number_format((float) str_replace(",", ".", $peso), 2, '.', '');

        return $this;
    }

    /**
     * Get peso
     *
     * @return float 
     */
    public function getPeso() {
        return $this->peso;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Produto";
    }

}
