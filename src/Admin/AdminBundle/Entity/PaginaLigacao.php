<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PaginaLigacao
 *
 * @ORM\Table(name="pagina_ligacao")
 * @ORM\Entity
 */
class PaginaLigacao extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="pagina_ligacao_id_seq")
	 */
	private $id;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="prioridade", type="integer", nullable=true)
     */
    private $prioridade;

    /**
     * @var \Pagina
     * @Assert\NotBlank(message="A página pai deve ser especificada")
     * @ORM\ManyToOne(targetEntity="Pagina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pagina", referencedColumnName="id")
     * })
     */
    private $idPagina;

    /**
     * @var \Pagina
     * @Assert\NotBlank(message="A página filha deve ser especificada")
     * @ORM\ManyToOne(targetEntity="Pagina")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pagina_filho", referencedColumnName="id")
     * })
     */
    private $idPaginaFilho;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set prioridade
     *
     * @param integer $prioridade
     * @return PaginaLigacao
     */
    public function setPrioridade($prioridade) {
        $this->prioridade = $prioridade;

        return $this;
    }

    /**
     * Get prioridade
     *
     * @return integer 
     */
    public function getPrioridade() {
        return $this->prioridade;
    }

    /**
     * Set idPagina
     *
     * @param \Admin\AdminBundle\Entity\Pagina $idPagina
     * @return PaginaLigacao
     */
    public function setIdPagina(\Admin\AdminBundle\Entity\Pagina $idPagina = null) {
        $this->idPagina = $idPagina;

        return $this;
    }

    /**
     * Get idPagina
     *
     * @return \Admin\AdminBundle\Entity\Pagina 
     */
    public function getIdPagina() {
        return $this->idPagina;
    }

    /**
     * Set idPaginaFilho
     *
     * @param \Admin\AdminBundle\Entity\Pagina $idPaginaFilho
     * @return PaginaLigacao
     */
    public function setIdPaginaFilho(\Admin\AdminBundle\Entity\Pagina $idPaginaFilho = null) {
        $this->idPaginaFilho = $idPaginaFilho;

        return $this;
    }

    /**
     * Get idPaginaFilho
     *
     * @return \Admin\AdminBundle\Entity\Pagina 
     */
    public function getIdPaginaFilho() {
        return $this->idPaginaFilho;
    }

    public function returnEntity() {
        return "\Admin\AdminBundle\Entity\PaginaLigacao";
    }

    public function returnPrioridade(\Doctrine\ORM\EntityManager $em, $repository, $idPagina) {

        $prioridade = $em->getRepository($repository)->createQueryBuilder('item')
                ->where('item.idPagina = :idPaginaPai')
                ->orderBy("item.id", 'DESC')
                ->setMaxResults(1)
                ->setParameter('idPaginaPai', $idPagina)
                ->getQuery()
                ->getResult();

        if (empty($prioridade)) {
            return 1;
        }

        return $prioridade[0]->getPrioridade() + 1;
    }

}
