<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Contrato
 *
 * @ORM\Table(name="contrato_item")
 * @ORM\Entity
 */
class ContratoItem extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="contrato_item_id_seq")
	 */
	private $id;
	
    /**
     * @var \Contrato
     *
     * @ORM\ManyToOne(targetEntity="Contrato", inversedBy="itemContrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="CursoItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso_item", referencedColumnName="id")
     * })
     */
    private $idCursoItem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idContrato
     *
     * @param \Admin\AdminBundle\Entity\Contrato $idContrato
     * @return Contrato
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato = null) {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return \Admin\AdminBundle\Entity\Contrato 
     */
    public function getIdContrato() {
        return $this->idContrato;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCursoItem
     * @return Contrato
     */
    public function setIdCursoItem(\Admin\AdminBundle\Entity\CursoItem $idCursoItem = null) {
        $this->idCursoItem = $idCursoItem;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCursoItem() {
        return $this->idCursoItem;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\ContratoItem";
    }

}
