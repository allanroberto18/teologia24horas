<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AulaQuestaoAlternativa
 *
 * @ORM\Table(name="aula_questao_alternativa")
 * @ORM\Entity
 */
class AulaQuestaoAlternativa extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="aula_questao_alternativa_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Alternativa é obrigatório")
     * @ORM\Column(name="alternativa", type="string", length=255, nullable=true)
     */
    private $alternativa;

    /**
     * @var integer
     *
     * @ORM\Column(name="situacao", type="integer", nullable=true)
     */
    private $situacao = '2';

    /**
     * @var \AulaQuestao
     *
     * @ORM\ManyToOne(targetEntity="AulaQuestao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aula_questao", referencedColumnName="id")
     * })
     */
    private $idAulaQuestao;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set alternativa
     *
     * @param string $alternativa
     *
     * @return AulaQuestaoAlternativa
     */
    public function setAlternativa($alternativa)
    {
        $this->alternativa = $alternativa;

        return $this;
    }

    /**
     * Get alternativa
     *
     * @return string
     */
    public function getAlternativa()
    {
        return $this->alternativa;
    }

    /**
     * Set situacao
     *
     * @param integer $situacao
     *
     * @return AulaQuestaoAlternativa
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;

        return $this;
    }

    /**
     * Get situacao
     *
     * @return integer
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param \AulaQuestao $idAulaQuestao
     */
    public function setIdAulaQuestao(\Admin\AdminBundle\Entity\AulaQuestao $idAulaQuestao)
    {
        $this->idAulaQuestao = $idAulaQuestao;
    }

    /**
     * Get idAulaQuestao
     *
     * @return \Admin\AdminBundle\Entity\AulaQuestao
     */
    public function getIdAulaQuestao()
    {
        return $this->idAulaQuestao;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\AulaQuestaoAlternativa";
    }


}
