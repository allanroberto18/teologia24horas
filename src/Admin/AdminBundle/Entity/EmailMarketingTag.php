<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailMarketingTag
 *
 * @ORM\Table(name="email_marketing_tag")
 * @ORM\Entity
 */
class EmailMarketingTag extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="email_marketing_tag_id_seq")
	 */
	private $id;
	
    /**
     * @var \EmailMarketing
     *
     * @ORM\ManyToOne(targetEntity="EmailMarketing", inversedBy="tags")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_email_marketing", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idEmailMarketing;

    /**
     * @var \Tag
     *
     * @ORM\ManyToOne(targetEntity="Tag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tag", referencedColumnName="id")
     * })
     */
    private $idTag;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idEmailMarketing
     *
     * @param \Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing
     * @return EmailMarketingTag
     */
    public function setIdEmailMarketing(\Admin\AdminBundle\Entity\EmailMarketing $idEmailMarketing = null)
    {
        $this->idEmailMarketing = $idEmailMarketing;

        return $this;
    }

    /**
     * Get idEmailMarketing
     *
     * @return \Admin\AdminBundle\Entity\EmailMarketing 
     */
    public function getIdEmailMarketing()
    {
        return $this->idEmailMarketing;
    }

    /**
     * Set idTag
     *
     * @param \Admin\AdminBundle\Entity\Tag $idTag
     * @return EmailMarketingTag
     */
    public function setIdTag(\Admin\AdminBundle\Entity\Tag $idTag = null)
    {
        $this->idTag = $idTag;

        return $this;
    }

    /**
     * Get idTag
     *
     * @return \Admin\AdminBundle\Entity\Tag 
     */
    public function getIdTag()
    {
        return $this->idTag;
    }
    
    public function returnEntity() {
        return "\Admin\AdminBundle\Entity\EmailMarketingTag";
    }

}
