<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CepbrEndereco
 *
 * @ORM\Table(name="cepbr_endereco")
 * @ORM\Entity
 */
class CepbrEndereco {

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=200, nullable=true)
     */
    private $endereco;

    /**
     * @var \CepbrBairro
     * @ORM\Column(name="id_bairro", type="integer", length=200, nullable=true)
     */
    private $idBairro;
    
    /**
     * @var \CepbrBairro
     * @ORM\ManyToOne(targetEntity="CepbrBairro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_bairro", referencedColumnName="id_bairro")
     * })
     */
    private $bairro;

    /**
     * @var \CepbrCidade
     *
     * @ORM\ManyToOne(targetEntity="CepbrCidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cidade", referencedColumnName="id_cidade")
     * })
     */
    private $idCidade;

    public function __construct($cep = null) {
        if (!empty($cep)) {
            $this->cep = $cep;
        }
    }
    
    public function __toString() {
        return $this->cep;
    }

    /**
     * Get cep
     *
     * @return string 
     */
    public function getCep() {
        return $this->cep;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     * @return CepbrEndereco
     */
    public function setEndereco($endereco) {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string 
     */
    public function getEndereco() {
        return $this->endereco;
    }
    
    /**
     * Set idBairro
     *
     * @param string $idBairro
     * @return CepbrIdBairro
     */
    public function setIdBairro($idBairro) {
        $this->idBairro = $idBairro;

        return $this;
    }

    /**
     * Get idBairro
     *
     * @return string 
     */
    public function getIdBairro() {
        return $this->idBairro;
    }

    /**
     * Set Bairro
     *
     * @param \Admin\AdminBundle\Entity\CepbrBairro $bairro
     * @return CepbrEndereco
     */
    public function setBairro(\Admin\AdminBundle\Entity\CepbrBairro $bairro = null) {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get Bairro
     *
     * @return \Admin\AdminBundle\Entity\CepbrBairro 
     */
    public function getBairro() {
        return $this->bairro;
    }

    /**
     * Set idCidade
     *
     * @param \Admin\AdminBundle\Entity\CepbrCidade $idCidade
     * @return CepbrEndereco
     */
    public function setIdCidade(\Admin\AdminBundle\Entity\CepbrCidade $idCidade = null) {
        $this->idCidade = $idCidade;

        return $this;
    }

    /**
     * Get idCidade
     *
     * @return \Admin\AdminBundle\Entity\CepbrCidade 
     */
    public function getIdCidade() {
        return $this->idCidade;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\CepbrEndereco";
    }

}
