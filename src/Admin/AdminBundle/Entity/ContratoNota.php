<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ContratoNota
 *
 * @ORM\Table(name="contrato_nota")
 * @ORM\Entity
 */
class ContratoNota extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="contrato_nota_id_seq")
	 */
	private $id;
	
    /**
     * @var \Contrato
     * @ORM\ManyToOne(targetEntity="Contrato", inversedBy="notas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * @var string
     * @Assert\NotBlank(message="O campo Média é obrigatório")
     * @ORM\Column(name="media", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $media;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idContrato
     *
     * @param integer $idContrato
     * @return ContratoNota
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato)
    {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return integer 
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }

    /**
     * Set media
     *
     * @param string $media
     * @return ContratoNota
     */
    public function setMedia($media)
    {
        $this->media = number_format((float)str_replace(",", ".", $media), 2, '.', '');

        return $this;
    }

    /**
     * Get media
     *
     * @return string 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return ContratoNota
     */
    public function setDatahoraCadastro($datahoraCadastro)
    {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro()
    {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return ContratoNota
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao)
    {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao()
    {
        return $this->datahoraAtualizacao;
    }

    function returnEntity()
    {
        return 'Admin\AdminBundle\Entity\ContratoCurso';
    }
}
