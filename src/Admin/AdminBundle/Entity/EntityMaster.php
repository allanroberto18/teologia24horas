<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityManager;

abstract class EntityMaster {

    protected $countPrioridade;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    protected $status = '1';

    

    /**
     * Set status
     *
     * @param integer $status
     * @return Menu
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }

    public function returnStatus() {
        switch ($this->status) {
            case 1:
                print "<div style='margin-top: 5px;'><span class='label label-success'><span class='fa fa-check'></span> Ativo</span></div>";
                break;
            case 2:
                print "<div style='margin-top: 5px;'><span class='label label-danger'><span class='fa fa-close'></span> Inativo</span></div>";
                break;
            case 3:
                print "<div style='margin-top: 5px;'><span class='label label-warning'><span class='fa fa-exclamation'></span> Cancelado</span></div>";
                break;
        }
    }

    abstract function returnEntity();

    public function lastPrioridade(EntityManager $em, $repository) {
        $prioridade = $em->getRepository($repository)->createQueryBuilder('item')
                ->orderBy("item.id", 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
        
        if (empty($prioridade)) {
            return 1;
        }
        
        return $prioridade[0]->getPrioridade() + 1;
    }

}
