<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AulaAnotacao
 *
 * @ORM\Table(name="aula_anotacao")
 * @ORM\Entity
 */
class AulaAnotacao extends EntityMaster {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="aula_anotacao_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \Disciplina
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Aula")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aula", referencedColumnName="id")
     * })
     */
    private $idAula;

    /**
     * @var \Disciplina
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Contrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAula
     * @param Aula $idAula
     * @return Aula
     */
    public function setIdAula(\Admin\AdminBundle\Entity\Aula $idAula)
    {
        $this->idAula = $idAula;

        return $this;
    }

    /**
     * Get idAula
     *
     * @return integer
     */
    public function getIdAula()
    {
        return $this->idAula;
    }

    /**
     * Set idContrato
     * @param Contrato $idContrato
     * @return Contrato
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato)
    {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return integer
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return AulaAnotacao
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\AulaAnotacao";
    }
}
