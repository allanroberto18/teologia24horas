<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CursoCategoria
 *
 * @ORM\Table(name="curso_categoria")
 * @ORM\Entity
 */
class CursoCategoria extends EntityMaster {

    public function __toString() {
        return $this->getTitulo();
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="curso_categoria_id_seq")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     * @Gedmo\Slug(fields={"titulo"})
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return CursoCategoria
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return CursoCategoria
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }



    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\CursoCategoria";
    }
}
