<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AulaQuestaoAlternativaContrato
 *
 * @ORM\Table(name="aula_questao_alternativa_contrato")
 * @ORM\Entity
 */
class AulaQuestaoAlternativaContrato extends EntityMaster
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="aula_questao_alternativa_contrato_id_seq")
	 */
	private $id;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_contrato", type="integer", nullable=true)
     */
    private $idContrato;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \AulaQuestaoAlternativa
     *
     * @ORM\ManyToOne(targetEntity="AulaQuestaoAlternativa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aula_questao_alternativa", referencedColumnName="id")
     * })
     */
    private $idAulaQuestaoAlternativa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idContrato
     *
     * @param integer $idContrato
     *
     * @return AulaQuestaoAlternativaContrato
     */
    public function setIdContrato($idContrato)
    {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return integer
     */
    public function getIdContrato()
    {
        return $this->idContrato;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     *
     * @return AulaQuestaoAlternativaContrato
     */
    public function setDatahoraCadastro($datahoraCadastro)
    {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime
     */
    public function getDatahoraCadastro()
    {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     *
     * @return AulaQuestaoAlternativaContrato
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao)
    {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime
     */
    public function getDatahoraAtualizacao()
    {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idAulaQuestao
     *
     * @param \Admin\AdminBundle\Entity\AulaQuestaoAlternativa $idAulaQuestao
     *
     * @return AulaQuestaoAlternativaContrato
     */
    public function setIdAulaQuestaoAlternativa(\Admin\AdminBundle\Entity\AulaQuestaoAlternativa $idAulaQuestaoAlternativa = null)
    {
        $this->idAulaQuestaoAlternativa = $idAulaQuestaoAlternativa;

        return $this;
    }

    /**
     * Get idAulaQuestao
     *
     * @return \Admin\AdminBundle\Entity\AulaQuestaoAlternativa
     */
    public function getIdAulaQuestaoAlternativa()
    {
        return $this->idAulaQuestaoAlternativa;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\AulaQuestaoAlternativaContrato";
    }
}
