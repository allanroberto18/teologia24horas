<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Admin\AdminBundle\lib\Util;

/**
 * CursoItem
 *
 * @ORM\Table(name="curso_item")
 * @ORM\Entity
 */
class CursoItem extends EntityMaster {

    public function __toString() {
        return $this->titulo;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="curso_item_id_seq")
     */
    private $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=125, nullable=false)
     */
    private $titulo;

    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=2, nullable=false)
     */
    private $valor = 0;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="item")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return CursoItem
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return CursoItem
     */
    public function setValor($valor) {
        $this->valor = number_format((float)str_replace(",", ".", $valor), 2, '.', '');
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }
    
    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     * @return CursoImagem
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null) {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCurso() {
        return $this->idCurso;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\CursoItem";
    }

}
