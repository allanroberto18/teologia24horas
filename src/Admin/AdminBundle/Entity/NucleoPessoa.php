<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NucleoPessoa
 *
 * @ORM\Table(name="nucleo_pessoa")
 * @ORM\Entity
 */
class NucleoPessoa extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="nucleo_pessoa_id_seq")
	 */
	private $id;
	
    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;

    /**
     * @var \Nucleo
     *
     * @ORM\ManyToOne(targetEntity="Nucleo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_nucleo", referencedColumnName="id")
     * })
     */
    private $idNucleo;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return NucleoPessoa
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null)
    {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa()
    {
        return $this->idPessoa;
    }

    /**
     * Set idNucleo
     *
     * @param \Admin\AdminBundle\Entity\Nucleo $idNucleo
     * @return NucleoPessoa
     */
    public function setIdNucleo(\Admin\AdminBundle\Entity\Nucleo $idNucleo = null)
    {
        $this->idNucleo = $idNucleo;

        return $this;
    }

    /**
     * Get idNucleo
     *
     * @return \Admin\AdminBundle\Entity\Nucleo 
     */
    public function getIdNucleo()
    {
        return $this->idNucleo;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\NucleoPessoa";
    }


}
