<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Fatura
 *
 * @ORM\Table(name="fatura")
 * @ORM\Entity
 */
class Fatura extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="fatura_id_seq")
	 */
	private $id;
	
    /**
     * @var float
     * @Assert\NotBlank(message="O campo Valor é obrigatório")
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message="O campo Vencimento é obrigatório")
     * @ORM\Column(name="vencimento", type="date", nullable=true)
     */
    private $vencimento;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="text", nullable=true)
     */
    private $observacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pagamento_data", type="date", nullable=true)
     */
    private $pagamentoData;

    /**
     * @var float
     *
     * @ORM\Column(name="pagamento_valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $pagamentoValor;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \Contrato
     *
     * @ORM\OneToOne(targetEntity="Contrato", inversedBy="fatura")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set valor
     *
     * @param float $valor
     * @return Fatura
     */
    public function setValor($valor) {
        if (empty($valor)) {
            $valor = 0;
        }
        $this->valor = number_format((float) str_replace(",", ".", $valor), 2, '.', '');

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor() {
        return $this->valor;
    }

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     * @return Fatura
     */
    public function setVencimento($vencimento) {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime 
     */
    public function getVencimento() {
        return $this->vencimento;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     * @return Fatura
     */
    public function setObservacao($observacao) {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string 
     */
    public function getObservacao() {
        return $this->observacao;
    }

    /**
     * Set pagamentoData
     *
     * @param \DateTime $pagamentoData
     * @return Fatura
     */
    public function setPagamentoData($pagamentoData) {
        $this->pagamentoData = $pagamentoData;

        return $this;
    }

    /**
     * Get pagamentoData
     *
     * @return \DateTime 
     */
    public function getPagamentoData() {
        return $this->pagamentoData;
    }

    /**
     * Set pagamentoValor
     *
     * @param float $pagamentoValor
     * @return Fatura
     */
    public function setPagamentoValor($pagamentoValor) {
        if (empty($pagamentoValor)) {
            $pagamentoValor = 0;
        }
        $this->pagamentoValor = number_format((float) str_replace(",", ".", $pagamentoValor), 2, '.', '');

        return $this;
    }

    /**
     * Get pagamentoValor
     *
     * @return float 
     */
    public function getPagamentoValor() {
        return $this->pagamentoValor;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     * @return Fatura
     */
    public function setDatahoraCadastro($datahoraCadastro) {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime 
     */
    public function getDatahoraCadastro() {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     * @return Fatura
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao) {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime 
     */
    public function getDatahoraAtualizacao() {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idContrato
     *
     * @param \Admin\AdminBundle\Entity\Contrato $idContrato
     * @return Fatura
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato = null) {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return \Admin\AdminBundle\Entity\Contrato 
     */
    public function getIdContrato() {
        return $this->idContrato;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\Fatura";
    }

    public function returnStatus() {
        switch ($this->status) {
            case 1:
                print "<div class='mt'><span class='label label-warning'><span class='fa fa-exclamation-circle'></span> Pendente</span></div>";
                break;
            case 2:
                print "<div class='mt'><span class='label label-success'><span class='fa fa-check'></span> Pago</span></div>";
                break;
        }
    }

}
