<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CursoImagem
 *
 * @ORM\Table(name="curso_imagem")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CursoImagem extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="curso_imagem_id_seq")
	 */
	private $id;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="imagem", type="string", length=255, nullable=true)
     */
    private $imagem;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="imagem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return CursoImagem
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set imagem
     *
     * @param string $imagem
     * @return CursoImagem
     */
    public function setImagem($imagem) {
        $this->imagem = $imagem;

        return $this;
    }

    /**
     * Get imagem
     *
     * @return string 
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     * @return CursoImagem
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null) {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso 
     */
    public function getIdCurso() {
        return $this->idCurso;
    }
    
    public function getAbsolutePath() {
        return null === $this->imagem ? null : $this->getUploadRootDir() . '/' . $this->imagem;
    }

    public function getWebPath() {
        return null === $this->imagem ? null : $this->getUploadDir() . '/' . $this->imagem;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/cursos';
    }

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;
    private $temp;

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->imagem)) {
            // store the old name to delete after the update
            $this->temp = $this->imagem;
            $this->imagem = null;
        } else {
            $this->imagem = 'initial';
        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->imagem = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->imagem);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            //$this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    public function returnTipo() {
        switch ($this->tipo) {
            case 1:
                $result = 'Cover Fundo 1920 x 400';
                break;
            case 2:
                $result = 'Cover Frente 300 x 300';
                break;
            case 3:
                $result = 'Banner Slider 228 x 170';
                break;
        }
        return $result;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\CursoOfertadoImagem";
    }

    function __toString()
    {
        return $this->imagem;
    }


}
