<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Notificacao
 *
 * @ORM\Table(name="notificacao")
 * @ORM\Entity
 */
class Notificacao extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="notificacao_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Texto é obrigatório")
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="datahora_cadastro", type="datetime", nullable=true)
     */
    private $datahoraCadastro;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="datahora_atualizacao", type="datetime", nullable=true)
     */
    private $datahoraAtualizacao;

    /**
     * @var \Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $idCurso;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Notificacao
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set datahoraCadastro
     *
     * @param \DateTime $datahoraCadastro
     *
     * @return Notificacao
     */
    public function setDatahoraCadastro($datahoraCadastro)
    {
        $this->datahoraCadastro = $datahoraCadastro;

        return $this;
    }

    /**
     * Get datahoraCadastro
     *
     * @return \DateTime
     */
    public function getDatahoraCadastro()
    {
        return $this->datahoraCadastro;
    }

    /**
     * Set datahoraAtualizacao
     *
     * @param \DateTime $datahoraAtualizacao
     *
     * @return Notificacao
     */
    public function setDatahoraAtualizacao($datahoraAtualizacao)
    {
        $this->datahoraAtualizacao = $datahoraAtualizacao;

        return $this;
    }

    /**
     * Get datahoraAtualizacao
     *
     * @return \DateTime
     */
    public function getDatahoraAtualizacao()
    {
        return $this->datahoraAtualizacao;
    }

    /**
     * Set idCurso
     *
     * @param \Admin\AdminBundle\Entity\Curso $idCurso
     *
     * @return Notificacao
     */
    public function setIdCurso(\Admin\AdminBundle\Entity\Curso $idCurso = null)
    {
        $this->idCurso = $idCurso;

        return $this;
    }

    /**
     * Get idCurso
     *
     * @return \Admin\AdminBundle\Entity\Curso
     */
    public function getIdCurso()
    {
        return $this->idCurso;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\Notificacao";
    }
}
