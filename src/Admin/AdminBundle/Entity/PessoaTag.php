<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PessoaTag
 *
 * @ORM\Table(name="pessoa_tag")
 * @ORM\Entity
 */
class PessoaTag extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="pessoa_tag_id_seq")
	 */
	private $id;
	
    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa", inversedBy="tags")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;

    /**
     * @var \Tag
     *
     * @ORM\ManyToOne(targetEntity="Tag")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tag", referencedColumnName="id")
     * })
     */
    private $idTag;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return PessoaTag
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null)
    {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa 
     */
    public function getIdPessoa()
    {
        return $this->idPessoa;
    }

    /**
     * Set idTag
     *
     * @param \Admin\AdminBundle\Entity\Tag $idTag
     * @return PessoaTag
     */
    public function setIdTag(\Admin\AdminBundle\Entity\Tag $idTag = null)
    {
        $this->idTag = $idTag;

        return $this;
    }

    /**
     * Get idTag
     *
     * @return \Admin\AdminBundle\Entity\Tag 
     */
    public function getIdTag()
    {
        return $this->idTag;
    }

    function returnEntity()
    {
        return '\Admin\AdminBundle\Entity\PessoaTag';
    }


}
