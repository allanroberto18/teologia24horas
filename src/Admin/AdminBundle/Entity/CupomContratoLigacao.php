<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CupomContratoLigacao
 *
 * @ORM\Table(name="cupom_contrato_ligacao")
 * @ORM\Entity
 */
class CupomContratoLigacao extends EntityMaster {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="cupom_contrato_ligacao_id_seq")
	 */
	private $id;
	
    /**
     * @var \Cupom
     *
     * @ORM\ManyToOne(targetEntity="Cupom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cupom", referencedColumnName="id")
     * })
     */
    private $idCupom;

    /**
     * @var \Contrato
     *
     * @ORM\ManyToOne(targetEntity="Contrato")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contrato", referencedColumnName="id")
     * })
     */
    private $idContrato;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set idCupom
     *
     * @param \Admin\AdminBundle\Entity\Cupom $idCupom
     * @return CupomContratoLigacao
     */
    public function setIdCupom(\Admin\AdminBundle\Entity\Cupom $idCupom = null) {
        $this->idCupom = $idCupom;

        return $this;
    }

    /**
     * Get idCupom
     *
     * @return \Admin\AdminBundle\Entity\Cupom 
     */
    public function getIdCupom() {
        return $this->idCupom;
    }

    /**
     * Set idContrato
     *
     * @param \Admin\AdminBundle\Entity\Contrato $idContrato
     * @return CupomContratoLigacao
     */
    public function setIdContrato(\Admin\AdminBundle\Entity\Contrato $idContrato = null) {
        $this->idContrato = $idContrato;

        return $this;
    }

    /**
     * Get idContrato
     *
     * @return \Admin\AdminBundle\Entity\Contrato 
     */
    public function getIdContrato() {
        return $this->idContrato;
    }
    
    public function returnEntity() {
        return "Admin\AdminBundle\Entity\CupomContratoLigacao";
    }

}
