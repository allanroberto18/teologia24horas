<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AulaAcesso
 *
 * @ORM\Table(name="forum_ligacao")
 * @ORM\Entity
 */
class ForumLigacao extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="forum_ligacao_id_seq")
	 */
	private $id;
	
    /**
     * @var \Forum
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Forum")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_forum", referencedColumnName="id")
     * })
     */
    private $idPost;

    /**
     * @var \Forum
     *
     * @ORM\ManyToOne(targetEntity="Admin\AdminBundle\Entity\Forum")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_resposta", referencedColumnName="id")
     * })
     */
    private $idResposta;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * @return \Forum
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * @param \Forum $idPost
     */
    public function setIdPost(\Admin\AdminBundle\Entity\Forum $idPost)
    {
        $this->idPost = $idPost;
    }

    /**
     * @return \Forum
     */
    public function getIdResposta()
    {
        return $this->idResposta;
    }

    /**
     * @param \Forum $idResposta
     */
    public function setIdResposta(\Admin\AdminBundle\Entity\Forum $idResposta)
    {
        $this->idResposta = $idResposta;
    }

    public function returnEntity() {
        return "Admin\AdminBundle\Entity\ForumLigacao";
    }

}
