<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Nucleo
 *
 * @ORM\Table(name="nucleo")
 * @ORM\Entity
 */
class Nucleo extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="nucleo_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Título é obrigatório")
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var \Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $idPessoa;

    function __toString()
    {
        return $this->titulo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Nucleo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set idPessoa
     *
     * @param \Admin\AdminBundle\Entity\Pessoa $idPessoa
     * @return Nucleo
     */
    public function setIdPessoa(\Admin\AdminBundle\Entity\Pessoa $idPessoa = null)
    {
        $this->idPessoa = $idPessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Admin\AdminBundle\Entity\Pessoa
     */
    public function getIdPessoa()
    {
        return $this->idPessoa;
    }

    public function returnEntity()
    {
        return "\AdminBundle\Admin\Entity\Nucleo";
    }
}
