<?php

namespace Admin\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AulaQuestao
 *
 * @ORM\Table(name="aula_questao")
 * @ORM\Entity
 */
class AulaQuestao extends EntityMaster
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="SEQUENCE")
	 * @ORM\SequenceGenerator(sequenceName="aula_questao_id_seq")
	 */
	private $id;
	
    /**
     * @var string
     * @Assert\NotBlank(message="O campo Questão é obrigatório")
     * @ORM\Column(name="questao", type="string", length=255, nullable=true)
     */
    private $questao;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer", nullable=true)
     */
    private $tipo = '1';

    /**
     * @var \Aula
     *
     * @ORM\ManyToOne(targetEntity="Aula")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_aula", referencedColumnName="id")
     * })
     */
    private $idAula;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set questao
     *
     * @param string $questao
     *
     * @return AulaQuestao
     */
    public function setQuestao($questao)
    {
        $this->questao = $questao;

        return $this;
    }

    /**
     * Get questao
     *
     * @return string
     */
    public function getQuestao()
    {
        return $this->questao;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     *
     * @return AulaQuestao
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set idAula
     *
     * @param \Admin\AdminBundle\Entity\Aula $idAula
     *
     * @return AulaQuestao
     */
    public function setIdAula(\Admin\AdminBundle\Entity\Aula $idAula = null)
    {
        $this->idAula = $idAula;

        return $this;
    }

    /**
     * Get idAula
     *
     * @return \Admin\AdminBundle\Entity\Aula
     */
    public function getIdAula()
    {
        return $this->idAula;
    }

    function returnEntity()
    {
        return "\Admin\AdminBundle\Entity\AulaQuestao";
    }


}
