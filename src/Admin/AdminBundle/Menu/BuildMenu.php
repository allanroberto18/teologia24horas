<?php

namespace Admin\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class BuildMenu extends ContainerAware {

    public function mainMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root', array(
            'childrenAttributes' => array(
                'class' => 'sidebar-menu',
            ),
        ));

        $menu->addChild('home', array('route' => 'admin_home', 'label' => "Home"))->setAttribute('icon', 'fa fa-desktop');

        $menu->addChild('email_marketing', array('label' => "E-mail Marketing"));
        $menu['email_marketing']->setUri('#');
        $menu['email_marketing']->setChildrenAttribute('class', "treeview-menu")->setAttribute('icon', 'fa fa-envelope');
        $menu['email_marketing']->addChild('email_marketing', array('route' => 'EmailMarketing', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt')->setAttribute('class', 'treeview');
        $menu['email_marketing']->addChild('email_marketing_new', array('route' => 'EmailMarketing_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('categoria', array('label' => "Curso/Categoria"))->setAttribute('icon', 'fa fa-book');
        $menu['categoria']->setChildrenAttribute('class', "treeview-menu");
        $menu['categoria']->setUri('#');
        $menu['categoria']->addChild('categoria', array('route' => 'Curso_Categoria', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['categoria']->addChild('categoria_new', array('route' => 'Curso_Categoria_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('tabela_preco', array('label' => "Tabela de Preços"))->setAttribute('icon', 'fa fa-book');
        $menu['tabela_preco']->setChildrenAttribute('class', "treeview-menu");
        $menu['tabela_preco']->setUri('#');
        $menu['tabela_preco']->addChild('tabela_preco', array('route' => 'Tabela_Preco', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['tabela_preco']->addChild('tabela_preco_new', array('route' => 'Tabela_Preco', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');
        
        $menu->addChild('curso', array('label' => "Curso"))->setAttribute('icon', 'fa fa-book');
        $menu['curso']->setChildrenAttribute('class', "treeview-menu");
        $menu['curso']->setUri('#');
        $menu['curso']->addChild('curso', array('route' => 'Curso', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['curso']->addChild('curso_new', array('route' => 'Curso_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');
        
        $menu->addChild('remessa', array('label' => "Remessa Bancária"))->setAttribute('icon', 'fa fa-dollar');
        $menu['remessa']->setChildrenAttribute('class', "treeview-menu");
        $menu['remessa']->setUri('#');
        $menu['remessa']->addChild('remessa', array('route' => 'Remessa_Bancaria', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['remessa']->addChild('remessa_new', array('route' => 'Remessa_Bancaria_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('p_fisica', array('label' => "Pessoa Física"))->setAttribute('icon', 'fa fa-group');
        $menu['p_fisica']->setChildrenAttribute('class', "treeview-menu");
        $menu['p_fisica']->setUri('#');
        $menu['p_fisica']->addChild('p_fisica', array('route' => 'Pessoa_Fisica', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');

//        $menu->addChild('p_juridica', array('label' => "Pessoa Jurídica"))->setAttribute('icon', 'fa fa-group');
//        $menu['p_juridica']->setChildrenAttribute('class', "treeview-menu");
//        $menu['p_juridica']->setUri('#');
//        $menu['p_juridica']->addChild('p_juridica', array('route' => 'Pessoa_Juridica', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');

        //$menu->addChild('mensagem', array('label' => "Mensagem"))->setAttribute('icon', 'fa fa-comments-o');
        //$menu['mensagem']->setUri('#mensagem');
        //$menu['mensagem']->addChild('mensagem', array('route' => 'Mensagem', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        //$menu['mensagem']->addChild('mensagem_new', array('route' => 'Mensagem_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('pagina', array('label' => "Página"))->setAttribute('dropdown', true)->setAttribute('icon', 'fa fa-chain');
        $menu['pagina']->setChildrenAttribute('class', "treeview-menu");
        $menu['pagina']->setUri('#');
        $menu['pagina']->addChild('pagina', array('route' => 'Pagina', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['pagina']->addChild('pagina_new', array('route' => 'Pagina_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        $menu->addChild('usuario', array('label' => "Usuário"))->setAttribute('icon', 'fa fa-user');
        $menu['usuario']->setChildrenAttribute('class', "treeview-menu");
        $menu['usuario']->setUri('#');
        $menu['usuario']->addChild('usuario', array('route' => 'Usuario', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['usuario']->addChild('usuario_new', array('route' => 'Usuario_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');

        
        $menu->addChild('tag', array('label' => "Tag"))->setAttribute('icon', 'fa fa-tags');
        $menu['tag']->setChildrenAttribute('class', "treeview-menu");
        $menu['tag']->setUri('#');
        $menu['tag']->addChild('tag', array('route' => 'Tag', 'label' => "Listar Registros"))->setAttribute('icon', 'fa fa-list-alt');
        $menu['tag']->addChild('tag_new', array('route' => 'Tag_new', 'label' => "Adicionar Registros"))->setAttribute('icon', 'fa fa-plus');
        
        $menu->addChild("sair", array('route' => 'admin_logout', 'label' => "Sair"))->setAttribute('icon', 'fa fa-sign-out');

        return $menu;
    }

}
