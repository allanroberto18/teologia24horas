<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MensagemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', 'text', array('label' => 'Título:', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb')))
            ->add('conteudo', 'textarea', array('label' => 'Texto:', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'rows' => 10)))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Mensagem'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_mensagem';
    }
}
