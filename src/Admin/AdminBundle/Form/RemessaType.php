<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RemessaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('file', 'file', array('label' => 'Arquivo: ', 'attr' => array('class' => 'mb')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Remessa'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_remessa';
    }

}
