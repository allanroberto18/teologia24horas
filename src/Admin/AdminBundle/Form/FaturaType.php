<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FaturaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('valor', 'text', array('label' => 'Valor: ', 'read_only' => true, 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb valor', 'placeholder' => 'Valor da Fatura')))
                ->add('vencimento', 'date', array(
                    'label' => 'Vencimento:', 'read_only' => true, 'error_bubbling' => true, 'required' => true, 
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control mb datemask',
                        'placeholder' => 'Inserir a Data de Nascimento'
                    )
                ))
                ->add('pagamentoData', 'date', array(
                    'label' => 'Data de Pagamento:', 'error_bubbling' => true, 'required' => true, 
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control mb datemask',
                        'placeholder' => 'Inserir a Data de Nascimento'
                    )
                ))
                ->add('pagamentoValor', 'text', array('label' => 'Valor: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb valor', 'placeholder' => 'Valor da Fatura')))
                ->add('observacao', 'textarea', array('label' => 'Observação: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Valor da Fatura')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'Pendente ',
                        2 => 'Pago ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Fatura'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_fatura';
    }

}
