<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class NucleoPessoaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idNucleo', 'entity', array(
                    'class' => 'Admin\AdminBundle\Entity\Nucleo',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('entity')
                                ->orderBy('entity.titulo', 'ASC');
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Núcleo: ',
                    'error_bubbling' => true,
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control mb20',
                        'placeholder' => 'Selecione o Núcleo'
                    )
                ))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\NucleoPessoa'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_nucleo_pessoa';
    }

}
