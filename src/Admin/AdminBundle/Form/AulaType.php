<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AulaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titulo', 'text', array('label' => 'Título: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Título do Material')))
                ->add('resumo', 'textarea', array('label' => 'Resumo: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Resumo do Material')))
                ->add('tipoConteudo', 'choice', array('label' => 'Tipo:',
                    'choices' => array(
                        1 => 'vimeo ',
                        2 => 'youtube ',
                        3 => 'slideshare ',
                        4 => 'avaliação ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
                ->add('link', 'text', array('label' => 'Link: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb20', 'placeholder' => 'Link')))
                ->add('ordem', 'text', array('label' => 'Posição: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb20', 'placeholder' => 'Órdem')))
                ->add('duracao', 'text', array('label' => 'Duração: ', 'label_attr' => array('class' => 'mt20'), 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb20', 'placeholder' => 'Duração')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Aula'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_aula';
    }

}
