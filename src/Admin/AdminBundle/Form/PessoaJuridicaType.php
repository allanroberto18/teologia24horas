<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PessoaJuridicaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idEmailMarketing', 'hidden')
                ->add('pjRazaoSocial', 'text', array('label' => 'Razão Social: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Razão Social')))
                ->add('pjNomeFantasia', 'text', array('label' => 'Nome Fantasia: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Nome Fantasia')))
                ->add('pjCnpj', 'text', array('label' => 'CNPJ: ', 'attr' => array('class' => 'form-control mb cnpj', 'placeholder' => 'Inserir o CNPJ')))
                ->add('pjInscEstadual', 'text', array('label' => 'Inscrição Estadual: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Inscrição Estadual')))
                ->add('pjInscMunicipal', 'text', array('label' => 'Inscrição Municipal: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Inscrição Municipal')))
                ->add('pjSite', 'text', array('label' => 'Site: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Site da Empresa')))
                ->add('pjContato', 'text', array('label' => 'Nome da Empresa: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Contato da Empresa')))
                ->add('celular', 'text', array('label' => 'Celular: ', 'attr' => array('class' => 'form-control mb celular', 'placeholder' => 'Inserir o Telefone Celular do Contato')))
                ->add('email', 'text', array('label' => 'E-mail: ', 'read_only' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o E-mail Para Acesso a Plataforma')))
                ->add('senha', 'text', array('label' => 'Senha: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Senha Para Acesso a Plataforma')))
                ->add('dicaSenha', 'text', array('label' => 'Dica de Senha: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Dica de Senha')))
                ->add('biografia', 'textarea', array('label' => 'Biografia: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Biografia', 'rows' => 5)))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\PessoaJuridica'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_pessoajuridica';
    }

}
