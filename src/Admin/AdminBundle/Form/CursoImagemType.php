<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CursoImagemType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('tipo', 'choice', array('label' => 'Tipo de Imagem:', 'error_bubbling' => true, 'required' => true,
                    'choices' => array(
                        1 => 'Cover Fundo 1920 x 400',
                        2 => 'Cover Frente 300 x 300',
                        3 => 'Banner Slider 228 x 170',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    ))
                )
                ->add('file', 'file', array('label' => 'Foto: ', 'attr' => array('class' => 'mb')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\CursoImagem'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_cursoimagem';
    }

}
