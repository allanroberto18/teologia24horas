<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DisciplinaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titulo', 'text', array('label' => 'Título: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Título da Aula')))
                ->add('descricao', 'textarea', array('label' => 'Descrição: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => 2, 'maxlength' => 80, 'class' => 'form-control mb20', 'placeholder' => 'Desaque do Curso')))
                ->add('semana', 'choice', array('label' => 'Semana:',
                    'choices' => array(
                        1 => 'Primeira Semana ',
                        2 => 'Segunda Semana ',
                        3 => 'Terceira Semana ',
                        4 => 'Quarta Semana ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
                ->add('aula', 'choice', array('label' => 'Aula:',
                    'choices' => array(
                        0 => 'Introdução',
                        1 => 'Aula 1',
                        2 => 'Aula 2',
                        3 => 'Aulas 1 e 2',
                        4 => 'Aula 3',
                        5 => 'Aula 4',
                        6 => 'Aulas 3 e 4',
                        7 => 'Aula 5',
                        8 => 'Aula 6',
                        9 => 'Aulas 5 e 6',
                        10 => 'Aula 7',
                        11 => 'Aula 8',
                        12 => 'Aulas 7 e 8',
                        13 => 'Conclusão',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
                
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Disciplina'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_disciplina';
    }

}
