<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ContratoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('DataContratacao', 'date', array(
                    'label' => 'Data de Contratação:', 'error_bubbling' => true, 'required' => true, 
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => array(
                        'class' => 'form-control mb datemask',
                        'placeholder' => 'Inserir a Data de Contratação'
                    )
                ))
                ->add('idCurso', 'entity', array(
                    'class' => 'Admin\AdminBundle\Entity\Curso',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('entity')
                                ->orderBy('entity.titulo', 'ASC');
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Nome do Curso: ',
                    'error_bubbling' => true,
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control mb20',
                        'placeholder' => 'Selecione a Categoria'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Contrato'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_contrato';
    }

}
