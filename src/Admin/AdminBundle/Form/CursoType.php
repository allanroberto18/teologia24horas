<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CursoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('idPessoa', 'entity', array(
                    'class' => 'Admin\AdminBundle\Entity\Pessoa',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('entity')
                                ->orderBy('entity.pfNome', 'ASC');
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Pessoa: ',
                    'error_bubbling' => true,
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control mb20',
                        'placeholder' => 'Selecione a Categoria'
                    )
                ))
                ->add('idCursoCategoria', 'entity', array(
                    'class' => 'Admin\AdminBundle\Entity\CursoCategoria',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('entity')
                                ->orderBy('entity.titulo', 'ASC');
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Categoria: ',
                    'error_bubbling' => true,
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control mb20',
                        'placeholder' => 'Selecione a Categoria'
                    )
                ))
                ->add('idTabelaPreco', 'entity', array(
                    'class' => 'Admin\AdminBundle\Entity\TabelaPreco',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('entity')
                                ->orderBy('entity.titulo', 'ASC');
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'label' => 'Tabela de Preços: ',
                    'error_bubbling' => true,
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control mb20',
                        'placeholder' => 'Selecione a Categoria'
                    )
                ))
                ->add('titulo', 'text', array('label' => 'Nome:', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb')))
                ->add('ordem', 'text', array('label' => 'Órdem: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Órdem do Curso')))
                ->add('duracao', 'text', array('label' => 'Duração: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Duração do Curso')))
                ->add('horaAula', 'text', array('label' => 'Horas/Aula: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Horas/Aula do Curso')))
                ->add('credito', 'text', array('label' => 'Créditos: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control  mb20', 'placeholder' => 'Créditos do Curso')))
                ->add('destaque', 'textarea', array('label' => 'Destaque do Curso: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => 2, 'maxlength' => 80, 'class' => 'form-control mb20', 'placeholder' => 'Desaque do Curso')))
                ->add('apresentacao', 'textarea', array('label' => 'Apresentação: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => 2, 'maxlength' => 255, 'class' => 'form-control  mb20', 'placeholder' => 'Apresentação do Curso')))
                ->add('chamada', 'textarea', array('label' => 'Chamada: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control  mb20', 'placeholder' => 'Chamada do Curso')))
                ->add('descricao', 'textarea', array('label' => 'Descrição: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Descrição do Curso')))
                ->add('objetivo', 'textarea', array('label' => 'Objetivo: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Objetivo do Curso')))
                ->add('areaAtuacao', 'textarea', array('label' => 'Área de Atuação: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Área de Atuação do Curso')))
                ->add('publicoAlvo', 'textarea', array('label' => 'Público Alvo: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Público Alvo do Curso')))
                ->add('materialDidatico', 'textarea', array('label' => 'Material Didático: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Material Didático do Curso')))
                ->add('certificacao', 'textarea', array('label' => 'Certificação: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Certificação do Curso')))
                ->add('avaliacao', 'textarea', array('label' => 'Avaliação: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Avaliação do Curso')))
                ->add('tutoria', 'textarea', array('label' => 'Tutoria: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mb20', 'placeholder' => 'Tutoria do Curso')))
                ->add('tecnologia', 'textarea', array('label' => 'Tecnologia: ', 'error_bubbling' => true, 'required' => true, 'label_attr' => array('class' => 'mt20'), 'attr' => array('rows' => 10, 'maxlength' => 255, 'class' => 'form-control mt20 mb20', 'placeholder' => 'Tecnologia do Curso')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                        3 => 'cancelado',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Curso'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_curso';
    }

}
