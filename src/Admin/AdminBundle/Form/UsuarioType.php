<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('identificador', 'hidden')
                ->add('nome', 'text', array('label' => 'Nome: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Nome do Usuário')))
                ->add('email', 'text', array('label' => 'E-mail: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o E-mail Para Acesso a Administração')))
                ->add('senha', 'password', array('label' => 'Senha: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Senha Para Acesso a Administração')))
                ->add('dicaSenha', 'text', array('label' => 'Dica de Senha: ', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir a Dica de Senha')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_usuario';
    }

}
