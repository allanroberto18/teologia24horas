<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ForumItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comentario')
            ->add('status', 'choice', array('label' => 'Status:',
                'choices' => array(
                    1 => 'Liberar ',
                    2 => 'Aguardando Liberação ',
                ),
                'multiple' => false,
                'expanded' => true,
                'attr' => array(
                    'class' => 'mb'
                )
            ))
            ->add('actions', 'form_actions', [
                'buttons' => [
                    'salvar' => ['type' => 'submit', 'options' => ['label' => 'Salvar', 'attr' => ['class' => 'btn btn-primary']]],
                    'salvarNovo' => ['type' => 'submit', 'options' => ['label' => 'Salvar e novo', 'attr' => ['class' => 'btn btn-primary']]],
                    'cancelar' => ['type' => 'reset', 'options' => ['label' => 'Cancelar', 'attr' => ['class' => 'btn btn-warning']]],
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\ForumItem'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_adminbundle_forumitem';
    }
}
