<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaginaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titulo', 'text', array('label' => 'Título: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o Título')))
                ->add('resumo', 'textarea', array('label' => 'Resumo: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => '3','class' => 'form-control mb', 'placeholder' => 'Inserir o Resumo')))
                ->add('texto', 'textarea', array('label' => 'Texto: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('rows' => '10','class' => 'form-control mb', 'placeholder' => 'Inserir o Texto')))
                ->add('file', 'file', array('label' => 'Foto: ', 'label_attr' => array('class' => 'mt20'), 'attr' => array('class' => 'mb20')));
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\Pagina'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_pagina';
    }

}
