<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmailMarketingType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('identificador', 'hidden')
                ->add('email', 'text', array('label' => 'E-mail', 'attr' => array('class' => 'form-control mb', 'placeholder' => 'Inserir o E-mail')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\EmailMarketing'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_adminbundle_emailmarketing';
    }

}
