<?php

namespace Admin\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TabelaPrecoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titulo', 'text', array('label' => 'Nome do Curso: ', 'error_bubbling' => true, 'required' => true, 'attr' => array('maxlength' => 255, 'class' => 'form-control  mb20', 'placeholder' => 'Nome do Curso')))
                ->add('valor', 'text', array('label' => 'Valor:', 'error_bubbling' => true, 'required' => true, 'attr' => array('class' => 'form-control mb valor')))
                ->add('status', 'choice', array('label' => 'Status:',
                    'choices' => array(
                        1 => 'ativo ',
                        2 => 'inativo ',
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'label_attr' => array('class' => 'mt20'),
                    'attr' => array(
                        'class' => 'mb'
                    )
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\AdminBundle\Entity\TabelaPreco'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'admin_tabelapreco';
    }

}
