<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Admin\AdminBundle\lib;

use Symfony\Component\HttpFoundation\Session\Session;
use Admin\AdminBundle\Entity\EmailMarketing;

class EmailMarketingConsult extends AppConsult
{

    private $identificador;
    public $session;

    public function __construct($repository = null)
    {
        parent::__construct($repository);
        $this->session = new Session();
    }

    function getIdentificador()
    {
        return $this->identificador;
    }

    function setIdentificador($identificador)
    {
        $this->identificador = $identificador;
    }

    public function setParams($identificador)
    {
        $this->setIdentificador($identificador);
    }

    public function validarUsuario($em)
    {
        $msg = "";
        if (empty($this->identificador)) {

            $msg .= "<span class='fa fa-remove'></span> Usuário não identificado<br />";

            $this->session->getFlashBag()->add("error", $msg);

            return;
        }
        return $this->efetuarLogin($em);
    }

    public function efetuarLogin(EmailMarketing $emailMarketing)
    {
        $repository = $em->getRepository("\Admin\AdminBundle\Entity\EmailMarketing");

        $sql = $repository->createQueryBuilder('p')
            ->where('p.identificador = :identificador')
            ->setParameter('identificador', $this->identificador)
            ->getQuery();

        $result = $sql->getSingleResult();

        if (empty($result) && !$result instanceof EmailMarketing) {
            $this->session->getFlashBag()->set("error", "Usuário inválido");
            return false;
        }

        $pessoa = $result->getPessoa();

        if (count($pessoa) > 0) {
            $pessoaConsult = new PessoaConsult();
            $pessoaConsult->mountSessao($em, $pessoa);

            header("location:/AVA/Usuario/Home/");
            exit();
        }
        $value['id'] = null;
        $value['id_email_marketing'] = $result->getId();
        $value['nome'] = "";
        $value['email'] = $result->getEmail();
        $value['foto'] = 'user.png';
        $value['check'] = '0';

        $this->session->set('ava', $value);

        header("location:/AVA/Usuario/Perfil/usuario/novo");
        exit();
    }

}
