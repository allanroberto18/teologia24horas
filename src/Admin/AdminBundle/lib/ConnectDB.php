<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 22/04/15
 * Time: 16:58
 */

namespace Admin\AdminBundle\lib;


class ConnectDB {

    private $host;
    private $db;
    private $port;
    private $user;
    private $pwd;
    private $conn;

    function __construct($db=null)
    {
        $this->host = 'localhost';
        $this->db = empty($db) ? 'ibivirtual' : $db;
        $this->port = '5432';
        $this->user = 'ibivirtual';
        $this->pwd = 'kerberos280104';
    }

    function connect()
    {
        if ($this->conn instanceof \PDO)
        {
            print "NÃO CONECTADO";
            return;
        }

        try {
            $this->conn = new \PDO("pgsql:dbname={$this->db};host={$this->host}", $this->user, $this->pwd);
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $this->conn;

        } catch (\PDOException $e)
        {
            return $e->getMessage();
        }
    }

    function closeConnect()
    {
        if ($this->conn instanceof \PDO)
        {
            $this->conn = null;
        }
    }

    function __destruct() {
        $this->closeConnect();
    }

}