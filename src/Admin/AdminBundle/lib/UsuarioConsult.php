<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Admin\AdminBundle\lib;

use Symfony\Component\HttpFoundation\Session\Session;

class UsuarioConsult extends AppConsult {

    private $email;
    private $senha;
    public $session;

    public function __construct($repository) {
        parent::__construct($repository);
        $this->session = new Session();
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = !empty($senha) ? md5($senha) : "";
    }

    public function setParams($email, $senha) {
        $this->setEmail($email);
        $this->setSenha($senha);
    }

    public function validarUsuario() {
        $msg = "";
        if (empty($this->email)) {
            $msg .= "<span class='fa fa-remove'></span> Informe o e-mail do usuário<br />";
        }
        if (empty($this->senha)) {
            $msg .= "<span class='fa fa-remove'></span> Informe a senha do usuário";
        }
        if (!empty($msg)) {
            $this->session->getFlashBag()->add("error", $msg);
            return;
        }
        return $this->efetuarLogin();
    }

    public function efetuarLogin() {
        $sql = $this->repository->createQueryBuilder('p')
                ->where('p.email = :email')
                ->andWhere('p.senha = :senha')
                ->setParameter('email', $this->email)
                ->setParameter('senha', $this->senha)
                ->getQuery();

        $result = $sql->getArrayResult();

        if (empty($result[0])) {
            $this->session->getFlashBag()->set("error", "Usuário ou senha inválida");
            return false;
        }

        $this->session->set('admin', $result[0]);
        
        header("location:/admin/");
        exit();
    }

}
