<?php

namespace Admin\AdminBundle\lib;

use Admin\AdminBundle\Entity\Curso;

class CursoConsult {

    public function returnCursoById($id, $em) {
        $curso = $this->returnCurso($id, $em);

        $resultSet = array(
            "Curso" => $curso,
            "Imagens" => $this->returnCursoImagem($id, $em),
            "Extra" => array('duracao' => $curso->getDuracao(), 'hora_aula' => $curso->getHoraAula(), 'credito' => $curso->getCredito()),
        );

        return $resultSet;
    }

    public function returnCurso($id, $em) {
        $repository = $em->getRepository('\Admin\AdminBundle\Entity\Curso');
        return $repository->createQueryBuilder('item')
                        ->where('item.id = :id')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getSingleResult();
    }

    public function returnCursoImagemSlider($id, $em) {

        $repository = $em->getRepository('\Admin\AdminBundle\Entity\CursoImagem');

        return $repository->createQueryBuilder('item')
                        ->where('item.tipo = :tipo')
                        ->andWhere('item.idCurso = :idCurso')
                        ->setParameter('tipo', 3)
                        ->setParameter('idCurso', $id)
                        ->getQuery()
                        ->getArrayResult();
    }

    public function returnCursoImagem($id, $em) {

        $repository = $em->getRepository('\Admin\AdminBundle\Entity\CursoImagem');

        return $repository->createQueryBuilder('item')
                        ->where('item.tipo < :tipo')
                        ->andWhere('item.idCurso = :idCurso')
                        ->setParameter('tipo', 3)
                        ->setParameter('idCurso', $id)
                        ->getQuery()
                        ->getArrayResult();
    }
    
    public function getCursosBySlider($em) {
        $repository = $em->getRepository('\Admin\AdminBundle\Entity\Curso');
        $curso = $repository->createQueryBuilder('item')
                ->where('item.status < :status')
                ->setParameter('status', 3)
                ->orderBy('item.status', 'ASC')
                ->getQuery()
                ->getResult();

        $resultSet = array();
        $cursoConsult = new CursoConsult();
        
        if (empty($curso)) {
            return null;
        }
       
        for ($i = 0; $i < count($curso); $i++) {
            if (!$curso[$i] instanceof Curso) {
                continue;
            }
            $imagem = $cursoConsult->returnCursoImagemSlider($curso[$i]->getId(), $em);
            
            $resultSet[$i]['id'] = $curso[$i]->getId();
            $resultSet[$i]['titulo'] = $curso[$i]->getTitulo();
            $resultSet[$i]['id_categoria'] = $curso[$i]->getIdCursoCategoria()->getId();
            $resultSet[$i]['categoria'] = $curso[$i]->getIdCursoCategoria()->getTitulo();
            $resultSet[$i]['slug_categoria'] = $curso[$i]->getIdCursoCategoria()->getSlug();
            $resultSet[$i]['slug'] = $curso[$i]->getSlug();
            $resultSet[$i]["duracao"] = $curso[$i]->getDuracao();
            $resultSet[$i]["hora_aula"] = $curso[$i]->getHoraAula();
            $resultSet[$i]["credito"] = $curso[$i]->getCredito();
            $resultSet[$i]["valor"] = $curso[$i]->getIdTabelaPreco()->getValor();
            $resultSet[$i]["imagem"] = empty($imagem) ? "2.jpg" : $imagem[0]["imagem"];
            $resultSet[$i]["status"] = $curso[$i]->getStatus();
        }
        return $resultSet;
    }
}
