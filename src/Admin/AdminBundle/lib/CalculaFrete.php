<?php

namespace Admin\AdminBundle\lib;

class CalculaFrete {

    public static function calcular($cep_destino, $cep_origem = 77064719) {

        $cod_servico = '41106';
        $peso = '0.2';
        $altura = '2';
        $largura = '23';
        $comprimento = '30';
        $valor_declarado = '0.50';

        $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=" . $cep_origem . "&sCepDestino=" . $cep_destino . "&nVlPeso=" . $peso . "&nCdFormato=1&nVlComprimento=" . $comprimento . "&nVlAltura=" . $altura . "&nVlLargura=" . $largura . "&sCdMaoPropria=n&nVlValorDeclarado=" . $valor_declarado . "&sCdAvisoRecebimento=n&nCdServico=" . $cod_servico . "&nVlDiametro=0&StrRetorno=xml";

        $xml = simplexml_load_file($correios);
        if ($xml->cServico->Erro == '0')
            return $xml->cServico->Valor;
        else
            return false;
    }

}
