<?php

namespace Admin\AdminBundle\lib;

class MakeCalendar {

    private $day;
    private $firstDay;
    private $lastDay;
    private $mounth;
    private $year;

    function __construct() {
        $this->setFirstDay();
        $this->setMounth();
        $this->setYear();
        $this->setLastDay();
    }

    function getFirstDay() {
        return $this->firstDay;
    }

    function getLastDay() {
        return $this->lastDay;
    }

    function getMounth() {
        return $this->mounth;
    }

    function getYear() {
        return $this->year;
    }

    function setFirstDay() {
        $this->firstDay = 01;
    }

    function setLastDay() {
        $lastDay = date("t", mktime(0, 0, 0, $this->mounth, '01', $this->year));
        $this->lastDay = $lastDay;
    }

    function setMounth() {
        $this->mounth = date('m');
    }

    function setYear() {
        $this->year = date('Y');
    }
    
    public function mount() {
        $this->day = 01;
        $calendario = array();
        for ($i = 0; $i < $this->lastDay; $i++) {
            $day = ($i == 0) ? $this->firstDay : $this->day;
            $this->day++;
            $calendario[$i]  = array("dia" => $day, "dia_semana" => $this->retunrDayOfWeek($day), "mes" => $this->returnMounth(), "ano" => $this->year);
            if ($i < 7) {
                $calendario[$i]["class"] = "green-color";
            } elseif ($i > 6 || $i < 14) {
                $calendario[$i]["class"] = "ciano-color";
            } elseif ($i > 14 || $i < 21) {
                $calendario[$i]["class"] = "yellow-color";
            } elseif ($i > 21 || $i < 28) {
                $calendario[$i]["class"] = "black-color";
            }  else {
                $calendario[$i]["class"] = "empty";
            }
            if (($i+1) == date('d')) {
                $calendario[$i]["class"] = "red-color";
            }
        }
        return $calendario;
    }
    
    public function returnMounth() {
        switch ($this->mounth) {
            case 1:
                return "Janeiro";
                break;
            case 2:
                return "Fevereiro";
                break;
            case 3:
                return "Março";
                break;
            case 4:
                return "Abril";
                break;
            case 5:
                return "Maio";
                break;
            case 6:
                return "Junho";
                break;
            case 7:
                return "Julho";
                break;
            case 8:
                return "Agosto";
                break;
            case 9:
                return "Setembro";
                break;
            case 10:
                return "Outubro";
                break;
            case 11:
                return "Novembro";
                break;
            case 12:
                return "Dezembro";
                break;
        }
    }
    
    public function retunrDayOfWeek($day) {
        $date = date($this->year . "-" . $this->mounth . "-" . $day);
        $dayOfWeek = date("N", strtotime($date));
        switch ($dayOfWeek) {
            case 1:
                return "Segunda";
                break;
            case 2:
                return "Terça";
                break;
            case 3:
                return "Quarta";
                break;
            case 4:
                return "Quinta";
                break;
            case 5:
                return "Sexta";
                break;
            case 6:
                return "Sábado";
                break;
            case 7:
                return "Domingo";
                break;
        }
    }

}
