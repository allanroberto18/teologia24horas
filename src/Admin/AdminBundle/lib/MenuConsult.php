<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Admin\AdminBundle\lib;

class MenuConsult extends AppConsult {

    public function __construct($repository) {
        parent::__construct($repository);
    }

    public function returnPrioridade() {
        $sql = $this->repository->createQueryBuilder('p')
                ->orderBy('p.id', 'DESC')
                ->getQuery();
        $result = $sql->getResult();

        if (empty($result)) {
            return 1;
        }
        return $result[0]->getPrioridade() + 1;
    }

    public function returnFilhos($pai = 1, $tipo = 1, $nivel = 3) {
        $sql = $this->repository->createQueryBuilder('p')
                ->where('p.tipo = :tipo')
                ->andWhere('p.idMenuPai = :pai')
                ->andWhere('p.nivel = :nivel')
                ->orderBy('p.prioridade', 'ASC')
                ->setParameter('pai', $pai)
                ->setParameter('tipo', $tipo)
                ->setParameter('nivel', $nivel)
                ->getQuery();

        return $sql->getArrayResult();
    }

    public function returnMenu($pai = 1, $tipo = 1, $nivel = 3) {
        $sql = $this->repository->createQueryBuilder('p')
                ->where('p.tipo = :tipo')
                ->andWhere('p.idMenuPai = :pai')
                ->andWhere('p.nivel = :nivel')
                ->orderBy('p.prioridade', 'ASC')
                ->setParameter('pai', $pai)
                ->setParameter('tipo', $tipo)
                ->setParameter('nivel', $nivel)
                ->getQuery();

        $resultSet = $sql->getArrayResult();

        if (empty($resultSet)) {
            return false;
        }
        for ($i = 0; $i < count($resultSet); $i++) {
            if ($resultSet[$i]['id'] != $pai) {
                $resultSet[$i]['Filhos'] = $this->returnFilhos($resultSet[$i]['id'], $tipo, $nivel);
            }
        }
        return $resultSet;
    }

    

}
