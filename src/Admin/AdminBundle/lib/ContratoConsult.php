<?php

namespace Admin\AdminBundle\lib;

use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\Curso;

class ContratoConsult {

    public function returnCursosContratados($idPessoa, $em) {

        $contrato = $this->returnContratos($idPessoa, $em);
        if (empty($contrato)) {
            return false;
        }

        $resultSet = array();

        $consult = new CursoConsult();
        for ($i = 0; $i < count($contrato); $i++) {

            if ($contrato[$i]->getIdCurso()->getStatus() != 3) {
                $cursos = $consult->returnCurso($contrato[$i]->getIdCurso()->getId(), $em);

                $imagem = $consult->returnCursoImagemSlider($contrato[$i]->getIdCurso()->getId(), $em);

                $resultSet[$i] = $this->mountCursosContratados($cursos, $contrato[$i]->getStatus(), $imagem[0]);
            }
        }

        return $resultSet;
    }

    public function returnContratosByCurso($idPessoa, $idCurso, $em) {
        $repository = $em->getRepository("\Admin\AdminBundle\Entity\Contrato");
        return $repository->createQueryBuilder("item")
                        ->where("item.idPessoa = :idPessoa")
                        ->andWhere("item.idCurso = :idCurso")
                        ->setParameter("idPessoa", $idPessoa)
                        ->setParameter("idCurso", $idCurso)
                        ->getQuery()
                        ->getResult();
    }

    public function returnContratos($idPessoa, $em) {
        $repository = $em->getRepository('\Admin\AdminBundle\Entity\Contrato');
        return $repository->createQueryBuilder('item')
                        ->where('item.idPessoa = :idPessoa')
                        ->setParameter('idPessoa', $idPessoa)
                        ->orderBy('item.id', 'DESC')
                        ->getQuery()
                        ->getResult();
    }

    public function mountCursosContratados(Curso $curso, $status, $imagem) {
        return array(
            'id' => $curso->getId(),
            'titulo' => $curso->getTitulo(),
            'slug' => $curso->getSlug(),
            'valor' => $curso->getIdTabelaPreco()->getValor(),
            'duracao' => $curso->getDuracao(),
            'hora_aula' => $curso->getHoraAula(),
            'credito' => $curso->getCredito(),
            'imagem' => $imagem["imagem"],
            'status' => $status,
        );
    }

    public function returnContratosFaturasByPessoa($idPessoa, $em) {
        $obj = $this->returnContratos($idPessoa, $em);

        if (!empty($obj)) {
            $result = array();
            for ($i = 0; $i < count($obj); $i++) {
                $result[$i]["id"] = $obj[$i]->getId();
                $result[$i]["Matricula"] = $obj[$i]->getMatricula();
                $result[$i]["Curso"] = $obj[$i]->getIdCurso()->getTitulo();
                $result[$i]["Faturas"] = $this->returnFaturas($obj[$i]->getId(), $em);
                $result[$i]["Situacao"] = $obj[$i]->getStatus();
            }
        }
        return $result;
    }

    public function returnFaturas($idContrato, $em) {
        $repository = $em->getRepository('\Admin\AdminBundle\Entity\Fatura');
        $faturas = $repository->createQueryBuilder('item')
                ->where('item.idContrato = :idContrato')
                ->andWhere('item.status = :status')
                ->setParameter('idContrato', $idContrato)
                ->setParameter('status', 1)
                ->getQuery()
                ->getArrayResult();

        return $faturas;
    }

    public function returnFaturaAtual($idPessoa, $em) {
        $date = date("Y-m-d");
        $d = explode("-", $date);

        $paramDate = $d[0] . "-" . $d[1] . "-25";

        $repository = $em->getRepository('\Admin\AdminBundle\Entity\Fatura');
        $faturas = $repository->createQueryBuilder('fatura')
                ->select('fatura', 'contrato')
                ->leftJoin('fatura.idContrato', 'contrato')
                ->where('fatura.vencimento = :vencimento')
                ->andWhere('contrato.idPessoa = :idPessoa')
                ->setParameter('vencimento', $paramDate)
                ->setParameter('idPessoa', $idPessoa)
                ->getQuery()
                ->getResult();

        return $faturas;
    }

}
