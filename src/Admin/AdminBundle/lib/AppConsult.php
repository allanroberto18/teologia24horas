<?php

namespace Admin\AdminBundle\lib;

class AppConsult {

    protected $repository;

    public function __construct($repository=null) {
        if ($repository) {
            $this->repository = $repository;
        }
    }

    function getRepository() {
        return $this->repository;
    }

    function setRepository($repository) {
        $this->repository = $repository;
    }
}
