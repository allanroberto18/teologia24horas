<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 22/04/15
 * Time: 16:54
 */

namespace Admin\AdminBundle\lib;


class Migrate
{

    private $table;
    private $attrib;
    private $value;
    private $order;
    private $attribOrder;

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getAttrib()
    {
        return $this->attrib;
    }

    /**
     * @param mixed $attrib
     */
    public function setAttrib($attrib)
    {
        $this->attrib = $attrib;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        switch ($order) {
            case 1 :
                $order = 'ORDER BY ' . $this->attribOrder . ' ASC';
                break;
            case 2 :
                $order = 'ORDER BY ' . $this->attribOrder . ' DESC';
                break;
            default :
                $order = 'ORDER BY ' . $this->attribOrder . ' ASC';
        }
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getAttribOrder()
    {
        return $this->attribOrder;
    }

    /**
     * @param mixed $attribOrder
     */
    public function setAttribOrder($attribOrder)
    {
        $this->attribOrder = $attribOrder;
    }

    function __construct($table = null, $order = 1, $attribOrder = 'id')
    {
        $this->setTable($table);
        $this->setAttribOrder($attribOrder);
        $this->setOrder($order);
    }

    public function queryList()
    {
        if (empty($this->table) && empty($this->order)) {
            return false;
        }
        $query = "SELECT * FROM {$this->table} ";
        if (!empty($this->attrib)) {
            $query .= "WHERE {$this->attrib} = :{$this->attrib} ";
        }
        $query .= $this->order;

        return $query;
    }

    public function returnKeys(array $registers)
    {
        return array_keys($registers);
    }

    public function queryInsert($table, array $registers)
    {
        if (empty($table) && empty($registers))
            return false;

        $keys = $this->returnKeys($registers);
        $query = "INSERT INTO {$table} (";
        for ($i = 0; $i < count($keys); $i++) {
            $query .= ($i == 0) ? $keys[$i] : ", " . $keys[$i];
        }
        $query .= ") VALUES (";
        for ($i = 0; $i < count($keys); $i++) {
            $query .= ($i == 0) ? ":" . $keys[$i] : ", :" . $keys[$i];
        }
        $query .= ")";

        return $query;
    }

    public function queryUpdate($table, array $registers, array $conditions)
    {
        if (empty($table) && empty($registers))
            return false;

        $keys = $this->returnKeys($registers);
        $keysCondition = $this->returnKeys($conditions);

        $query = "UPDATE {$table} SET ";
        for ($i = 0; $i < count($keys); $i++) {
            $query .= ($i == 0) ? ($keys[$i] . " = :" . $keys[$i]) : (", " . $keys[$i] . " = :" . $keys[$i]);
        }
        if (empty($conditions)) {
            return $query;
        }
        $query .= " WHERE ";
        for ($i = 0; $i < count($keysCondition); $i++) {
            $query .= ($i == 0) ? ($keysCondition[$i] . " = :" . $keysCondition[$i]) : (", " . $keysCondition[$i] . " = :" . $keysCondition[$i]);
        }
        return $query;
    }

    public function returnList(ConnectDB $conn, $type = 1)
    {
        $pdo = $conn->connect();
        $stmt = $pdo->prepare($this->queryList());

        if (!empty($this->attrib)) {
            $stmt->bindValue(":{$this->attrib}", "{$this->value}");
        }
        try {
            $stmt->execute();
        } catch (\PDOException $e) {
            return $e->getMessage();
        } finally {
            $result = ($type == 1) ? $stmt->fetchAll(\PDO::FETCH_ASSOC) : $stmt->fetch(\PDO::FETCH_ASSOC);

            $conn->closeConnect();

            return $result;
        }
    }

    public function insertData($table, array $registers)
    {
        if (empty($table) && empty($registers))
            return false;

        $conn = new ConnectDB('ead_teste');

        $query = $this->queryInsert($table, $registers);
        $keys = $this->returnKeys($registers);

        $pdo = $conn->connect();
        $stmt = $pdo->prepare($query);

        foreach ($keys as $key) {
            $stmt->bindValue(":{$key}", is_numeric($registers[$key]) ? $registers[$key] :  "'" . $registers[$key] . "'");
        }

        $stmt->execute();

        $id = $pdo->lastInsertId("{$table}_id_seq");

        $conn->closeConnect();

        return $id;

    }

    public function updateData(ConnectDB $conn, $table, array $registers, array $conditions)
    {
        if (empty($table) && empty($registers))
            return false;

        $pdo = $conn->connect();
        $stmt = $pdo->prepare($this->queryInsert($table, $registers));

        $keys = $this->returnKeys($registers);
        $keysCondition = $this->returnKeys($conditions);

        foreach ($keys as $key) {
            $stmt->bindValue(":{$key}", "'{$registers[$key]}'");
        }

        foreach ($keysCondition as $key) {
            $stmt->bindValue(":{$key}", "'{$conditions[$key]}'");
        }

        try {
            $stmt->execute();
        } catch (\PDOException $e) {
            return $e->getMessage();
        } finally {
            $conn->closeConnect();

            return true;
        }
    }

    function pgsqlLastInsertId($table)
    {
        // Gets this table's last sequence value
        $query = "SELECT currval('{$table}_id_seq') AS last_value";

        $conn = new ConnectDB('ead_teste');

        $pdo = $conn->connect();
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        $result = false;

        if ($stmt) {
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        }

        $conn->closeConnect();

        return ($result) ? $result['last_value'] : false;;
    }
}