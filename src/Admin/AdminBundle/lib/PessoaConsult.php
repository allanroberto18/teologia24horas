<?php

namespace Admin\AdminBundle\lib;

use Symfony\Component\HttpFoundation\Session\Session;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\PessoaEndereco;

class PessoaConsult extends AppConsult {

    private $email;
    private $senha;
    public $session;

    public function __construct($repository = null) {
        parent::__construct($repository);
        $this->session = new Session();
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = !empty($senha) ? md5($senha) : "";
    }

    function setSenhaClean($senha) {
        $this->senha = $senha;
    }

    public function setParams($email, $senha) {
        $this->setEmail($email);
        $this->setSenha($senha);
    }

    public function validarUsuario($em) {
        $msg = "";
        if (empty($this->email)) {
            $msg .= "<span class='fa fa-remove'></span> Informe o e-mail do usuário<br />";
        }
        if (empty($this->senha)) {
            $msg .= "<span class='fa fa-remove'></span> Informe a senha do usuário";
        }
        
        if (!empty($msg)) {
            $this->session->getFlashBag()->add("error", $msg);
            return;
        }
        return $this->efetuarLogin($em);
    }

    public function efetuarLogin($em) {
        $sql = $this->repository->createQueryBuilder('p')
                ->where('p.email = :email')
                ->andWhere('p.senha = :senha')
                ->setParameter('email', $this->email)
                ->setParameter('senha', $this->senha)
                ->getQuery();

        $result = $sql->getResult();

        if (empty($result[0])) {
            $this->session->getFlashBag()->set("error", "Usuário ou senha inválida");
            return false;
        }
        if (!$result[0] instanceof Pessoa) {
            $this->session->getFlashBag()->set("error", "Usuário ou senha inválida");
            return false;
        }

        $this->mountSessao($em, $result[0]);

        $carrinho = $this->session->get('carrinho');

        if (!empty($carrinho)) {
            header("location:/AVA/Aluno/curso/selecionar/{$carrinho['curso']}/{$carrinho['slug']}");
            exit();
        }

        header("location:/AVA/Usuario/Home/");
        exit();
    }

    public function getPessoaEndereco($idPessoa, $em) {
        $repository = $em->getRepository("\Admin\AdminBundle\Entity\PessoaEndereco");
        return $repository->createQueryBuilder('item')
                        ->where('item.idPessoa = :idPessoa')
                        ->setParameter('idPessoa', $idPessoa)
                        ->getQuery()
                        ->getSingleResult();
    }

    public function checkContratosForAmbiente(Pessoa $pessoa) {
        $contratos = $pessoa->getContrato();

        $check = 0;
        if (count($contratos) > 0) {
            for ($i = 0; $i < count($contratos); $i++) {
                if ($contratos[$i] instanceof Contrato) {
                    if ($contratos[$i]->getIdCurso()->getIdTabelaPreco()->getValor() == 0) {
                        continue;
                    } else {
                        $check = 1;
                    }
                }
            }
        }
        return $check;
    }

    public function mountSessao($em, Pessoa $pessoa) {
        $value['id'] = $pessoa->getId();
        $value['nome'] = $pessoa->getPfNome();
        $value['email'] = $pessoa->getEmail();
        if (empty($pessoa->getFoto())) {
            $value['foto'] = ($pessoa->getPfSexo() == 1) ? "user.png" : "user_woman.png";
        } else {
            $value['foto'] = $pessoa->getFoto();
        }
        $value['check'] = $this->checkContratosForAmbiente($pessoa);

        try {
            $endereco = $this->getPessoaEndereco($pessoa->getId(), $em);
            $value["cep"] = $endereco->getCep();
        } catch (\Exception $exc) {
            $value["cep"] = null;
        }

        $this->session->set('ava', $value);

        $curso = new CursoConsult();
        $cursos = $curso->getCursosBySlider($em);

        $this->session->set('cursos', $cursos);

        return $value;
    }

}
