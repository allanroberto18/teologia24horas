<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Admin\AdminBundle\Entity\PessoaEndereco;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Form\PessoaEnderecoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\CepbrEndereco;
use Admin\AdminBundle\Entity\CepbrEstado;
use Admin\AdminBundle\Entity\CepbrCidade;

/**
 * PessoaEndereco controller.
 *
 */
class PessoaEnderecoController extends MainController {

    /**
     * Lists all PessoaEndereco entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:PessoaEndereco');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.idPessoa', array('label' => 'Pessoa')));
        $gridConfig->addField(new Field('item.cep', array('label' => 'CEP', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.complemento', array('label' => 'Complemento', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:PessoaEndereco:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Endereço da Pessoa'
        ));
    }

    /**
     * Creates a new PessoaEndereco entity.
     *
     */
    public function createAction($idPessoa, Request $request) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        
        $entity = new PessoaEndereco();
        $form = $this->createCreateForm($idPessoa, $entity, $em);
        
        $dados = $request->request->all();
        $errors = array();
        
        if (!empty($dados)) {
            $service = $this->container->get('admin.localizacao');
            $result = $service->persistLocalizacao($idPessoa, $dados["admin_endereco"]);

            if ($result instanceof PessoaEndereco) {
                $entity = $result;
                return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
            }

            $errors = $result;
        }

        return $this->render('AdminBundle:PessoaEndereco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Endereço da Pessoa',
                    'errors' => $errors,
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a form to create a PessoaEndereco entity.
     *
     * @param PessoaEndereco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($idPessoa, PessoaEndereco $entity, $em) {
        $form = $this->createForm(new PessoaEnderecoType($em), $entity, array(
            'action' => $this->generateUrl('Pessoa_Endereco_create', array('idPessoa' => $idPessoa)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new PessoaEndereco entity.
     *
     */
    public function newAction($idPessoa) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);
        if (!$pessoa instanceof Pessoa) {
            $this->redirect($this->generateUrl("PessoaFisica"));
        }

        $entity = new PessoaEndereco();
        $form = $this->createCreateForm($idPessoa, $entity, $em);

        return $this->render('AdminBundle:PessoaEndereco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Endereço da Pessoa',
                    'errors' => '',
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Finds and displays a PessoaEndereco entity.
     *
     */
    public function showAction($idPessoa, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);
        if (!$pessoa instanceof Pessoa) {
            $this->redirect($this->generateUrl("PessoaFisica"));
        }

        $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaEndereco entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PessoaEndereco:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Endereço da Pessoa'
        ));
    }

    /**
     * Displays a form to edit an existing PessoaEndereco entity.
     *
     */
    public function editAction($idPessoa, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);
        if (!$pessoa instanceof Pessoa) {
            $this->redirect($this->generateUrl("PessoaFisica"));
        }

        $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);
        $form = $this->createEditForm($idPessoa, $entity, $em);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaEndereco entity.');
        }

        return $this->render('AdminBundle:PessoaEndereco:edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Endereço da Pessoa',
                    'errors' => '',
                    'idPessoa' => $idPessoa,
                    'id' => $id,
        ));
    }

    /**
     * Creates a form to edit a PessoaEndereco entity.
     *
     * @param PessoaEndereco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm($idPessoa, PessoaEndereco $entity, $em) {
        $form = $this->createForm(new PessoaEnderecoType($em), $entity, array(
            'action' => $this->generateUrl('Pessoa_Endereco_update', array('idPessoa' => $idPessoa, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing PessoaEndereco entity.
     *
     */
    public function updateAction($idPessoa, Request $request, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);
        $form = $this->createEditForm($idPessoa, $entity, $em);

        $errors = array();
        $dados = $request->request->all();

        if (!empty($dados)) {
            $service = $this->container->get('admin.localizacao');
            $result = $service->persistLocalizacao($idPessoa, $dados["admin_endereco"], $id);

            if ($result instanceof PessoaEndereco) {
                $entity = $result;
                return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
            }

            $errors = $result;
        }

        return $this->render('AdminBundle:PessoaEndereco:edit.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Endereço da Pessoa',
                    'errors' => $errors,
                    'idPessoa' => $idPessoa,
                    'id' => $id,
        ));
    }

    /**
     * Deletes a PessoaEndereco entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:PessoaEndereco')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PessoaEndereco entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Pessoa_Endereco'));
    }

    /**
     * Creates a form to delete a PessoaEndereco entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Pessoa_Endereco_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function getCEPAction(Request $request) {
        $data = $request->request->all();

        //$cep = str_replace(".", "", $data["cep"]);
        //$cep = str_replace("-", "", $cep);
        $id = $data["id"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if ($repository instanceof Pessoa) {
            $return = array(
                'cidade' => $repository->getCidade(),
                'uf' => $repository->getEstado(),
                'pais' => $repository->getPais(),
            );
        } else {
            $return = array(
                'cidade' => '',
                'uf' => '',
                'pais' => 'Brasil',
            );
        }

        return new Response(json_encode($return), 200, array('Content-Type' => 'application/json'));
    }

    public function getUFAction(Request $request) {
        $data = $request->request->all();
        $pais = $data['param'];
        if ($pais == "Brasil") {
            $em = $this->getDoctrine()->getManager();
            $estados = $em->getRepository('AdminBundle:CepbrEstado')->findAll();

            foreach ($estados as $estado) {
                if ($estado instanceof CepbrEstado) {
                    $return[] = array(
                        'uf' => $estado->getUf(),
                        'estado' => $estado->getEstado(),
                    );
                }
            }
        }

        return new Response(json_encode($return), 200, array('Content-Type' => 'application/json'));
    }

    public function getCidadesAction(Request $request) {
        $data = $request->request->all();

        $uf = $data["param"];

        $em = $this->getDoctrine()->getManager();
        $cidades = $em->getRepository('AdminBundle:CepbrCidade')->findBy(array('uf' => $uf));

        $return = array();

        foreach ($cidades as $cidade) {
            if ($cidade instanceof CepbrCidade) {
                $return[] = array(
                    'cidade' => $cidade->getCidade(),
                );
            }
        }

        return new Response(json_encode($return), 200, array('Content-Type' => 'application/json'));
    }

}
