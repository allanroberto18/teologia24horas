<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Usuario;
use Admin\AdminBundle\Form\UsuarioType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Usuario controller.
 *
 */
class UsuarioController extends MainController {

    /**
     * Lists all Usuario entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Usuario');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.nome', array('label' => 'Nome', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.email', array('label' => 'Usuário', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");
        
        return $this->render('AdminBundle:Usuario:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Usuário'
        ));
    }

    /**
     * Creates a new Usuario entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Usuario();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Usuario"));
        $breadcrumbs->addItem("Salvar Registro");
        
        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity->setIdentificador(md5(date("Ymdhis") . mt_rand()));
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Usuario_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Usuario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Usuário',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuario $entity) {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('Usuario_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Usuario entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new Usuario();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Usuario"));
        $breadcrumbs->addItem("Novo");
        
        return $this->render('AdminBundle:Usuario:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Usuário',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Usuario entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Usuario"));
        $breadcrumbs->addItem("Visualiar");
        
        return $this->render('AdminBundle:Usuario:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Usuário'
        ));
    }

    /**
     * Displays a form to edit an existing Usuario entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Usuario"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Usuario_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");
        
        return $this->render('AdminBundle:Usuario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Usuário',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Usuario entity.
     *
     * @param Usuario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Usuario $entity) {
        $form = $this->createForm(new UsuarioType(), $entity, array(
            'action' => $this->generateUrl('Usuario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Usuario entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Usuario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);
        
        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Usuario_show', array('id' => $id)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Usuario"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Usuario_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");
        
        return $this->render('AdminBundle:Usuario:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Usuário',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Usuario entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Usuario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Usuario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Usuario'));
    }

    /**
     * Creates a form to delete a Usuario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Usuario_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
