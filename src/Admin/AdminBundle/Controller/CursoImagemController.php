<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\CursoImagem;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Form\CursoImagemType;

/**
 * CursoImagem controller.
 *
 */
class CursoImagemController extends MainController {

    public function checkCurso($idCurso) {
        if (empty($idCurso)) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Curso")->find($idCurso);
        return $entity;
    }

    /**
     * Lists all CursoImagem entities.
     *
     */
    public function indexAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoImagem');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.idCurso = :idCurso')
                ->setParameter('idCurso', $idCurso);
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.tipo', array(
            'label' => 'Tipo',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                $imagem = new CursoImagem();
                $imagem->setTipo($value);
                return $imagem->returnTipo();
            }
        )));
        $gridConfig->addField(new Field('item.imagem', array(
            'label' => 'Imagem',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                $imagem = new CursoImagem();
                $imagem->setImagem($value);
                print "<img src='/files/cursos/" . $value . "' class='col-xs-6 col-md-3'/>";
            }
        )));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Listagem de Imagens");

        return $this->render('AdminBundle:CursoImagem:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Imagens do Curso',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a new CursoImagem entity.
     *
     */
    public function createAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new CursoImagem();
        $form = $this->createCreateForm($entity, $idCurso);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdCurso($curso);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Salvar Imagem");

        return $this->render('AdminBundle:CursoImagem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Imagens do Curso',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to create a CursoImagem entity.
     *
     * @param CursoImagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoImagem $entity, $idCurso) {
        $form = $this->createForm(new CursoImagemType(), $entity, array(
            'action' => $this->generateUrl('Curso_Imagem_create', array('idCurso' => $idCurso)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoImagem entity.
     *
     */
    public function newAction($idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new CursoImagem();
        $form = $this->createCreateForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Nova Imagem");

        return $this->render('AdminBundle:CursoImagem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Imagens do Curso',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Finds and displays a CursoImagem entity.
     *
     */
    public function showAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoImagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoImagem entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Imagem");

        return $this->render('AdminBundle:CursoImagem:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Imagens do Curso',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Displays a form to edit an existing CursoImagem entity.
     *
     */
    public function editAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoImagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoImagem entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Imagem", $this->get("router")->generate("Curso_Imagem_show", array('id' => $id, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:CursoImagem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Imagens do Curso',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to edit a CursoImagem entity.
     *
     * @param CursoImagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoImagem $entity, $idCurso) {
        $form = $this->createForm(new CursoImagemType(), $entity, array(
            'action' => $this->generateUrl('Curso_Imagem_update', array('idCurso' => $idCurso, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoImagem entity.
     *
     */
    public function updateAction(Request $request, $idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoImagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoImagem entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Imagem", $this->get("router")->generate("Curso_Imagem_show", array('id' => $id, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:CursoImagem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Imagens do Curso',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Deletes a CursoImagem entity.
     *
     */
    public function deleteAction(Request $request, $idCurso, $id) {
        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $form = $this->createDeleteForm($id, $idCurso);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoImagem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoImagem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('CursoImagem'));
    }

    /**
     * Creates a form to delete a CursoImagem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $idCurso) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('CursoImagem_delete', array('idCurso' => $idCurso, 'id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
