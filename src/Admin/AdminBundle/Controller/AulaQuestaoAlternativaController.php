<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\AulaQuestao;
use Admin\AdminBundle\Entity\AulaQuestaoAlternativa;
use Admin\AdminBundle\Form\AulaQuestaoAlternativaType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AulaQuestaoAlternativaController extends MainController
{
    /**
     * @Route("/Questao/{idQuestao}/novo", name="admin_aula_questao_alternativa_novo")
     * @Template("AdminBundle:AulaQuestaoAlternativa:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idQuestao, Request $request)
    {
        $this->checkLogin();

        $questao = $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $entity = new AulaQuestaoAlternativa();

        $form = $this->createForm(new AulaQuestaoAlternativaType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdAulaQuestao($questao);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_alternativa_novo'
                : 'admin_aula_questao_alternativa_listar';

            return $this->redirectToRoute($nextAction, array('idQuestao' => $idQuestao));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Alternativas da Questão: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Alternativas da Questão', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Questao/{idQuestao}/{id}/atualizar", name="admin_aula_questao_alternativa_atualizar")
     * @Template("AdminBundle:AulaQuestaoAlternativa:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idQuestao, $id, Request $request)
    {
        $this->checkLogin();

        $questao = $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativa')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativa) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
        }

        $form = $this->createForm(new AulaQuestaoAlternativaType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_alternativa_novo'
                : 'admin_aula_questao_alternativa_listar';

            return $this->redirectToRoute($nextAction, array('idQuestao' => $idQuestao));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas da Questão: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao)));
        $breadcrumbs->addItem('Visualizar: ' . $entity->getTitulo(), $this->get('router')->generate('admin_aula_questao_alternativa_visualizar', array('idQuestao' => $idQuestao, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Alternativas da Questão', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Questao/{idQuestao}/listar", name="admin_aula_questao_alternativa_listar")
     * @Template("AdminBundle:AulaQuestaoAlternativa:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idQuestao, Request $request)
    {
        $this->checkLogin();

        $questao = $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $repository = $this->getDoctrine()->getRepository('AdminBundle:AulaQuestaoAlternativa');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.idAulaQuestao = :questao')
            ->setParameter('status', '1')
            ->setParameter('questao', $idQuestao)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.alternativa', array('label' => 'Alternativa', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas da Questão: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('admin_aula_questao_alternativa_delete_selecionado', ['idQuestao' => $idQuestao]),
            'novo' => $this->generateUrl('admin_aula_questao_alternativa_novo', ['idQuestao' => $idQuestao]),
            'modulo' => array('titulo' => 'Alternativas da Questão', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Questao/{idQuestao}/{id}/visualizar", name="admin_aula_questao_alternativa_visualizar")
     * @Template("AdminBundle:AulaQuestaoAlternativa:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idQuestao, $id)
    {
        $this->checkLogin();

        $questao = $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativa')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativa) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas da Qestão: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Alternativas da Qestão', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Questao/{idQuestao}/{id}/delete", name="admin_aula_questao_alternativa_delete")
     * @Method("GET")
     */
    public function deleteAction($idQuestao, $idQuestao, $id)
    {
        $this->checkLogin();

        $questao = $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativa')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativa) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
    }

    /**
     * @Route("/Questao/{idQuestao}/delete/selecionados", name="admin_aula_questao_alternativa_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idQuestao, Request $request)
    {
        $this->checkLogin();

        $this->checkParent($idQuestao, "AdminBundle", 'AulaQuestao', 'admin_home', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativa')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_alternativa_listar', array('idQuestao' => $idQuestao));
    }
}
