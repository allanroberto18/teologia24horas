<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\AulaQuestaoAlternativaContrato;
use Admin\AdminBundle\Form\AulaQuestaoAlternativaContratoType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AulaQuestaoAlternativaContratoController extends MainController
{
    /**
     * @Route("/Contrato/{idContrato}/novo", name="admin_aula_questao_alternativa_contrato_novo")
     * @Template("AdminBundle:AulaQuestaoAlternativaContrato:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $entity = new AulaQuestaoAlternativaContrato();

        $form = $this->createForm(new AulaQuestaoAlternativaContratoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdContrato($contrato);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_alternativa_contrato_novo'
                : 'admin_aula_questao_alternativa_contrato_listar';

            return $this->redirectToRoute($nextAction, array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Alternativas Selecionadas: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Alternativas Selecionadas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/atualizar", name="admin_aula_questao_alternativa_contrato_atualizar")
     * @Template("AdminBundle:AulaQuestaoAlternativaContrato:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idContrato, $id, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativaContrato')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativaContrato) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
        }

        $form = $this->createForm(new AulaQuestaoAlternativaContratoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_alternativa_contrato_novo'
                : 'admin_aula_questao_alternativa_contrato_listar';

            return $this->redirectToRoute($nextAction, array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas Selecionadas: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('admin_aula_questao_alternativa_contrato_visualizar', array('idContrato' => $idContrato, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Alternativas Selecionadas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/listar", name="admin_aula_questao_alternativa_contrato_listar")
     * @Template("AdminBundle:AulaQuestaoAlternativaContrato:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $repository = $this->getDoctrine()->getRepository('AdminBundle:AulaQuestaoAlternativaContrato');
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, alternativa')
            ->from('AdminBundle:AulaAcesso', 'item')
            ->leftJoin('item.idAulaQuestaoAlternativa', 'alternativa')
            ->where('item.idContrato = :contrato')
            ->setParameter('contrato', $idContrato)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('alternativa.alternativa', array('label' => 'Alternativa Selecionada', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas Selecionadas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('admin_aula_questao_alternativa_contrato_delete_selecionado', ['idContrato' => $idContrato]),
            'novo' => $this->generateUrl('admin_aula_questao_alternativa_contrato_novo', ['idContrato' => $idContrato]),
            'modulo' => array('titulo' => 'Alternativas Selecionadas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/visualizar", name="admin_aula_questao_alternativa_contrato_visualizar")
     * @Template("AdminBundle:AulaQuestaoAlternativaContrato:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idContrato, $id)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativaContrato')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativaContrato) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Alternativas Selecionadas: Listar Registros', $this->get('router')->generate('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Alternativas Selecionadas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/delete", name="admin_aula_questao_alternativa_contrato_delete")
     * @Method("GET")
     */
    public function deleteAction($idContrato, $id)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativaContrato')->find($id);
        if (!$entity instanceof AulaQuestaoAlternativaContrato) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
    }

    /**
     * @Route("/Contrato/{idContrato}/delete/selecionados", name="admin_aula_questao_alternativa_contrato_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:AulaQuestaoAlternativaContrato')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_alternativa_contrato_listar', array('idContrato' => $idContrato));
    }
}
