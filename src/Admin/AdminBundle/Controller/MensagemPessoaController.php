<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\AdminBundle\Entity\MensagemPessoa;
use Admin\AdminBundle\Form\MensagemPessoaType;

/**
 * MensagemPessoa controller.
 *
 */
class MensagemPessoaController extends Controller
{

    /**
     * Lists all MensagemPessoa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:MensagemPessoa')->findAll();

        return $this->render('AdminBundle:MensagemPessoa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new MensagemPessoa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new MensagemPessoa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_Pessoa_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:MensagemPessoa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MensagemPessoa entity.
     *
     * @param MensagemPessoa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MensagemPessoa $entity)
    {
        $form = $this->createForm(new MensagemPessoaType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_Pessoa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MensagemPessoa entity.
     *
     */
    public function newAction()
    {
        $entity = new MensagemPessoa();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:MensagemPessoa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MensagemPessoa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemPessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemPessoa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MensagemPessoa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MensagemPessoa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemPessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemPessoa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MensagemPessoa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a MensagemPessoa entity.
    *
    * @param MensagemPessoa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MensagemPessoa $entity)
    {
        $form = $this->createForm(new MensagemPessoaType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_Pessoa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing MensagemPessoa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemPessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemPessoa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_Pessoa_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:MensagemPessoa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a MensagemPessoa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:MensagemPessoa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MensagemPessoa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Mensagem_Pessoa'));
    }

    /**
     * Creates a form to delete a MensagemPessoa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('Mensagem_Pessoa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
