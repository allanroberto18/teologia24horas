<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\Aula;
use Admin\AdminBundle\Entity\AulaQuestao;
use Admin\AdminBundle\Form\AulaQuestaoType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AulaQuestaoController extends MainController
{
    /**
     * @Route("/Aula/{idAula}/novo", name="admin_aula_questao_novo")
     * @Template("AdminBundle:AulaQuestao:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idAula, Request $request)
    {
        $this->checkLogin();

        $aula = $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $disciplina = $aula->getIdDisciplina();
        $curso = $disciplina->getIdCurso();

        $entity = new AulaQuestao();

        $form = $this->createForm(new AulaQuestaoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdAula($aula);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_novo'
                : 'admin_aula_questao_listar';

            return $this->redirectToRoute($nextAction, array('idAula' => $idAula));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $curso->getId())));
        $breadcrumbs->addItem("Disciplina", $this->get('router')->generate('Disciplina_show', array('idCurso' => $curso->getId(), 'id' => $disciplina->getId())));
        $breadcrumbs->addItem("Aula", $this->get('router')->generate('Disciplina_Aula_show', array('idCurso' => $curso->getId(), 'idDisciplina' => $disciplina->getId(), 'id' => $idAula)));
        $breadcrumbs->addItem('Questões da Avaliação: Listar Registros', $this->get('router')->generate('admin_aula_questao_listar', array('idAula' => $idAula)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Questões da Avaliação', 'descricao' => ''),
            'idAula' => $idAula,
            'idDisciplina' => $disciplina->getId(),
            'idCurso' => $curso->getId(),
        );
    }

    /**
     * @Route("/Aula/{idAula}/{id}/atualizar", name="admin_aula_questao_atualizar")
     * @Template("AdminBundle:AulaQuestao:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idAula, $id, Request $request)
    {
        $this->checkLogin();

        $aula = $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $disciplina = $aula->getIdDisciplina();
        $curso = $disciplina->getIdCurso();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestao')->find($id);
        if (!$entity instanceof AulaQuestao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
        }

        $form = $this->createForm(new AulaQuestaoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_questao_novo'
                : 'admin_aula_questao_listar';

            return $this->redirectToRoute($nextAction, array('idAula' => $idAula));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $curso->getId())));
        $breadcrumbs->addItem("Disciplina", $this->get('router')->generate('Disciplina_show', array('idCurso' => $curso->getId(), 'id' => $disciplina->getId())));
        $breadcrumbs->addItem("Aula", $this->get('router')->generate('Disciplina_Aula_show', array('idCurso' => $curso->getId(), 'idDisciplina' => $disciplina->getId(), 'id' => $idAula)));
        $breadcrumbs->addItem('Questões da Aula: Listar Registros', $this->get('router')->generate('admin_aula_questao_listar', array('idAula' => $idAula)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('admin_aula_questao_visualizar', array('idAula' => $idAula, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Questões da Aula', 'descricao' => ''),
            'idAula' => $idAula,
            'idDisciplina' => $disciplina->getId(),
            'idCurso' => $curso->getId(),
        );
    }

    /**
     * @Route("/Aula/{idAula}/listar", name="admin_aula_questao_listar")
     * @Template("AdminBundle:AulaQuestao:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idAula, Request $request)
    {
        $this->checkLogin();

        $aula = $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $disciplina = $aula->getIdDisciplina();
        $curso = $disciplina->getIdCurso();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:AulaQuestao');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.idAula = :aula')
            ->setParameter('status', '1')
            ->setParameter('aula', $idAula)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('questao.questao', array('label' => 'Questão', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $curso->getId())));
        $breadcrumbs->addItem("Disciplina", $this->get('router')->generate('Disciplina_show', array('idCurso' => $curso->getId(), 'id' => $disciplina->getId())));
        $breadcrumbs->addItem("Aula", $this->get('router')->generate('Disciplina_Aula_show', array('idCurso' => $curso->getId(), 'idDisciplina' => $disciplina->getId(), 'id' => $idAula)));
        $breadcrumbs->addItem('Questões da Aula: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('admin_aula_questao_delete_selecionado', ['idAula' => $idAula]),
            'novo' => $this->generateUrl('admin_aula_questao_novo', ['idAula' => $idAula]),
            'modulo' => array('titulo' => 'Questões da Aula', 'descricao' => ''),
            'idAula' => $idAula,
            'idDisciplina' => $disciplina->getId(),
            'idCurso' => $curso->getId(),
        );
    }

    /**
     * @Route("/Aula/{idAula}/{id}/visualizar", name="admin_aula_questao_visualizar")
     * @Template("AdminBundle:AulaQuestao:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idAula, $id)
    {
        $this->checkLogin();

        $aula = $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $disciplina = $aula->getIdDisciplina();
        $curso = $disciplina->getIdCurso();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestao')->find($id);
        if (!$entity instanceof AulaQuestao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $curso->getId())));
        $breadcrumbs->addItem("Disciplina", $this->get('router')->generate('Disciplina_show', array('idCurso' => $curso->getId(), 'id' => $disciplina->getId())));
        $breadcrumbs->addItem("Aula", $this->get('router')->generate('Disciplina_Aula_show', array('idCurso' => $curso->getId(), 'idDisciplina' => $disciplina->getId(), 'id' => $idAula)));
        $breadcrumbs->addItem('Questões da Aula: Listar Registros', $this->get('router')->generate('admin_aula_questao_listar', array('idAula' => $idAula)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Questões da Aula', 'descricao' => ''),
            'idAula' => $idAula,
            'idDisciplina' => $disciplina->getId(),
            'idCurso' => $curso->getId(),
        );
    }

    /**
     * @Route("/Aula/{idAula}/{id}/delete", name="admin_aula_questao_delete")
     * @Method("GET")
     */
    public function deleteAction($idAula, $id)
    {
        $this->checkLogin();

        $aula = $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaQuestao')->find($id);
        if (!$entity instanceof AulaQuestao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
    }

    /**
     * @Route("/Aula/{idAula}/delete/selecionados", name="admin_aula_questao_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idAula, Request $request)
    {
        $this->checkLogin();

        $this->checkParent($idAula, "AdminBundle", 'Aula', 'admin_home', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:AulaQuestao')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_aula_questao_listar', array('idAula' => $idAula));
    }
}
