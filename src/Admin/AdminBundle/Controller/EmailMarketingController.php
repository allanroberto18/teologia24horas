<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Form\EmailMarketingType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * EmailMarketing controller.
 *
 */
class EmailMarketingController extends MainController
{

    /**
     * Lists all EmailMarketing entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:EmailMarketing');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->orderBy("item.datahoraCadastro", "DESC");

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.email', array('label' => 'E-mail', "sortable" => true, "filterable" => true)));
        $gridConfig->addField(new Field('item.datahoraCadastro', array(
            'label' => 'Data do Cadastro',
            'sortable' => true,
            'formatValueCallback' => function ($value) {
                return $value->format("d/m/Y H:i:s");
            }
        )));
        $gridConfig->addField(new Field('item.id',
            array(
                'label' => 'Tags',
                "sortable" => true,
                'formatValueCallback' => function ($value) {
                    $tags = $this->getDoctrine()->getRepository('AdminBundle:EmailMarketingTag')->findBy(array('idEmailMarketing' => $value));
                    $tag = $this->getDoctrine()->getRepository('AdminBundle:EmailMarketing')->find($value);
                    $result = "";
                    if (empty($tags))
                    {
                        $result .= "<div class='mt'><span class='label label-default'><span class='fa fa-exclamation-circle'></span> Não possui tag</span></div>";
                    }
                    $result .= "<div class='mt'>";
                    for ($i = 0; $i < count($tags); $i++)
                    {
                        $result .= "<div class='mb'>";
                        $result .= "<span class='label label-primary'><span class='fa fa-plus'></span> " . $tags[$i]->getIdTag()->getTitulo() . "</span><br />";
                        $result .= "</div>";
                    }
                    if ($tag->getPessoa()) {
                        $result .= "<span class='label label-primary'><span class='fa fa-plus'></span> Cadastrado </span><br />";
                    }
                    $result .= "</div>";

                    print $result;
                }
            ))
        );

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");
        
        return $this->render('AdminBundle:EmailMarketing:index.html.twig', array(
            'grid' => $grid,
            'titulo' => 'Listar Registros',
            'modulo' => 'E-mail Marketing'
        ));
    }

    /**
     * Creates a new EmailMarketing entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new EmailMarketing();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $em = $this->getDoctrine()->getManager();

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("email" => $entity->getEmail()));
            
            if (!$repository instanceof EmailMarketing) {
                $em->persist($entity);
                $em->flush();

                $container = $this->container->get('admin.tags');
                $container->persistEmailMarketingTag($entity, 1);
                
                $repository = $entity;
            }
            
            $message = $this->container->get('admin.email');
            $message->confirmarCadastro($repository);
            
            return $this->redirect($this->generateUrl('EmailMarketing_show', array('id' => $repository->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Salvar");
        
        return $this->render('AdminBundle:EmailMarketing:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a EmailMarketing entity.
     *
     * @param EmailMarketing $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EmailMarketing $entity)
    {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('EmailMarketing_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Creates a form to create a EmailMarketing entity.
     *
     * @param EmailMarketing $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEmailByPessoaForm(EmailMarketing $entity, $idPessoa)
    {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('EmailMarketing_update_email', array('idPessoa' => $idPessoa)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }
    
    /**
     * Displays a form to create a new EmailMarketing entity.
     *
     */
    public function newAction()
    {
        $this->checkLogin();

        $entity = new EmailMarketing();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Novo");
        
        return $this->render('AdminBundle:EmailMarketing:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => '',
        ));
    }
    
    public function editEmailAction($idPessoa)
    {
        $this->checkLogin();

        $entity = new EmailMarketing();
        $form = $this->createEmailByPessoaForm($entity, $idPessoa);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Pessoa", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Editar E-mail");
        
        return $this->render('AdminBundle:EmailMarketing:newByPessoa.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => '',
        ));
    }
    
    public function updateEmailAction(Request $request, $idPessoa)
    {
        $entity = new EmailMarketing();

        $form = $this->createEmailByPessoaForm($entity, $idPessoa);
        
        $dados = $request->request->all();
        $error = array();

        if (!empty($dados)) {
            $service = $this->container->get('admin.emailMarketing');

            $result = $service->changeEmailMarketing($idPessoa, $dados["admin_adminbundle_emailmarketing"]);

            if (empty($result)) {
                $this->get('session')->getFlashBag()->set('msg', 'E-mail alterado com sucesso, entre na sua caixa postal e valide o seu e-mail');
                
                return $this->redirect($this->generateUrl("Pessoa_Fisica_show", array('id' => $idPessoa)));
            }
            
            $error[] = $result;
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Pessoa", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Atualizar E-mail");

        return $this->render('AdminBundle:EmailMarketing:newByPessoa.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => $error,
        ));
    }

    /**
     * Finds and displays a EmailMarketing entity.
     *
     */
    public function showAction($id)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:EmailMarketing')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailMarketing entity.');
        }

        $tags = $entity->getTags();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Editar", $this->get("router")->generate("EmailMarketing_edit", array('id' => $id)));
        $breadcrumbs->addItem("Visualizar");
        
        return $this->render('AdminBundle:EmailMarketing:show.html.twig', array(
            'entity' => $entity,
            'titulo' => 'Visualizar Registro',
            'modulo' => 'E-mail Marketing',
            'tags' => $tags
        ));
    }

    /**
     * Displays a form to edit an existing EmailMarketing entity.
     *
     */
    public function editAction($id)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:EmailMarketing')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailMarketing entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("EmailMarketing_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");
        
        return $this->render('AdminBundle:EmailMarketing:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a EmailMarketing entity.
     *
     * @param EmailMarketing $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EmailMarketing $entity)
    {
        $form = $this->createForm(new EmailMarketingType(), $entity, array(
            'action' => $this->generateUrl('EmailMarketing_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing EmailMarketing entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:EmailMarketing')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EmailMarketing entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('EmailMarketing_show', array('id' => $id)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("EmailMarketing_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");
        
        return $this->render('AdminBundle:EmailMarketing:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'E-mail Marketing',
            'errors' => $errors,
        ));
    }

    /**
     * Deletes a EmailMarketing entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:EmailMarketing')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EmailMarketing entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('EmailMarketing'));
    }

    /**
     * Creates a form to delete a EmailMarketing entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('EmailMarketing_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

}
