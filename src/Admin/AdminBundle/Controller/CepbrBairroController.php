<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
/**
 * CepbrBairro controller.
 *
 */
class CepbrBairroController extends MainController
{

    /**
     * Lists all CepbrBairro entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->checkLogin();
        
        $repository = $this->getDoctrine()->getRepository('AdminBundle:CepbrBairro');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.idCidade', array('label' => 'Cidade', "sortable" => true)));
        $gridConfig->addField(new Field('item.bairro', array('label' => 'Bairro', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CepbrBairro:index.html.twig', array(
            'grid' => $grid,
            'titulo' => 'Listar Registros',
            'modulo' => 'Bairro'
        ));
    }

    /**
     * Finds and displays a CepbrBairro entity.
     *
     */
    public function showAction($id)
    {
        $this->checkLogin();
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CepbrBairro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CepbrBairro entity.');
        }

        return $this->render('AdminBundle:CepbrBairro:show.html.twig', array(
            'entity'      => $entity,
            'titulo' => 'Visualizar Registro',
            'modulo' => 'Bairro'
        ));
    }
}
