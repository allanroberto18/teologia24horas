<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\PessoaJuridica;
use Admin\AdminBundle\Form\PessoaJuridicaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * PessoaJuridica controller.
 *
 */
class PessoaJuridicaController extends MainController {

    /**
     * Lists all PessoaJuridica entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:PessoaJuridica');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.tipoPessoa = :tipoPessoa')
                ->setParameter('tipoPessoa', 2);

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.pf_cnpj', array('label' => 'CNPJ')));
        $gridConfig->addField(new Field('item.razaoSocial', array('label' => 'Razão Social', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.nomeFantasia', array('label' => 'Nome Fantasia', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.email', array('label' => 'E-mail')));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:PessoaJuridica:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Pessoa Jurídica'
        ));
    }

    /**
     * Creates a new PessoaJuridica entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new PessoaJuridica();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Juridica_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:PessoaJuridica:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Pessoa Jurídica',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a PessoaJuridica entity.
     *
     * @param PessoaJuridica $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PessoaJuridica $entity) {
        $form = $this->createForm(new PessoaJuridicaType(), $entity, array(
            'action' => $this->generateUrl('Pessoa_Juridica_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new PessoaJuridica entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new PessoaJuridica();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:PessoaJuridica:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Pessoa Jurídica',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a PessoaJuridica entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaJuridica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaJuridica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PessoaJuridica:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Pessoa Jurídica'
        ));
    }

    /**
     * Displays a form to edit an existing PessoaJuridica entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaJuridica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaJuridica entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PessoaJuridica:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Pessoa Jurídica',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a PessoaJuridica entity.
     *
     * @param PessoaJuridica $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PessoaJuridica $entity) {
        $form = $this->createForm(new PessoaJuridicaType(), $entity, array(
            'action' => $this->generateUrl('Pessoa_Juridica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing PessoaJuridica entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaJuridica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaJuridica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Juridica_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:PessoaJuridica:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Pessoa Jurídica',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a PessoaJuridica entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:PessoaJuridica')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PessoaJuridica entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Pessoa_Juridica'));
    }

    /**
     * Creates a form to delete a PessoaJuridica entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Pessoa_Juridica_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
