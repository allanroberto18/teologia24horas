<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\Forum;
use Admin\AdminBundle\Entity\ForumItem;
use Admin\AdminBundle\Entity\Notificacao;
use Admin\AdminBundle\Form\ForumItemType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class ForumItemController extends MainController
{
    /**
     * @Route("/Forum/{idForum}/novo", name="admin_curso_forum_item_novo")
     * @Template("AdminBundle:ForumItem:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idForum, Request $request)
    {
        $this->checkLogin();

        $forum = $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $entity = new ForumItem();

        $form = $this->createForm(new ForumItemType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdForum($forum);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_curso_forum_item_novo'
                : 'admin_curso_forum_item_listar';

            return $this->redirectToRoute($nextAction, array('idForum' => $idForum));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Fórum', $this->get('router')->generate('admin_curso_forum_visualizar', ['idCurso' => $forum->getIdCurso()->getId(), 'id' => $idForum]));
        $breadcrumbs->addItem('Ítens do Fórum: Listar Registros', $this->get('router')->generate('admin_curso_forum_item_listar', array('idForum' => $idForum)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'idForum' => $idForum,
            'idCurso' => $forum->getIdCurso()->getId(),
            'modulo' => array('titulo' => 'Ítens do Fórum', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Forum/{idForum}/{id}/atualizar", name="admin_curso_forum_item_atualizar")
     * @Template("AdminBundle:ForumItem:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idForum, $id, Request $request)
    {
        $this->checkLogin();

        $forum = $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:ForumItem')->find($id);
        if (!$entity instanceof ForumItem) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
        }

        $form = $this->createForm(new ForumItemType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_curso_forum_item_novo'
                : 'admin_curso_forum_item_listar';

            return $this->redirectToRoute($nextAction, array('idForum' => $idForum));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Fórum', $this->get('router')->generate('admin_curso_forum_visualizar', ['idCurso' => $forum->getIdCurso()->getId(), 'id' => $idForum]));
        $breadcrumbs->addItem('Ítens do Fórum: Listar Registros', $this->get('router')->generate('admin_curso_forum_item_listar', array('idForum' => $idForum)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('admin_curso_forum_item_visualizar', array('idForum' => $idForum, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'idForum' => $idForum,
            'idCurso' => $forum->getIdCurso()->getId(),
            'entity' => $entity,
            'modulo' => array('titulo' => 'Ítens do Fórum', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Forum/{idForum}/listar", name="admin_curso_forum_item_listar")
     * @Template("AdminBundle:ForumItem:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idForum, Request $request)
    {
        $this->checkLogin();

        $forum = $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $repository = $this->getDoctrine()->getRepository('AdminBundle:ForumItem');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.idForum = :forum')
            ->setParameter('forum', $idForum)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.comentario', array('label' => 'Comentário', 'filterable' => 'true', 'sortable' => true)))
            ->addField(new Field('item.status', array('label' => 'Status',
                'formatValueCallback' => function($value) {
                    switch ($value)
                    {
                        case 1 :
                            return "Liberado";
                        case 2 :
                            return "Bloqueado";
                    }
                }
            )));

        $gridConfig->addSelector(array('label' => 'Liberado', 'field' => 'item.status', 'value' => '1'));
        $gridConfig->addSelector(array('label' => 'Aguardando Liberação', 'field' => 'item.status', 'value' => '2'));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Fórum', $this->get('router')->generate('admin_curso_forum_visualizar', ['idCurso' => $forum->getIdCurso()->getId(), 'id' => $idForum]));
        $breadcrumbs->addItem('Ítens do Fórum: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'idCurso' => $forum->getIdCurso()->getId(),
            'idForum' => $idForum,
            'delete' => $this->generateUrl('admin_curso_forum_item_delete_selecionado', ['idForum' => $idForum]),
            'novo' => $this->generateUrl('admin_curso_forum_item_novo', ['idForum' => $idForum]),
            'modulo' => array('titulo' => 'Ítens do Fórum', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Forum/{idForum}/{id}/visualizar", name="admin_curso_forum_item_visualizar")
     * @Template("AdminBundle:ForumItem:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idForum, $id)
    {
        $this->checkLogin();

        $forum = $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:ForumItem')->find($id);
        if (!$entity instanceof ForumItem) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Fórum', $this->get('router')->generate('admin_curso_forum_visualizar', ['idCurso' => $forum->getIdCurso()->getId(), 'id' => $idForum]));
        $breadcrumbs->addItem('Ítens do Fórum: Listar Registros', $this->get('router')->generate('admin_curso_forum_item_listar', array('idForum' => $idForum)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'idForum' => $idForum,
            'idCurso' => $forum->getIdCurso()->getId(),
            'modulo' => array('titulo' => 'Ítens do Fórum', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Forum/{idForum}/{id}/delete", name="admin_curso_forum_item_delete")
     * @Method("GET")
     */
    public function deleteAction($idForum, $id)
    {
        $this->checkLogin();

        $forum = $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:ForumItem')->find($id);
        if (!$entity instanceof ForumItem) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
    }

    /**
     * @Route("/Forum/{idForum}/delete/selecionados", name="admin_curso_forum_item_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idForum, Request $request)
    {
        $this->checkLogin();

        $this->checkParent($idForum, "AdminBundle", 'Forum', 'admin_home', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:ForumItem')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_curso_forum_item_listar', array('idForum' => $idForum));
    }
}
