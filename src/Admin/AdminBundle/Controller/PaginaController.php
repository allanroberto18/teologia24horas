<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Pagina;
use Admin\AdminBundle\Form\PaginaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Pagina controller.
 *
 */
class PaginaController extends MainController {

    /**
     * Lists all Pagina entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Pagina');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título')));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:Pagina:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Página'
        ));
    }

    /**
     * Creates a new Pagina entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Pagina();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Pagina_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Pagina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Página',
                    'errors' => $errors,
        ));
    }
    
    public function createFilhoAction(Request $request, $idPaginaPai) {
        $entity = new Pagina();
        $form = $this->createCreateFilhoForm($entity, $idPaginaPai);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Pagina")->find($idPaginaPai);
        
        if (!$repository instanceof Pagina) {
            return $this->redirect($this->generateUrl('Pagina'));
        }
        
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
                                   
            $ligacao = new \Admin\AdminBundle\Entity\PaginaLigacao();

            $ligacao->setIdPagina($repository);
            $ligacao->setIdPaginaFilho($entity);
            $ligacao->setPrioridade($ligacao->returnPrioridade($em, "AdminBundle:PaginaLigacao", $idPaginaPai));
            
            $em->persist($ligacao);
            $em->flush();

            return $this->redirect($this->generateUrl('Pagina_show', array('id' => $idPaginaPai)));
        }

        return $this->render('AdminBundle:Pagina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Página',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Pagina entity.
     *
     * @param Pagina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pagina $entity) {
        $form = $this->createForm(new PaginaType(), $entity, array(
            'action' => $this->generateUrl('Pagina_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Creates a form to create a Pagina entity.
     *
     * @param Pagina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateFilhoForm(Pagina $entity, $idPaginaPai) {
        $form = $this->createForm(new PaginaType(), $entity, array(
            'action' => $this->generateUrl('Pagina_filho_create', array('idPaginaPai' => $idPaginaPai)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }
    
    /**
     * Displays a form to create a new Pagina entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new Pagina();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Pagina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Página',
                    'errors' => '',
        ));
    }
    
    public function newFilhoAction($idPaginaPai) {
        $this->checkLogin();

        $entity = new Pagina();
        $form = $this->createCreateFilhoForm($entity, $idPaginaPai);

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository("AdminBundle:Pagina")->find($idPaginaPai);
        
        if (!$repository instanceof Pagina) {
            return $this->redirect($this->generateUrl('Pagina'));
        }
        
        return $this->render('AdminBundle:Pagina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Página',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Pagina entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pagina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pagina entity.');
        }
        
        $filhos = $em->getRepository("AdminBundle:PaginaLigacao")->createQueryBuilder('item')
                ->where('item.idPagina = :idPaginaPai')
                ->orderBy("item.id", 'DESC')
                ->setParameter('idPaginaPai', $id)
                ->getQuery()
                ->getResult();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Pagina:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Página',
                    'filhos' => $filhos,
        ));
    }

    /**
     * Displays a form to edit an existing Pagina entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pagina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pagina entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Pagina:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Página',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Pagina entity.
     *
     * @param Pagina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pagina $entity) {
        $form = $this->createForm(new PaginaType(), $entity, array(
            'action' => $this->generateUrl('Pagina_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Pagina entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pagina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pagina entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pagina_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:Pagina:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Página',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Pagina entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Pagina')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pagina entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Pagina'));
    }

    /**
     * Creates a form to delete a Pagina entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Pagina_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
