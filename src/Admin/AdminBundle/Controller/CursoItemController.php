<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\CursoItem;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Form\CursoItemType;

/**
 * CursoItem controller.
 *
 */
class CursoItemController extends MainController {

    public function checkCurso($idCurso) {
        if (empty($idCurso)) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Curso")->find($idCurso);
        return $entity;
    }

    /**
     * Lists all CursoItem entities.
     *
     */
    public function indexAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoItem');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.idCurso = :idCurso')
                ->setParameter('idCurso', $idCurso);
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.valor', array('label' => 'valor', 'formatValueCallback' => function ($value) {
                return number_format($value, 2, ",", ".");
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Listagem de Itens do Curso");

        return $this->render('AdminBundle:CursoItem:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Itens do Curso',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a new CursoItem entity.
     *
     */
    public function createAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new CursoItem();
        $form = $this->createCreateForm($entity, $idCurso);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdCurso($curso);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Salvar Item");

        return $this->render('AdminBundle:CursoItem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Itens do Curso',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to create a CursoItem entity.
     *
     * @param CursoItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoItem $entity, $idCurso) {
        $form = $this->createForm(new CursoItemType(), $entity, array(
            'action' => $this->generateUrl('Curso_Item_create', array('idCurso' => $idCurso)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoItem entity.
     *
     */
    public function newAction($idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new CursoItem();
        $form = $this->createCreateForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Nova Item");

        return $this->render('AdminBundle:CursoItem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Itens do Curso',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Finds and displays a CursoItem entity.
     *
     */
    public function showAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoItem entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Item");

        return $this->render('AdminBundle:CursoItem:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Itens do Curso',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Displays a form to edit an existing CursoItem entity.
     *
     */
    public function editAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoItem entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Item", $this->get("router")->generate("Curso_Item_show", array('id' => $id, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:CursoItem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Itens do Curso',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to edit a CursoItem entity.
     *
     * @param CursoItem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoItem $entity, $idCurso) {
        $form = $this->createForm(new CursoItemType(), $entity, array(
            'action' => $this->generateUrl('Curso_Item_update', array('idCurso' => $idCurso, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoItem entity.
     *
     */
    public function updateAction(Request $request, $idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoItem entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Item", $this->get("router")->generate("Curso_Item_show", array('id' => $id, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:CursoItem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Itens do Curso',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Deletes a CursoItem entity.
     *
     */
    public function deleteAction(Request $request, $idCurso, $id) {
        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $form = $this->createDeleteForm($id, $idCurso);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoItem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoItem entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('CursoItem'));
    }

    /**
     * Creates a form to delete a CursoItem entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $idCurso) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('CursoItem_delete', array('idCurso' => $idCurso, 'id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
