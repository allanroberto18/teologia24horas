<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * CepbrCidade controller.
 *
 */
class CepbrCidadeController extends MainController {

    /**
     * Lists all CepbrCidade entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Menu');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.cidade', array('label' => 'Cidade', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.uf', array('label' => 'bairro', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.codIbge', array('label' => 'Cod. IBGE', "sortable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CepbrCidade:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Cidade'
        ));
    }

    /**
     * Finds and displays a CepbrCidade entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CepbrCidade')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CepbrCidade entity.');
        }

        return $this->render('AdminBundle:CepbrCidade:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Cidade'
        ));
    }
}
