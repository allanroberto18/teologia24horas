<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Nucleo;
use Admin\AdminBundle\Form\NucleoType;
use Admin\AdminBundle\Entity\Pessoa;

/**
 * Nucleo controller.
 *
 */
class NucleoController extends MainController {

    /**
     * Creates a new Nucleo entity.
     *
     */
    public function createAction($idPessoa, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);

        if (!$pessoa instanceof Pessoa) {
            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $Nucleo = new Nucleo();
        $form = $this->createCreateForm($idPessoa, $Nucleo);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($Nucleo);

        if ($form->isValid() && count($errors) == 0) {
            $Nucleo->setIdPessoa($pessoa);

            $em->persist($Nucleo);

            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Sakvar");

        return $this->render('AdminBundle:Nucleo:new.html.twig', array(
                    'entity' => $Nucleo,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Nucleo',
                    'errors' => $errors,
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a form to create a Nucleo entity.
     *
     * @param Nucleo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($idPessoa, Nucleo $entity) {
        $form = $this->createForm(new NucleoType(), $entity, array(
            'action' => $this->generateUrl('Nucleo_create', array('idPessoa' => $idPessoa)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Nucleo entity.
     *
     */
    public function newAction($idPessoa) {
        $this->checkLogin();

        $entity = new Nucleo();
        $form = $this->createCreateForm($idPessoa, $entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Nucleo:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Nucleo',
                    'errors' => '',
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Displays a form to edit an existing Nucleo entity.
     *
     */
    public function editAction($idPessoa, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Nucleo')->findOneBy(array('id' => $id, 'idPessoa' => $idPessoa));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nucleo entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Nucleo_show", array('idPessoa' => $idPessoa, 'id' => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:Nucleo:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Nucleo',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Nucleo entity.
     *
     * @param Nucleo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Nucleo $entity) {
        $form = $this->createForm(new NucleoType(), $entity, array(
            'action' => $this->generateUrl('Nucleo_update', array('idPessoa' => $entity->getIdPessoa()->getId(), 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Nucleo entity.
     *
     */
    public function updateAction($idPessoa, Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Nucleo')->findOneBy(array('id' => $id, 'idPessoa' => $idPessoa));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nucleo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Nucleo_show", array('idPessoa' => $idPessoa, 'id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:Nucleo:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Nucleo',
                    'errors' => $errors,
        ));
    }

    /**
     * Displays a form to edit an existing Nucleo entity.
     *
     */
    public function showAction($idPessoa, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Nucleo')->findOneBy(array('id' => $id, 'idPessoa' => $idPessoa));

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Editar", $this->get("router")->generate("Nucleo_edit", array('idPessoa' => $idPessoa, 'id' => $id)));
        $breadcrumbs->addItem("Visualizar");

        $nucleoPessoa = $em->getRepository('AdminBundle:NucleoPessoa')->findBy(array('idNucleo' => $id));

        return $this->render('AdminBundle:Nucleo:show.html.twig', array(
            'entity' => $entity,
            'titulo' => 'Alterar Registro',
            'modulo' => 'Nucleo',
            'errors' => '',
            'pessoas' => $nucleoPessoa,
        ));
    }

    
}
