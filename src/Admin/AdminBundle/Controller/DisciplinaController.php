<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Disciplina;
use Admin\AdminBundle\Form\DisciplinaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\Curso;

/**
 * Disciplina controller.
 *
 */
class DisciplinaController extends MainController {

    public function checkCurso($idCurso) {
        if (empty($idCurso)) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Curso")->find($idCurso);
        return $entity;
    }

    /**
     * Lists all Disciplina entities.
     *
     */
    public function indexAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Disciplina');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.idCurso = :idCurso')
                ->setParameter('idCurso', $idCurso);
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.semana', array(
            'label' => 'Semana',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                $disciplina = new Disciplina();
                $disciplina->setSemana($value);
                return $disciplina->returnSemana();
            }
        )));
        $gridConfig->addField(new Field('item.aula', array(
            'label' => 'Aula',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                $disciplina = new Disciplina();
                $disciplina->setAula($value);
                return $disciplina->returnAula();
            }
        )));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Listagem de Disciplinas");
        
        return $this->render('AdminBundle:Disciplina:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Disciplina',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a new Disciplina entity.
     *
     */
    public function createAction(Request $request, $idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new Disciplina();
        $form = $this->createCreateForm($entity, $idCurso);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdCurso($curso);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Salvar Disciplina");
        
        return $this->render('AdminBundle:Disciplina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Disciplina',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to create a Disciplina entity.
     *
     * @param Disciplina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Disciplina $entity, $idCurso) {
        $form = $this->createForm(new DisciplinaType(), $entity, array(
            'action' => $this->generateUrl('Disciplina_create', array('idCurso' => $idCurso)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Disciplina entity.
     *
     */
    public function newAction($idCurso) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new Disciplina();
        $form = $this->createCreateForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Nova Disciplina");
        
        return $this->render('AdminBundle:Disciplina:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Disciplina',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Finds and displays a Disciplina entity.
     *
     */
    public function showAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Disciplina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disciplina entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Disciplina");
        
        return $this->render('AdminBundle:Disciplina:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Disciplina',
                    'idCurso' => $idCurso,
        ));
    }

    /**
     * Displays a form to edit an existing Disciplina entity.
     *
     */
    public function editAction($idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Disciplina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disciplina entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $id,'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Editar");
        
        return $this->render('AdminBundle:Disciplina:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Disciplina',
                    'errors' => '',
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Creates a form to edit a Disciplina entity.
     *
     * @param Disciplina $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Disciplina $entity, $idCurso) {
        $form = $this->createForm(new DisciplinaType(), $entity, array(
            'action' => $this->generateUrl('Disciplina_update', array('idCurso' => $idCurso, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Disciplina entity.
     *
     */
    public function updateAction(Request $request, $idCurso, $id) {

        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Disciplina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Disciplina entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $id,'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:Disciplina:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Disciplina',
                    'errors' => $errors,
                    'idCurso' => $idCurso
        ));
    }

    /**
     * Deletes a Disciplina entity.
     *
     */
    public function deleteAction($idCurso, $id) {
        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Disciplina')->find($id);

        if (!$entity && !$entity instanceof Disciplina) {
            throw $this->createNotFoundException('Unable to find Disciplina entity.');
        }

        $entity->setStatus(2);

        $em->flush();

        return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
    }

    /**
     * Deletes a Disciplina entity.
     *
     */
    public function restaurarAction($idCurso, $id) {
        $this->checkLogin();

        $curso = $this->checkCurso($idCurso);
        if (!$curso instanceof Curso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Disciplina')->find($id);

        if (!$entity && !$entity instanceof Disciplina) {
            throw $this->createNotFoundException('Unable to find Disciplina entity.');
        }

        $entity->setStatus(1);

        $em->flush();

        return $this->redirect($this->generateUrl('Curso_show', array('id' => $idCurso)));
    }
}
