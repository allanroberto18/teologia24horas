<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\CursoOfertadoLigacao;
use Admin\AdminBundle\Form\CursoOfertadoLigacaoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\CursoOfertado;
use DateTime;
/**
 * CursoOfertadoLigacao controller.
 *
 */
class CursoOfertadoLigacaoController extends MainController {

    public function checkCursoOfertado($idCursoOfertado) {
        if (empty($idCursoOfertado)) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:CursoOfertado")->find($idCursoOfertado);
        return $entity;
    }

    /**
     * Lists all CursoOfertadoLigacao entities.
     *
     */
    public function indexAction(Request $request, $idCursoOfertado) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertadoLigacao');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.idCursoOfertado = :idCursoOfertado')
                ->setParameter('idCursoOfertado', $idCursoOfertado);
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CursoOfertadoLigacao:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Montando Curso',
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Creates a new CursoOfertadoLigacao entity.
     *
     */
    public function createAction(Request $request, $idCursoOfertado) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $entity = new CursoOfertadoLigacao();
        $data = $request->request->all();

        $form = $this->createCreateForm($entity, $idCursoOfertado);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdCursoOfertado($curso);
            $em->persist($entity);
              
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_show', array('id' => $idCursoOfertado)));
        }

        return $this->render('AdminBundle:CursoOfertadoLigacao:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Montando Curso',
                    'errors' => $errors,
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Creates a form to create a CursoOfertadoLigacao entity.
     *
     * @param CursoOfertadoLigacao $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoOfertadoLigacao $entity, $idCursoOfertado) {
        $form = $this->createForm(new CursoOfertadoLigacaoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_Ligacao_create', array('idCursoOfertado' => $idCursoOfertado)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoOfertadoLigacao entity.
     *
     */
    public function newAction($idCursoOfertado) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $entity = new CursoOfertadoLigacao();
        $form = $this->createCreateForm($entity, $idCursoOfertado);

        return $this->render('AdminBundle:CursoOfertadoLigacao:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Montando Curso',
                    'errors' => '',
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Finds and displays a CursoOfertadoLigacao entity.
     *
     */
    public function showAction($idCursoOfertado, $id) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoLigacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoLigacao entity.');
        }

        return $this->render('AdminBundle:CursoOfertadoLigacao:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Montando Curso',
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Displays a form to edit an existing CursoOfertadoLigacao entity.
     *
     */
    public function editAction($idCursoOfertado, $id) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoLigacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoLigacao entity.');
        }

        $editForm = $this->createEditForm($entity, $idCursoOfertado);

        return $this->render('AdminBundle:CursoOfertadoLigacao:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Montando Curso',
                    'errors' => '',
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Creates a form to edit a CursoOfertadoLigacao entity.
     *
     * @param CursoOfertadoLigacao $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoOfertadoLigacao $entity, $idCursoOfertado) {
        $form = $this->createForm(new CursoOfertadoLigacaoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_Ligacao_update', array('idCursoOfertado' => $idCursoOfertado, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoOfertadoLigacao entity.
     *
     */
    public function updateAction(Request $request, $idCursoOfertado, $id) {

        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoLigacao')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoLigacao entity.');
        }

        $editForm = $this->createEditForm($entity, $idCursoOfertado);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_show', array('idCursoOfertado' => $idCursoOfertado)));
        }

        return $this->render('AdminBundle:CursoOfertadoLigacao:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Montando Curso',
                    'errors' => $errors,
                    'idCursoOfertado' => $idCursoOfertado
        ));
    }

    /**
     * Deletes a CursoOfertadoLigacao entity.
     *
     */
    public function deleteAction(Request $request, $idCursoOfertado, $id) {
        $this->checkLogin();

        $curso = $this->checkCursoOfertado($idCursoOfertado);
        if (!$curso instanceof CursoOfertado) {
            return $this->redirect($this->generateUrl('Curso_Ofertado'));
        }

        $form = $this->createDeleteForm($id, $idCursoOfertado);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoOfertadoLigacao')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoOfertadoLigacao entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('CursoOfertadoLigacao'));
    }

    /**
     * Creates a form to delete a CursoOfertadoLigacao entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $idCursoOfertado) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Curso_Ofertado_Ligacao_delete', array('idCursoOfertado' => $idCursoOfertado, 'id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
