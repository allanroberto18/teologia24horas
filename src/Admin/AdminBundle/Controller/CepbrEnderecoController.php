<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * CepbrEndereco controller.
 *
 */
class CepbrEnderecoController extends MainController {

    /**
     * Lists all CepbrEndereco entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CepbrEndereco');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.idBairro', array('label' => 'Bairro', "sortable" => true)));
        $gridConfig->addField(new Field('item.idCidade', array('label' => 'Cidade', "sortable" => true)));
        $gridConfig->addField(new Field('item.cep', array('label' => 'Código', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.endereco', array('label' => 'Título', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CepbrEndereco:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Endereço'
        ));
    }

    /**
     * Finds and displays a CepbrEndereco entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CepbrEndereco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CepbrEndereco entity.');
        }

        return $this->render('AdminBundle:CepbrEndereco:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Endereço'
        ));
    }
}
