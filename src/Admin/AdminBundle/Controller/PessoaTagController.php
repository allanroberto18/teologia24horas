<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\AdminBundle\Entity\Tag;
use Admin\AdminBundle\Entity\PessoaTag;
use Admin\AdminBundle\Form\PessoaTagType;

/**
 * PessoaTag controller.
 *
 */
class PessoaTagController extends MainController {

    /**
     * Lists all PessoaTag entities.
     *
     */
    public function indexAction() {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:PessoaTag')->findAll();

        return $this->render('AdminBundle:PessoaTag:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new PessoaTag entity.
     *
     */
    public function createAction(Request $request) {
        $this->checkLogin();

        $entity = new PessoaTag();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Tag_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:PessoaTag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    public function createByPessoaAction(Request $request) {
        $this->checkLogin();

        $form = $request->request->all();

        $session = $this->get('session');
        if (!array_key_exists('tag', $form)) {
            $session->getFlashBag()->set('msgAdmin', 'A tag deve ser selecioniada');
            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $form['pessoa'])));
        }
        $em = $this->getDoctrine()->getManager();

        $pessoa = $em->getRepository('AdminBundle:Pessoa')->find($form['pessoa']);

        foreach ($form['tag'] as $item) {
            $tag = $em->getRepository('AdminBundle:Tag')->find($item);
            if (!$tag instanceof Tag) {
                $session->getFlashBag()->set('msgAdmin', 'A tag selecionada não foi localizada');
                return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $form['pessoa'])));
            }

            $entity = new PessoaTag();
            $entity->setIdPessoa($pessoa);
            $entity->setIdTag($tag);
            $entity->setStatus(1);

            $em->persist($entity);
        }
        $em->flush();
        $session->getFlashBag()->set('msgAdmin', 'Tag adicionada com sucesso');

        return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $form['pessoa'])));
    }

    /**
     * Creates a form to create a PessoaTag entity.
     *
     * @param PessoaTag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PessoaTag $entity) {
        $form = $this->createForm(new PessoaTagType(), $entity, array(
            'action' => $this->generateUrl('Pessoa_Tag_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PessoaTag entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new PessoaTag();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:PessoaTag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PessoaTag entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaTag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PessoaTag:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PessoaTag entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaTag entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:PessoaTag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a PessoaTag entity.
     *
     * @param PessoaTag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PessoaTag $entity) {
        $form = $this->createForm(new PessoaTagType(), $entity, array(
            'action' => $this->generateUrl('Pessoa_Tag_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing PessoaTag entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:PessoaTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaTag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Tag_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:PessoaTag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PessoaTag entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:PessoaTag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PessoaTag entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Pessoa_Tag'));
    }

    /**
     * Creates a form to delete a PessoaTag entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Pessoa_Tag_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function excluirTagAction(Request $request) {
        $data = $request->request->all();

        $id = $data["param"];

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:PessoaTag')->find($id);

        if (!$entity) {
            return new Response(json_encode(array("msg" => "Não foi possível remover essa Tag")), 200, array('Content-Type' => 'application/json'));
        }

        $em->remove($entity);
        $em->flush();

        return new Response(json_encode(array("msg" => "Tag removida com Sucesso")), 200, array('Content-Type' => 'application/json'));
    }

}
