<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\Notificacao;
use Admin\AdminBundle\Form\NotificacaoType;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class NotificacaoController extends MainController
{
    /**
     * @Route("/Curso/{idCurso}/novo", name="admin_curso_notificacao_novo")
     * @Template("AdminBundle:Notificacao:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idCurso, Request $request)
    {
        $curso = $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $entity = new Notificacao();

        $form = $this->createForm(new NotificacaoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdCurso($curso);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $this->container->get('admin.notificacao')->createMessageNotificacao($entity, "Mensagem do Curso de " . $curso->getTitulo());

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_curso_notificacao_novo'
                : 'admin_curso_notificacao_listar';

            return $this->redirectToRoute($nextAction, array('idCurso' => $idCurso));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $idCurso)));
        $breadcrumbs->addItem('Notificações do Curso: Listar Registros', $this->get('router')->generate('admin_curso_notificacao_listar', array('idCurso' => $idCurso)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Notificações do Curso', 'descricao' => ''),
            'idCurso' => $idCurso,
        );
    }

    /**
     * @Route("/Curso/{idCurso}/{id}/atualizar", name="admin_curso_notificacao_atualizar")
     * @Template("AdminBundle:Notificacao:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idCurso, $id, Request $request)
    {
        $curso = $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Notificacao')->find($id);
        if (!$entity instanceof Notificacao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
        }

        $form = $this->createForm(new NotificacaoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_curso_notificacao_novo'
                : 'admin_curso_notificacao_listar';

            return $this->redirectToRoute($nextAction, array('idCurso' => $idCurso));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $idCurso)));
        $breadcrumbs->addItem('Notificações do Curso: Listar Registros', $this->get('router')->generate('admin_curso_notificacao_listar', array('idCurso' => $idCurso)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('admin_curso_notificacao_visualizar', array('idCurso' => $idCurso, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Notificações do Curso', 'descricao' => ''),
            'idCurso' => $idCurso,
        );
    }

    /**
     * @Route("/Curso/{idCurso}/listar", name="admin_curso_notificacao_listar")
     * @Template("AdminBundle:Notificacao:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idCurso, Request $request)
    {
        $curso = $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Notificacao');
        $queryBuilder = $repository->createQueryBuilder('item')
            ->where('item.status = :status')
            ->andWhere('item.idCurso = :curso')
            ->setParameter('status', '1')
            ->setParameter('curso', $idCurso)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('item.id', array('label' => 'Código', 'sortable' => true)))
            ->addField(new Field('item.texto', array('label' => 'Notificacao', 'filterable' => 'true', 'sortable' => true)))
        ;

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $idCurso)));
        $breadcrumbs->addItem('Notificações do Curso: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('admin_curso_notificacao_delete_selecionado', ['idCurso' => $idCurso]),
            'novo' => $this->generateUrl('admin_curso_notificacao_novo', ['idCurso' => $idCurso]),
            'modulo' => array('titulo' => 'Notificações do Curso', 'descricao' => ''),
            'idCurso' => $idCurso,
        );
    }

    /**
     * @Route("/Curso/{idCurso}/{id}/visualizar", name="admin_curso_notificacao_visualizar")
     * @Template("AdminBundle:Notificacao:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idCurso, $id)
    {
        $curso = $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Notificacao')->find($id);
        if (!$entity instanceof Notificacao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Cursos: Listar Registros', $this->get('router')->generate('Curso'));
        $breadcrumbs->addItem($curso->getTitulo(), $this->get('router')->generate('Curso_show', array('id' => $idCurso)));
        $breadcrumbs->addItem('Notificações do Curso: Listar Registros', $this->get('router')->generate('admin_curso_notificacao_listar', array('idCurso' => $idCurso)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Notificações do Curso', 'descricao' => ''),
            'idCurso' => $idCurso,
        );
    }

    /**
     * @Route("/Curso/{idCurso}/{id}/delete", name="admin_curso_notificacao_delete")
     * @Method("GET")
     */
    public function deleteAction($idCurso, $id)
    {
        $curso = $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Notificacao')->find($id);
        if (!$entity instanceof Notificacao) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
    }

    /**
     * @Route("/Curso/{idCurso}/delete/selecionados", name="admin_curso_notificacao_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idCurso, Request $request)
    {
        $this->checkParent($idCurso, "AdminBundle", 'Curso', 'Curso', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:Notificacao')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_curso_notificacao_listar', array('idCurso' => $idCurso));
    }
}
