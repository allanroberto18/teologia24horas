<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\CursoOfertado;
use Admin\AdminBundle\Form\CursoOfertadoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\CursoOfertadoImagem;

/**
 * CursoOfertado controller.
 *
 */
class CursoOfertadoController extends MainController {

    /**
     * Lists all CursoOfertado entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertado');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.valor', array('label' => 'Preço', 'formatValueCallback' => function ($value) {
                return number_format($value, 2, ',', '.');
            })));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Excluído</span>";
                        break;
                };
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CursoOfertado:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Curso Ofertado'
        ));
    }

    /**
     * Creates a new CursoOfertado entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new CursoOfertado();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:CursoOfertado:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Curso Ofertado',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a CursoOfertado entity.
     *
     * @param CursoOfertado $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoOfertado $entity) {
        $form = $this->createForm(new CursoOfertadoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoOfertado entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new CursoOfertado();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:CursoOfertado:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Curso Ofertado',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a CursoOfertado entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertado entity.');
        }

        $repositoryImagem = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertadoImagem');
        $queryBuilderImagem = $repositoryImagem->createQueryBuilder('item')
                ->where('item.idCursoOfertado = :idCursoOfertado')
                ->setParameter('idCursoOfertado', $id)
                ->getQuery();

        $imagens = $queryBuilderImagem->getResult();

        $repositoryCursos = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertadoLigacao');
        $queryBuilderCursos = $repositoryCursos->createQueryBuilder('item')
                ->where('item.idCursoOfertado = :idCursoOfertado')
                ->setParameter('idCursoOfertado', $id)
                ->getQuery();

        $cursoLigacao = $queryBuilderCursos->getResult();

        $repositoryItem = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertadoItem');
        $queryBuilderItem = $repositoryItem->createQueryBuilder('item')
                ->where('item.idCursoOfertado = :idCursoOfertado')
                ->setParameter('idCursoOfertado', $id)
                ->getQuery();

        $cursoItem = $queryBuilderItem->getResult();

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:CursoOfertado:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Curso Ofertado',
                    'imagens' => $imagens,
                    'cursos' => $cursoLigacao,
                    'itens' => $cursoItem,
        ));
    }

    /**
     * Displays a form to edit an existing CursoOfertado entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertado entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:CursoOfertado:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Curso Ofertado',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a CursoOfertado entity.
     *
     * @param CursoOfertado $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoOfertado $entity) {
        $form = $this->createForm(new CursoOfertadoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoOfertado entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:CursoOfertado:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Curso Ofertado',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a CursoOfertado entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoOfertado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoOfertado entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Curso_Ofertado'));
    }

    /**
     * Creates a form to delete a CursoOfertado entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Curso_Ofertado_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
