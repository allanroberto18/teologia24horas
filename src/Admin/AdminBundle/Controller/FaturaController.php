<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\Form\FaturaNewType;
use Admin\AdminBundle\Form\FaturaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Fatura controller.
 *
 */
class FaturaController extends MainController {

    /**
     * Lists all Fatura entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Fatura');
        $queryBuilder = $repository->createQueryBuilder('item')
                //->where('item.status = :status')
                //->setParameter('status', 2)
                ->orderBy('item.pagamentoData', 'DESC');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', 'sortable' => true)));
        $gridConfig->addField(new Field('item.idContrato', array('label' => 'Contrato')));
        $gridConfig->addField(new Field('item.vencimento', array(
            'label' => 'Vencimento',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                return $value->format("d/m/Y");
            }
        )));
        $gridConfig->addField(new Field('item.valor', array('label' => 'Total',
            'formatValueCallback' => function($value) {
                if (empty($value)) {
                    return "";
                }
                return number_format($value, 2, ',', '.');
            }
        )));
        $gridConfig->addField(new Field('item.pagamentoData', array(
            'label' => 'Data de Pagamento',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                if (empty($value)) {
                    return "";
                }
                return $value->format("d/m/Y");
            }
        )));
        $gridConfig->addField(new Field('item.pagamentoValor', array('label' => 'Total',
            'formatValueCallback' => function($value) {
                return number_format($value, 2, ',', '.');
            }
        )));
        $gridConfig->addField(new Field('item.status', array('label' => 'Total')));

        $gridConfig->addSelector(array('label' => 'Pendentes', 'field' => 'item.status', 'value' => '1'));
        $gridConfig->addSelector(array('label' => 'Pagas', 'field' => 'item.status', 'value' => '2'));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:Fatura:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Fatura da Contratação'
        ));
    }

    /**
     * Creates a new Fatura entity.
     *
     */
    public function createAction(Request $request, $idContrato) {
        $entity = new Fatura();
        $form = $this->createCreateForm($entity, $idContrato);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository('AdminBundle:Contrato')->find($idContrato);
        
        if ($form->isValid() && count($errors) == 0) {
            
            $entity->setIdContrato($contrato);
                        
            $em->persist($entity);
            $em->flush();

            //return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $contrato->getIdPessoa()->getId())));
            return $this->redirect($this->generateUrl('Contrato_Fatura_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Fatura:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Fatura da Contratação',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Fatura entity.
     *
     * @param Fatura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Fatura $entity, $idContrato) {
        $form = $this->createForm(new FaturaNewType(), $entity, array(
            'action' => $this->generateUrl('Contrato_Fatura_create', array('idContrato' => $idContrato)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Fatura entity.
     *
     */
    public function newAction($idContrato) {
        $this->checkLogin();

        $entity = new Fatura();
        $form = $this->createCreateForm($entity, $idContrato);

        return $this->render('AdminBundle:Fatura:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Fatura da Contratação',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Fatura entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        echo "<script>parent.$.fn.colorbox.close(); </script>";

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Fatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Fatura:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Fatura da Contratação'
        ));
    }

    /**
     * Displays a form to edit an existing Fatura entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Fatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Fatura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Fatura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Fatura da Contratação',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Fatura entity.
     *
     * @param Fatura $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Fatura $entity) {
        $form = $this->createForm(new FaturaType(), $entity, array(
            'action' => $this->generateUrl('Contrato_Fatura_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('baixa'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Fatura entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Fatura')->find($id);

        if (!$entity && !$entity instanceof Fatura) {
            throw $this->createNotFoundException('Unable to find Fatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $contrato = $entity->getIdContrato();
            $em->flush();
            
            if ($entity->getStatus() == 2 && $contrato->getStatus() == 2) {
                $contrato->setStatus(1);

                $em->flush();

                $email = $this->container->get('admin.email');
                $email->confirmacaoPagamento($contrato, $entity);
            }
            return $this->redirect($this->generateUrl('Contrato_Fatura_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:Fatura:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Fatura da Contratação',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Fatura entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Fatura')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Fatura entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Contrato_Fatura'));
    }

    /**
     * Creates a form to delete a Fatura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Contrato_Fatura_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
