<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\TabelaPreco;
use Admin\AdminBundle\Form\TabelaPrecoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * TabelaPreco controller.
 *
 */
class TabelaPrecoController extends MainController {

    /**
     * Lists all TabelaPreco entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:TabelaPreco');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.valor', array('label' => 'valor', 'formatValueCallback' => function ($value) {
                return number_format($value, 2, ",", ".");
            })));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Excluído</span>";
                        break;
                };
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:TabelaPreco:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Tabela de Preço'
        ));
    }

    /**
     * Creates a new TabelaPreco entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new TabelaPreco();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Tabela_Preco_show', array('id' => $entity->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Tabela_Preco"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:TabelaPreco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Tabela de Preço',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a TabelaPreco entity.
     *
     * @param TabelaPreco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TabelaPreco $entity) {
        $form = $this->createForm(new TabelaPrecoType(), $entity, array(
            'action' => $this->generateUrl('Tabela_Preco_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new TabelaPreco entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new TabelaPreco();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Tabela_Preco"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:TabelaPreco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Tabela de Preço',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a TabelaPreco entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TabelaPreco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TabelaPreco entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Tabela_Preco"));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:TabelaPreco:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Tabela de Preço'
        ));
    }

    /**
     * Displays a form to edit an existing TabelaPreco entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TabelaPreco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TabelaPreco entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Tabela_Preco"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Tabela_Preco_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:TabelaPreco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Tabela de Preço',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a TabelaPreco entity.
     *
     * @param TabelaPreco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(TabelaPreco $entity) {
        $form = $this->createForm(new TabelaPrecoType(), $entity, array(
            'action' => $this->generateUrl('Tabela_Preco_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing TabelaPreco entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:TabelaPreco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TabelaPreco entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Tabela_Preco"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Tabela_Preco_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Tabela_Preco_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:TabelaPreco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Tabela de Preço',
                    'errors' => $errors,
        ));
    }
}
