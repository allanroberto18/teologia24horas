<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\EntityMaster;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller {

    public function login() {
        return $this->get('session')->get('admin');
    }

    public function redirectLogin() {
        $route = "/admin/login";
        $this->goToPage($route);
    }
    
    public function redirectHome() {
        $route = "/admin/";
        $this->goToPage($route);
    }

    public function logout() {
        $this->get('session')->remove('admin');
        $this->get('session')->remove('menu_1');
        return $this->redirectLogin();
    }
    
    public function checkLogin() {
        $login = $this->login();
        if (empty($login)) {
            return $this->redirectLogin();
        }
    }

    /**
     * @param string $route
     */
    private function goToPage($route)
    {
        header("location:" . $route);
        exit();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param $id
     * @param $bundleName
     * @param $entityName
     * @param $route
     * @param $routeParams
     * @return Retornar entitidade ou redirecionar para rota
     */
    protected function checkParent($id, $bundleName, $entityName, $route, $routeParams)
    {
        $entity = $this->getEntityManager()->getRepository($bundleName . ":" . $entityName . '')->find($id);
        if (empty($entity)) {
            $this->addFlash('error', "Relacionamento não localizado");

            $route = $this->generateUrl($route, array($routeParams));
            $this->goToPage($route);
        }
        return $entity;
    }


}
