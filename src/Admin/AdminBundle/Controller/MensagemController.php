<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Mensagem;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\MensagemPessoa;
use Admin\AdminBundle\Entity\MensagemEmailMarketing;
use Admin\AdminBundle\Form\MensagemType;
use Admin\AdminBundle\Entity\EmailMarketing;

/**
 * Mensagem controller.
 *
 */
class MensagemController extends MainController {

    public function checkPessoa($idPessoa) {
        return $this->getDoctrine()->getRepository("AdminBundle:Pessoa")->find($idPessoa);
    }

    public function checkEmailMarketing($idEmailMarketing) {
        return $this->getDoctrine()->getRepository("AdminBundle:EmailMarketing")->find($idEmailMarketing);
    }

    /**
     * Lists all Mensagem entities.
     *
     */
    public function indexAction() {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Mensagem')->findAll();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:Mensagem:index.html.twig', array(
                    'entities' => $entities,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Mensagem'
        ));
    }

    /**
     * Creates a new Mensagem entity.
     *
     */
    public function createAction(Request $request, $idPessoa) {
        $this->checkLogin();

        $pessoa = $this->checkPessoa($idPessoa);

        $sessionController = $this->get('session');
        if (!$pessoa instanceof Pessoa) {
            $this->addFlash('info', 'Aluno não localizado');
            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $entity = new Mensagem();
        $form = $this->createCreateForm($entity, $idPessoa);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $em->flush();

            $email = $this->container->get('admin.email');
            $email->mensagemByPessoa($pessoa, $entity);

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:Mensagem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Salvar Registro',
                    'modulo' => 'Mensagem',
                    'errors' => $errors,
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a form to create a Mensagem entity.
     *
     * @param Mensagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Mensagem $entity, $idPessoa) {
        $form = $this->createForm(new MensagemType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_create', array('idPessoa' => $idPessoa)),
            'method' => 'POST',
            'validation_groups' => array('admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Enviar", 'attr' => array('class' => 'btn btn-primary mt20')));

        return $form;
    }

    /**
     * Displays a form to create a new Mensagem entity.
     *
     */
    public function newAction($idPessoa) {
        $this->checkLogin();

        $pessoa = $this->checkPessoa($idPessoa);

        $sessionController = $this->get('session');
        if (!$pessoa instanceof Pessoa) {
            $this->addFlash('info', 'Aluno não localizado');
            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $entity = new Mensagem();
        $form = $this->createCreateForm($entity, $idPessoa);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Mensagem:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Mensagem',
                    'errors' => '',
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a new Mensagem entity.
     *
     */
    public function createEmailMarketingAction(Request $request, $idEmailMarketing) {
        $this->checkLogin();

        $emailMarketing = $this->checkEmailMarketing($idEmailMarketing);

        $sessionController = $this->get('session');
        if (!$emailMarketing instanceof EmailMarketing) {
            $this->addFlash('info', 'E-mail não localizado');
            return $this->redirect($this->generateUrl('EmailMarketing'));
        }

        $entity = new Mensagem();
        $form = $this->createCreateEmailMarketingForm($entity, $idEmailMarketing);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $em->flush();

            $email = $this->container->get('admin.email');
            $email->mensagemByEmailMarketing($emailMarketing, $entity);

            return $this->redirect($this->generateUrl('EmailMarketing'));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("E-mail Marketing", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:Mensagem:new_email_marketing.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Salvar Registro',
                    'modulo' => 'Mensagem',
                    'errors' => $errors,
                    'idEmailMarketing' => $idEmailMarketing
        ));
    }

    /**
     * Creates a form to create a Mensagem entity.
     *
     * @param Mensagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateEmailMarketingForm(Mensagem $entity, $idEmailMarketing) {
        $form = $this->createForm(new MensagemType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_email_create', array('idEmailMarketing' => $idEmailMarketing)),
            'method' => 'POST',
            'validation_groups' => array('admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Enviar", 'attr' => array('class' => 'btn btn-primary mt20')));

        return $form;
    }

    /**
     * Displays a form to create a new Mensagem entity.
     *
     */
    public function newEmailMarketingAction($idEmailMarketing) {
        $this->checkLogin();

        $emailMarketing = $this->checkEmailMarketing($idEmailMarketing);

        $sessionController = $this->get('session');
        if (!$emailMarketing instanceof EmailMarketing) {
            $this->addFlash('info', 'E-mail não localizado');
            return $this->redirect($this->generateUrl('EmailMarketing'));
        }

        $entity = new Mensagem();
        $form = $this->createCreateEmailMarketingForm($entity, $idEmailMarketing);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("E-mail Marketing", $this->get("router")->generate("EmailMarketing"));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Mensagem:new_email_marketing.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Mensagem',
                    'errors' => '',
                    'idEmailMarketing' => $idEmailMarketing
        ));
    }

    /**
     * Creates a new Mensagem entity.
     *
     */
    public function createTagAction(Request $request) {
        $this->checkLogin();

        $entity = new Mensagem();
        $form = $this->createCreateTagForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity->setStatus(2);
            $em->persist($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_show', array('id' => $entity->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:Mensagem:new_tag.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Salvar Registro',
                    'modulo' => 'Mensagem',
                    'errors' => $errors
        ));
    }

    /**
     * Creates a form to create a Mensagem entity.
     *
     * @param Mensagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateTagForm(Mensagem $entity) {
        $form = $this->createForm(new MensagemType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_tag_create'),
            'method' => 'POST',
            'validation_groups' => array('admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Enviar", 'attr' => array('class' => 'btn btn-primary mt20')));

        return $form;
    }

    /**
     * Displays a form to create a new Mensagem entity.
     *
     */
    public function newTagAction() {
        $this->checkLogin();

        $entity = new Mensagem();
        $form = $this->createCreateTagForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Mensagens", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Mensagem:new_tag.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Mensagem',
                    'errors' => ''
        ));
    }

    /**
     * Finds and displays a Mensagem entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Mensagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensagem entity.');
        }

        $tags = $em->getRepository("AdminBundle:Tag")->findBy(array('status' => 1));
        $nucleos = $em->getRepository("AdminBundle:Nucleo")->findBy(array('status' => 1));

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Visualizar");

        $localizacao = $this->container->get('admin.localizacao');
        $paises = $localizacao->listPaises();

        return $this->render('AdminBundle:Mensagem:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Mensagem',
                    'tags' => $tags,
                    'nucleos' => $nucleos,
                    'paises' => $paises,
        ));
    }

    /**
     * Displays a form to edit an existing Mensagem entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Mensagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensagem entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Mensagem_show", array("id" => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:Mensagem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Editar Registro',
                    'errors' => '',
                    'modulo' => 'Mensagem'
        ));
    }

    /**
     * Creates a form to edit a Mensagem entity.
     *
     * @param Mensagem $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Mensagem $entity) {
        $form = $this->createForm(new MensagemType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary mt20')));

        return $form;
    }

    /**
     * Edits an existing Mensagem entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Mensagem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensagem entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_edit', array('id' => $id)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Mensagem"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Mensagem_show", array("id" => $id)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:Mensagem:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Atualizar Registro',
                    'errors' => $errors,
                    'modulo' => 'Mensagem'
        ));
    }

}
