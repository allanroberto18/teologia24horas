<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\Mensagem;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\Fatura;

class EmailController extends Controller {

    private $dominio, $controller;

    public function __construct(Controller $controller) {
        $this->dominio = "http://teologia24horas.com.br/";
        $this->controller = $controller;
    }

    public function sendMessageForPeople(Pessoa $pessoa, Mensagem $mensagem)
    {
        $body = $this->controller->renderView("AdminBundle:Email:message.html.twig", array(
            'titulo' => $mensagem->getTitulo(),
            'conteudo' => $mensagem->getConteudo(),
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));
        $this->sendAction($mensagem->getTitulo(), $body, $pessoa->getEmail());
    }
    
    public function sendMessageForEmailMarketing(EmailMarketing $emailMarketing, Mensagem $mensagem)
    {
        $body = $this->controller->renderView("AdminBundle:Email:message_email_marketing.html.twig", array(
            'titulo' => $mensagem->getTitulo(),
            'conteudo' => $mensagem->getConteudo(),
            'dominio' => $this->dominio,
        ));
        $this->sendAction($mensagem->getTitulo(), $body, $emailMarketing->getEmail());
    }
    
    public function sendConfirmarCadastroAction(EmailMarketing $entity) {
        $titulo = 'Falta só um clique...';

        $mensagem = $this->controller->renderView("AdminBundle:Email:confirmar_cadastro.html.twig", array(
            'titulo' => $titulo,
            'entity' => $entity,
            'dominio' => $this->dominio,
        ));

        $this->sendAction($titulo, $mensagem, $entity->getEmail());
    }

    public function sendCadastroConfirmadoAction(EmailMarketing $entity) {
        $titulo = 'Seu CURSO GRÁTIS já está disponível...';

        $mensagem = $this->controller->renderView("AdminBundle:Email:cadastro_confirmado.html.twig", array(
            'titulo' => $titulo,
            'entity' => $entity,
            'dominio' => $this->dominio,
        ));

        $this->sendAction($titulo, $mensagem, $entity->getEmail());
    }

    public function sendDicaSenha(Pessoa $entity) {
        $titulo = "Dados para acessar a Área Exclusiva";

        $mensagem = $this->controller->renderView("AdminBundle:Email:dica_senha.html.twig", array(
            'titulo' => $titulo,
            'entity' => $entity,
            'dominio' => $this->dominio,
        ));

        $this->sendAction($titulo, $mensagem, $entity->getEmail());
    }
    
    public function sendConfirmPayment(Contrato $contrato, Fatura $fatura)
    {
        $titulo = "Pagamento Confirmado";
        $pessoa = $contrato->getIdPessoa();
        $curso = $contrato->getIdCurso();
        
        $mensagem = $this->controller->renderView("AdminBundle:Email:confirmacao_pagamento.html.twig", array(
            'titulo' => $titulo,
            'curso' => $curso,
            'fatura' => $fatura,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $this->sendAction($titulo, $mensagem, $pessoa->getEmail());
    }
    
    public function sendAction($titulo, $mensagem, $destino, $origem = 'teologia24horas@teologia24horas.com.br') {

        $mailer = $this->controller->get('mailer');

        $swiftMailer = \Swift_Message::newInstance()
                ->setSubject($titulo)
                ->setFrom($origem)
                ->setTo($destino)
                ->setBody($mensagem, 'text/html');

        $mailer->send($swiftMailer);

        // now manually flush the queue
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->controller->get('swiftmailer.transport.real');

        $spool->flushQueue($transport);
    }

}
