<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\ContratoNota;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\EmailMarketingTag;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\Mensagem;
use Admin\AdminBundle\lib\ConnectDB;
use Admin\AdminBundle\lib\Migrate;
use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\lib\UsuarioConsult;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AdminController extends MainController
{

    public function homeAction()
    {
        $login = $this->login();
        if (empty($login)) {
            return $this->redirectLogin();
        }

        $dataInicial = date("Y-m-01 00:00:00");
        $dataFinal = date("Y-m-t 23:59:59");

        $repository = $this->getDoctrine()->getRepository("AdminBundle:EmailMarketing");
        $list = $repository->createQueryBuilder("item")
            ->where("item.datahoraCadastro between :dataInicial AND :dataFinal")
            ->setParameter('dataInicial', $dataInicial)
            ->setParameter('dataFinal', $dataFinal)
            ->getQuery()
            ->getResult();

        $mensagens = $this->getDoctrine()->getRepository('AdminBundle:Mensagem');
        $queryBuilder = $mensagens->createQueryBuilder('item')
            ->orderBy("item.datahoraCadastro", "DESC");
        
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true)));

        $gridConfig->addField(new Field('item.id', array('label' => 'Dados do Contato', "sortable" => true,
            'formatValueCallback' => function ($value) {
                $result = "";
                $mensagemEmailMarketing = $this->getDoctrine()->getRepository("AdminBundle:MensagemEmailMarketing")->findBy(array('idMensagem' => $value));
                if (count($mensagemEmailMarketing) > 0) {
                    $result .= $this->mountEmailMarketing($mensagemEmailMarketing);
                }
                $mensagemPessoa = $this->getDoctrine()->getRepository("AdminBundle:MensagemPessoa")->findBy(array('idMensagem' => $value));
                if (count($mensagemPessoa) > 0) {
                    $result .= $this->mountPessoa($mensagemPessoa);
                }
                print $result;
            })));

        $gridConfig->addField(new Field('item.datahora', array('label' => 'Data de Envio', "sortable" => true,
            'formatValueCallback' => function ($value) {
                $date = new \DateTime($value);
                return $date->format('d/m/Y H:i:s');
            })));

        $gridConfig->addSelector(array('label' => 'Mensagens Enviadas', 'field' => 'item.status', 'value' => 1));
        $gridConfig->addSelector(array('label' => 'Mensagens Recebidas', 'field' => 'item.status', 'value' => 2));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal");

        return $this->render('AdminBundle:Admin:index.html.twig', array(
            'titulo' => 'Página Principal',
            'modulo' => 'Home',
            'inscritos' => $this->totalEmailTag($list),
            'cadastrados' => $this->totalEmailTag($list, 2),
            'usuarios' => $this->totalCadastrados($list),
            'dataInicial' => $dataInicial,
            'dataFinal' => $dataFinal,
            'grid' => $grid,
        ));
    }

    public function mountEmailMarketing($emailMarketing)
    {
        $result = "<strong> E-mail Marketing: </strong><br />";
        foreach ($emailMarketing as $value) {
            $result .= $value->getIdEmailMarketing()->getEmail() . "<br />";
        }
        return $result;
    }

    public function mountPessoa($pessoa)
    {
        $result = "<strong> Pessoas: </strong><br />";
        foreach ($pessoa as $value) {
            $result .= $value->getIdPessoa()->getPfNome() . "<br />";
        }
        return $result;
    }

    public function deleteAction(Request $request)
    {
        $form = $request->request->all();
        $session = $this->get('session');
        if (empty($form)) {
            $session->getFlashBag()->set("msg", 'Nenhuma mensagem foi selecionada para exclusão');
            return $this->redirect($this->generateUrl('admin_index'));
        }

        $mensagens = $form["check"];

        foreach ($mensagens as $id) {
            $em = $this->getDoctrine()->getManager();
            $mensagem = $em->getRepository("AdminBundle:Mensagem")->find($id);

            if ($mensagem instanceof Mensagem) {

                if (count($mensagem->getMensagemPessoa()) > 0) {
                    $emailPessoas = $mensagem->getMensagemPessoa();

                    foreach ($emailPessoas as $ligacao) {
                        $em->remove($ligacao);
                    }
                }
                if (count($mensagem->getMensagemEmailMarketing()) > 0) {
                    $emailMarketingMessages = $mensagem->getMensagemEmailMarketing();

                    foreach ($emailMarketingMessages as $ligacao) {
                        $em->remove($ligacao);
                    }
                }
                if (count($mensagem->getMensagemTag()) > 0) {
                    $tagMessages = $mensagem->getMensagemTag();

                    foreach ($tagMessages as $ligacao) {
                        $em->remove($ligacao);
                    }
                }
                $em->remove($mensagem);
                $em->flush();

                $session->getFlashBag()->set("msg", 'Mensagens excluídas com sucesso');
            }
        }

        return $this->redirect($this->generateUrl("admin_index"));
    }

    public function totalEmailTag($list, $idTag = 1)
    {
        if (empty($list)) {
            return 0;
        }
        if ($idTag == 1) {
            return count($list);
        }
        $em = $this->getDoctrine()->getManager();
        $i = 0;
        foreach ($list as $item) {
            $tag = $em->getRepository('AdminBundle:EmailMarketingTag')->findOneBy(array('idEmailMarketing' => $item->getId(), 'idTag' => $idTag));
            if ($tag instanceof EmailMarketingTag) {
                $i++;
            }
        }
        return $i;
    }

    public function totalCadastrados($list)
    {
        if (empty($list)) {
            return 0;
        }
        $em = $this->getDoctrine()->getManager();
        $i = 0;
        foreach ($list as $item) {
            if ($item->getPessoa()) {
                $i++;
            }
        }
        return $i;
    }

    public function generateTags()
    {
        $repository = $this->getDoctrine()->getRepository("AdminBundle:EmailMarketing");
        $list = $repository->createQueryBuilder("item")
            ->orderBy("item.datahoraCadastro", "ASC")
            ->getQuery()
            ->getResult();

        $sql = "DELETE FROM email_marketing_tag WHERE id_tag <> 2; <br />";
        $sql .= "TRUNCATE TABLE pessoa_tag; <br />";
        for ($i = 0; $i < count($list); $i++) {
            if ($list[$i]->getPessoa() instanceof Pessoa) {
                if ($list[$i]->getPessoa()->getPfCpf()) {
                    $sql .= "INSERT INTO pessoa_tag (id_pessoa, id_tag, status) VALUES (" . $list[$i]->getPessoa()->getId() . ", 4, 1);<br />";
                }
                if ($list[$i]->getPessoa()->getSenha()) {
                    $sql .= "INSERT INTO pessoa_tag (id_pessoa, id_tag, status) VALUES (" . $list[$i]->getPessoa()->getId() . ", 5, 1);<br />";
                }
                if ($list[$i]->getPessoa()->getEndereco()) {
                    $sql .= "INSERT INTO pessoa_tag (id_pessoa, id_tag, status) VALUES (" . $list[$i]->getPessoa()->getId() . ", 7, 1);<br />";
                }
            }
            $sql .= "INSERT INTO email_marketing_tag (id_email_marketing, id_tag, status) VALUES (" . $list[$i]->getId() . ", 1, 1);<br /><br />";
        }
    }

    public function gerarContrato(Pessoa $pessoa, Curso $curso)
    {
        $em = $this->getDoctrine()->getManager();

        $checkContrato = $em->getRepository('AdminBundle:Contrato')->findBy(array('idCurso' => $curso->getId(), 'idPessoa' => $pessoa->getId()));
        if (count($checkContrato) > 0) {
            return false;
        }

        $contrato = new Contrato();
        $contrato->setIdPessoa($pessoa);
        $contrato->setIdCurso($curso);
        $contrato->setStatus(2);

        $em->persist($contrato);

        $tag = $em->getRepository("AdminBundle:Tag")->findOneBy(array('titulo' => $curso->getTitulo()));

        $emailTag = new EmailMarketingTag();
        $emailTag->setIdTag($tag);
        $emailTag->setIdEmailMarketing($pessoa->getIdEmailMarketing());
        $emailTag->setStatus(1);

        $em->persist($emailTag);

        $dataVencimento = new \DateTime(date("Y-m-d"));
        $dataVencimento->modify('+5 days');

        $fatura = new Fatura();
        $fatura->setIdContrato($contrato);
        $fatura->setValor($curso->getIdTabelaPreco()->getValor());
        $fatura->setPagamentoValor(null);
        $fatura->setVencimento($dataVencimento);
        $fatura->getStatus(1);

        $em->persist($fatura);

        $em->flush();

        return $contrato->getId();
    }

    public function loginAction(Request $request)
    {
        $login = $this->login();
        if (!empty($login)) {
            return $this->redirectHome();
        }

        $form = $request->request->all();

        if (!empty($form["submit"])) {
            $userRepository = $this->getDoctrine()->getRepository('AdminBundle:Usuario');

            $usuario = new UsuarioConsult($userRepository);
            $usuario->setParams($form["email"], $form["senha"]);
            $usuario->validarUsuario();
        }

        return $this->render('AdminBundle:Admin:login.html.twig', array(
            'titulo' => 'Autenticar Usuário',
            'modulo' => 'Home',
        ));
    }

    public function logoutAction()
    {
        return $this->logout();
    }

    public function executarMigracao()
    {
        $em = $this->getDoctrine()->getManager();

        $faturas = $em->getRepository("AdminBundle:Fatura")
            ->createQueryBuilder('item')
            ->where('item.valor > :valor')
            ->andWhere('item.vencimento BETWEEN :inicio AND :fim')
            ->setParameter('valor', 0)
            ->setParameter('inicio', '2015-01-01')
            ->setParameter('fim', '2015-04-30')
            ->orderBy('item.vencimento', 'DESC')
            ->getQuery()
            ->getResult();

        if (count($faturas) > 0) {
            for ($i = 0; $i < count($faturas); $i++) {
                $this->criarContrato($faturas[$i]);
            }
        }
        return $faturas;
    }

    public function criarCursosGratuitos()
    {
        $em = $this->getDoctrine()->getManager();

        $contratos = $em->getRepository("AdminBundle:Contrato")->findBy(array("idCurso" => 1));

        $result = array();
        foreach ($contratos as $contrato) {
            $fatura = new \Admin\AdminBundle\Entity\Fatura();
            $fatura->setIdContrato($contrato);
            $fatura->setValor(0);
            $fatura->setVencimento(new \DateTime(date("Y-m-d")));
            $fatura->setPagamentoData(new \DateTime(date("Y-m-d")));
            $fatura->setPagamentoValor(0);
            $fatura->setStatus(2);

            $em->persist($fatura);

            $result[] = $fatura;
        }
        $em->flush();

        return $result;
    }

    public function criarContrato(\Admin\AdminBundle\Entity\Fatura $fatura)
    {
        $vencimento = $fatura->getVencimento()->format("Y-m-d");

        $d = explode("-", $vencimento);
        $idCurso = "";
        switch ($d[1]) {
            case 1:
                $idCurso = 9;
                break;
            case 2:
                $idCurso = 3;
                break;
            case 3:
                $idCurso = 10;
                break;
            case 4:
                $idCurso = 11;
                break;
        }
        if (empty($idCurso)) {
            return false;
        }

        $em = $this->getDoctrine()->getManager();
        try {
            $pessoa = $fatura->getIdContrato()->getIdPessoa();

            $curso = $em->getRepository("AdminBundle:Curso")->find($idCurso);

            $status = $fatura->getStatus() == 1 ? 2 : 1;

            $contrato = new \Admin\AdminBundle\Entity\Contrato();
            $contrato->setIdPessoa($pessoa);
            $contrato->setIdCurso($curso);
            $contrato->setStatus($status);

            $em->persist($contrato);

            $fatura->setIdContrato($contrato);

            $em->flush();
        } catch (Exception $ex) {
            print "Problema com o contrato: " . $fatura->getId() . "<br />";
        }
    }

    public function migrateAction()
    {
        $pessoa = $this->consultPessoa('Jandira Batista Carneiro Marinho');
        $contrato = $this->consultContrato($pessoa);
        $faturas = $this->consultFatura($contrato);

        return $this->render('AdminBundle:Admin:migrate.html.twig', array(
            'dados' => $faturas,
            'pessoa' => $pessoa,
//            'modulos' => $modulos,
        ));
    }

    /**
     * Consulta registros via PDO
     * @param $table
     * @param null $attrib
     * @param null $value
     * @param int $type
     * @return array
     */
    public function consult($table, $attrib = null, $value = null, $type = 1)
    {
        $migrate = new Migrate($table);

        //$conn = new ConnectDB('ead_teste');
        $conn = new ConnectDB();

        $migrate->setAttrib($attrib);
        $migrate->setValue($value);

        return $migrate->returnList($conn, $type);
    }

    public function consultPessoa($name)
    {
        return $this->consult('tb_pessoa', 'pf_nome', $name, 2);
    }

    public function consultContrato(array $resultSet)
    {
        if (!is_array($resultSet) && empty($resultSet) && !array_key_exists('id', $resultSet)) {
            return false;
        }
        return $this->consult('tb_contratacao', 'id_pessoa', $resultSet["id"]);
    }

    public function consultFatura(array $resultSet)
    {
        if (!is_array($resultSet) && empty($resultSet) && !array_key_exists('id', $resultSet)) {
            return false;
        }
        $fatura = array();
        foreach ($resultSet as $result) {
            $fatura[] = $this->consult('tb_contratacao_fatura', 'id_contratacao', $result["id"]);
        }
        return $fatura;
    }

    public function consultNotas()
    {
        $notas = $this->consult('tb_curso_avaliacao_nota');

        if (empty($notas)) {
            return false;
        }

        for ($i = 0; $i < count($notas); $i++) {
            $media = ($notas[$i]['avaliacao_recuperacao'] > 0) ?: $this->returnMedia($notas[$i]);

            //$return = $this->contratarCurso($notas[$i]['id_pessoa'], $this->returnModulo($notas[$i]['id_curso_modulo']), $notas[$i]['data_avaliacao'], $media);
            $id[] = $this->contratarCursoNew($notas[$i]['id_pessoa'], $this->returnModulo($notas[$i]['id_curso_modulo']), empty($notas[$i]['data_avaliacao']) ? $notas[$i]['data_lancamento'] : $notas[$i]['data_avaliacao'], $media);
        }
        return $id;
    }

    public function returnMedia(array $nota)
    {
        return number_format(($nota['avaliacao'] + $nota['presenca'] + $nota['avaliacao_complementar'] + $nota['resumo_livro']) / 4, 2);
    }

    public function returnModulo($id)
    {
        switch ($id) {
            case 1 :
                $modulo = "4";
                break;
            case 2 :
                $modulo = "5";
                break;
            case 3 :
                $modulo = "6";
                break;
            case 4 :
                $modulo = "7";
                break;
            case 5 :
                $modulo = "8";
                break;
            case 6 :
                $modulo = "9";
                break;
            case 7 :
                $modulo = "3";
                break;
            case 8 :
                $modulo = "10";
                break;
            case 9 :
                $modulo = "11";
                break;
            case 10 :
                $modulo = "12";
                break;
            case 11 :
                $modulo = "13";
                break;
            case 12 :
                $modulo = "14";
                break;
            case 13 :
                $modulo = "15";
                break;
            case 14 :
                $modulo = "16";
                break;
            case 15 :
                $modulo = "17";
                break;
            case 16 :
                $modulo = "18";
                break;
            case 17 :
                $modulo = "19";
                break;
            case 18 :
                $modulo = "20";
                break;
            case 19 :
                $modulo = "21";
                break;
            case 20 :
                $modulo = "22";
                break;
        }

        return $modulo;
    }

    public function returnById($em, $repository, $id)
    {
        return $em->getRepository("{$repository}")->find($id);
    }

    public function returnOneBy($em, $repository, array $fields)
    {
        return $em->getRepository("{$repository}")->findOneBy($fields);
    }

    public function contratarCurso($idPessoa, $idCurso, $dataAvaliacao, $media)
    {
        $em = $this->getDoctrine()->getManager();

        $Pessoa = $this->returnById($em, 'AdminBundle:Pessoa', $idPessoa);

        $Curso = $this->returnById($em, 'AdminBundle:Curso', $idCurso);

        if (!$Pessoa instanceof Pessoa) {
            $message[] = "Pessoa não localizada - id: " . $idPessoa;
        }

        if (!$Curso instanceof Curso) {
            $message[] = "Curso não localizado - id: " . $idCurso;
        }

        $dataCadastro = new \DateTime($dataAvaliacao);

        $contrato = new Contrato();
        $contrato->setIdPessoa($Pessoa);
        $contrato->setIdCurso($Curso);
        $contrato->setDataCadastro($dataCadastro);
        $contrato->setDataContratacao($dataCadastro);
        $contrato->setStatus(2);

        $em->persist($contrato);

        $em->flush();

//        $vencimento = $this->alterarData($dataAvaliacao);
//        $fatura = new Fatura();
//        $fatura->setIdContrato($contrato);
//        $fatura->setVencimento($vencimento);
//        $fatura->setValor(89.90);
//        $fatura->setPagamentoValor(89.90);
//        $fatura->setPagamentoData($vencimento);
//        $fatura->setStatus(2);
//
//        $em->persist($fatura);

        $EmailMarketing = $Pessoa->getIdEmailMarketing();

        if (!$EmailMarketing instanceof EmailMarketing) {
            $message[] = "E-mail não localizado - id: " . $idPessoa;
        } else {
            $tag = $this->returnOneBy($em, 'AdminBundle:Tag', array('titulo' => $Curso->getTitulo()));

            $tagLigacao = new EmailMarketingTag();
            $tagLigacao->setIdEmailMarketing($EmailMarketing);
            $tagLigacao->setIdTag($tag);
            $tagLigacao->setStatus(1);

            $em->persist($tagLigacao);

            $em->flush();
        }

        $nota = new ContratoNota();
        $nota->setIdContrato($contrato);
        $nota->setMedia($media);
        $nota->setStatus(1);

        $em->persist($nota);

        $em->flush();

        return $message;
    }

    public function contratarCursoNew($idPessoa, $idCurso, $dataAvaliacao, $media)
    {
        $contrato = array(
            'id_pessoa' => $idPessoa,
            'id_curso' => $idCurso,
            'data_cadastro' => $dataAvaliacao,
            'data_contratacao' => $dataAvaliacao,
            'status' => 2,
        );

        $migrate = new Migrate();
        $idContrato = $migrate->insertData('contrato', $contrato);

        $nota = array(
            'id_contrato' => $idContrato,
            'media' => $media,
            'status' => 1
        );
        return $migrate->insertData('contrato_nota', $nota);
    }

    function alterarData($dataAvaliacao)
    {
        $d = explode("-", $dataAvaliacao);
        return new \DateTime($d[0] . "-" . $d[1] . "-25");
    }

    public function testeAction()
    {

        $this->container->get('api.locaweb')->removeEmails();

        return $this->render('AdminBundle:Admin:teste.html.twig', array(
            'titulo' => 'Página Principal',
            'result' => 'ok',
            'message' => '',
        ));
    }

    public function listEmailDeleteAction()
    {
        $email = $this->container->get('admin.emailMarketing')->listEmailsToRemove();

        return $this->render('AdminBundle:Admin:teste.html.twig', array(
            'titulo' => 'Página Principal',
            'result' => $email,
        ));
    }

    public function deleteEmailAction()
    {
        $this->container->get('admin.emailmarketing')->deleteEmail();

        return $this->render('AdminBundle:Admin:teste.html.twig', array(
            'titulo' => 'Página Principal',
            'result' => '',
            'message' => '',
        ));
    }

    public function deleteEmailsRecusados()
    {
        $this->container->get('api.emails.recusados')->removeEmails();

        return $this->render('AdminBundle:Admin:teste.html.twig', array(
            'titulo' => 'Página Principal',
            'result' => '',
            'message' => '',
        ));
    }

}
        