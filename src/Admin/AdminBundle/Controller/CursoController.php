<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\Tag;
use Admin\AdminBundle\Form\CursoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Curso controller.
 *
 */
class CursoController extends MainController {

    /**
     * Lists all Curso entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Curso');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Inativo</span>";
                        break;
                    case 3:
                        print "<span class='label label-warning'><span class='fa fa-exclamation'></span> Cancelado</span>";
                        break;
                };
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:Curso:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Curso'
        ));
    }

    /**
     * Creates a new Curso entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Curso();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $tag = new Tag();
            $tag->setTitulo($entity->getTitulo());
            $tag->setStatus(1);

            $em->persist($tag);

            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $entity->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:Curso:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Curso',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Curso entity.
     *
     * @param Curso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Curso $entity) {
        $form = $this->createForm(new CursoType(), $entity, array(
            'action' => $this->generateUrl('Curso_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Curso entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new Curso();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Curso:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Curso',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Curso entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Curso')->find($id);

        if (!$entity && !$entity instanceof Curso) {
            throw $this->createNotFoundException('Unable to find Curso entity.');
        }

//        $repository = $this->getDoctrine()->getRepository('AdminBundle:Forum');
//        $queryBuilder = $repository->createQueryBuilder('item')
//                ->where('item.idCurso = :idCurso')
//                ->add('orderBy', 'item.datahoraCadastro DESC')
//                ->setParameter('idCurso', $id);
//
//        $gridConfig = new GridConfig();
//        // Passagem de dados
//        $gridConfig->setQueryBuilder($queryBuilder);
//
//        // Contagem de registros
//        $gridConfig->setCountFieldName('item.id');
//
//        $gridConfig->addField(new Field('item.tema', array('label' => 'Comentario', "sortable" => true)));
//        $gridConfig->addField(new Field('item.datahoraCadastro', array(
//            'label' => 'Cadastro',
//            'sortable' => true,
//            'formatValueCallback' => function($value) {
//                return date_format($value, "d/m/Y H:i:s");
//            }
//        )));
//        $gridConfig->addField(new Field('item.datahoraAtualizacao', array(
//            'label' => 'Atualizacao',
//            'sortable' => true,
//            'formatValueCallback' => function($value) {
//                return date_format($value, "d/m/Y H:i:s");
//            }
//        )));
//        $gridConfig->addField(new Field('item.status', array(
//            'label' => 'Situação',
//            'sortable' => true,
//            'formatValueCallback' => function($value) {
//                switch ($value) {
//                    case 1:
//                        $result = "Ativo";
//                        break;
//                    case 2:
//                        $result = "Inativo";
//                        break;
//                }
//                return $result;
//            }
//        )));
//
//        $gridManager = $this->get('kitpages_data_grid.grid_manager');
//        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso"));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:Curso:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Curso',
                        //'grid' => $grid,
        ));
    }

    /**
     * Displays a form to edit an existing Curso entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Curso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Curso entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Curso_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");
        $breadcrumbs->addItem("Voltar", "javascript: history.go(-1)");

        return $this->render('AdminBundle:Curso:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Curso',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Curso entity.
     *
     * @param Curso $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Curso $entity) {
        $form = $this->createForm(new CursoType(), $entity, array(
            'action' => $this->generateUrl('Curso_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Curso entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Curso')->find($id);

        $tag = $em->getRepository("AdminBundle:Tag")->findOneBy(array('titulo' => $entity->getTitulo()));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Curso entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Curso_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        if ($editForm->isValid() && count($errors) == 0) {
            if (empty($tag)) {
                $tag = new Tag();
                $tag->setTitulo($entity->getTitulo());
                $tag->setStatus(1);
                
                $em->persist($tag);
            } else {
                $tag->setTitulo($entity->getTitulo());
            }

            $em->flush();

            return $this->redirect($this->generateUrl('Curso_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:Curso:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Curso',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Curso entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Curso')->find($id);

        $entity->setStatus(2);
        $em->flush();

        return $this->redirect($this->generateUrl('Curso_show', array('id' => $id)));
    }

    public function reordenarCursosAction() {
        $em = $this->getDoctrine()->getManager();

        $listCursos = $em->getRepository("AdminBundle:Curso")
                ->createQueryBuilder('item')
                ->where('item.ordem > :ordem')
                ->setParameter('ordem', 0)
                ->orderBy('item.ordem', 'ASC')
                ->getQuery()
                ->getResult();

        $count = count($listCursos);

        for ($i = 0; $i < count($listCursos); $i++) {
            $var = $listCursos[$i]->getOrdem();
            switch ($var) {
                case 1:
                    $var = $count;
                    break;
                default :
                    --$var;
                    break;
            }
            $listCursos[$i]->setOrdem($var);

            $em->flush();
        }

        return $this->render('AdminBundle:Curso:cron.html.twig', array(
                    'entity' => $listCursos,
                    'count' => $count,
        ));
    }

}
