<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\AulaAcesso;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Form\AulaAcessoType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

class AulaAcessoController extends MainController
{
    /**
     * @Route("/Contrato/{idContrato}/novo", name="admin_aula_acesso_novo")
     * @Template("AdminBundle:AulaAcesso:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function novoAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $entity = new AulaAcesso();

        $form = $this->createForm(new AulaAcessoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdContrato($contrato);

            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', 'Dados salvos com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_acesso_novo'
                : 'admin_aula_acesso_listar';

            return $this->redirectToRoute($nextAction, array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home'));
        $breadcrumbs->addItem('Acesso às Aulas: Listar Registros', $this->get('router')->generate('admin_aula_acesso_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Novo Registro');

        return array(
            'form' => $form->createView(),
            'titulo' => "Novo Registro",
            'modulo' => array('titulo' => 'Acesso às Aulas', 'descricao' => ''),
        );

    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/atualizar", name="admin_aula_acesso_atualizar")
     * @Template("AdminBundle:AulaAcesso:form.html.twig")
     * @Method({"GET", "POST"})
     */
    public function updateAction($idContrato, $id, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaAcesso')->find($id);
        if (!$entity instanceof AulaAcesso) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
        }

        $form = $this->createForm(new AulaAcessoType(), $entity, [ 'attr' => [ 'class' => 'form', 'novalidate' => 'novalidate' ] ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Dados alterados com sucesso');

            $nextAction = $form->get('actions')->get('salvarNovo')->isClicked()
                ? 'admin_aula_acesso_novo'
                : 'admin_aula_acesso_listar';

            return $this->redirectToRoute($nextAction, array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Acesso às Aulas: Listar Registros', $this->get('router')->generate('admin_aula_acesso_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar', $this->get('router')->generate('admin_aula_acesso_visualizar', array('idContrato' => $idContrato, 'id' => $id)));
        $breadcrumbs->addItem('Atualizar');

        return array(
            'titulo' => 'Alterar Registro',
            'form' => $form->createView(),
            'modulo' => array('titulo' => 'Acesso às Aulas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/listar", name="admin_aula_acesso_listar")
     * @Template("AdminBundle:AulaAcesso:list.html.twig")
     * @Method({"GET", "POST"})
     */
    public function listAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $repository = $this->getDoctrine()->getRepository('AdminBundle:AulaAcesso');
        $queryBuilder = $repository->createQueryBuilder()
            ->select('item, aula')
            ->from('AdminBundle:AulaAcesso', 'item')
            ->leftJoin('item.idAula', 'aula')
            ->where('item.status = :status')
            ->andWhere('item.idContrato = :contrato')
            ->setParameter('status', '1')
            ->setParameter('contrato', $idContrato)
        ;

        $gridConfig = new GridConfig();
        $gridConfig->setQueryBuilder($queryBuilder)
            ->setCountFieldName('item.id')
            ->addField(new Field('aula.titulo', array('label' => 'Aula', 'filterable' => 'true', 'sortable' => true)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $request);

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Acessos às Aulas: Listar registros');

        return array(
            'titulo' => 'Listagem de Registros',
            'grid' => $grid,
            'delete' => $this->generateUrl('admin_aula_acesso_delete_selecionado', ['idContrato' => $idContrato]),
            'novo' => $this->generateUrl('admin_aula_acesso_novo', ['idContrato' => $idContrato]),
            'modulo' => array('titulo' => 'Acessos às Aulas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/visualizar", name="admin_aula_acesso_visualizar")
     * @Template("AdminBundle:AulaAcesso:view.html.twig")
     * @Method("GET")
     */
    public function viewAction($idContrato, $id)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaAcesso')->find($id);
        if (!$entity instanceof AulaAcesso) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Home', $this->get('router')->generate('admin_home', array()));
        $breadcrumbs->addItem('Acessos às Aulas: Listar Registros', $this->get('router')->generate('admin_aula_acesso_listar', array('idContrato' => $idContrato)));
        $breadcrumbs->addItem('Visualizar');

        return array(
            'titulo' => 'Visualizar: ',
            'entity' => $entity,
            'modulo' => array('titulo' => 'Acessos às Aulas', 'descricao' => ''),
        );
    }

    /**
     * @Route("/Contrato/{idContrato}/{id}/delete", name="admin_aula_acesso_delete")
     * @Method("GET")
     */
    public function deleteAction($idContrato, $id)
    {
        $this->checkLogin();

        $contrato = $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:AulaAcesso')->find($id);
        if (!$entity instanceof AulaAcesso) {
            $this->addFlash('error', 'Não foi possível localizar este registro');

            return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
        }
        $entity->setStatus(2);
        $em->flush();

        $this->addFlash('success', 'Dados removidos com sucesso');

        return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
    }

    /**
     * @Route("/Contrato/{idContrato}/delete/selecionados", name="admin_aula_acesso_delete_selecionado")
     * @Method("POST")
     */
    public function deleteSelecionadosAction($idContrato, Request $request)
    {
        $this->checkLogin();

        $this->checkParent($idContrato, "AdminBundle", 'Contrato', 'admin_home', null);

        $data = $request->request->all();

        if (empty($data)) {
            $this->addFlash('info', 'Não foi possível excluir nenhum registro, pois nenhum foi selecionado');

            return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
        }
        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($data['check']); $i++) {
            $entity = $em->getRepository('AdminBundle:AulaAcesso')->find($data['check'][$i]);
            $entity->setStatus(2);

            $em->flush();
        }

        $this->addFlash('success', 'Dados excluídos com sucesso');

        return $this->redirectToRoute('admin_aula_acesso_listar', array('idContrato' => $idContrato));
    }

    public function ajaxAcessoAction(Request $request)
    {
        $data = $request->request->all();
        if (empty($data))
        {
            return new JsonResponse([ 'mensagem' => 'Não existe contrato disponível para acessar essa aula']);
        }
        $em = $this->getDoctrine()->getManager();
        $acesso = $em->getRepository('AdminBundle:AulaAcesso')->findBy(['idAula' => $data['aula'], 'idContrato' => $data['contrato']]);
        if (count($acesso) > 0)
        {
            return new JsonResponse([ 'mensagem' => 'Acesso já estava salvo']);
        }

        $contrato = $em->getRepository("AdminBundle:Contrato")->find($data['contrato']);
        $aula = $em->getRepository("AdminBundle:Aula")->find($data['aula']);

        $entity = new AulaAcesso();
        $entity->setIdAula($aula);
        $entity->setIdContrato($contrato);
        $entity->setStatus(1);

        $em->persist($entity);
        $em->flush();

        if (empty($entity->getId()))
        {
            return new JsonResponse([ 'error' => 'Não foi possível salvar o acesso, contate o suporte']);
        }
        return new JsonResponse([ 'success' => 'Acesso salvo com sucesso']);
    }
}
