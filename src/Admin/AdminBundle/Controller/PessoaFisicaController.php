<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Form\PessoaFisicaType2;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Admin\AdminBundle\Entity\PessoaFisica;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Form\PessoaFisicaType;
use Admin\AdminBundle\Entity\Contrato;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * PessoaFisica controller.
 *
 */
class PessoaFisicaController extends MainController {

    /**
     * Lists all PessoaFisica entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Pessoa');
        $queryBuilder = $repository->createQueryBuilder('item')
                ->where('item.tipoPessoa = :tipoPessoa')
                ->setParameter('tipoPessoa', 1);

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.pfCpf', array('label' => 'CPF')));
        $gridConfig->addField(new Field('item.pfNome', array('label' => 'Nome', "sortable" => true, "filterable" => true, 'formatValueCallback' => function($value) {
                return ucwords(strtolower($value));
            })));
        $gridConfig->addField(new Field('item.email', array('label' => 'email')));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:PessoaFisica:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Pessoa Física'
        ));
    }

    /**
     * Creates a new PessoaFisica entity.
     *
     */
    public function createAction($idEmailMarketing, Request $request) {
        $email = $this->getDoctrine()->getRepository("AdminBundle:EmailMarketing")->find($idEmailMarketing);

        if (!$email instanceof EmailMarketing) {
            return $this->redirect($this->generateUrl("EmailMarketing"));
        }

        $entity = new Pessoa();
        $checkForm = 1;
        $dados = $request->request->all();
        if (!empty($dados))
        {
            switch ($dados["ava_perfil"]["pais"])
            {
                case "Brasil" :
                    $checkForm = 1;
                    break;
                default :
                    $checkForm = 2;
                    break;
            }
        }
        $form = $this->createCreateForm($entity, $idEmailMarketing, $this->getDoctrine()->getEntityManager(), $checkForm);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $entity->setIdEmailMarketing($email);
            $entity->setEmail($email->getEmail());
            $entity->setSenha();

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $container = $this->container->get('admin.tags');
            $container->persistPessoaTag($entity->getId(), 4);

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $entity->getId())));
        }
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:PessoaFisica:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Pessoa Física',
                    'errors' => $errors,
                    'idEmailMarketing' => $idEmailMarketing,
        ));
    }

    /**
     * Creates a form to create a PessoaFisica entity.
     *
     * @param PessoaFisica $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pessoa $entity, $idEmailMarketing, $em, $value=1) {
        switch ($value)
        {
            case 1:
                $class = new PessoaFisicaType($em);
                break;
            case 2:
                $class = new PessoaFisicaType2($em);
                break;
        }
        $form = $this->createForm($class, $entity, array(
            'action' => $this->generateUrl('Pessoa_Fisica_create', array("idEmailMarketing" => $idEmailMarketing)),
            'method' => 'POST',
            'validation_groups' => array('perfil_admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new PessoaFisica entity.
     *
     */
    public function newAction($idEmailMarketing) {
        $this->checkLogin();

        $email = $this->getDoctrine()->getRepository("AdminBundle:EmailMarketing")->find($idEmailMarketing);

        if (!$email instanceof EmailMarketing) {
            return $this->redirect($this->generateUrl("EmailMarketing"));
        }

        $entity = new Pessoa();
        $form = $this->createCreateForm($entity, $idEmailMarketing, $this->getDoctrine()->getEntityManager());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:PessoaFisica:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Pessoa Física',
                    'errors' => '',
                    'idEmailMarketing' => $idEmailMarketing,
        ));
    }

    /**
     * Finds and displays a PessoaFisica entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaFisica entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Editar", $this->get("router")->generate("Pessoa_Fisica_edit", array('id' => $id)));
        $breadcrumbs->addItem("Visualizar");

        $tags = $em->getRepository('AdminBundle:Tag')->findBy(array('status' => 1));

        return $this->render('AdminBundle:PessoaFisica:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Pessoa Física',
                    'tags' => $tags,
        ));
    }

    /**
     * Displays a form to edit an existing PessoaFisica entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaFisica entity.');
        }

        $editForm = $this->createEditForm($entity, $em);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:PessoaFisica:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Pessoa Física',
                    'errors' => '',
                    'id' => $id,
        ));
    }

    /**
     * Creates a form to edit a PessoaFisica entity.
     *
     * @param PessoaFisica $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pessoa $entity, $em, $value=1) {
        switch ($value)
        {
            case 1:
                $class = new PessoaFisicaType($em);
                break;
            case 2:
                $class = new PessoaFisicaType2($em);
                break;
        }
        $form = $this->createForm($class, $entity, array(
            'action' => $this->generateUrl('Pessoa_Fisica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('perfil_admin'),
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing PessoaFisica entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PessoaFisica entity.');
        }

        $checkForm = 1;
        $dados = $request->request->all();
        if (!empty($dados))
        {
            switch ($dados["ava_perfil"]["pais"])
            {
                case "Brasil" :
                    $checkForm = 1;
                    break;
                default :
                    $checkForm = 2;
                    break;
            }
        }
        $editForm = $this->createEditForm($entity, $em, $checkForm);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $id)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        return $this->render('AdminBundle:PessoaFisica:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Pessoa Física',
                    'errors' => $errors,
                    'id' => $id,
        ));
    }

    /**
     * Deletes a PessoaFisica entity.
     *
     */
    public function deleteAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Pessoa')->find($id);

        if (!$entity instanceof Pessoa) {
            $this->get('session')->getFlashBag()->set("msg", "<span class='fa fa-close'></span> Aluno não localizado");
            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $id)));
        }
        
        $contratos = $entity->getContrato();
        if (count($contratos) > 0) {
            $this->get('session')->getFlashBag()->set("msg", "<span class='fa fa-close'></span> O Aluno possui contratos em seu nome. Regularize a situação dos contratos e tente novamente mais tarde");
            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $id)));
        }

        $emailMarketing = $entity->getIdEmailMarketing(); 
        
        $tags = $em->getRepository('AdminBundle:PessoaTag')->findBy(array('idPessoa' => $id));

        if (count($tags) > 0) {
            foreach ($tags as $value) {
                $em->remove($value);
            }
        }

        $mensagens = $em->getRepository('AdminBundle:MensagemPessoa')->findBy(array('idPessoa' => $id));

        if (count($mensagens) > 0) {
            foreach ($mensagens as $value) {
                $em->remove($value);
            }
        }

        $nucleoPessoa = $em->getRepository('AdminBundle:NucleoPessoa')->findBy(array('idPessoa' => $id));

        if (count($nucleoPessoa) > 0) {
            foreach ($nucleoPessoa as $value) {
                $em->remove($value);
            }
        }

        $nucleo = $em->getRepository('AdminBundle:Nucleo')->findBy(array('idPessoa' => $id));

        if (count($nucleo) > 0) {
            foreach ($nucleo as $value) {
                $em->remove($value);
            }
        }

        $em->remove($entity);
        
        $emailMarketingTag = $em->getRepository('AdminBundle:EmailMarketingTag')->findBy(array('idEmailMarketing' => $emailMarketing->getId()));

        if (count($emailMarketingTag) > 0) {
            foreach ($emailMarketingTag as $value) {
                $em->remove($value);
            }
        }
        
        $mensagemEmailMarketing = $em->getRepository('AdminBundle:MensagemEmailMarketing')->findBy(array('idEmailMarketing' => $emailMarketing->getId()));

        if (count($mensagemEmailMarketing) > 0) {
            foreach ($mensagemEmailMarketing as $value) {
                $em->remove($value);
            }
        }
        
        $em->remove($emailMarketing);
        
        $em->flush();

        $this->get('session')->getFlashBag()->set("msg", "<span class='fa fa-check'></span> O Aluno e seu E-mail foram removidos com sucesso.");

        return $this->redirect($this->generateUrl('Pessoa_Fisica'));
    }

    public function AtualizaPessoaByCPFByIdEmailMarketingAction(Request $request) {
        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $pessoa = $this->returnPessoaByCPF($data["cpf"]);
        $emailMarketing = $this->returnEmailMarketingByEmail($data["email"]);

        if (!$pessoa[0] instanceof Pessoa && !$emailMarketing instanceof EmailMarketing) {
            return new Response(json_encode(array('status' => 2)), 200, array('Content-Type' => 'application/json'));
        }

        if ($data["email"] != $pessoa[0]->getIdEmailMarketing()->getId()) {
            $this->updateTagForAjax($pessoa[0]->getIdEmailMarketing());
        }

        $pessoa[0]->setIdEmailMarketing($emailMarketing);
        $pessoa[0]->setEmail($emailMarketing->getEmail());

        $em->flush();
        $em->clear();

        return new Response(json_encode(array('status' => $emailMarketing->getEmail())), 200, array('Content-Type' => 'application/json'));
    }

    public function AtualizaPessoaByCPFByIdEmailMarketingForAdminAction(Request $request) {
        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $pessoa = $this->returnPessoaByCPF($data["cpf"]);
        $emailMarketing = $this->returnEmailMarketingByEmail($data["email"]);

        if (!$pessoa[0] instanceof Pessoa && !$emailMarketing instanceof EmailMarketing) {
            return new Response(json_encode(array('status' => 2)), 200, array('Content-Type' => 'application/json'));
        }

        if ($data["email"] != $pessoa[0]->getIdEmailMarketing()->getId()) {
            $this->updateTagForAjax($pessoa[0]->getIdEmailMarketing());
        }

        $pessoa[0]->setIdEmailMarketing($emailMarketing);
        $pessoa[0]->setEmail($emailMarketing->getEmail());

        $em->flush();
        $em->clear();

        return new Response(json_encode(array('status' => $pessoa[0]->getId())), 200, array('Content-Type' => 'application/json'));
    }

    public function returnPessoaByCPF($cpf1) {
        $cpf = str_replace(".", "", $cpf1);
        $cpf = str_replace("-", "", $cpf);

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Pessoa');
        return $repository->createQueryBuilder('item')
                        ->where('item.pfCpf = :cpf1')
                        ->orWhere('item.pfCpf = :cpf2')
                        ->orderBy('item.id', 'ASC')
                        ->setParameter('cpf1', $cpf)
                        ->setParameter('cpf2', $cpf)
                        ->getQuery()
                        ->getResult();
    }

    public function returnEmailMarketingByEmail($email) {
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository("AdminBundle:EmailMarketing")->findOneBy(array("id" => $email));
    }

    public function updateTagForAjax(EmailMarketing $emailMarketing) {
        $container = $this->container->get('admin.tags');
        $container->removeEmailMarketingTag($emailMarketing, 2);
        $container->persistEmailMarketingTag($emailMarketing, 3);
    }

    public function dicaSenhaAction(Request $request) {
        $data = $request->request->all();
        $email = $data["email"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Pessoa')->findOneBy(array('email' => $email));

        if ($repository instanceof Pessoa) {
            $email = $this->container->get('admin.email');

            $email->dicaSenha($repository);

            return new Response(json_encode(array('status' => $repository->getEmail())), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('status' => 2)), 200, array('Content-Type' => 'application/json'));
    }

    public function returnFaturaByEmailAction(Request $request) {
        $data = $request->request->all();
        $email = $data["email"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AdminBundle:Pessoa')->findOneBy(array('email' => $email));

        if (!$repository instanceof Pessoa) {
            return new Response(json_encode(array('status' => 2, "msg" => "Aluno não Localizado")), 200, array('Content-Type' => 'application/json'));
        }
        $contratos = $repository->getContrato();

        if (empty($contratos)) {
            return new Response(json_encode(array('status' => 2, "msg" => "Não existe faturas para este usuário")), 200, array('Content-Type' => 'application/json'));
        }
        $result = array();
        $dia = 25;
        $mes = date('m');
        $ano = date('Y');
        $data = $dia . "/" . $mes . "/" . $ano;

        for ($i = 0; $i < count($contratos); $i++) {
            if ($contratos[$i] instanceof Contrato) {
                $preFaturas = $contratos[$i]->getPreFatura();
                if (count($preFaturas) > 0) {
                    for ($j = 0; $j < count($preFaturas); $j++) {
                        $vencimento = $preFaturas[$j]->getVencimento()->format("d/m/Y");
                        if ($vencimento == $data) {
                            $result[] = array('id' => $preFaturas[$j]->getId(), "vencimento" => $preFaturas[$j]->getVencimento()->format("d/m/Y"), "curso" => $contratos[$i]->getIdCursoOfertado()->getTitulo());
                        }
                        continue;
                    }
                }
            }
        }
        return new Response(json_encode(array('status' => 1, 'result' => $result)), 200, array('Content-Type' => 'application/json'));
    }

}
