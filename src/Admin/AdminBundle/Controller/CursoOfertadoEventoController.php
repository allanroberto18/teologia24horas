<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\CursoOfertadoEvento;
use Admin\AdminBundle\Form\CursoOfertadoEventoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * CursoOfertadoEvento controller.
 *
 */
class CursoOfertadoEventoController extends MainController {

    /**
     * Lists all CursoOfertadoEvento entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoOfertadoEvento');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.idCursoOfetado', array('label' => 'Curso Ofertado')));
        $gridConfig->addField(new Field('item.dataInicial', array('label' => 'Data Inicial')));
        $gridConfig->addField(new Field('item.dataFinal', array('label' => 'Data Final')));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CursoOfertadoEvento:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Endereço do Evento'
        ));
    }

    /**
     * Creates a new CursoOfertadoEvento entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new CursoOfertadoEvento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_Evento_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:CursoOfertadoEvento:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Endereço do Evento',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a CursoOfertadoEvento entity.
     *
     * @param CursoOfertadoEvento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoOfertadoEvento $entity) {
        $form = $this->createForm(new CursoOfertadoEventoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_Evento_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoOfertadoEvento entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new CursoOfertadoEvento();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:CursoOfertadoEvento:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Endereço do Evento',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a CursoOfertadoEvento entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoEvento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoEvento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:CursoOfertadoEvento:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Endereço do Evento'
        ));
    }

    /**
     * Displays a form to edit an existing CursoOfertadoEvento entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoEvento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoEvento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:CursoOfertadoEvento:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Endereço do Evento',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a CursoOfertadoEvento entity.
     *
     * @param CursoOfertadoEvento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoOfertadoEvento $entity) {
        $form = $this->createForm(new CursoOfertadoEventoType(), $entity, array(
            'action' => $this->generateUrl('Curso_Ofertado_Evento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoOfertadoEvento entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoOfertadoEvento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoOfertadoEvento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Ofertado_Evento_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:CursoOfertadoEvento:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Endereço do Evento',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a CursoOfertadoEvento entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoOfertadoEvento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoOfertadoEvento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Curso_Ofertado_Evento'));
    }

    /**
     * Creates a form to delete a CursoOfertadoEvento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Curso_Ofertado_Evento_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }
}
