<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\NucleoPessoa;
use Admin\AdminBundle\Form\NucleoPessoaType;
use Admin\AdminBundle\Entity\Pessoa;

/**
 * NucleoPessoa controller.
 *
 */
class NucleoPessoaController extends MainController
{

    /**
     * Creates a new Nucleo entity.
     *
     */
    public function createAction($idPessoa, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);

        if (!$pessoa instanceof Pessoa) {
            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $Nucleo = new NucleoPessoa();
        $Nucleo->setIdPessoa($pessoa);
        $form = $this->createCreateForm($Nucleo);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($Nucleo);

        if ($form->isValid() && count($errors) == 0) {
            $Nucleo->setIdPessoa($pessoa);

            $em->persist($Nucleo);

            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Sakvar");

        return $this->render('AdminBundle:NucleoPessoa:new.html.twig', array(
            'entity' => $Nucleo,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'Nucleo Aluno Ligação ',
            'errors' => $errors,
            'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a form to create a Nucleo entity.
     *
     * @param Nucleo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NucleoPessoa $entity)
    {
        $form = $this->createForm(new NucleoPessoaType(), $entity, array(
            'action' => $this->generateUrl('NucleoPessoa_create', array('idPessoa' => $entity->getIdPessoa()->getId())),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Nucleo entity.
     *
     */
    public function newAction($idPessoa)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $pessoa = $em->getRepository("AdminBundle:Pessoa")->find($idPessoa);

        if (!$pessoa instanceof Pessoa) {
            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $entity = new NucleoPessoa();
        $entity->setIdPessoa($pessoa);

        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:NucleoPessoa:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'Nucleo Aluno Ligação ',
            'errors' => '',
            'idPessoa' => $idPessoa
        ));
    }


    public function deleteAction($idPessoa, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:NucleoPessoa')->findOneBy(array('id' => $id, 'idPessoa' => $idPessoa));

        $idCurso = $entity->getIdNucleo()->getId();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Nucleo entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('Nucleo_show', array('idPessoa' => $idPessoa, 'id' => $idCurso)));
    }

    /**
     * Displays a form to edit an existing Nucleo entity.
     *
     */
    public function showAction($idPessoa, $id)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:NucleoPessoa')->findOneBy(array('id' => $id, 'idPessoa' => $idPessoa));

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Perfil do Responsável", $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $idPessoa)));
        $breadcrumbs->addItem("Remover", $this->get("router")->generate("NucleoPessoa_delete", array('idPessoa' => $idPessoa, 'id' => $id)));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:NucleoPessoa:show.html.twig', array(
            'entity' => $entity,
            'titulo' => 'Visualizar Registro',
            'modulo' => 'Nucleo Aluno Ligação ',
            'errors' => '',
        ));
    }


}
