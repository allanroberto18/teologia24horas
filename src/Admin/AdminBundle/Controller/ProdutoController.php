<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\Produto;
use Admin\AdminBundle\Form\ProdutoType;

/**
 * Produto controller.
 *
 */
class ProdutoController extends MainController {

    /**
     * Lists all Produto entities.
     *
     */
    public function indexAction(Request $request) {

        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Produto');
        $queryBuilder = $repository->createQueryBuilder('item');
        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados

        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.tipo', array(
            'label' => 'Tipo',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                switch ($value) {
                    case 1:
                        print "Apostila";
                        break;
                    case 2:
                        print "Outros";
                        break;
                }
            }
        )));
        $gridConfig->addField(new Field('item.valor', array(
            'label' => 'Preço',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                return number_format($value, 2, ',', '.');
            }
        )));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status',
            'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Excluído</span>";
                        break;
                }
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");
        
        return $this->render('AdminBundle:Produto:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Produtos',
        ));
    }

    /**
     * Creates a new Produto entity.
     *
     */
    public function createAction(Request $request) {

        $this->checkLogin();

        $entity = new Produto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Produto_show', array('id' => $entity->getId())));
        }
        
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Produto"));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:Produto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Produtos',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Produto entity.
     *
     * @param Produto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Produto $entity) {
        $form = $this->createForm(new ProdutoType(), $entity, array(
            'action' => $this->generateUrl('Produto_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Produto entity.
     *
     */
    public function newAction() {

        $this->checkLogin();

        $entity = new Produto();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Produto"));
        $breadcrumbs->addItem("Novo");
        
        return $this->render('AdminBundle:Produto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Produtos',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Produto entity.
     *
     */
    public function showAction($id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Produto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produto entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Produto"));
        $breadcrumbs->addItem("Visualizar");
        
        return $this->render('AdminBundle:Produto:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Produtos',
        ));
    }

    /**
     * Displays a form to edit an existing Produto entity.
     *
     */
    public function editAction($id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Produto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produto entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Produto"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Produto_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");
        
        return $this->render('AdminBundle:Produto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Produtos',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Produto entity.
     *
     * @param Produto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Produto $entity) {
        $form = $this->createForm(new ProdutoType(), $entity, array(
            'action' => $this->generateUrl('Produto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Produto entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Produto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Produto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();
            return $this->redirect($this->generateUrl('Produto_show', array('id' => $id)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Produto"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Produto_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");
        
        return $this->render('AdminBundle:Produto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Produtos',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Produto entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Produto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Produto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Produto'));
    }

    /**
     * Creates a form to delete a Produto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Produto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
