<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\AdminBundle\Entity\MensagemTag;
use Admin\AdminBundle\Form\MensagemTagType;
use Admin\AdminBundle\Form\Mensagem as EntityMensagem;

//use Admin\AdminBundle\Entity\Pessoa;
//use Admin\AdminBundle\Entity\Tag;
//use Admin\AdminBundle\Entity\MensagemPessoa;

/**
 * MensagemTag controller.
 *
 */
class MensagemTagController extends Controller {

    /**
     * Lists all MensagemTag entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:MensagemTag')->findAll();

        return $this->render('AdminBundle:MensagemTag:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function salvarMensagemByTagAction(Request $request) {
        $form = $request->request->all();

        $em = $this->getDoctrine()->getManager();
        $mensagem = $em->getRepository("\Admin\AdminBundle\Entity\Mensagem")->find($form['idMensagem']);

        $session = $this->get('session');

        $mensagemTag = $this->container->get('admin.emailTag');
        try {
            $result = $mensagemTag->salvarMensagemByTag($form, $mensagem);
            $count = count($result);

            if (is_array($result)) {
                for ($i = 0; $i < $count; $i++) {
                    $email = $this->container->get('admin.email');
                    if ($result[$i] instanceof \Admin\AdminBundle\Entity\EmailMarketing) {
                        $email->sendEmailMarketing($mensagem->getTitulo(), $mensagem->getConteudo(), $result[$i]);
                    } else {
                        $email->sendEmailPessoa($mensagem->getTitulo(), $mensagem->getConteudo(), $result[$i]->getEmail());;
                    }
                }
            } else {
                $session->getFlashBag()->set('msgAdmin', $result);
            }
            
            $mensagem->setStatus(1);
            
            $em->flush();
        } catch (\Exception $exc) {
            $session->getFlashBag()->set('msgAdmin', $exc->getMessage());
        }
        return $this->redirect($this->generateUrl('Mensagem_show', array('id' => $mensagem->getId())));
    }

    /**
     * Creates a new MensagemTag entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new MensagemTag();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_Tag_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:MensagemTag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a MensagemTag entity.
     *
     * @param MensagemTag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MensagemTag $entity) {
        $form = $this->createForm(new MensagemTagType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_Tag_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MensagemTag entity.
     *
     */
    public function newAction() {
        $entity = new MensagemTag();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:MensagemTag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a MensagemTag entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemTag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MensagemTag:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing MensagemTag entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemTag entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:MensagemTag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a MensagemTag entity.
     *
     * @param MensagemTag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MensagemTag $entity) {
        $form = $this->createForm(new MensagemTagType(), $entity, array(
            'action' => $this->generateUrl('Mensagem_Tag_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing MensagemTag entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:MensagemTag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MensagemTag entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('Mensagem_Tag_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:MensagemTag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a MensagemTag entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:MensagemTag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MensagemTag entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Mensagem_Tag'));
    }

    /**
     * Creates a form to delete a MensagemTag entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Mensagem_Tag_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
