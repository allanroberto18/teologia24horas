<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Aula;
use Admin\AdminBundle\Form\AulaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\Disciplina;

/**
 * Aula controller.
 *
 */
class AulaController extends MainController {

    public function checkDisciplina($idDisciplina) {
        if (empty($idDisciplina)) {
            return $this->redirect($this->generateUrl('Curso'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("AdminBundle:Disciplina")->find($idDisciplina);
        return $entity;
    }

    /**
     * Lists all Aula entities.
     *
     */
    public function indexAction(Request $request, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $repository = $this->getDoctrine()->getManager();
        $queryBuilder = $repository->createQueryBuilder()
                ->select('item, disciplina')
                ->from('AdminBundle:Aula', 'item')
                ->leftJoin('item.idDisciplina', 'disciplina');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('disciplina.titulo', array('label' => 'Disciplina', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Listar Aulas");

        return $this->render('AdminBundle:Aula:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Aula',
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
        ));
    }

    /**
     * Creates a new Aula entity.
     *
     */
    public function createAction(Request $request, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new Aula();
        $form = $this->createCreateForm($entity, $idCurso, $idDisciplina);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $entity->setIdDisciplina($disciplina);

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Disciplina_show', array('idCurso' => $idCurso, "id" => $idDisciplina)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Salvar Aula");

        return $this->render('AdminBundle:Aula:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Aula',
                    'errors' => $errors,
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
        ));
    }

    /**
     * Creates a form to create a Aula entity.
     *
     * @param Aula $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Aula $entity, $idCurso, $idDisciplina) {
        $form = $this->createForm(new AulaType(), $entity, array(
            'action' => $this->generateUrl('Disciplina_Aula_create', array('idCurso' => $idCurso, 'idDisciplina' => $idDisciplina)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Aula entity.
     *
     */
    public function newAction($idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $entity = new Aula();
        $form = $this->createCreateForm($entity, $idCurso, $idDisciplina);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Nova Aula");

        return $this->render('AdminBundle:Aula:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Aula',
                    'errors' => '',
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
        ));
    }

    /**
     * Finds and displays a Aula entity.
     *
     */
    public function showAction($id, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Aula')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aula entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Aula");

        return $this->render('AdminBundle:Aula:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Aula',
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
                    'modulo' => ['titulo' => 'Visualizar Aula', 'descricao' => '']
        ));
    }

    /**
     * Displays a form to edit an existing Aula entity.
     *
     */
    public function editAction($id, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Aula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aula entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso, $idDisciplina);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Aula", $this->get("router")->generate("Disciplina_show", array('id' => $id, 'idDisciplina' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:Aula:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Atualizar Registro',
                    'modulo' => 'Aula',
                    'errors' => '',
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
        ));
    }

    /**
     * Creates a form to edit a Aula entity.
     *
     * @param Aula $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Aula $entity, $idCurso, $idDisciplina) {
        $form = $this->createForm(new AulaType(), $entity, array(
            'action' => $this->generateUrl('Disciplina_Aula_update', array('idCurso' => $idCurso, 'idDisciplina' => $idDisciplina, 'id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Aula entity.
     *
     */
    public function updateAction(Request $request, $id, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Aula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Aula entity.');
        }

        $editForm = $this->createEditForm($entity, $idCurso, $idDisciplina);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {

            $entity->setIdDisciplina($disciplina);

            $em->flush();

            return $this->redirect($this->generateUrl('Disciplina_show', array('idCurso' => $idCurso, "id" => $idDisciplina)));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Curso", $this->get("router")->generate("Curso_show", array('id' => $idCurso)));
        $breadcrumbs->addItem("Disciplina", $this->get("router")->generate("Disciplina_show", array('id' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Visualizar Aula", $this->get("router")->generate("Disciplina_show", array('id' => $id, 'idDisciplina' => $idDisciplina, 'idCurso' => $idCurso)));
        $breadcrumbs->addItem("Alterar");

        return $this->render('AdminBundle:Aula:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Atualizar Registro',
                    'modulo' => 'Aula',
                    'errors' => $errors,
                    'idCurso' => $idCurso,
                    'idDisciplina' => $idDisciplina,
        ));
    }

    /**
     * Deletes a Aula entity.
     *
     */
    public function deleteAction($id, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Aula')->find($id);

        if (!$entity && !$entity instanceof Aula) {
            throw $this->createNotFoundException('Unable to find Aula entity.');
        }
        $entity->setStatus(2);
        $em->flush();

        return $this->redirect($this->generateUrl('Disciplina_show', array('idCurso' => $idCurso, "id" => $idDisciplina)));
    }

    /**
     * Recuperando a Aula entity.
     *
     */
    public function restaurarAction($id, $idCurso, $idDisciplina) {
        $this->checkLogin();

        $disciplina = $this->checkDisciplina($idDisciplina);
        if (!$disciplina instanceof Disciplina && $disciplina->getIdCurso()->getId() != $idCurso) {
            return $this->redirect($this->generateUrl('Curso'));
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:Aula')->find($id);

        if (!$entity && !$entity instanceof Aula) {
            throw $this->createNotFoundException('Unable to find Aula entity.');
        }
        $entity->setStatus(1);
        $em->flush();

        return $this->redirect($this->generateUrl('Disciplina_show', array('idCurso' => $idCurso, "id" => $idDisciplina)));
    }

}
