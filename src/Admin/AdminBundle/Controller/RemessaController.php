<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;
use Admin\AdminBundle\Entity\Remessa;
use Admin\AdminBundle\Entity\Fatura;
use Umbrella\Ya\RetornoBoleto\ProcessFactory;
use Umbrella\Ya\RetornoBoleto\ProcessHandler;
use Admin\AdminBundle\Form\RemessaType;

/**
 * Remessa controller.
 *
 */
class RemessaController extends MainController {

    /**
     * Lists all Remessa entities.
     *
     */
    public function indexAction(Request $request) {

        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Remessa');
        $queryBuilder = $repository->createQueryBuilder('item')->orderBy('item.id', 'DESC');
        $gridConfig = new GridConfig();

        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.datahoraCadastro', array(
            'label' => 'Data da Remessa',
            'sortable' => true,
            'formatValueCallback' => function($value) {
                return $value->format("d/m/Y H:i:s");
            }
        )));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Excluído</span>";
                        break;
                };
            })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:Remessa:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Remessa Bancária',
        ));
    }

    /**
     * Creates a new Remessa entity.
     *
     */
    public function createAction(Request $request) {

        $this->checkLogin();

        $entity = new Remessa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Remessa_Bancaria"));
        $breadcrumbs->addItem("Salvar");

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Remessa_Bancaria_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Remessa:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Remessa Bancária',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Remessa entity.
     *
     * @param Remessa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Remessa $entity) {
        $form = $this->createForm(new RemessaType(), $entity, array(
            'action' => $this->generateUrl('Remessa_Bancaria_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Remessa entity.
     *
     */
    public function newAction() {

        $this->checkLogin();

        $entity = new Remessa();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Remessa_Bancaria"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:Remessa:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Remessa Bancária',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Remessa entity.
     *
     */
    public function showAction($id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Remessa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Remessa entity.');
        }

        $boletos = $this->returnBoletos($entity->getArquivo());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Remessa_Bancaria"));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:Remessa:show.html.twig', array(
                    'entity' => $entity,
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Remessa Bancária',
                    'boleto' => $boletos,
        ));
    }

    public function returnBoletos($arquivo) {
        if (empty($arquivo)) {
            return false;
        }

        $cnab = ProcessFactory::getRetorno("files/remessa/" . $arquivo);
        $processor = new ProcessHandler($cnab);
        $retorno = $processor->processar();

        $lotes = $retorno->getLotes()[0]->getDetails();

        $boletos = array();
        $j = 0;
        for ($i = 0; $i < count($lotes); $i++) {
            if ($i % 2 == 0) {
                $indice = $i + 1;
                $boletos[$j] = $this->processarFatura(intval(substr(trim($lotes[$i]->getNossoNumero()), 2, -1)), $lotes[$indice]->getDataCredito()->format("Y-m-d H:i:s"), $lotes[$indice]->getDadosTitulo()->getValorPago());
                $j++;
            }
        }
        return $boletos;
    }

    public function processarFatura($idFatura, $dataPagamento, $valor) {
        $em = $this->getDoctrine()->getManager();

        $fatura = $em->getRepository("AdminBundle:Fatura")->find($idFatura);
        if (!$fatura instanceof Fatura) {
            return false;
        }

        if ($fatura->getStatus() == 1) {
            $fatura->setPagamentoData(new \DateTime($dataPagamento));
            $fatura->setPagamentoValor($valor);
            $fatura->setStatus(2);

            $contrato = $fatura->getIdContrato();
            $contrato->setStatus(1);

            $email = $this->container->get('admin.email');
            $email->confirmacaoPagamento($contrato, $fatura);
            
            $em->flush();
        }
        return $fatura;
    }

    /**
     * Displays a form to edit an existing Remessa entity.
     *
     */
    public function editAction($id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Remessa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Remessa entity.');
        }

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Remessa_Bancaria"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Remessa_Bancaria_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:Remessa:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Remessa Bancária',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Remessa entity.
     *
     * @param Remessa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Remessa $entity) {
        $form = $this->createForm(new RemessaType(), $entity, array(
            'action' => $this->generateUrl('Remessa_Bancaria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Remessa entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Remessa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Remessa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Remessa_Bancaria"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Remessa_Bancaria_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        if ($editForm->isValid() && count($errors) == 0) {

            $em->flush();

            return $this->redirect($this->generateUrl('Remessa_Bancaria_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:Remessa:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Remessa Bancária',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Remessa entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Remessa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Remessa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Remessa'));
    }

    /**
     * Creates a form to delete a Remessa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Remessa_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
