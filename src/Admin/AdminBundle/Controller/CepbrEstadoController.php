<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * CepbrEstado controller.
 *
 */
class CepbrEstadoController extends MainController
{

    /**
     * Lists all CepbrEstado entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->checkLogin();
        
        $repository = $this->getDoctrine()->getRepository('AdminBundle:CepbrEstado');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.uf', array('label' => 'UF', "sortable" => true)));
        $gridConfig->addField(new Field('item.estado', array('label' => 'Estado', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.codIbge', array('label' => 'Cod. IBGE', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:CepbrEstado:index.html.twig', array(
            'grid' => $grid,
            'titulo' => 'Listar Registros',
            'modulo' => 'Estado'
        ));
    }

    /**
     * Finds and displays a CepbrEstado entity.
     *
     */
    public function showAction($id)
    {
        $this->checkLogin();
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CepbrEstado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CepbrEstado entity.');
        }

        return $this->render('AdminBundle:CepbrEstado:show.html.twig', array(
            'entity'      => $entity,
            'titulo' => 'Visualizar Registro',
            'modulo' => 'Estado'
        ));
    }
}
