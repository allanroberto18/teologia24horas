<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\Contrato;
use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\ContratoNota;
use Admin\AdminBundle\Form\ContratoNotaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * ContratoNota controller.
 *
 */
class ContratoNotaController extends MainController
{

    public function checkContrato($idContrato)
    {
        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository("AdminBundle:Contrato")->find($idContrato);

        if (!$contrato instanceof Contrato) {
            $sessionController = $this->get('session');
            $this->addFlash('info', 'Contrato não localizado');

            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }
        return $contrato;
    }

    /**
     * Lists all ContratoNota entities.
     *
     */
    public function indexAction(Request $request, $idContrato)
    {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:ContratoNota');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.media', array('label' => 'Média', "sortable" => true)));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
            switch ($value) {
                case 1:
                    print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                    break;
                case 2:
                    print "<span class='label label-danger'><span class='fa fa-close'></span> Inativo</span>";
                    break;
            };
        })));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:ContratoNota:index.html.twig', array(
            'grid' => $grid,
            'titulo' => 'Listar Registros',
            'modulo' => 'ContratoNota'
        ));
    }

    /**
     * Creates a new ContratoNota entity.
     *
     */
    public function createAction(Request $request, $idContrato)
    {

        $contrato = $this->checkContrato($idContrato);

        $pessoa = $contrato->getIdPessoa();

        $entity = new ContratoNota();
        $entity->setIdContrato($contrato);

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $contrato->getIdPessoa()->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem de Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $pessoa->getId())));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:ContratoNota:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'ContratoNota',
            'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a ContratoNota entity.
     *
     * @param ContratoNota $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ContratoNota $entity)
    {
        $form = $this->createForm(new ContratoNotaType(), $entity, array(
            'action' => $this->generateUrl('ContratoNota_create', array('idContrato' => $entity->getIdContrato()->getId())),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new ContratoNota entity.
     *
     */
    public function newAction($idContrato)
    {
        $this->checkLogin();

        $contrato = $this->checkContrato($idContrato);

        $entity = new ContratoNota();
        $entity->setIdContrato($contrato);

        $form = $this->createCreateForm($entity);

        $pessoa = $contrato->getIdPessoa();

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem de Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $pessoa->getId())));
        $breadcrumbs->addItem("Nova Nota");

        return $this->render('AdminBundle:ContratoNota:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'titulo' => 'Novo Registro',
            'modulo' => 'Nota do Aluno',
            'errors' => '',
        ));
    }

    /**
     * Finds and displays a ContratoNota entity.
     *
     */
    public function showAction($id, $idContrato)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:ContratoNota')->find($id);

        if (!$entity && !$entity instanceof ContratoNota) {
            throw $this->createNotFoundException('Unable to find ContratoNota entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("ContratoNota", array('idContrato' => $idContrato)));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:ContratoNota:show.html.twig', array(
            'entity' => $entity,
            'titulo' => 'Visualizar Registro',
            'modulo' => 'ContratoNota',
        ));
    }

    /**
     * Displays a form to edit an existing ContratoNota entity.
     *
     */
    public function editAction($id, $idContrato)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:ContratoNota')->findOneBy(array('id' => $id, 'idContrato' => $idContrato));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContratoNota entity.');
        }
        $pessoa = $entity->getIdContrato()->getIdPessoa();

        $editForm = $this->createEditForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem de Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $pessoa->getId())));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:ContratoNota:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'ContratoNota',
            'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a ContratoNota entity.
     *
     * @param ContratoNota $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ContratoNota $entity)
    {
        $form = $this->createForm(new ContratoNotaType(), $entity, array(
            'action' => $this->generateUrl('ContratoNota_update', array('id' => $entity->getId(), 'idContrato' => $entity->getIdContrato()->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing ContratoNota entity.
     *
     */
    public function updateAction(Request $request, $id, $idContrato)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:ContratoNota')->findOneBy(array('id' => $id, 'idContrato' => $idContrato));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContratoNota entity.');
        }

        $pessoa = $entity->getIdContrato()->getIdPessoa();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem de Pessoas", $this->get("router")->generate("Pessoa_Fisica"));
        $breadcrumbs->addItem("Aluno: " . $pessoa->getPfNome(), $this->get("router")->generate("Pessoa_Fisica_show", array('id' => $pessoa->getId())));
        $breadcrumbs->addItem("Atualizar");

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $entity->getIdContrato()->getIdPessoa()->getId())));
        }

        return $this->render('AdminBundle:ContratoNota:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'titulo' => 'Alterar Registro',
            'modulo' => 'ContratoNota',
            'errors' => $errors,
        ));
    }

    /**
     * Deletes a Usuario entity.
     *
     */
    public function deleteAction($id, $idContrato)
    {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AdminBundle:ContratoNota')->findOneBy(array('id' => $id, 'idContrato' => $idContrato));

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $entity->getIdContrato()->getIdPessoa()->getId())));
    }
}
