<?php

namespace Admin\AdminBundle\Controller;

use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\ContratoItem;
use Admin\AdminBundle\Form\ContratoType;
use Admin\AdminBundle\Form\ContratoEditType;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\PessoaEndereco;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\lib\CalculaFrete;

/**
 * Contrato controller.
 *
 */
class ContratoController extends MainController {

    /**
     * Creates a new Contrato entity.
     *
     */
    public function createAction($idPessoa, Request $request) {

        $contrato = new Contrato();
        $form = $this->createCreateForm($idPessoa, $contrato);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($contrato);

        $dataVencimento = new \DateTime(date("Y-m-d"));
        $dataVencimento->modify('+5 days');

        $session = $this->get('session');
        if ($form->isValid() && count($errors) == 0) {
            $data = $form->getData();

            $idCurso = $data->getIdCurso()->getId();
            $dataContrato = $data->getDataContratacao();
            try {
                $contratar = $this->container->get('admin.contratacao');
                $contratar->persistContrato($idPessoa, $idCurso, $dataContrato);
            } catch (\Exception $exc) {
                $session->getFlashBag()->set('msgAdmin', $exc->getMessage());
            }

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $idPessoa)));
        }

        return $this->render('AdminBundle:Contrato:new.html.twig', array(
                    'entity' => $contrato,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Contrato',
                    'errors' => $errors,
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Creates a form to create a Contrato entity.
     *
     * @param Contrato $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($idPessoa, Contrato $entity) {
        $form = $this->createForm(new ContratoType(), $entity, array(
            'action' => $this->generateUrl('Contrato_create', array('idPessoa' => $idPessoa)),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Contrato entity.
     *
     */
    public function newAction($idPessoa) {
        $this->checkLogin();

        $entity = new Contrato();
        $form = $this->createCreateForm($idPessoa, $entity);

        return $this->render('AdminBundle:Contrato:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Contrato',
                    'errors' => '',
                    'idPessoa' => $idPessoa
        ));
    }

    /**
     * Displays a form to edit an existing Contrato entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Contrato')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contrato entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('AdminBundle:Contrato:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Contrato',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Contrato entity.
     *
     * @param Contrato $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Contrato $entity) {
        $form = $this->createForm(new ContratoEditType(), $entity, array(
            'action' => $this->generateUrl('Contrato_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Contrato entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Contrato')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contrato entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $entity->getIdPessoa()->getId())));
        }

        return $this->render('AdminBundle:Contrato:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Contrato',
                    'errors' => $errors,
        ));
    }

    public function atualizarFaturaAction($idPessoa, $idFatura, $tipo) {
        $em = $this->getDoctrine()->getManager();

        $fatura = $em->getRepository("AdminBundle:Fatura")->find($idFatura);

        if (!$fatura instanceof Fatura) {
            return false;
        }

        $contrato = $fatura->getIdContrato();
        $curso = $contrato->getIdCurso();
        $item = $em->getRepository("AdminBundle:CursoItem")->findBy(array('idCurso' => $curso->getId()));

        $tabelaPrecos = $curso->getIdTabelaPreco();
        $itemContrato = $contrato->getItemContrato();

        switch ($tipo) {
            case 1:
                if (count($itemContrato) > 0) {
                    continue;
                }

                if (count($item) > 0) {
                    $novoItem = new ContratoItem();
                    $novoItem->setIdContrato($contrato);
                    $novoItem->setIdCursoItem($item[0]);
                    $novoItem->setStatus(1);

                    $em->persist($novoItem);
                }
                $em->flush();
                $fatura->setValor($this->valorFatura($contrato));

                break;
            case 2:
                if (count($itemContrato) > 0) {
                    foreach ($itemContrato as $item) {
                        $em->remove($item);
                    }
                }
                $fatura->setValor($tabelaPrecos->getValor());
                break;
        }
        $em->flush();

        return $this->redirect($this->generateUrl("Pessoa_Fisica_show", array('id' => $idPessoa)));
    }

    public function valorFatura(Contrato $contrato) {
        $pessoa = $contrato->getIdPessoa();
        $endereco = $pessoa->getEndereco();
        $curso = $contrato->getIdCurso();
        $frete = 0;
        $itensContrato = $contrato->getItemContrato();
        if ($itensContrato) {
            for ($i = 0; $i < count($itensContrato); $i++) {
                $frete += $this->valorFrete($endereco) + $itensContrato[$i]->getIdCursoItem()->getValor();
            }
        }
        return $curso->getIdTabelaPreco()->getValor() + $frete;
    }

    public function valorFrete(PessoaEndereco $endereco) {
        return CalculaFrete::calcular($endereco->getCep());
    }

    public function gerarContrato(Pessoa $pessoa, Curso $curso) {
        $em = $this->getDoctrine()->getManager();

        $checkContrato = $em->getRepository('AdminBundle:Contrato')->findBy(array('idCurso' => $curso->getId(), 'idPessoa' => $pessoa->getId()));
        if (count($checkContrato) > 0) {
            return false;
        }

        $contrato = new Contrato();
        $contrato->setIdPessoa($pessoa);
        $contrato->setIdCurso($curso);
        $contrato->setStatus(2);

        $em->persist($contrato);

        $tag = $em->getRepository("AdminBundle:Tag")->findOneBy(array('titulo' => $curso->getTitulo()));

        $emailTag = new \Admin\AdminBundle\Entity\EmailMarketingTag();
        $emailTag->setIdTag($tag);
        $emailTag->setIdEmailMarketing($pessoa->getIdEmailMarketing());
        $emailTag->setStatus(1);

        $em->persist($emailTag);

        $dataVencimento = new \DateTime(date("Y-m-d"));
        $dataVencimento->modify('+5 days');

        $fatura = new Fatura();
        $fatura->setIdContrato($contrato);
        $fatura->setValor($curso->getIdTabelaPreco()->getValor());
        $fatura->setPagamentoValor(null);
        $fatura->setVencimento($dataVencimento);
        $fatura->getStatus(1);

        $em->persist($fatura);

        $em->flush();

        return $contrato->getId();
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository('AdminBundle:Contrato')->find($id);

        $sessionController = $this->get('session');
        if (!$contrato instanceof Contrato) {
            $this->addFlash('info', 'Contrato não localizado');

            return $this->redirect($this->generateUrl('Pessoa_Fisica'));
        }

        $curso = $contrato->getIdCurso();
        $pessoa = $contrato->getIdPessoa();

        $serviceTag = $this->container->get('admin.tags');
        $serviceTag->removePessoaTag($pessoa, $curso->getTitulo());

        $faturas = $em->getRepository('AdminBundle:Fatura')->findBy(array('idContrato' => $id));
        if (count($faturas) > 0) {
            foreach ($faturas as $fatura) {
                $em->remove($fatura);
            }
        }

        $contratoItem = $em->getRepository('AdminBundle:ContratoItem')->findBy(array('idContrato' => $contrato->getId()));
        if (count($contratoItem) > 0) {
            foreach ($contratoItem as $item) {
                $em->remove($item);
            }
        }

        $em->remove($contrato);
        $em->flush();

        $this->addFlash('info', 'Contrato excluído com sucesso');

        return $this->redirect($this->generateUrl('Pessoa_Fisica_show', array('id' => $pessoa->getId())));
    }

}
