<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\CursoCategoria;
use Admin\AdminBundle\Form\CursoCategoriaType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * CursoCategoria controller.
 *
 */
class CursoCategoriaController extends MainController {

    /**
     * Lists all CursoCategoria entities.
     *
     */
    public function indexAction(Request $request) {
        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:CursoCategoria');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.status', array('label' => 'Status', 'formatValueCallback' => function ($value) {
                switch ($value) {
                    case 1:
                        print "<span class='label label-info'><span class='fa fa-check'></span> Ativo</span>";
                        break;
                    case 2:
                        print "<span class='label label-danger'><span class='fa fa-close'></span> Excluído</span>";
                        break;
                };
            })));

        $gridConfig->addSelector(array('label' => 'Ativo', 'field' => 'item.status', 'value' => '1'));
        $gridConfig->addSelector(array('label' => 'Inativo', 'field' => 'item.status', 'value' => '2'));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem");

        return $this->render('AdminBundle:CursoCategoria:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Categoria do Curso'
        ));
    }

    /**
     * Creates a new CursoCategoria entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new CursoCategoria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Categoria_show', array('id' => $entity->getId())));
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso_Categoria"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Curso_Categoria_show", array('id' => $id)));
        $breadcrumbs->addItem("Salvar");

        return $this->render('AdminBundle:CursoCategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Categoria do Curso',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a CursoCategoria entity.
     *
     * @param CursoCategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CursoCategoria $entity) {
        $form = $this->createForm(new CursoCategoriaType(), $entity, array(
            'action' => $this->generateUrl('Curso_Categoria_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new CursoCategoria entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new CursoCategoria();
        $form = $this->createCreateForm($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso_Categoria"));
        $breadcrumbs->addItem("Novo");

        return $this->render('AdminBundle:CursoCategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Categoria do Curso',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a CursoCategoria entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoCategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoCategoria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso_Categoria"));
        $breadcrumbs->addItem("Visualizar");

        return $this->render('AdminBundle:CursoCategoria:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Categoria do Curso'
        ));
    }

    /**
     * Displays a form to edit an existing CursoCategoria entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoCategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoCategoria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso_Categoria"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Curso_Categoria_show", array('id' => $id)));
        $breadcrumbs->addItem("Editar");

        return $this->render('AdminBundle:CursoCategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Categoria do Curso',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a CursoCategoria entity.
     *
     * @param CursoCategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CursoCategoria $entity) {
        $form = $this->createForm(new CursoCategoriaType(), $entity, array(
            'action' => $this->generateUrl('Curso_Categoria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing CursoCategoria entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:CursoCategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CursoCategoria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Principal", $this->get("router")->generate("admin_home"));
        $breadcrumbs->addItem("Listagem", $this->get("router")->generate("Curso_Categoria"));
        $breadcrumbs->addItem("Visualizar", $this->get("router")->generate("Curso_Categoria_show", array('id' => $id)));
        $breadcrumbs->addItem("Atualizar");

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Curso_Categoria_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:CursoCategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Categoria do Curso',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a CursoCategoria entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:CursoCategoria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CursoCategoria entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Curso_Categoria'));
    }

    /**
     * Creates a form to delete a CursoCategoria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Curso_Categoria_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
