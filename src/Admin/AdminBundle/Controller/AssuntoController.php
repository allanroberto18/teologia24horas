<?php

namespace Admin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Admin\AdminBundle\Entity\Assunto;
use Admin\AdminBundle\Form\AssuntoType;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Assunto controller.
 *
 */
class AssuntoController extends MainController {

    /**
     * Lists all Assunto entities.
     *
     */
    public function indexAction(Request $request) {

        $this->checkLogin();

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Assunto');
        $queryBuilder = $repository->createQueryBuilder('item');

        $gridConfig = new GridConfig();
        // Passagem de dados
        $gridConfig->setQueryBuilder($queryBuilder);

        // Contagem de registros
        $gridConfig->setCountFieldName('item.id');

        // Campos Consultados
        $gridConfig->addField(new Field('item.id', array('label' => 'Código', "sortable" => true)));
        $gridConfig->addField(new Field('item.titulo', array('label' => 'Título', "sortable" => true, "filterable" => true,)));
        $gridConfig->addField(new Field('item.email', array('label' => 'E-mail', "sortable" => true, "filterable" => true,)));

        $gridManager = $this->get('kitpages_data_grid.grid_manager');
        $grid = $gridManager->getGrid($gridConfig, $this->getRequest());

        return $this->render('AdminBundle:Assunto:index.html.twig', array(
                    'grid' => $grid,
                    'titulo' => 'Listar Registros',
                    'modulo' => 'Assunto das Mensagens'
        ));
    }

    /**
     * Creates a new Assunto entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Assunto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($form->isValid() && count($errors) == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('Assunto_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Assunto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Assunto das Mensagens',
                    'errors' => $errors,
        ));
    }

    /**
     * Creates a form to create a Assunto entity.
     *
     * @param Assunto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Assunto $entity) {
        $form = $this->createForm(new AssuntoType(), $entity, array(
            'action' => $this->generateUrl('Assunto_create'),
            'method' => 'POST',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Assunto entity.
     *
     */
    public function newAction() {
        $this->checkLogin();

        $entity = new Assunto();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Assunto:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'titulo' => 'Novo Registro',
                    'modulo' => 'Assunto das Mensagens',
                    'errors' => '',
        ));
    }

    /**
     * Finds and displays a Assunto entity.
     *
     */
    public function showAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Assunto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assunto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Assunto:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Visualizar Registro',
                    'modulo' => 'Assunto das Mensagens',
        ));
    }

    /**
     * Displays a form to edit an existing Assunto entity.
     *
     */
    public function editAction($id) {
        $this->checkLogin();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Assunto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assunto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Assunto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Assunto das Mensagens',
                    'errors' => '',
        ));
    }

    /**
     * Creates a form to edit a Assunto entity.
     *
     * @param Assunto $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Assunto $entity) {
        $form = $this->createForm(new AssuntoType(), $entity, array(
            'action' => $this->generateUrl('Assunto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'form'
            )
        ));

        $form->add('submit', 'submit', array('label' => "Salvar", 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Assunto entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Assunto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Assunto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if ($editForm->isValid() && count($errors) == 0) {
            $em->flush();

            return $this->redirect($this->generateUrl('Assunto_show', array('id' => $id)));
        }

        return $this->render('AdminBundle:Assunto:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'titulo' => 'Alterar Registro',
                    'modulo' => 'Assunto das Mensagens',
                    'errors' => $errors,
        ));
    }

    /**
     * Deletes a Assunto entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $this->checkLogin();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Assunto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Assunto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('Assunto'));
    }

    /**
     * Creates a form to delete a Assunto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('Assunto_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }
}
