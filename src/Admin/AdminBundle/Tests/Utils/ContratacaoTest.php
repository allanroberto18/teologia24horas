<?php

namespace Admin\AdminBundle\Tests\Utils;

use Admin\Utils\Contratacao;

class ContratacaoTest extends \PHPUnit_Framework_TestCase {

    public function getEntityManager() {
        
    }

    public function testGetPessoa() {

        $idPessoa = 1;
        $pfNome = "Allan Roberto";
        
        // Objeto que será trabalhado
        $pessoa = $this->getMock('\Admin\AdminBundle\Entity\Pessoa');

        // Repositório que irá retornar o objeto
        $repository = $this
                ->getMockBuilder('\Doctrine\ORM\EntityRepository')
                ->disableOriginalConstructor()
                ->setMethods(array('find'))
                ->getMock();

        $repository->expects($this->once())
                ->method('find')
                ->will($this->returnValue($pessoa));

        // Entity Manager para executar a consulta
        $entityManager = $this
                ->getMockBuilder('\Doctrine\ORM\EntityManager')
                ->setMethods(array('getRepository'))
                ->disableOriginalConstructor()
                ->getMock();
        $entityManager->expects($this->once())
                ->method('getRepository')
                ->with('\Admin\AdminBundle\Entity\Pessoa')
                ->will($this->returnValue($repository));

        $util = new Contratacao($entityManager);
        $result = $util->getPessoa($idPessoa);

        $this->assertNull($result->getPfNome(), 'Aluno Não Localizado');
        $this->assertInstanceOf('\Admin\AdminBundle\Entity\Pessoa', $result);
        $this->assertContains($pfNome, array($result->getPfNome()));
        //$this->assertEquals($pfNome, $result->getPfNome());
    }

}
