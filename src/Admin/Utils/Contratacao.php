<?php

/**
 * Created by PhpStorm.
 * User: allan
 * Date: 06/05/15
 * Time: 10:14
 */

namespace Admin\Utils;

use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\ContratoItem;
use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\PessoaEndereco;
use Admin\AdminBundle\Entity\TabelaPreco;

class Contratacao {

    protected $mailer;
    protected $templating;
    protected $entityManager;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $entityManager) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * getPessoa
     * @param $idPessoa
     * @return Pessoa|bool
     */
    public function getPessoa($idPessoa) {
        $pessoa = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pessoa')->find($idPessoa);
        return ($pessoa instanceof Pessoa) ? $pessoa : false;
    }

    /**
     * getCurso
     * @param $idCurso
     * @return Curso|bool
     */
    public function getCurso($idCurso) {
        $curso = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Curso')->find($idCurso);
        return ($curso instanceof Curso) ? $curso : false;
    }

    /**
     * getItensCurso
     * @param array $itensCurso
     * @return Array of Objects CursoItem
     */
    public function getItensCurso(array $itensCurso = null) {
        if ($itensCurso = null && !array_key_exists('item', $itensCurso))
            ;
        return false;

        $result = array();
        for ($i = 0; $i < count($itensCurso); $i++) {
            $result[] = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\CursoItem')->find($itensCurso['item'][$i]);
        }
        return $item;
    }

    /**
     * getContrato
     * @param $idPessoa
     * @param $idCurso
     * @return Contrato
     */
    public function getContrato($idPessoa, $idCurso) {
        return $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Contrato')->findOneBy(array('idPessoa' => $idPessoa, 'idCurso' => $idCurso));
    }

    public function getContrato2($idPessoa) {
        return $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Contrato')->findOneBy(array('idPessoa' => $idPessoa));
    }

    /**
     * checkFaturaEmAberto
     * @param $idContrato
     * @return Fatura
     */
    public function checkFaturaEmAberto($idContrato) {
        return $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Fatura')->findOneBy(array('idContrato' => $idContrato, 'status' => 1));
    }

    /**
     * persistContrato
     * @param $idPessoa
     * @param $idCurso
     * @param array $itensCurso selecionados
     * @return bool
     */
    public function persistContrato($idPessoa, $idCurso, $dataContrato = null) {
        $pessoa = $this->entityManager->getRepository("\Admin\AdminBundle\Entity\Pessoa")->find($idPessoa);
        if (!$pessoa instanceof Pessoa) {
            throw new Exception("Não foi possível localizar o aluno");
        }
        $curso = $this->getCurso($idCurso);
        if (!$curso instanceof Curso) {
            throw new Exception("Não foi possível localizar o curso");
        }
        $check = $this->getContrato($idPessoa, $idCurso);
        $validacao = $this->validarContratacao($pessoa, $curso, $check);
        $status = ($idCurso == 1) ? 1 : 2;

        if (!empty($validacao)) {
            return $validacao;
        }
        if ($check instanceof Contrato) {
            return false;
        }

        $contrato = new Contrato();
        $contrato->setIdPessoa($pessoa);
        $contrato->setIdCurso($curso);
        if ($dataContrato instanceof \DateTime) {
            $contrato->setDataContratacao($dataContrato);
        }
        $contrato->setStatus($status);

        $this->entityManager->persist($contrato);
        $this->entityManager->flush();
        
        $email = new Email($this->mailer, $this->templating, $this->entityManager);

        switch ($idCurso) {
            case 1:
                $email->perfilCadastrado($pessoa);
                break;

            default:
                $email->corfirmacaoContrato($pessoa, $curso);
                break;
        }
        
        return $this->persistFatura($contrato->getId(), $curso);
    }

    /**
     * persistItensContrato
     * @param $contrato
     * @param $itensCurso
     * @return bool
     */
    public function persistItensContrato(Contrato $contrato, $itensCurso) {
        $itens = $this->getItensCurso($itensCurso);
        $itensContratados = array();
        if (empty($itens)) {
            return true;
        }
        foreach ($itens as $item) {
            $contratoItem = new ContratoItem();
            $contratoItem->setIdContrato($contrato);
            $contratoItem->setIdCursoItem($item);
            $contratoItem->setStatus(1);

            $this->entityManager->persist($contratoItem);

            $itensContratados[] = $contratoItem;
        }
        $this->entityManager->flush();

        return true;
    }

    /**
     * persistFatura
     * @param Contrato $contrato
     * @param PessoaEndereco $endereco
     * @param TabelaPreco $tbPreco
     * @param $itensCurso
     * @return bool
     */
    public function persistFatura($idContrato, Curso $curso) {
        $tbPreco = $curso->getIdTabelaPreco();
        $check = $this->checkFaturaEmAberto($idContrato);
        if ($check instanceof Fatura) {
            return $this->updateFatura($check, $tbPreco);
        }

        $contrato = $this->entityManager->getRepository("\Admin\AdminBundle\Entity\Contrato")->find($idContrato);
        if (!$contrato instanceof Contrato) {
            throw new Exception("Contrato não localizado");
        }
        $dataVencimento = new \DateTime(date("Y-m-d"));
        $dataVencimento->modify('+5 days');

        $valorPago = null;
        $status = 1;
        $dataPagamento = null;

        if ($curso->getId() == 1) {
            $valorPago = 0;
            $status = 2;
            $dataPagamento = $dataVencimento;

            $tag = new Tags($this->entityManager);
            $tag->persistPessoaTag($contrato->getIdPessoa()->getId(), "", $curso->getTitulo());
        }

        $fatura = new Fatura();
        $fatura->setIdContrato($contrato);
        $fatura->setValor($tbPreco->getValor());
        $fatura->setPagamentoValor($valorPago);
        $fatura->setVencimento($dataVencimento);
        $fatura->setPagamentoData($dataPagamento);
        $fatura->getStatus($status);

        $this->entityManager->persist($fatura);
        $this->entityManager->flush();

        return $fatura;
    }

    /**
     * updateFatura
     * @param Contrato $contrato
     * @param Fatura $fatura
     * @param PessoaEndereco $endereco
     * @param TabelaPreco $tbPreco
     * @param $itensCurso
     * @return bool
     */
    public function updateFatura(Fatura $fatura, TabelaPreco $tbPreco) {

        $fatura->setValor($tbPreco->getValor());

        $this->entityManager->flush();

        return true;
    }

    /**
     * valorFatura
     * @param PessoaEndereco $endereco
     * @param TabelaPreco $tbPreco
     * @param $itens
     * @return float
     */
    public function valorFatura(PessoaEndereco $endereco, TabelaPreco $tbPreco, $itens) {
        if ($tbPreco->getValor() == 0)
            return 0;

        $frete = 0;
        if (!empty($itens)) {
            for ($i = 0; $i < count($itens); $i++) {
                $frete += ($itens[$i]->getTipo() == 2) ? 0 : $this->valorFrete($endereco) + $itens[$i]->getIdCursoItem()->getValor();
            }
        }
        return $tbPreco->getValor() + $frete;
    }

    /**
     * validarContratacao
     * @param $pessoa
     * @param $curso
     * @param $contrato
     * @return array
     */
    public function validarContratacao($pessoa, $curso) {
        $msg = array();
        if (!$pessoa instanceof Pessoa) {
            $msg[] = "Aluno não localizado";
        }
        if (!$curso instanceof Curso) {
            $msg[] = "Curso Não localizado";
        }
        if ($curso->getStatus() == 3) {
            $msg[] = "O Curso não está disponível para contratação";
        }
        return $msg;
    }

}
