<?php

namespace Admin\Utils;

use Admin\AdminBundle\Entity\Pessoa;
use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;
use Admin\AdminBundle\Entity\EmailMarketingTag;
use Admin\AdminBundle\Entity\EmailMarketing as EntityEmailMarketing;

class EmailMarketing {

    protected $mailer;
    protected $templating;
    protected $dominio;
    protected $em;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $em) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->em = $em;
        $this->dominio = "http://teologia24horas.com.br/";
    }

    public function getEmailMarketingByEmail($email) {
        if (empty($email)) {
            throw new \Exception("O E-mail deve ser especificado");
        }

        $emailMarketing = $this->em->getRepository('\Admin\AdminBundle\Entity\EmailMarketing')->findOneBy(array('email' => $email));

        if ($emailMarketing instanceof EmailMarketing) {
            return $emailMarketing;
        }
        return true;
    }

    public function confirmEmail($ide) {
        $emailMarketing = $this->em->getRepository("\Admin\AdminBundle\Entity\EmailMarketing")->findOneBy(array('identificador' => $ide));

        if ($emailMarketing instanceof EntityEmailMarketing) {
            $tag = new Tags($this->em);
            $tag->persistEmailMarketingTag($emailMarketing, 2);

            $email = new Email($this->mailer, $this->templating, $this->em);
            $email->cadastroConfirmado($emailMarketing);

            return true;
        }
        return false;
    }

    public function deleteEmail() {
        $emails = $this->listEmailsToRemove();

        if ($emails == false) {
            return false;
        }

        for ($i = 0; $i < count($emails); $i++) {
            $tags = $this->em->getRepository("\Admin\AdminBundle\Entity\EmailMarketingTag")->findBy(array('idEmailMarketing' => $emails[$i]->getId()));
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $this->em->remove($tag);
                }
            }

            $mensagensTags = $this->em->getRepository("\Admin\AdminBundle\Entity\MensagemEmailMarketing")->findBy(array('idEmailMarketing' => $emails[$i]->getId()));
            if (!empty($mensagensTags)) {
                foreach ($mensagensTags as $mensagemTag) {
                    $this->em->remove($mensagemTag);
                }
            }

            $this->em->remove($emails[$i]);

            $this->em->flush();
        }
    }

    public function listEmailsToRemove() {
        $consult = $this->em->createQueryBuilder();
        $consult->select('email');
        $consult->from('\Admin\AdminBundle\Entity\EmailMarketing', 'email');
        $consult->join(
                '\Admin\AdminBundle\Entity\EmailMarketingTag', 'tag', \Doctrine\ORM\Query\Expr\Join::WITH, 'tag.idEmailMarketing = email.id'
        );
        $consult->where('tag.idTag <> :tag');
        $consult->setParameter('tag', 2);

        $resultSet = $consult->getQuery()->getResult();

        $result = array();

        if (count($resultSet) == 0) {
            return false;
        }

        for ($i = 0; $i < count($resultSet); $i++) {
            $tags = $resultSet[$i]->getTags();

            if (count($tags) == 1)
            {
                $result[] = $resultSet[$i];
            }
        }

        return $result;
    }

    public function changeEmailMarketing($idPessoa, array $dados) {
        $pessoa = $this->em->getRepository('\Admin\AdminBundle\Entity\Pessoa')->find($idPessoa);

        $email = $dados["email"];
        if (empty($email)) {
            return "O campo e-mail é obrigatório";
        }
        
        $check = $this->em->getRepository('\Admin\AdminBundle\Entity\EmailMarketing')->findOneBy(array('email' => $email));
        if ($check instanceof EntityEmailMarketing) {
            return "O e-mail informado já está cadastrado na nossa base de dados";
        }

        $emailMarketing = new EntityEmailMarketing();
        $emailMarketing->setIdentificador("");
        $emailMarketing->setEmail($email);

        $this->em->persist($emailMarketing);

        $tag = $this->em->getRepository("\Admin\AdminBundle\Entity\Tag")->find(1);
        
        $tagEmailMarketing = new EmailMarketingTag();
        $tagEmailMarketing->setIdEmailMarketing($emailMarketing);
        $tagEmailMarketing->setIdTag($tag);
        $tagEmailMarketing->setStatus(1);
        
        $this->em->persist($tagEmailMarketing);
        
        $oldEmailMarketing = $pessoa->getIdEmailMarketing();

        $tags = $this->em->getRepository("\Admin\AdminBundle\Entity\EmailMarketingTag")->findBy(array('idEmailMarketing' => $oldEmailMarketing->getId()));
        if (!empty($tags)) {
            foreach ($tags as $item) {
                $this->em->remove($item);
            }
        }

        $mensagensTags = $this->em->getRepository("\Admin\AdminBundle\Entity\MensagemEmailMarketing")->findBy(array('idEmailMarketing' => $oldEmailMarketing->getId()));
        if (!empty($mensagensTags)) {
            foreach ($mensagensTags as $mensagemTag) {
                $this->em->remove($mensagemTag);
            }
        }

        $this->em->remove($oldEmailMarketing);

        $pessoa->setIdEmailMarketing($emailMarketing);
        $pessoa->setEmail($email);

        $this->em->flush();
        
        $mensagem = new Email($this->mailer, $this->templating, $this->em);
        
        $mensagem->alterarEmail($pessoa);
    }

}
