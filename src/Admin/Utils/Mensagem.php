<?php

/**
 * Created by PhpStorm.
 * User: allan
 * Date: 06/05/15
 * Time: 10:14
 */

namespace Admin\Utils;

use Doctrine\ORM\EntityManager;
use Admin\AdminBundle\Entity\Mensagem as EntityMensagem;
use Admin\AdminBundle\Entity\Tag;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\MensagemTag;
use Admin\AdminBundle\Entity\MensagemPessoa;
use Admin\AdminBundle\Entity\MensagemEmailMarketing;

class Mensagem {

    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function consultEmailMarketing() {
        $consult = $this->em->createQueryBuilder();
        $consult->select('email');
        $consult->from('\Admin\AdminBundle\Entity\EmailMarketing', 'email');
        $consult->join('\Admin\AdminBundle\Entity\EmailMarketingTag', 'tag', \Doctrine\ORM\Query\Expr\Join::WITH, 'tag.idEmailMarketing = email.id');
        $consult->where('tag.idTag = :tag');
        $consult->setParameter('tag', 2);

        return $consult->getQuery()->getResult();
    }

    public function consultSelecionadosByTag(array $form) {
        if (!array_key_exists('aceitar', $form) && empty($form['cidade']) && empty($form['estado']) && empty($form['pais']) && empty($form['nucleo'])) {
            return $this->consultEmailMarketing();
        }
        
        $consult = $this->em->createQueryBuilder();
        $consult->select('pessoa');
        $consult->from('\Admin\AdminBundle\Entity\Pessoa', 'pessoa');
        $consult->join(
                '\Admin\AdminBundle\Entity\PessoaTag', 'tag', \Doctrine\ORM\Query\Expr\Join::WITH, 'tag.idPessoa = pessoa.id'
        );
        $consult->join(
                '\Admin\AdminBundle\Entity\EmailMarketingTag', 'etag', \Doctrine\ORM\Query\Expr\Join::WITH, 'etag.idEmailMarketing = pessoa.idEmailMarketing'
        );
        if (!empty($form['nucleo'])) {
            $consult->join(
                    '\Admin\AdminBundle\Entity\NucleoPessoa', 'nucleo', \Doctrine\ORM\Query\Expr\Join::WITH, 'nucleo.idPessoa = pessoa.id');
        }
        if (!empty($form['pais'])) {
            $consult->join(
                    '\Admin\AdminBundle\Entity\PessoaEndereco', 'endereco', \Doctrine\ORM\Query\Expr\Join::WITH, 'endereco.idPessoa = pessoa.id');
        }
        $consult->where('etag.idTag = :tag');
        if (array_key_exists('aceitar', $form)) {
            for ($i = 0; $i < count($form['aceitar']); $i++) {
                if ($i == 0) {
                    $consult->andWhere('tag.idTag = :tag_' . $i);
                } else {
                    $consult->orWhere('tag.idTag = :tag_' . $i);
                }
            }
        }
        if (!empty($form['cidade'])) {
            $consult->andwhere('endereco.cidade = :cidade');
        }
        if (!empty($form['estado'])) {
            $consult->andwhere('endereco.estado = :estado');
        }
        if (!empty($form['pais'])) {
            $consult->andwhere('endereco.pais = :pais');
        }
        if (!empty($form['nucleo'])) {
            $consult->andWhere('nucleo.idNucleo = :idNucleo');
            $consult->setParameter('idNucleo', $form['nucleo']);
        }
        $consult->setParameter(':tag', 2);
        if (array_key_exists('aceitar', $form)) {
            for ($i = 0; $i < count($form['aceitar']); $i++) {
                $consult->setParameter(':tag_' . $i, $form['aceitar'][$i]);
            }
        }
        if (!empty($form['cidade'])) {
            $consult->setParameter('cidade', $form['cidade']);
        }
        if (!empty($form['estado'])) {
            $consult->setParameter('estado', $form['estado']);
        }
        if (!empty($form['pais'])) {
            $consult->setParameter('pais', $form['pais']);
        }
        return $consult->getQuery()->getResult();
    }

    public function returnByTag(array $form, $selecionado = null) {
        if ($selecionado instanceof Pessoa) {
            $consult = $this->em->createQueryBuilder();
            $consult->select('selecionados');

            $consult->from('\Admin\AdminBundle\Entity\Pessoa', 'selecionados');
            $consult->join('\Admin\AdminBundle\Entity\PessoaTag', 'tag', \Doctrine\ORM\Query\Expr\Join::WITH, 'tag.idPessoa = selecionados.id');
            
            if (count($form['recusar']) > 0) {
                for ($i = 0; $i < count($form['recusar']); $i++) {
                    if ($i == 0) {
                        $consult->where('tag.idTag = :tag_' . $i);
                        $consult->andwhere('selecionados.id = :selecionado');
                    } else {
                        $consult->orWhere('tag.idTag = :tag_' . $i);
                        $consult->andWhere('selecionados.id = :selecionado');
                    }
                    
                }
            }

            $consult->setParameter('selecionado', $selecionado->getId());

            if (count($form['recusar']) > 0) {
                for ($i = 0; $i < count($form['recusar']); $i++) {
                    $consult->setParameter(':tag_' . $i, $form['recusar'][$i]);
                }
            }

            $result = $consult->getQuery()->getResult();

            return count($result) > 0 ? false : true;
        }
        
        return $this->testPessoaByEmailMarketing($selecionado->getId());
    }
    
    public function testPessoaByEmailMarketing($idEmailMarketing) {
        
        $pessoa = $this->em->getRepository("\Admin\AdminBundle\Entity\Pessoa")->findBy(array('idEmailMarketing' => $idEmailMarketing));
        
        return count($pessoa) > 0 ? false : true;
    }

    public function returnSelecionados(array $form) {

        $candidatos = $this->consultSelecionadosByTag($form);

        if (empty($candidatos)) {
            throw new \Exception('Nenhum aluno se qualifica para esse tipo de envio.');
        }
        
        $result = array();

        if (array_key_exists('recusar', $form)) {
            for ($i = 0; $i < count($candidatos); $i++) {
                $check = $this->returnByTag($form, $candidatos[$i]);

                if ($check) {
                    $result[] = $candidatos[$i];
                }
            }
            
            return $result;
        }
        return $candidatos;
    }

    public function salvarMensagemByTag(array $form, EntityMensagem $mensagem) {

        try {
            if (array_key_exists('aceitar', $form)) {
                for ($i = 0; $i < count($form['aceitar']); $i++) {
                    $tag = $this->em->getRepository("\Admin\AdminBundle\Entity\Tag")->find($form['aceitar'][$i]);
                    if ($tag instanceof Tag) {
                        $mensagemTag = new MensagemTag();
                        $mensagemTag->setIdTag($tag);
                        $mensagemTag->setIdMensagem($mensagem);
                        $mensagemTag->setStatus(1);

                        $this->em->persist($mensagemTag);
                        $this->em->flush();
                    }
                }
            }
            if (array_key_exists('recusar', $form)) {
                for ($i = 0; $i < count($form['recusar']); $i++) {
                    $tag = $this->em->getRepository("\Admin\AdminBundle\Entity\Tag")->find($form['recusar'][$i]);
                    if ($tag instanceof Tag) {
                        $mensagemTag = new MensagemTag();
                        $mensagemTag->setIdTag($tag);
                        $mensagemTag->setIdMensagem($mensagem);
                        $mensagemTag->setStatus(2);

                        $this->em->persist($mensagemTag);
                        $this->em->flush();
                    }
                }
            }

            $selecionados = $this->returnSelecionados($form);

            if (count($selecionados) > 0) {
                for ($i = 0; $i < count($selecionados); $i++) {
                    if ($selecionados[$i] instanceof Pessoa) {
                        $mensagemLigacao = new MensagemPessoa();
                        $mensagemLigacao->setIdMensagem($mensagem);
                        $mensagemLigacao->setIdPessoa($selecionados[$i]);
                        $mensagemLigacao->setStatus(1);

                        $this->em->persist($mensagemLigacao);
                    } else {
                        $mensagemLigacao = new MensagemEmailMarketing();
                        $mensagemLigacao->setIdMensagem($mensagem);
                        $mensagemLigacao->setIdEmailMarketing($selecionados[$i]);
                        $mensagemLigacao->setStatus(1);

                        $this->em->persist($mensagemLigacao);
                    }

                    $this->em->flush();
                }
            }
            return $selecionados;
        } catch (\Exception $exc) {
            return $exc->getMessage();
        }
    }

    public function listPessoasByMensagem($idMensagem) {
        $consult = $this->em->createQueryBuilder()
                ->select('pessoa')
                ->from('\Admin\AdminBundle\Entity\Pessoa', 'pessoa')
                ->join('\Admin\AdminBundle\Entity\MensagemPessoa', 'mensagem', \Doctrine\ORM\Query\Expr\Join::WITH, 'mensagem.idPessoa = pessoa.id')
                ->where('mensagem.id = :id')
                ->setParameter('id', $idMensagem);

        return $consult->getQuery()->getResult();
    }

}
