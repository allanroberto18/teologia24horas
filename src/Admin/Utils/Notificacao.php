<?php

namespace Admin\Utils;

use Admin\AdminBundle\Entity\Curso;
use Admin\AdminBundle\Entity\Notificacao as Entity;
use Admin\AdminBundle\Entity\Tag;
use Admin\Utils\Email;
use Admin\Utils\Tags;
use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;

class Notificacao
{
    protected $mailer;
    protected $templating;
    protected $entityManager;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $entityManager)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Curso $curso
     * @param $tema
     * @return Entity
     */
    public function prepareNotificacaoForum(Curso $curso, $tema)
    {
        $entity = new Entity();

        $entity->setIdCurso($curso);
        $entity->setTexto($tema);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->createMessageNotificacao($entity, "Participe do Fórum do Curso: " . $curso->getTitulo());
    }

    /**
     * @param Notificacoa $notificacao
     */
    public function createMessageNotificacao(Entity $notificacao, $titulo)
    {
        $tagEntity = new Tags($this->entityManager);

        $tag = $tagEntity->checkTagByTitulo($titulo);
        if (!$tag instanceof Tag)
        {
            return false;
        }
        $selecionados = $tagEntity->returnSelecionadosByTag($tag->getId());
        if (empty($selecionados))
        {
            return false;
        }
        $i = 0;
        $count = count($selecionados);
        while ($count < $i)
        {
            $messageService = new Email($this->mailer, $this->templating, $this->entityManager);
            $messageService->send($titulo, $notificacao->getTexto(), $selecionados[$i]->getEmail());

            $selecionados[$i];
            $i++;
        }

    }


}