<?php

namespace Admin\Utils;

use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\lib\CursoConsult;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;

class Login {

    private $em;
    private $email, $senha;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    function getEmail() {
        return $this->email;
    }

    function getSenha() {
        return $this->senha;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setSenha($senha) {
        $this->senha = !empty($senha) ? md5($senha) : "";
    }

    public function setParams($email, $senha) {
        $this->setEmail($email);
        $this->setSenha($senha);
    }

    public function validarUsuario() {
        $msg = "";
        if (empty($this->email)) {
            $msg .= "<span class='fa fa-remove'></span> Informe o e-mail do usuário<br />";
        }
        if (empty($this->senha)) {
            $msg .= "<span class='fa fa-remove'></span> Informe a senha do usuário<br />";
        }

        return $msg;
    }

    public function efetuarLogin() {
        $session = new Session();

        $msg = $this->validarUsuario();
        if (!empty($msg)) {

            $session->getFlashBag()->set('error', $msg);
            return false;
        }

        $pessoa = $this->getPessoa();

        if (!$pessoa instanceof Pessoa) {
            $session->getFlashBag()->set('error', "<span class='fa fa-remove'></span> Usuário ou senha inválida<br />");
            return false;
        }

        $check = $this->checkUsuarioValido($pessoa->getId());
        if(!empty($check)) {
            $session->getFlashBag()->set('error', $check);
            return false;
        }

        $this->mountSessao($pessoa);

        $carrinho = $session->get('carrinho');

        if (!empty($carrinho)) {
            header("location:/AVA/Aluno/curso/selecionar/{$carrinho['curso']}/{$carrinho['slug']}");
            exit();
        }

        header("location:/AVA/Usuario/Home/");
        exit();
    }

    public function checkUsuarioValido($id) {
        $result = $this->em->createQueryBuilder()
                ->select('pessoa')
                ->from('\Admin\AdminBundle\Entity\Pessoa', 'pessoa')
                ->join('\Admin\AdminBundle\Entity\EmailMarketing', 'email', \Doctrine\ORM\Query\Expr\Join::WITH, 'email.id = pessoa.idEmailMarketing')
                ->join('\Admin\AdminBundle\Entity\EmailMarketingTag', 'tag', \Doctrine\ORM\Query\Expr\Join::WITH, 'email.id = tag.idEmailMarketing')
                ->where('pessoa.id = :id')
                ->andWhere('tag.idTag = :tag')
                ->setParameter('id', $id)
                ->setParameter('tag', 2)
                ->getQuery()
                ->getResult();

        return count($result) > 0 ? null : "<span class='fa fa-remove'></span> O usuário não possui um e-mail válido, faça sua inscrição novamente e confirme nossa mensagem na sua caixa de entrada.";
    }

    public function getPessoa() {
         $pessoa = $this->em->getRepository('\Admin\AdminBundle\Entity\Pessoa')->findOneBy(array('email' => $this->email, 'senha' => $this->senha));
         
         return $pessoa instanceof Pessoa > 0  ? $pessoa : "<span class='fa fa-remove'></span> Usuário ou senha inválida<br />";
    }

    public function mountSessao(Pessoa $pessoa) {
        $value['id'] = $pessoa->getId();
        $value['nome'] = $pessoa->getPfNome();
        $value['email'] = $pessoa->getEmail();
        if (empty($pessoa->getFoto())) {
            $value['foto'] = ($pessoa->getPfSexo() == 1) ? "user.png" : "user_woman.png";
        } else {
            $value['foto'] = $pessoa->getFoto();
        }
        $value['check'] = $this->checkContratosForAmbiente($pessoa);

        $session = new Session();
        
        $session->set('ava', $value);

        // Alterar
        $curso = new CursoConsult();
        $cursos = $curso->getCursosBySlider($this->em);

        $session->set('cursos', $cursos);

        return $value;
    }

    public function checkContratosForAmbiente(Pessoa $pessoa) {
        $contratos = $pessoa->getContrato();

        $check = 0;
        if (count($contratos) > 0) {
            for ($i = 0; $i < count($contratos); $i++) {
                if ($contratos[$i] instanceof Contrato) {
                    if ($contratos[$i]->getIdCurso()->getIdTabelaPreco()->getValor() == 0) {
                        continue;
                    } else {
                        $check = 1;
                    }
                }
            }
        }
        return $check;
    }
    
    public function generateSessionByEmailMarketing(EmailMarketing $emailMarketing) {
        $value = array();
        $value['id'] = null;
        $value['id_email_marketing'] = $emailMarketing->getId();
        $value['nome'] = "";
        $value['email'] = $emailMarketing->getEmail();
        $value['foto'] = 'user.png';
        $value['check'] = '0';

        $session = new Session();
        $session->set('ava', $value);

        header("location:/AVA/Usuario/Perfil/usuario/novo");
        exit();
    }

}
