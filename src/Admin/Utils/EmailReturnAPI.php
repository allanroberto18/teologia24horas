<?php
/**
 * Created by PhpStorm.
 * User: allan
 * Date: 19/08/2015
 * Time: 20:41
 */

namespace Admin\Utils;

use Admin\AdminBundle\Entity\Tag;
use Doctrine\ORM\EntityManager;
use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;

class EmailReturnAPI
{
    private $user, $apiKey, $date, $link, $entityManager, $mailer, $templating;

    /**
     * LocawebAPI constructor.
     * @param $entityManager
     */
    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $entityManager)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->user = 'azure_57c39f1459a4ec5967fbd3c783736c64@azure.com';
        $this->apiKey = 'pLc6c0qcrl138cp';
        $this->date = date("Y-m-d");
        $this->link = "https://api.sendgrid.com/api/bounces.get.json?api_user=" . $this->user . "&api_key=" . $this->apiKey . "&date=" . $this->date;
    }

    public function getMessages()
    {
        $request = $this->link;
        $params = array();

        $session = curl_init($this->link);

        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $this->apiKey));
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($session);

        curl_close($session);

        return json_decode($response, true);
    }

    public function getData()
    {
        $message = $this->getMessages();
        if (empty($message))
        {
            return false;
        }
        //$this->link = $message['links']['next'];

        //$data = $message['data']['messages'];
        $i = 0;

        $emailObj = new Email($this->mailer, $this->templating, $this->entityManager);
        $tagObj = new Tags($this->entityManager);

        while ($i < count($message))
        {
            $email = $message[$i]["email"];
            $titulo = "E-mail cancelado: " . $message[$i]["email"];
            $body = "<strong>Mensagem enviada: </strong>" . $message[$i]["reason"] . "<br />";
            $body .= "<strong>Retorno da Mensagem: </strong>" . $message[$i]["reason"] . "<br />";
            $body .= "<strong>Data de Envio: </strong>" . $message[$i]["created"];

            $emailMarketing = $this->entityManager->getRepository("AdminBundle:EmailMarketing")->findBy(array('email' => $email));
            if (!empty($emailMarketing))
            {
                $tagObj->removeEmailMarketingTag($emailMarketing[0], 2);
                $emailObj->send($titulo, $body, "franciscomiranda@teologia24horas.com.br");
            }
            $i++;
        }
    }

    public function removeEmails()
    {
        while (!empty($this->link))
        {
            $this->getData();
        }
    }



}