<?php

namespace Admin\Utils;

use Swift_Message;
use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Admin\AdminBundle\Entity\Contrato;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\EmailMarketingTag;
use Admin\AdminBundle\Entity\Fatura;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\Mensagem as ObjMensagem;
use Admin\AdminBundle\Entity\MensagemPessoa;
use Admin\AdminBundle\Entity\MensagemEmailMarketing;
use Admin\AdminBundle\Entity\Curso;
use Doctrine\ORM\EntityManager;

class Email
{

    protected $mailer;
    protected $templating;
    protected $dominio;
    protected $entityManager;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $entityManager)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->entityManager = $entityManager;
        $this->dominio = "http://teologia24horas.com.br/";
    }

    public function saveMensagem($titulo, $conteudo)
    {
        $mensagem = new ObjMensagem();
        $mensagem->setTitulo($titulo);
        $mensagem->setConteudo($conteudo);

        $this->entityManager->persist($mensagem);
        $this->entityManager->flush();

        return $mensagem;
    }

    public function saveMensagemPessoaLigacao(ObjMensagem $mensagem, Pessoa $pessoa)
    {

        $ligacao = new MensagemPessoa();
        $ligacao->setIdMensagem($mensagem);
        $ligacao->setIdPessoa($pessoa);

        $this->entityManager->persist($ligacao);
        $this->entityManager->flush();
    }

    public function saveMensagemEmailMarketingLigacao(ObjMensagem $mensagem, EmailMarketing $emailMarketing)
    {

        $ligacao = new MensagemEmailMarketing();
        $ligacao->setIdMensagem($mensagem);
        $ligacao->setIdEmailMarketing($emailMarketing);

        $this->entityManager->persist($ligacao);
        $this->entityManager->flush();
    }

    /**
     * send
     * @param type $titulo
     * @param type $mensagem
     * @param type $destino
     * @param type $origem
     * @param type $tipo
     */
    public function send($titulo, $mensagem, $destino, $origem = 'sac@teologia24horas.com.br')
    {
        $swiftMailer = Swift_Message::newInstance()
            ->setSubject($titulo)
            ->setFrom(array($origem => 'IBI/Teologia24Horas'))
            ->setTo($destino)
            ->setBody($mensagem, 'text/html');

        $email = $this->mailer->send($swiftMailer);

        $this->mailer->getTransport()->stop();

        return $email;
    }

    public function sendEmailPessoa($titulo, $conteudo, $destino)
    {
        $tag = new Tags($this->entityManager);
        $check = $tag->checkEmailAutorizado($destino);

        if ($check instanceof EmailMarketingTag) {

            $pessoa = $this->entityManager->getRepository('\Admin\AdminBundle\Entity\Pessoa')->findOneBy(array('idEmailMarketing' => $check->getIdEmailMarketing()->getId()));

            if ($pessoa instanceof Pessoa) {
                $body = $this->templating->render("Emails/message.html.twig", array(
                    'titulo' => $titulo,
                    'entity' => $pessoa,
                    'conteudo' => $conteudo,
                    'dominio' => $this->dominio,
                ));

                $this->send($titulo, $body, $destino);
            }
        }
    }

    public function sendEmailMarketing($titulo, $conteudo, EmailMarketing $emailMarketing)
    {
        $tag = new Tags($this->entityManager);
        $check = $tag->checkEmailMarketingTag($emailMarketing->getId());

        if ($check instanceof EmailMarketingTag) {
            $body = $this->templating->render("Emails/message_email_marketing.html.twig", array(
                'titulo' => $titulo,
                'conteudo' => $conteudo,
                'dominio' => $this->dominio,
            ));

            $this->send($titulo, $body, $emailMarketing->getEmail());
        }
    }

    public function dicaSenha(Pessoa $pessoa)
    {
        $titulo = "Dados para acessar a Área Exclusiva";

        $conteudo = $this->templating->render("Emails/dica_senha.html.twig", array(
            'titulo' => $titulo,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);

        $this->sendEmailPessoa($titulo, $conteudo, $pessoa->getEmail());
    }

    public function alterarEmail(Pessoa $pessoa)
    {
        $titulo = "Confirmação de Alteração de E-mail";

        $conteudo = $this->templating->render("Emails/alteracao_email.html.twig", array(
            'titulo' => $titulo,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);

        $this->send($titulo, $conteudo, $pessoa->getEmail());
    }

    public function mensagemByPessoa(Pessoa $pessoa, ObjMensagem $mensagem)
    {

        $conteudo = $this->templating->render("Emails/pessoa.html.twig", array(
            'titulo' => $mensagem->getTitulo(),
            'conteudo' => $mensagem->getConteudo(),
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);

        $this->sendEmailPessoa($mensagem->getTitulo(), $conteudo, $pessoa->getEmail());
    }

    public function mensagemByEmailMarketing(EmailMarketing $emailMarketing, ObjMensagem $mensagem)
    {
        $conteudo = $this->templating->render("Emails/email_marketing.html.twig", array(
            'titulo' => $mensagem->getTitulo(),
            'conteudo' => $mensagem->getConteudo(),
            'dominio' => $this->dominio,
        ));

        $this->saveMensagemEmailMarketingLigacao($mensagem, $emailMarketing);

        $this->sendEmailMarketing($mensagem->getTitulo(), $conteudo, $emailMarketing);
    }

    public function confirmarCadastro(EmailMarketing $emailMarketing)
    {
        $titulo = 'Falta só um clique...';

        $conteudo = $this->templating->render("Emails/confirmar_cadastro.html.twig", array(
            'titulo' => $titulo,
            'entity' => $emailMarketing,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemEmailMarketingLigacao($mensagem, $emailMarketing);

        $this->send($titulo, $conteudo, $emailMarketing->getEmail());
    }

    public function alteracaoEmail(Pessoa $pessoa)
    {
        $titulo = 'Falta só um clique...';

        $conteudo = $this->templating->render("Emails/alteracao_email.html.twig", array(
            'titulo' => $titulo,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);

        $this->send($titulo, $conteudo, $pessoa->getEmail());
    }

    public function cadastroConfirmado(EmailMarketing $emailMarketing)
    {
        $titulo = 'Obrigado pela sua inscrição...';

        $conteudo = $this->templating->render("Emails/cadastro_confirmado.html.twig", array(
            'titulo' => $titulo,
            'entity' => $emailMarketing,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemEmailMarketingLigacao($mensagem, $emailMarketing);

        $this->send($titulo, $conteudo, $emailMarketing->getEmail());
    }

    public function perfilCadastrado(Pessoa $pessoa)
    {
        $titulo = 'O seu curso de INTRODUÇÃO à Teologia Sistemática, já está liberado...';

        $conteudo = $this->templating->render("Emails/perfil_confirmado.html.twig", array(
            'titulo' => $titulo,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);
        $this->sendEmailPessoa($titulo, $conteudo, $pessoa->getEmail());
    }

    public function confirmacaoPagamento(Contrato $contrato, Fatura $fatura)
    {
        $titulo = "Pagamento Confirmado";

        $pessoa = $contrato->getIdPessoa();
        $curso = $contrato->getIdCurso();

        $conteudo = $this->templating->render("Emails/confirmacao_pagamento.html.twig", array(
            'titulo' => $titulo,
            'curso' => $curso,
            'contrato' => $contrato,
            'fatura' => $fatura,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);
        $this->sendEmailPessoa($titulo, $conteudo, $pessoa->getEmail());

        $tag = new Tags($this->entityManager);
        $tag->persistPessoaTag($pessoa->getId(), "", $curso->getTitulo());
    }

    public function corfirmacaoContrato(Pessoa $pessoa, Curso $curso)
    {
        $titulo = "Compra efetuada com sucesso";

        $conteudo = $this->templating->render("Emails/confirmacao_compra.html.twig", array(
            'titulo' => $titulo,
            'curso' => $curso,
            'entity' => $pessoa,
            'dominio' => $this->dominio,
        ));

        $mensagem = $this->saveMensagem($titulo, $conteudo);

        $this->saveMensagemPessoaLigacao($mensagem, $pessoa);
        $this->sendEmailPessoa($titulo, $conteudo, $pessoa->getEmail());
    }
}
