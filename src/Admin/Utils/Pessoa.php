<?php

namespace Admin\Utils;

use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Doctrine\ORM\EntityManager;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\Tag;

class Pessoa {

    protected $mailer;
    protected $templating;
    protected $dominio;
    protected $em;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, EntityManager $em) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->em = $em;
        $this->dominio = "http://teologia24horas.com.br/";
    }

    public function dicaSenha($email) {
        if (empty($email)) {
            throw new \Exception("O E-mail deve ser especificado");
        }

        $pessoa = $this->em->getRepository('\Admin\AdminBundle\Entity\Pessoa')->findOneBy(array('email' => $email));

        if ($pessoa instanceof Pessoa) {
            return json_decode(array('status' => $pessoa->getEmail()));
        }

        return json_decode(array('status' => 2));
    }

    public function getPessoaByCPF($cpf) {
        if (empty($cpf)) {
            throw new \Exception("O CPF deve ser especificado");
        }

        $pessoa = $this->em->getRepository('\Admin\AdminBundle\Entity\Pessoa')->findOneBy(array('pfCpf' => $cpf));

        if ($pessoa instanceof Pessoa) {
            return $pessoa;
        }
        return true;
    }

    public function updateEmail($id, $email) {
        $pessoa = $this->em->getRepository("\Admin\AdminBundle\Entity\Pessoa")->find($id);

        $tag = $this->em->getRepository("\Admin\AdminBundle\Entity\Tag")->find(1);

        if (!$pessoa instanceof Pessoa) {
            throw new Exception("Não foi possível localizar o aluno");
        }

        $lastEmail = $pessoa->getIdEmailMarketing();

        if (!$tag instanceof Tag) {
            throw new Exception("Não foi possível localizar a tag para alteração do e-mail");
        }

        $emailMarketing = new EmailMarketing();
        $emailMarketing->setIdentificador(null);
        $emailMarketing->setEmail($email);
        $emailMarketing->setStatus(1);

        $this->em->persist($emailMarketing);
        
        $serviceTag = new Tags($this->em);
        $serviceTag->persistEmailMarketingTag($emailMarketing, 1);

        $pessoa->setIdEmailMarketing($emailMarketing);
        $pessoa->setEmail($emailMarketing->getEmail());

        $this->em->flush();

        $this->removeEmailMarketing($lastEmail);
        
        $service = new Email($this->mailer, $this->templating, $this->em);
        $service->alteracaoEmail($pessoa);
        
    }

    public function removeEmailMarketing(EmailMarketing $emailMarketing) 
    {
        $emailTags = $this->em->getRepository("\Admin\AdminBundle\Entity\EmailMarketingTag")->findBy(array('idEmailMarketing' => $emailMarketing->getId()));
        
        if (count($emailTags) > 0) {
            foreach ($emailTags as $tag) {
                $this->em->remove($tag);
            }
        }
        
        $this->em->remove($emailMarketing);
        
        $this->em->flush();
    }
    
    
    
}
