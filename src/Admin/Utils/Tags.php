<?php

namespace Admin\Utils;

use Admin\AdminBundle\Entity\EmailMarketing;
use Admin\AdminBundle\Entity\EmailMarketingTag;
use Admin\AdminBundle\Entity\Pessoa;
use Admin\AdminBundle\Entity\PessoaTag;
use Admin\AdminBundle\Entity\Tag;
use Doctrine\ORM\EntityManager;

class Tags {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Localiza Tag
     * @param $idTag
     * @return Tag $tag
     */
    public function getTag($idTag) {
        return $this->em->getRepository('\Admin\AdminBundle\Entity\Tag')->find($idTag);
    }

    /**
     * Verifica se existe uma Tag
     * @param $titulo
     * @return boolean
     */
    public function checkTagByTitulo($titulo) {
        $tag = $this->em->getRepository('\Admin\AdminBundle\Entity\Tag')->findOneBy(array('titulo' => $titulo));
        if (count($tag) > 0) {
            return $tag;
        }
        return $this->persistTag($titulo);
    }

    /**
     * Verifica Relacionamento entre Pessoa e Tag
     * @param $idPessoa
     * @param $idTag
     * @return boolean
     */
    public function checkPessoaTag($idPessoa, $idTag) {
        $pessoaTag = $this->em->getRepository('\Admin\AdminBundle\Entity\PessoaTag')->findOneBy(array('idPessoa' => $idPessoa, 'idTag' => $idTag));
        return count($pessoaTag) > 0 ? $pessoaTag : false;
    }

    /**
     * Verifica Relacionamento entre Pessoa e TaginnerJoin
     * @param $email
     * @param $idTag
     * @return boolean
     */
    public function checkEmailAutorizado($email, $idTag = 2) {
        $pessoa = $this->em->getRepository("\Admin\AdminBundle\Entity\Pessoa")->findOneBy(array('email' => $email));

        if ($pessoa instanceof Pessoa) {
            return $this->checkEmailMarketingTag($pessoa->getIdEmailMarketing()->getId(), $idTag);
        }
        return false;
    }

    /**
     * Verifica Relacionamento entre EmailMarketing e Tag
     * @param $idEmailMarketing
     * @param $idTag
     * @return boolean
     */
    public function checkEmailMarketingTag($idEmailMarketing, $idTag = 2) {
        $emailMarketingTag = $this->em->getRepository('\Admin\AdminBundle\Entity\EmailMarketingTag')->findOneBy(array('idEmailMarketing' => $idEmailMarketing, 'idTag' => $idTag));
        return count($emailMarketingTag) > 0 ? $emailMarketingTag : false;
    }

    /**
     * Gravar relacionamento EmailMarketingTag
     * @param EmailMarketing $emailMarketing
     * @param $idTag
     * @return boolean
     */
    public function persistEmailMarketingTag(EmailMarketing $emailMarketing, $idTag) {
        $check = $this->checkEmailMarketingTag($emailMarketing->getId(), $idTag);
        if ($check instanceof EmailMarketingTag)
            return false;

        $tag = $this->getTag($idTag);

        $emailMarketingTag = new EmailMarketingTag();
        $emailMarketingTag->setIdEmailMarketing($emailMarketing);
        $emailMarketingTag->setIdTag($tag);

        $this->em->persist($emailMarketingTag);
        $this->em->flush();

        return true;
    }

    /**
     * Gravar relacionamento PessoaTag
     * @param Pessoa $pessoa
     * @param $idTag
     * @return boolean
     */
    public function persistPessoaTag($idPessoa, $idTag = null, $titulo = null) {

        $pessoa = $this->em->getRepository("\Admin\AdminBundle\Entity\Pessoa")->find($idPessoa);

        if (!empty($titulo) && empty($idTag)) {
            $tag = $this->checkTagByTitulo($titulo);
            $idTag = $tag->getId();
        }

        $tag = $this->getTag($idTag);

        $check = $this->checkPessoaTag($idPessoa, $tag);
        if ($check instanceof PessoaTag)
            return false;

        $pessoaTag = new PessoaTag();
        $pessoaTag->setIdPessoa($pessoa);
        $pessoaTag->setIdTag($tag);

        $this->em->persist($pessoaTag);
        $this->em->flush();

        return true;
    }

    /**
     * Gravar relacionamento PessoaTag
     * @param Pessoa $pessoa
     * @param $idTag
     * @return boolean
     */
    public function removePessoaTag(Pessoa $pessoa, $titulo) {

        $idTag = $this->checkTagByTitulo($titulo);

        $pessoaTag = $this->checkPessoaTag($pessoa->getId(), $idTag->getId());
        if (!$pessoaTag)
            return false;

        $this->em->remove($pessoaTag);
        $this->em->flush();

        return true;
    }

    /**
     * Gravar relacionamento PessoaTag
     * @param Pessoa $pessoa
     * @param $idTag
     * @return boolean
     */
    public function removeEmailMarketingTag(EmailMarketing $emailMarketing, $idTag) {
        $emailMarketingTag = $this->checkEmailMarketingTag($emailMarketing->getId(), $idTag);
        if (is_bool($emailMarketingTag)) {
            return false;
        }
        $this->em->remove($emailMarketingTag);
        $this->em->flush();

        return true;
    }

    /**
     * Criar nova tag
     * @param $titulo
     * return Tag $tag
     */
    public function persistTag($titulo) {
        $tag = new Tag();
        $tag->setTitulo($titulo);
        $this->em->persist($tag);
        $this->em->flush();
        return $tag;
    }

    public function __destruct() {
        $this->em->clear();
    }

    /**
     * @param $tag
     * @return array
     */
    public function returnSelecionadosByTag($tag) {

        return $this->em->createQueryBuilder()
            ->select('pessoas')
            ->from('\Admin\AdminBundle\Entity\Pessoa', 'pessoas')
            ->join('\Admin\AdminBundle\Entity\EmailMarketingTag', 'etag', \Doctrine\ORM\Query\Expr\Join::WITH, 'etag.idEmailMarketing = pessoas.idEmailMarketing')
            ->join('\Admin\AdminBundle\Entity\PessoaTag', 'ptag', \Doctrine\ORM\Query\Expr\Join::WITH, 'ptag.idPessoa = pessoas.id')
            ->where('etag.idTag = :tag1')
            ->andWhere('ptag.idTag = :tag2')
            ->setParameters(['tag1' => 2, 'tag2' => $tag])
            ->getQuery()
            ->getResult()
        ;
    }

}
